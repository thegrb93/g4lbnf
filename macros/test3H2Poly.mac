#-----------------------------------------------------------------------------------
#
# An example script for running the Standard Neutrino Beam simulation in G4LBNE
# using a Geant4 generated proton beam. P. Lebrun
#
# Place here the commands that modify the default geometry. 
# We are at Geant4 stage call "PreInit" 
#
# Place this one always before the other geometrical option. 
#
#/LBNE/det/Use1p2MW True No longer needed.. 
/LBNE/det/UseMultiSphereTarget True
#
# Plug In Horn1 
#
#/LBNE/det/ConstructPlugInHorn1 True
#/LBNE/det/PlugLength 125 cm
#/LBNE/det/PlugInnerRadius 0.000001 cm
#/LBNE/det/PlugOuterRadius 1.25 cm
#/LBNE/det/PlugZPosition 2.0 m
#
# Custom Horn1 
#
/LBNE/det/UseHorn1Polycone True
/LBNE/det/RemoveDecayPipeSnout True
/LBNE/det/NumInnerPtsHorn1Polycone 15
/LBNE/det/Horn1PolyOuterRadius 360.0 mm
/LBNE/det/Horn1PolyPt0RinThickZ   40.000   2.500       0.00 mm
/LBNE/det/Horn1PolyPt1RinThickZ   19.000   2.500      12.50 mm
/LBNE/det/Horn1PolyPt2RinThickZ   19.000   2.500     850.00 mm
/LBNE/det/Horn1PolyPt3RinThickZ   98.167   2.495    1556.67 mm
/LBNE/det/Horn1PolyPt4RinThickZ  162.292   2.480    1733.33 mm
/LBNE/det/Horn1PolyPt5RinThickZ  212.958   2.455    1910.00 mm
/LBNE/det/Horn1PolyPt6RinThickZ  251.750   2.420    2086.67 mm
/LBNE/det/Horn1PolyPt7RinThickZ  280.250   2.375    2263.33 mm
/LBNE/det/Horn1PolyPt8RinThickZ  300.042   2.320    2440.00 mm
/LBNE/det/Horn1PolyPt9RinThickZ  312.708   2.255    2616.67 mm
/LBNE/det/Horn1PolyPt10RinThickZ  319.833   2.180    2793.33 mm
/LBNE/det/Horn1PolyPt11RinThickZ  323.000   2.095    2970.00 mm
/LBNE/det/Horn1PolyPt12RinThickZ  323.792   2.000    4146.67 mm
/LBNE/det/Horn1PolyPt13RinThickZ  323.792   1.895    5750.33 mm
/LBNE/det/Horn1PolyPt14RinThickZ  324.583   1.780    6000.00 mm
#
#/LBNE/det/waterThickInHorn 2 mm
/LBNE/det/TargetLengthOutsideHorn 30.0 cm
#/LBNE/det/TargetAndHorn1RadialSafety 0.1 mm
#/LBNE/det/TargetAndHorn1RadiusBigEnough True
#/LBNE/det/GraphiteTargetFinWidth 7.4 mm
#/LBNE/det/TargetAndHorn1RadialSafety 0.1 mm
#/LBNE/det/TargetAndHorn1RadiusBigEnough True
#/LBNE/det/GraphiteTargetFinWidth 13.5 mm
#/LBNE/det/decayPipeLength 204.55 m
/LBNE/det/seHornCurrent 200 kA
#
# 1p2 MW studies 
#
#/LBNE/det/UseSimpleCylindricalTarget True
#/LBNE/det/SimpleTargetRadius 12.5 mm
#
# 1p2 MW studies 
#
#/LBNE/det/UseSimpleCylindricalTarget True
#/LBNE/det/SimpleTargetRadius 12.5 mm
#
# Rescale Horn2 
#
/LBNE/det/Horn2LongRescale 1.5
/LBNE/det/Horn2RadialRescale 1.5
/LBNE/det/Horn2LongPosition 8500. mm

#
# Testing misalingments 
#
#/LBNE/Surveyor/PosUpstreamLeftPinTargetHeTube 3.1 0.0 0.
#/LBNE/Surveyor/PosUpstreamRightPinTargetHeTube 3.1 0.0 0.
#/LBNE/Surveyor/PosDownstreamLeftPinTargetHeTube 3.1 0.0 0.
#/LBNE/Surveyor/PosDownstreamRightPinTargetHeTube 3.1 0.0 0.
#/LBNE/Surveyor/PosUpstreamLeftBallHorn1 2.0 0. 0.
#/LBNE/Surveyor/PosUpstreamRightBallHorn1 2.0 0. 0.
#/LBNE/Surveyor/PosUpstreamLeftBallHorn1 2.0 0.0 0.
#/LBNE/Surveyor/PosUpstreamRightBallHorn1 2.0 0.0 0.
#
/LBNE/det/construct
#
#exit
/run/initialize
#
# We are at Geant4 stage "idle" 
#
/event/verbose 0
/tracking/verbose 0
#
#
# Primary vertex setting 
#
/LBNE/primary/useGeantino
#/LBNE/primary/useMuonGeantino
#/LBNE/primary/geantinoOpeningAngle 0.0185 radian
#/LBNE/primary/geantinoOpeningAngleMin 0.018 radian
/LBNE/primary/geantinoOpeningAngle 0.050 radian
/LBNE/primary/geantinoOpeningAngleMin 0.0002 radian
/LBNE/primary/geantinoZOrigin -5000. mm 
/LBNE/primary/geantinoSigmaZOrigin 0.0015 mm 
#/LBNE/primary/protonMomentum 5.0 GeV
#
#/LBNE/stepping/filename ./example/steppingPropa101.txt
# The order of these 2 cards matters 
/LBNE/stepping/name PropCOH1Poly
# on lbnegpvm02
/LBNE/stepping/filename  /scratch/lbne/lebrun/steppingPropCO103_H2PolyBig2.txt
#/LBNE/stepping/filename  /home/lebrun/G4LBNE/g4lbne_work/work2/g4lbne/Res1/steppingProptestH1PolyA1.txt
# laptop, home 
#/LBNE/stepping/filename  /home/lebrun/G4LBNE/g4lbne_work/lbne-beamSimB/lbne-beamsim/g4lbne/geantinoResults/steppingPropa107_a.txt
#/LBNE/stepping/keyVolumeForOutputTo Helium 
#/LBNE/stepping/keyVolumeForOutput Horn1TargetSimpleCylinder
#/LBNE/stepping/keyVolumeForOutput TargetUsptreamSimpleCylinder
#/LBNE/stepping/keyVolumeForOutput TargetUpstrDownstrSegmentRight
#/LBNE/stepping/keyVolumeForOutput TargetUpstrDownstrCoolingTubeWater
#/LBNE/stepping/keyVolumeForOutputTo Horn1TopLevelDownstr
#/LBNE/stepping/keyVolumeForOutputTo Horn1DownstrPart1Weld5
#/LBNE/stepping/keyVolumeForOutput  Horn1TopLevelDownstr
/LBNE/stepping/keyVolumeForOutputTo Horn2
/LBNE/stepping/keyVolumeForOutput  Horn2TopLevel
#/LBNE/stepping/keyVolumeForOutputTo TargetFinVertHeliumRoundedUpstrLeft
#/LBNE/stepping/keyVolumeForOutput TargetUpstrDownstrSegmentLeft
#/LBNE/stepping/keyVolumeForOutputTo Horn1
#/LBNE/stepping/keyVolumeForOutput Horn1DownstrPart1SubSect0 
#/LBNE/stepping/keyVolumeForOutputTo Helium 
#/LBNE/stepping/keyVolumeForOutput TargetFinVertExtra
#
# Allows the RunID, which is included in the
# output file name(s), to be set at runtime.
#
/LBNE/output/CreateOutput false
/LBNE/run/setRunID 11
#
# If NOT using an external hadron file then this establishes how 
# many particles(protons) to use during the simulation. To be consistent 
# with past neutrino ntuples each run uses 500000 or 100000 protons
#
/LBNE/run/NEvents 15000
# This will place Geant4 in the stage "EventProc", i.e. ,simulating events. 
#
/run/beamOn 
#
exit
#
#NOTE: To debug problems grep for "PROBLEM" in the output of the simulation
# "PROBLEM" statements are printed when there is a problem.
#



#Amit Bashyal
January 13, 2015

This is a small note for anybody who wants to look at the pion or any particle
flux distribution using the tracking planes.

In the ProductionScripts directory, there are two files, eventRates_H1 and
eventRates_H2 which are written for the tracking planes at the end of Horn1 and
Horn2 respectively.

The procedure to generate the histogram is same as the procedure to generate the
neutrino histograms using the eventRates script.

An example macro to generate pion flux at the end of Horn 2 is in example
directory (pidata_H2.C).

Simple run the "example_macro_010.mac" and "example_macro_011.mac" in the
directory. 

In the working directory Do:
$source setups/setup_g4lbne_fnal.sh
$./g4lbne example/example_macro_010.mac
&./g4lbne example/example_macro_011.mac

Then in the example directory:
Go to root (simple run root -l).
$ .L pidata_H2.C++
$ pidata_H2 t
$ t.Loop()

This will create a filename called g4lbne_fluxexample_pion.root.
You can open the root file and look at the histograms.


****************************************************************
FOR LARGER STATISTICS 
****************************************************************
For large statistics, there are files eventRates_H1 and eventRates_H2 in the
ProductionScripts directory. Use/Modify as per the need to generate the
histograms.

Use MakeLotsofFluxHistograms.sh file and makeFluxHistograms.C file of ProductionScripts Directory to run the eventRates scripts. The resulting root files are
generated in the ProductionScripts Directory.

A small change is required in the makeFluxHistograms.C file in the ProductionScripts Directory.

change the user from "beam" to your username.





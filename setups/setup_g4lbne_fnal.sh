
source /grid/fermiapp/products/common/etc/setups.sh
setup ifdhc


if [ -z "${G4LBNE_IS_SETUP}" ]; then
    
    case ${HOSTNAME} in
        argoneut*)
            export GROUP=argoneut
            export EXPERIMENT=argoneut
            ;;
        uboone*)
            export GROUP=microboone
            export EXPERIMENT=uboone
            ;;
        lbne*)
            export GROUP=dune
            export EXPERIMENT=dune
            ;;
        dune*)
            export GROUP=dune
            export EXPERIMENT=dune
            ;;
    esac
    echo experiment is $GROUP

     
    export LBNE_PRODUCT_DIR=/grid/fermiapp/products/larsoft/
    source /grid/fermiapp/products/larsoft/setup
    setup cmake v3_2_1
    setup geant4 v4_10_1_p02 -q e7:prof
    setup fftw v3_3_4 -q prof
    setup python v2_7_6
#    setup libxml2 v2_9_1 -q prof
#    export G4INCLUDE=${G4INCLUDE}/Geant4
#    export G4SYSTEM=Linux-g++
#    export G4INSTALL=${LBNE_PRODUCT_DIR}/geant4/v4_9_6_p02/Linux64bit+2.6-2.12-e4-prof/share/Geant4-9.6.2/geant4make
    setup dk2nu v01_01_03c -q e7:prof
    unset G4BIN

##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##
## Set G4WORKDIR to g4lbne directory, if not already set
##
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#    SETUPDIR=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)/`basename "${BASH_SOURCE[0]}"`
    SETUPDIR=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)
    echo "SETUPDIR is $SETUPDIR"
    #if [ -z "$G4WORKDIR" ]; then
#	export G4WORKDIR=`dirname $SETUPDIR`
#	echo "G4WORKDIR is not set... Setting it to "$G4WORKDIR 
#    fi
#
    export G4WORKDIR=`dirname ${SETUPDIR}`
    export G4LBNEWORKDIR=`dirname ${SETUPDIR}`
    export G4LBNE_DIR=`dirname $SETUPDIR`

    echo "G4WORKDIR: $G4WORKDIR"
    echo "G4LBNEWORKDIR: $G4LBNEWORKDIR"
    echo "G4LBNE_DIR: $G4LBNE_DIR"

    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$G4LIB/Linux-g++"

#    echo "*********************"
#    echo Locating ROOT...
#    env | grep root
#    echo "*********************"
#    echo Locating Geant4...
#    env | grep geant4
#    echo "*********************"
#    echo Locating CLHEP...
#    env | grep clhep
#    echo "*********************"
    
    export G4WORKDIR=`pwd`
    
    echo G4WORKDIR is ${G4WORKDIR}
    export LD_LIBRARY_PATH=${G4WORKDIR}:${LD_LIBRARY_PATH}

    echo LD_LIBRARY_PATH is ${LD_LIBRARY_PATH} 


    
    #
    # Set a flag to suppress unnecessary re-executions of this script.
    #    
    #export G4LBNE_IS_SETUP=1

# Setup grid submit tools if not on a grid node
if [ -z "${_CONDOR_SCRATCH_DIR}" ]; then
    echo "_CONDOR_SCRATCH_DIR is not set... so I'm assuming we're not running on a grid node.... Setting up jobsub tools."
    setup jobsub_tools
    setup jobsub_client
fi

fi


source /grid/fermiapp/products/common/etc/setup
setup ifdhc

# Setup grid submit tools if not on a grid node
if [ -z "${_CONDOR_SCRATCH_DIR}" ]; then
    echo "_CONDOR_SCRATCH_DIR is not set... so I'm assuming we're not running on a grid node.... Setting up jobsub tools."
    setup jobsub_tools
fi

if [ -z "${G4LBNE_IS_SETUP}" ]; then
    
    case ${HOSTNAME} in
        argoneut*)
            export GROUP=argoneut
            export EXPERIMENT=argoneut
            ;;
        uboone*)
            export GROUP=microboone
            export EXPERIMENT=uboone
            ;;
        lbne*)
            export GROUP=lbne
            export EXPERIMENT=lbne
            ;;
    esac
    echo experiment is $GROUP

    export SETUP_LOCATION=/grid/fermiapp/products/lbne/

    #
    #Locate and Setup UPS
    #
    # Make sure this file exists.
    if [ -f ${SETUP_LOCATION}/setup ]; then
	source ${SETUP_LOCATION}/setup
    fi
    #
    #Now SETUP_UPS and UPS_DIR env vars should now be set
    #so setup ups now
    #
    setup ${SETUP_UPS}

    #
    #Set the versions of root, and geant4 you want to setup
    #If you want to print the avaliable versions define print_products variable
    print_products=1
    #
    export ROOT_VERSION=v5_30_06
    export ROOT_FLAVOR=Linux64bit+2.6-2.5
    export ROOT_QUALIFIERS=gcc46:prof

    export GEANT4_VERSION=v4_9_4_p03
    export GEANT4_FLAVOR=Linux64bit+2.6-2.5
    export GEANT4_QUALIFIERS=gcc46:prof

    #

    if [ -z "${print_products}" ]; then
	echo "PRODUCTS = ${PRODUCTS}"
    
	ups list -a root
	ups list -a geant4
	ups list -a clhep
    
    fi


    echo "Setting up ROOT"
    export SETUP_ROOT="root ${ROOT_VERSION} -f ${ROOT_FLAVOR} -z ${SETUP_LOCATION} -q ${ROOT_QUALIFIERS}"
    setup ${SETUP_ROOT}
    echo "setup ${SETUP_ROOT}"
    
    echo "Setting up GEANT4"
    export SETUP_GEANT4="geant4 ${GEANT4_VERSION} -f ${GEANT4_FLAVOR} -z ${SETUP_LOCATION} -q ${GEANT4_QUALIFIERS}"
    setup ${SETUP_GEANT4}
    echo "setup ${SETUP_GEANT4}"

    echo "Setting up G4Photon"
    export SETUP_G4PHOTON="g4photon v2_1"
    setup ${SETUP_G4PHOTON}
    echo "setup ${SETUP_G4PHOTON}"

##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##
## Set G4WORKDIR to g4lbne directory, if not already set
##
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SETUPDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    
    if [ -z "$G4WORKDIR" ]; then
	export G4WORKDIR=`dirname $SETUPDIR`
	echo "G4WORKDIR is not set... Setting it to "$G4WORKDIR 
    fi

    export G4LBNEWORKDIR=`dirname $SETUPDIR`

    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$G4LIB/Linux-g++"




    echo "*********************"
    echo Locating ROOT...
    env | grep root
    echo "*********************"
    echo Locating Geant4...
    env | grep geant4
    echo "*********************"
    echo Locating CLHEP...
    env | grep clhep
    echo "*********************"
    
    setup dk2nu r487 -f Linux64bit+2.6-2.5 -q g4lbne-gcc46-r53006
    echo Locating Dk2Nu
    env | grep DK2NU
    echo "*********************"

    
    echo G4WORKDIR is ${G4WORKDIR}
    echo LD_LIBRARY_PATH is ${LD_LIBRARY_PATH} 
    
    #
    # Set a flag to suppress unnecessary re-executions of this script.
    #    
    #export G4LBNE_IS_SETUP=1

fi

import os, sys

copy_dir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r0p10/"

for root, dirs, files in os.walk(copy_dir):
    for file in files:

        from_dir = root
        dest_dir = from_dir.replace("/lbne/data/users/","/pnfs/lbne/scratch/users/");
        
        print "Copying "+os.path.join(root,file)+" TO "+dest_dir

        # make directories
        folders = dest_dir.split("/")
        temp = "/"
        for i in range(0,len(folders)):
            temp = os.path.join(temp,folders[i])
            if not os.path.exists(temp):
                print "Making "+temp
                os.system("ifdh mkdir "+temp)

                
        # copy file
        if os.path.exists(os.path.join(dest_dir,file)):
            if (os.path.getsize(os.path.join(dest_dir,file)) ==
                os.path.getsize(os.path.join(from_dir,file))): 
                print dest_dir+file+": File already exists and is identical..skipping"
            if (os.path.getsize(os.path.join(dest_dir,file)) >
                os.path.getsize(os.path.join(from_dir,file))): 
                print dest_dir+file+ ": File already exists and is bigger than source file..skipping"
            if (os.path.getsize(os.path.join(dest_dir,file)) <
                os.path.getsize(os.path.join(from_dir,file))): 
                print dest_dir+file+ ": File already exists but is smaller than source file... removing old file and replacing."
                os.remove(dest_dir+file)

        if not os.path.exists(os.path.join(dest_dir,file)):
            os.system("ifdh cp "+os.path.join(from_dir,file)+" "+os.path.join(dest_dir,file))

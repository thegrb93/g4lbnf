//////////////////////////////////////////////////////////
// Created by L. Loiacono
// Modified by L. Fields
//////////////////////////////////////////////////////////

#ifndef pidata_H2_h
#define pidata_H2_h

// C++
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TH2.h>

//ROOT
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TRandom3.h>

class pidata_H2 {
public :

   TChain          *fChain;   //!pointer to the analyzed TTree or TChain
   //TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   
   Double_t        fTotalPOT; //total pot used for all files
   std::string     ffilename; //filename for saving histograms
   std::string     filename;
   std::string     detectorname; // name for det location
   double detx;     // detector location
   double dety;
   double detz;
int tot = 0,piplus,piminus = 0,nproton = 0,nelectron = 0, kplus = 0, kminus = 0, numu = 0, nue = 0, nutau = 0, numubar = 0, nuebar = 0, nutaubar = 0,neutron = 0, muon =
0, rest = 0;
int pptot = 0, pmtot = 0, ptot = 0, etot = 0, kptot = 0, kmtot = 0, numutot = 0,
numubartot = 0, nuebartot = 0, nuetot = 0, ntot = 0, mutot = 0, rtot = 0, ntott = 0, neutot = 0;
   TRandom3 *rand3;
 // Declaration of leaf types
   Int_t           nH2P;
   Int_t           trackID;
   Int_t           pdgCode;
   Double_t        H2ImpWeight;
   Double_t        H2positionX;
   Double_t        H2positionY;
   Double_t        H2positionZ;
   Double_t        H2momentumdirX;
   Double_t        H2momentumdirY;
   Double_t        H2momentumdirZ;
   Double_t        mass;
   Double_t        energy;
   Double_t        H2momentumX;
   Double_t        H2momentumY;
   Double_t        H2momentumZ;
   Double_t        H2PXPZ;
   Double_t        H2PYPZ;
   Double_t        H2PProductionX;
   Double_t        H2PProductionY;
   Double_t        H2PProductionZ;
   Double_t        H2PmomentumdirX;
   Double_t        H2PmomentumdirY;
   Double_t        H2PmomentumdirZ;
   Int_t           nH1P;
 
/*   Double_t        DPPProductionX;
   Double_t        DPPProductionY;
   Double_t        DPPProductionZ;
   Int_t           DPPtrackID;
   Int_t           DPParentID;
   Double_t        DPenergy;
   Double_t        DPpositionX;
   Double_t        DPpositionY;
   Double_t        DPpositionZ;
   Double_t        DPmomentumX;
   Double_t        DPmomentumY;
   Double_t        DPmomentumZ;
   Int_t           DPpdgCode;*/

   // List of branches
   TBranch        *b_fH2NParticles;   //!
   TBranch        *b_fH2TrackID;   //!
   TBranch        *b_fH2ParticlePDG;   //!
   TBranch        *b_fNImpWt;   //!
   TBranch        *b_fH2ParticleX;   //!
   TBranch        *b_fH2ParticleY;   //!
   TBranch        *b_fH2ParticleZ;   //!
   TBranch        *b_fH2ParticleDX;   //!
   TBranch        *b_fH2ParticleDY;   //!
   TBranch        *b_fH2ParticleDZ;   //!
   TBranch        *b_fH2ParticleMass;   //!
   TBranch        *b_fH2ParticleEnergy;   //!
   TBranch        *b_fH2ParticlePX;   //!
   TBranch        *b_fH2ParticlePY;   //!
   TBranch        *b_fH2ParticlePZ;   //!
   TBranch        *b_fH2ParticlePXPZ;   //!
   TBranch        *b_fH2ParticlePYPZ;   //!
   TBranch        *b_fH2PProductionX;   //!
   TBranch        *b_fH2PProductionY;   //!
   TBranch        *b_fH2PProductionZ;   //!
   TBranch        *b_fH2PmomentumdirX;  //! 
   TBranch        *b_fH2PmomentumdirY;  //! 
   TBranch        *b_fH2PmomentumdirZ;  //! 

 /*  TBranch        *b_fDPPProductionX;   //!
   TBranch        *b_fDPPProductionY;   //!
   TBranch        *b_fDPPProductionZ;   //!
   TBranch        *b_fDPTrackID;   //!
   TBranch        *b_fDPParentID;   //!
   TBranch        *b_fDPParticleEnergy;   //!
   TBranch        *b_fDPParticleX;   //!
   TBranch        *b_fDPParticleY;   //!
   TBranch        *b_fDPParticleZ;   //!
   TBranch        *b_fDPParticlePX;   //!
   TBranch        *b_fDPParticlePY;   //!
   TBranch        *b_fDPParticlePZ;   //!
   TBranch        *b_fDPParticlePDG;   //!
   */
   pidata_H2(TTree *t=0);
   virtual ~pidata_H2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
void SetTitles1(TH1* h, 
		  const std::string &xtitle = "", 
		  const std::string &ytitle = "");
void SetTitles2(TH2* h, 
		  const std::string &xtitle = "", 
		  const std::string &ytitle = "");

   
};

#endif

#ifdef pidata_H2_cxx
pidata_H2::pidata_H2(TTree *tree)
{

//????????????????????????????
//????????????????????????????
//
   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   //
   //Add files here
   //
  std::vector<std::string> fFileVec;
  fFileVec.push_back("g4lbne_example_010.root");
  fFileVec.push_back("g4lbne_example_011.root");
  
   //
   //set number of pot per file !!!!!!!!!!!!!!!!!!!!!!!!!!!!
   //
   double potperfile = 10000.0;
   fTotalPOT  = potperfile*(double)fFileVec.size();

   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   //set the filename prefix for saving histogram plots 
   //
   ffilename = "g4lbne_fluxexample_pion.root";
  

//
//????????????????????????????
//????????????????????????????

   fChain = new TChain("H2trackingdata");
   for(std::vector<std::string>::const_iterator sit = fFileVec.begin(); sit != fFileVec.end(); ++sit)
   {
      fChain -> Add(sit -> c_str());
   }

   Init(fChain);


}
/*
pidata_H2::pidata_H2(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("g4lbne_example_le010z185i_NumiDPHelium_010.root");
      if (!f) {
         f = new TFile("g4lbne_example_le010z185i_NumiDPHelium_010.root");
      }
      tree = (TTree*)gDirectory->Get("pidata_H2");

   }
   Init(tree);
}
*/


pidata_H2::~pidata_H2()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t pidata_H2::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t pidata_H2::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}


void pidata_H2::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers

   fCurrent = -1;
   fChain->SetMakeClass(1);
   
 fChain->SetBranchAddress("nH2P", &nH2P, &b_fH2NParticles);
   fChain->SetBranchAddress("trackID", &trackID, &b_fH2TrackID);
   fChain->SetBranchAddress("pdgCode", &pdgCode, &b_fH2ParticlePDG);
   fChain->SetBranchAddress("H2ImpWeight", &H2ImpWeight, &b_fNImpWt);
   fChain->SetBranchAddress("H2positionX", &H2positionX, &b_fH2ParticleX);
   fChain->SetBranchAddress("H2positionY", &H2positionY, &b_fH2ParticleY);
   fChain->SetBranchAddress("H2positionZ", &H2positionZ, &b_fH2ParticleZ);
   fChain->SetBranchAddress("H2momentumdirX", &H2momentumdirX, &b_fH2ParticleDX);
   fChain->SetBranchAddress("H2momentumdirY", &H2momentumdirY, &b_fH2ParticleDY);
   fChain->SetBranchAddress("H2momentumdirZ", &H2momentumdirZ, &b_fH2ParticleDZ);
   fChain->SetBranchAddress("mass", &mass, &b_fH2ParticleMass);
   fChain->SetBranchAddress("energy", &energy, &b_fH2ParticleEnergy);
   fChain->SetBranchAddress("H2momentumX", &H2momentumX, &b_fH2ParticlePX);
   fChain->SetBranchAddress("H2momentumY", &H2momentumY, &b_fH2ParticlePY);
   fChain->SetBranchAddress("H2momentumZ", &H2momentumZ, &b_fH2ParticlePZ);
   fChain->SetBranchAddress("H2PXPZ", &H2PXPZ, &b_fH2ParticlePXPZ);
   fChain->SetBranchAddress("H2PYPZ", &H2PYPZ, &b_fH2ParticlePYPZ);
   fChain->SetBranchAddress("H2PProductionX", &H2PProductionX, &b_fH2PProductionX);
   fChain->SetBranchAddress("H2PProductionY", &H2PProductionY, &b_fH2PProductionY);
   fChain->SetBranchAddress("H2PProductionZ", &H2PProductionZ, &b_fH2PProductionZ);
  fChain->SetBranchAddress("H2PmomentumdirX",&H2PmomentumdirX,&b_fH2PmomentumdirX);
  fChain->SetBranchAddress("H2PmomentumdirY",&H2PmomentumdirY,&b_fH2PmomentumdirY);
  fChain->SetBranchAddress("H2PmomentumdirZ",&H2PmomentumdirZ,&b_fH2PmomentumdirZ);
/*   fChain->SetBranchAddress("DPPProductionX", &DPPProductionX, &b_fDPPProductionX);
   fChain->SetBranchAddress("DPPProductionY", &DPPProductionY, &b_fDPPProductionY);
   fChain->SetBranchAddress("DPPProductionZ", &DPPProductionZ, &b_fDPPProductionZ);
   fChain->SetBranchAddress("DPPtrackID", &DPPtrackID, &b_fDPTrackID);
   fChain->SetBranchAddress("DPParentID", &DPParentID, &b_fDPParentID);
   fChain->SetBranchAddress("DPenergy", &DPenergy, &b_fDPParticleEnergy);
   fChain->SetBranchAddress("DPpositionX", &DPpositionX, &b_fDPParticleX);
   fChain->SetBranchAddress("DPpositionY", &DPpositionY, &b_fDPParticleY);
   fChain->SetBranchAddress("DPpositionZ", &DPpositionZ, &b_fDPParticleZ);
   fChain->SetBranchAddress("DPmomentumX", &DPmomentumX, &b_fDPParticlePX);
   fChain->SetBranchAddress("DPmomentumY", &DPmomentumY, &b_fDPParticlePY);
   fChain->SetBranchAddress("DPmomentumZ", &DPmomentumZ, &b_fDPParticlePZ);
   fChain->SetBranchAddress("DPpdgCode", &DPpdgCode, &b_fDPParticlePDG);*/
   Notify();
}

Bool_t pidata_H2::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void pidata_H2::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t pidata_H2::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef pidata_H2_cxx

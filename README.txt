
G4LBNE Documentation

The Beam Simulation Group Wiki is here
https://cdcvs.fnal.gov/redmine/projects/lbne-beamsim/wiki
This contains information about Fermilab accounts,
g4lbne and flux files.  

*************************************************************
Getting g4lbne
*************************************************************
See the wiki for instructions on checking out g4lbne.  If you 
are working on the lbnegpvm's at Fermilab, you should check 
out the code into your /dune/app/<user> working area.  
In particular, you should not check the code out to your 
afs home area, which will lead to a variety of problems.


*************************************************************
Setting up products and work directory
*************************************************************

ON FERMILAB MACHINES:
On lbnegpvm03 (At Fermilab) ..... in the g4lbne/setups directory
There is a setup_g4lbne_fnal.sh file. (This file will setup
Geant4, ROOT and CLHEP ).  Do:

source setups/setup_g4lbne_fnal.sh

ELSEWHERE:
GEANT and ROOT must be installed on whatever machine you want to build g4lbne on.  
See setups/setup_g4lbne_fnal.sh to see the versions we currently build against.



*************************************************************
Compiling
*************************************************************

in the g4lbne directory,
for the first time type
cmake .

then 
make all

************************************************************


*************************************************************
Running  (see next section for a step by step example)
*************************************************************

There are several example macros in the macros/ directory.  To run in the default configuration, use the Nominal.mac file.  To run it, type

g4lbnf macros/Nominal.mac 



*************************************************************
Running and Plotting (a simple example)
*************************************************************

Once you have compiled g4lbne, you are ready to run basic beam simulation jobs.  For example, from the g4lbne directory run the following

./g4lbne example/example_macro_010.mac

If you examime the example/example_macro_010.mac macro...you 
will see that the Run ID is 10 and the output file name will be 
g4lbne_example_010.root in the example directory

once that is finished running, try another one...

./g4lbne example/example_macro_011.mac

Once that is finished...in the g4lbne/example directory you have 2 flux ntuple files called 

g4lbne_example_010.root
g4lbne_example_011.root

You can now plot the flux.  An example of how to do this is available in 
example/ nudata.C, a ROOT macro.  The input filenames are set in the nudata::nudata()  constructor in the file nudata.h.  These should already be set to the files you just created, but you can add more, change the names, etc.  If you change
the number of POT generated, set in the g4lbne macro in a line like this:

/LBNE/run/NEvents 10000

you should also change the "potperfile" variable in nudata.h to reflect your 
POT choice.

To make plots with nudata, open a root session and type the following:
type the following...(amongst the output)

root [0] .L nudata.C++
(const class nudata)30558496
root [1] nudata t
root [2] t.Loop()
...
root [3] .q

After each print statment you will have plots of the Total Flux, NuMu Flux only, NuMuBar Flux only, NuE Flux only and NuEBar flux only at the DUNE Far detector and at "some detector".  All of the histograms will also be written to the file flux_histograms.root.  

The DUNE far detector plots are produced using the location weights from the g4lbne ntuple.  The "some detector" plots use weights from the NuWeight() function in nudata.C, which reweights the default flux at some other (x,y,z) coordinate specified by the user.  The position of "some detector" is currently set to the position of the DUNE far detector, so the DUNEND and SOMEDETECTOR plots will be identical.  However, you may change the position of "some detector" to be anything you desire.  Each plot will be saved as an eps and a png in the g4lbne/example directory.  

More information on how the flux is calculated can be obtained by examining nudata.C.  Note than in addition to applying the detector position weights discussed above, the plots also use "importance weights" which are used to ensure that there are sufficient statistics in the high energy tails without exceedingly large file sizes.  If you modify nudata.C or create your own flux plotting macros, it is imperative that you use both the position weights and the importance weights when creating your plots.


*************************************************************
Generating and using large flux samples at Fermilab
*************************************************************
For generating and analyzing beam simulations on Fermilab resources, it is recommended that you use the infrastructure available in the ProductionScripts directory of g4lbne.  Below is a description of the procedure for creating a set of near and far detector flux histograms using the default beam configuration.  You will need to have Fermilab grid credentials to do this (see the wiki).

1. Generate flux ntuples and histograms

After obtaining credentials for running on the FermiGrid, you can use the script ProductionScripts/submit_flux.py to generate a large sample of flux monte carlo.  The various inputs and options of submit_flux.py are available by executing 

python submit_flux.py --help

The following commands will submit 250 jobs each in neutrino and anti-neutrino mode.  Each job will simulate 100,000 protons on target.  You can simulate more or less POT if you want, but it's important that you remember what POT/file you requested since it will be necessary later for normalizing plots.

python ProductionScripts/submit_flux.py -f 1 -l 250 -n 100000 --macro=Nominal  --mode neutrino
python ProductionScripts/submit_flux.py -f 1 -l 250 -n 100000 --macro=Nominal --mode antineutrino

Each job will take approximately two hours once it starts running on a grid machine.  The whole set of four samples will typically complete within a few hours.  You can see the status of your jobs by executing condor_q | grep <your_userid>.  When the jobs have completed, ntuple, histogram and logfiles should appear in your /pnfs/scratch/users/<your_userid>/fluxfiles/ directory.  If they don't, check the logfiles in your $CONDOR_TMP area, which will hopefully hold clues about what went wrong.

If you wish to simulate a special beam configuration with a different geometry, beam power, etc, you should create a new macro in the macros/ directory e.g. MyMacro.mac, then change the '--macro=Nominal' flag above to '--macro=MyMacro'.

2. Merge histograms 

The example job submission above will create 250 separate histogram files each in neutrino mode and antineutrino mode.  It is convienent to merge these all into one set of histograms for plotting.  This can be done with the ProductionScripts/merge_histograms.py script.  It takes inputs similar to the submit_flux.py script described above.  To merge this histograms you made in step 1. above, do the following: 

python ProductionScripts/merge_histograms.py -n 100000 --mode neutrino -m Macro
python ProductionScripts/merge_histograms.py -n 100000 --mode antineutrino -m Macro

3. Make comparison plots and tables

The root macro eventRateComparison.C in the 'example' directory can be used to make some simple plots comparing your two flux simulations.  You can run it by doing:

source doComp.sh <userid>/<userid> v3r0p4/v3r0p4 Nominal/MyMacro 200kA/200kA QGSP_BERT/QGSP_BERT LBNEND/LBNEND LBNEFD/LBNEFD Nominal/DPHelium
source doComp.sh <userid>/<userid> v3r0p4/v3r0p4 Nominal/MyMacro -200kA/-200kA QGSP_BERT/QGSP_BERT LBNEND/LBNEND LBNEFD/LBNEFD Nominal/DPHelium

The last two input strings are meant to describe the two samples and will be used in the plot legends and file names to identify what is being compared.  This will run several instances of eventRateComparison.C (for oscillated/unoscillated plots and FHC/RHC).  Each instance will create a set of image and text files will be written out to a folder called eventRateComparisons in your working directory. If you wish to do comparisons of more than two configurations, you can add more
strings delimited by "/" to the arguments of doComp.sh.  The last two input strings are meant to describe the two samples and will be used in the plot legends and file names to identify what is being compared.
  


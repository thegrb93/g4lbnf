##################################################
##
## copy_fastmc_flux.py
##
## Laura Fields
## Aug 2013
##
## Copies FastMC flux histograms from user area
## to FMC area
##
###################################################

from optparse import OptionParser
import sys, os, subprocess, shutil
opt_path = os.path.abspath('../BeamSimStudies/GeneticOptimization/')
sys.path.append(opt_path)
import OptimizationUtils


###################################################
#
# Setup parser that reads in options
#
###################################################

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)

parser.add_option("-d", "--fmc_dir", dest="fmc_dir",
                  help="Name of directory you want to create in $FMC_FLUX_FILES", default="")
parser.add_option("-c", "--current", dest="current",
                  help="Magnitude of horn current", default="neutrino")
parser.add_option("-p", "--physics_list", dest="physics_list",
                  help="Geant4 Physics List", default="QGSP_BERT");
parser.add_option("-v", "--g4lbne_version", dest="g4lbne_version",
                  help="G4LBNE version", default="v2r4p1");
parser.add_option("-w", "--fmc_version", dest="fmc_version",
                  help="FMC version", default="v3r2p4b");
parser.add_option("-m", "--macro", dest="macro",
                  help="G4LBNE Macro", default="nubeam-G4PBeam-stdnubeam")
parser.add_option("-t", "--test", dest="test",action="store_true",
                  help="Test mode -- just print file locations but don't move anything", default=False)
parser.add_option("-f", "--fdloc", dest="fdloc",
                  help="Far Detector Location", default="LBNEFD");
parser.add_option("-n", "--ndloc", dest="ndloc",
                  help="Near Detector Location", default="LBNEND");
parser.add_option("-e", "--proton_energy", dest="proton_energy",
                  help="Primary Proton Beam Energy in GeV")


(options, args) = parser.parse_args()

if options.fmc_dir == "":
    print "\nPlease specify a $FMC_FLUX_FILES subdirectory name using --fmc_dir flag"
    print "Execute \"python copy_fastmc_flux.py --help\" for more info\n"
    sys.exit()

###################################################
#
# Find source files
#
###################################################
username = os.getenv("USER")

sources = []

sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_"+options.current+"_"+options.fdloc+"_fastmc.root")
sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/anti"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_anti"+options.current+"_"+options.fdloc+"_fastmc.root")
sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_"+options.current+"_"+options.ndloc+"_fastmc.root")
sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/anti"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_anti"+options.current+"_"+options.ndloc+"_fastmc.root")

globes_sources = []

globes_sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_"+options.current+"_"+options.fdloc+"_globes_flux.txt")
globes_sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/anti"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_anti"+options.current+"_"+options.fdloc+"_globes_flux.txt")
globes_sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_"+options.current+"_"+options.ndloc+"_globes_flux.txt")
globes_sources.append("/dune/data/users/"+username+"/fluxfiles/g4lbne/"+options.g4lbne_version+"/"+options.physics_list+"/"+options.macro+"/anti"+options.current+"/flux/histos_g4lbne_"+options.g4lbne_version+"_"+options.physics_list+"_"+options.macro+"_anti"+options.current+"_"+options.ndloc+"_globes_flux.txt")

###################################################
#
# Define destination
#
###################################################

dests = []

if not os.getenv("FMC_FLUX_FILES"):
    print "$FMC_FLUX_FILES is not set.  Please run the FastMC Setup before running this script."

dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_FHC_FD.root")
dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_RHC_FD.root")
dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_FHC_ND.root")
dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_RHC_ND.root")

globes_dests = []

if not os.getenv("FMC_FLUX_FILES"):
    print "$FMC_FLUX_FILES is not set.  Please run the FastMC Setup before running this script."

globes_dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_FHC_FD_globes_flux.txt")
globes_dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_RHC_FD_globes_flux.txt")
globes_dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_FHC_ND_globes_flux.txt")
globes_dests.append(os.getenv("FMC_FLUX_FILES")+"/"+options.fmc_version+"/"+options.fmc_dir+"/g4lbne_"+options.fmc_version+"_RHC_ND_globes_flux.txt")

for i in range(0,4):
    if os.path.exists(sources[i]):
        print "Copying "+sources[i]+" to "+dests[i]
    else:
        print "ERROR: Could not find "+sources[i]
    if os.path.exists(globes_sources[i]):
        print "Copying "+globes_sources[i]+" to "+globes_dests[i]
    else:
        print "ERROR: Could not find "+globes_sources[i]

if options.test:
    
    print "You requested test mode, so I'm not actually going to copy anything."
    sys.exit()

###################################################
#
# Create Directories
#
###################################################
if not os.path.exists(os.path.dirname(dests[0])):
    os.makedirs(os.path.dirname(dests[0]))
    os.chmod(os.path.dirname(dests[0]),0777)

###################################################
#
# Add power factor for fast mc
#
###################################################
power_factor = OptimizationUtils.GetPowerPOTScaleFactor(float(options.proton_energy))
for i in range(0,4):
    if os.path.exists(sources[i]):
        print ("root -q -b addPowerFactor.C\(\\\""+sources[i]+"\\\","+str(power_factor)+"\)")
        os.system("root -q -b addPowerFactor.C\(\\\""+sources[i]+"\\\","+str(power_factor)+"\)")


###################################################
#
# Copy files
#
###################################################
for i in range(0,4):
    if os.path.exists(sources[i]):
        shutil.copyfile(sources[i],dests[i])
        try:
            os.chmod(dests[i],0777)
        except:
            print "Could not change permissions for file: "+dests[i]

    if os.path.exists(globes_sources[i]):
        shutil.copyfile(globes_sources[i],globes_dests[i])
        try:
            os.chmod(globes_dests[i],0777)
        except:
            print "Could not change permissions for file: "+globes_dests[i]

###################################################
#
# Create RIK files
#
###################################################
os.system("root -b -q $FMC_GENIE/addfineflux.C\\(\\\""+dests[0]+"\\)")
os.system("root -b -q $FMC_GENIE/addfineflux.C\\(\\\""+dests[1]+"\\)")
os.system("root -b -q $FMC_GENIE/addfineflux.C\\(\\\""+dests[2]+"\\)")
os.system("root -b -q $FMC_GENIE/addfineflux.C\\(\\\""+dests[3]+"\\)")

#ifndef LBNEDetectorMessenger_H
#define LBNEDetectorMessenger_H 1

#include "globals.hh"
#include "G4UImessenger.hh"

class LBNEDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class LBNEDetectorMessenger: public G4UImessenger {

public:
   LBNEDetectorMessenger(LBNEDetectorConstruction* );
   ~LBNEDetectorMessenger();
   
   void SetNewValue(G4UIcommand*, G4String);
   
private:
   LBNEDetectorConstruction* LBNEDetector;
   
   G4UIdirectory*              LBNEDir;
   G4UIdirectory*              detDir;


   G4UIcmdWithABool*           ConstructTarget;


   G4UIcmdWithoutParameter*    UpdateCmd;
   G4UIcmdWithoutParameter*    ConstructCmd;
   
   G4UIcmdWithADoubleAndUnit*         WaterLayerThickInHorn;

      
   G4UIcmdWithADoubleAndUnit*    SetBeamlineAngle;
   G4UIcmdWithADoubleAndUnit*    SetHornCurrent;
   G4UIcmdWithADoubleAndUnit*    SetSkinDepthInnerRad;
   std::vector<G4UIcmdWithADoubleAndUnit*>  SetHornsDeltaEccentricityIO;
   std::vector<G4UIcmdWithADoubleAndUnit*>  SetHornsDeltaEllipticityI;
   std::vector<G4UIcmdWithADoubleAndUnit*>  SetHornsCurrentEqualizerLongAbsLength;
   std::vector<G4UIcmdWithADouble*>  SetHornsCurrentEqualizerQuadAmpl;
   std::vector<G4UIcmdWithADouble*>  SetHornsCurrentEqualizerOctAmpl;
   std::vector<G4UIcmdWithADouble*>  SetHornsCurrentMultiplier;

   G4UIcmdWithABool*           ConstructSimpAbsorber;  // simplified absorber  
   G4UIcmdWithABool*           ConstructSculptedAbsorber;  // Absorber for the 2.3 MW beam, Aluminium, scuplted.  
   G4UIcmdWithABool*           DisableSpoiler;//Toggle spoiler
   G4UIcmdWithABool*           DisableSculptedLayers;//Toggle scalloping on aluminum layers

   G4UIcmdWithADoubleAndUnit*  DwStrAbsSteelWidth;
   G4UIcmdWithADoubleAndUnit*  DwStrAbsConcreteWidth;

  
   G4UIcmdWithADoubleAndUnit*    ZCoordForPerfectFocusing;

};

#endif

//---------------------------------------------------------------------------// 
// $Id: LBNEVolumePlacements.hh,v 1.1.2.35 2013/12/06 18:23:50 lebrun Exp $
//---------------------------------------------------------------------------// 

#ifndef LBNEVolumePlacement_H
#define LBNEVolumePlacement_H 1

#include <vector>
#include <map>

//#include "G4VUserVolumePlacement.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4PVPlacement.hh"
#include "G4ExceptionSeverity.hh"

//---------------------------------------------------------------------------// 
// This class controls the placement of everything within the geometry. 

//class LBNEVolumePlacement : public G4VUserVolumePlacement
//
// The file G4VUserVolumePlacement.hh was found missing in my cvs instance of the package.
// Not sure what happened here. 
// In any event, do we need inheritance here? 
//
// Rephrasing the scope and goal for this class: a container that all hold 
// critical volume nominal placements in the package. 
// Real placements can be modifed before physical placement by correcting 
// for misalignments using the LBNESurveyor data.
// Thus, for a given subVolume, we might have two placements, two entries in the 
// in the map.  
//
// Benefits of this class are (i) a central repository of all 
//   nominal and tried placements data. 
//  (ii) interface to the messenger to get nominal data from User's cards.  
//  (iii) interface with the Surveyor data class, in case misalignment are studied
//
// The Dectetor construction handles the list of materials, and will use this repository 
// of placement 
//
// Key functionality : 
//
//  CheckPlacement 
//
// This is a singleton class 
//

//
// Paul L. G. Lebrun, July 2013 
//
// Type of alignment study algorythms one could use 

enum LBNEVolumePlacements_AlignmentAlgo {eNominal, eFixed, eRandom}; 
//
// First, one more container to avoid the search through the map more then once 
//
class LBNEVolumePlacements;

struct LBNEVolumePlacementData {
  LBNEVolumePlacements_AlignmentAlgo fAlignmentModel;
  bool fIsPlaced;
  G4ThreeVector fPosition;
  G4RotationMatrix fRotation;
  bool fRotationIsUnitMatrix;
  std::vector<double> fParams; // sizes, or others.. 
  G4String fTypeName; // Box, tube, etc...
  G4VPhysicalVolume *fMother; // Not Owned. G4 maintain this pointer.  Do not delete in the destructor of this class. 
  G4LogicalVolume* fCurrent; // Same 

  void initialize() {
      fAlignmentModel = eNominal;
      fIsPlaced = false;
      fRotation = G4RotationMatrix();
      fRotationIsUnitMatrix = true;
      fPosition = G4ThreeVector(0.0, 0.0, 0.0);
      fParams.resize(3, 0.0);
      fMother = 0;
      fTypeName = G4String("Tubs"); // most common type of volume..
  }
 
};

class LBNEHornRadialEquation  {

  public:
      LBNEHornRadialEquation(); // to be able to store in an stl vector. 
      LBNEHornRadialEquation(double rSqrtCoefficient, double zCoefficient, double rOffset, bool parabolic=true);
      double GetVal(double z) const ; 
      void test1() const; // Cross check for equation 1. Will generate G4Exception 
  
  private:
      static double inchDef;
      bool parabolic;
      double rCoeff;
      double zCoeff;
      double rOff;
      double rResc;
      double rRescCst;
      double zResc;
      
  public:    
    inline void SetLongRescaleFactor(double r) {zResc = r;}
    inline void SetRadialRescaleFactor(double r) {rResc = r;}
    inline void SetRadialRescaleFactorCst(double r) {rRescCst = r;}

};

class LBNEVolumePlacements
{

private:
  LBNEVolumePlacements();
  ~LBNEVolumePlacements();
  LBNEVolumePlacements (LBNEVolumePlacements const &); // not implemented 
  LBNEVolumePlacements& operator=(LBNEVolumePlacements const&); // not implemented 
  static LBNEVolumePlacements* fInstance;

public:
//
// Alignment algorithm See also LBNE Surveyor 
// eNominal:  the element on the beam line is placed perfectly, no misalignment 
// eFixed:  The placement occurs after picking up the surveyed value for one or more points 
//          rigidly attached to the placed volume in question. 
// eRandom: The survey point is understood as a tolerance figure, the actual placement will be taken randomly
//           assuming the actual placement occurs within +- 2 sigma, truncated gaussian. (to be negotiated )
   
  static LBNEVolumePlacements* Instance(); 
  
  void InitializeMessenger(); // Can't be in the constructor, inf. loop otherwise
  void Initialize(const G4LogicalVolume* matriarch); // Define only the top of the hierarchy
 
  LBNEVolumePlacementData* Create(const G4String &name);     											    
  
  G4PVPlacement* PlaceFinal(const G4String &name, G4VPhysicalVolume *mother);
			       
  void PlaceFinalUpstrTarget(G4PVPlacement *mother); // Aug 2014: if NoSplit target, this method name is confusing
                                                     // As there is downstream target any longer. 
  void PlaceFinalDownstrTarget(G4PVPlacement *mother);
 
  void PlaceFinalHorn1(G4PVPlacement *mother,  G4PVPlacement *motherDownstrTarget);
  void PlaceFinalNoSplitHorn1(G4PVPlacement *mother, G4PVPlacement *motherTop); //2nd argument is to place plug in Horn1. 
  void UpdateParamsForHorn1MotherPoly();
// August 2015 Multiple Horns 
  void PlaceFinalSimpleHornPolyNumber( size_t iHorn, G4PVPlacement *mother); // For Horns Optimization studies. 
                             // iHorn ranges from 0 to 4, inclusive.   
  void UpdateParamsForHornMotherPolyNum(size_t iH);
  
  
  void ExtendChaseLengthForHorn2(); // Feb. 2015: Optimization seems to require longer chase. 
  void PlaceFinalHorn2(G4PVPlacement *mother);
  
  void PlaceFinalDecayPipeSnout(G4PVPlacement *mother);
  double GetMaxRadiusMotherHorn1();
  			       
  void PrintAll() const;

  inline double GetTargetHallZ() const { return fTargetHallZ; }
  inline double GetDecayHallZ()  const { return fDecayHallZ; }
  inline double GetDecayPipeLength()  const { return fDecayPipeLength; }
  inline double GetDecayPipeRadius()  const { return fDecayPipeRadius; }
  inline double GetDecayPipeUpstrWindowThick()  const { return fDecayPipeUpstrWindowThick; }
  inline double GetDecayPipeLongPosition()  const { return fDecayPipeLongPosition; }
  inline G4String GetDecayPipeGas() const {return fDecayPipeGas; }
  inline G4String GetHorn1InnerConductorMaterial() const {return fHorn1InnerCondMat; }
  inline G4String GetHorn2InnerConductorMaterial() const {return fHorn2InnerCondMat; }
  inline G4String GetHorn1AllConductorMaterial() const {return fHorn1AllCondMat; }
  inline G4String GetHorn2AllConductorMaterial() const {return fHorn2AllCondMat; }
  inline double GetAbsorberHallZ()  const { return fAbsorberHallZ; }
  inline double GetHorn1Length()  const { return fHorn1Length; }
  inline double GetBaffleLength() const  { return fBaffleLength; }
  inline double GetBaffleWindowThickness() const  { return fBaffleWindowThickness; }
  inline double GetBaffleInnerRadius() const  { return fBaffleInnerRadius; }
  inline double GetBaffleZPosition()  const { return fBaffleZPosition; }
  inline double GetPlugLength() const  { return fPlugLength; }
  inline double GetPlugInnerRadius() const  { return fPlugInnerRadius; }
  inline double GetPlugOuterRadius() const  { return fPlugOuterRadius; }
  inline double GetPlugZPosition()  const { return fPlugZPosition; }
  inline std::string GetPlugMaterial()  const { return fPlugMaterial; }
  
  // Interface to the Detector construction classes, or others..  
   
  inline void SetTotalLengthOfRock(double l) { fTotalLength = l;}
  inline double GetTotalLengthOfRock() const { return fTotalLength;}
  inline void SetLengthOfRockDownstreamAlcove(double l) {
   fLengthOfRockDownstr = 2.0*l; // Downstream and upstream, actually, the latter does not matter.
   const double aRockLength = 2.0*(fDecayPipeLength + 160.*CLHEP::m ); // before digging the tunnel. 
      // Approximate.. 150 m. is for the target hall, Horn1 + horn2, and the Hadron absorber hall + muon alcove. 
   fTotalLength = aRockLength+fLengthOfRockDownstr;
  }
  inline double GetLengthOfRockDownstreamAlcove() const { return fLengthOfRockDownstr;}
  
 // Interface to the Messenger command 
 
  inline void SetDecayPipeLength(double l) {
     const double lCorr = l + fDecayPipeLengthCorrection;
     fDecayPipeLength=lCorr; fTotalLength = 2.0*(fDecayPipeLength + 160.*CLHEP::m + fLengthOfRockDownstr);
  }
  inline void SetDecayPipeRadius(double l) {fDecayPipeRadius=l;}
  inline void SetDecayPipeLongPosition(double l) {fDecayPipeLongPosition=l;}
  inline void SetDecayPipeUpstrWindowThick(double l) {fDecayPipeUpstrWindowThick=l;}
  inline void SetDecayPipeGas(G4String &name) {fDecayPipeGas = name;}
  inline void SetHorn1InnerConductorMaterial(G4String &name) {fHorn1InnerCondMat = name;}
  inline void SetHorn2InnerConductorMaterial(G4String &name) {fHorn2InnerCondMat = name;}
  inline void SetHorn1AllConductorMaterial(G4String &name) {fHorn1AllCondMat = name;}
  inline void SetHorn2AllConductorMaterial(G4String &name) {fHorn2AllCondMat = name;}
  inline void SetTotalLength(double l) {fTotalLength=l;} // dangerous! Possible conflict with length of rock downstream 
  inline void SetWaterLayerThickInHorns(double l) { fWaterLayerThickInHorns = l;}
  inline void SetHorn1Length(double l) { fHorn1Length = l;}
  inline void SetBaffleLength(double l) { fBaffleLength = l;}
  inline void SetBaffleInnerRadius(double r) { fBaffleInnerRadius = r;}
  inline void SetBaffleZPosition(double z) { fBaffleZPosition = z;}
  inline void SetPlugLength(double l) { fPlugLength = l;}
  inline void SetPlugInnerRadius(double r) { fPlugInnerRadius = r;}
  inline void SetPlugOuterRadius(double r) { fPlugOuterRadius = r;}
  inline void SetPlugMaterial(std::string name) { fPlugMaterial = name;}
  inline void SetPlugZPosition(double z) { fPlugZPosition = z;}
  inline void SetConstructPlug(bool t) { fConstructPlug = t;}
  inline void SetConstructPlugInHorn1(bool t) { fConstructPlugInHorn1 = t;} // Added, August 31 2014. 
//
// december 2013: Simple target 
//
  inline void SetUseSimpleTargetBox(bool t) { fUseSimpleTargetBox = t;}
  inline void SetUseSimpleTargetCylinder(bool t) { fUseSimpleTargetCylinder = t;}
  inline void SetSimpleTargetRadius(double r) { fSimpleTargetRadius = r;} 
  inline void SetSimpleTargetWidth(double r) { fSimpleTargetWidth = r;} 
  inline void SetSimpleTargetHeight(double r) { fSimpleTargetHeight = r;} 
  inline void SetSimpleTargetLength(double l) { fSimpleTargetLength = l;} 
//
  inline bool GetUseSimpleTargetBox() const { return fUseSimpleTargetBox;}
  inline bool GetUseSimpleTargetCylinder() const { return fUseSimpleTargetCylinder;}
  inline bool GetConstructPlug() const { return fConstructPlug;}
  inline bool GetConstructPlugInHorn1() const { return fConstructPlugInHorn1;}
  inline double GetSimpleTargetRadius() const {return  fSimpleTargetRadius;} 
  inline double GetSimpleTargetWidth() const { return fSimpleTargetWidth;} 
  inline double GetSimpleTargetHeight() const {return  fSimpleTargetHeight;} 
  inline double GetSimpleTargetLength() const {return  fSimpleTargetLength;} 
//
// October 2015 
//
  inline void SetRemoveTargetAlltogether(bool t) { fRemoveTargetAlltogether = t;}
  inline bool GetRemoveTargetAlltogether() { return fRemoveTargetAlltogether;}
  
// Interface to the Messenger, Target stuff. 

  inline double GetTargetSLengthGraphite() const { return fTargetSLengthGraphite; }
  inline void SetTargetSLengthGraphite(double l) { fTargetSLengthGraphite = l; }
  inline double GetTargetFinWidth() const { return fTargetFinWidth; }
  inline void SetTargetFinWidth(double l) { 
       fTargetFinWidth = l; 
       if (fUse1p2MW) {
           fTargetFinWidth /=2.; // since we will place two of them side by side 
           fTargetFinWidthRequired = fTargetFinWidth; 
           if (fTargetFinWidth > fTargetCTubeOuterRadius) {
	      fTargetFinExtraWidth = (fTargetFinWidth - 2.0*fTargetCTubeOuterRadius);
	      fTargetFinWidth = fTargetCTubeOuterRadius - 0.001*CLHEP::mm;
	   }
        }
  }
  inline double GetTargetDensity() const { return fTargetDensity; }
  inline void SetTargetDensity(double l) { fTargetDensity = l; }

  inline double GetTargetSpecificLambda() const {return fTargetSpecificLambda;}
  inline void SetTargetSpecificLambda(double l) {fTargetSpecificLambda = l;}

  inline G4String GetTargetMaterialName() const { return fTargetMaterialName; }
  inline void SetTargetMaterialName(G4String &aName) { fTargetMaterialName = aName; }
  inline double GetTargetLengthIntoHorn() const { return fTargetLengthIntoHorn; }
  inline void SetTargetLengthIntoHorn(double l) { fTargetLengthIntoHorn = l; }
  inline void SetTargetLengthOutsideHorn(double l) { fTargetLengthOutsideHorn = l; }
  inline double GetTargetBerylDownstrWindowThick() const { return fTargetBerylDownstrWindowThick;}
  inline void SetTargetBerylDownstrWindowThick(double t) { fTargetBerylDownstrWindowThick = t;}
  inline void SetHorn1RadialSafetyMargin(double t) { fHorn1RadialSafetyMargin = t;}
  inline double GetHorn1RadialSafetyMargin() const { return fHorn1RadialSafetyMargin;}
  inline void SetHorn1RadiusBigEnough(bool t) { fHorn1RadiusBigEnough = t;}
  inline bool IsHorn1RadiusBigEnough() const { return fHorn1RadiusBigEnough;}
  
//
// Interface to the Messenger, Horn1 and Horn2 parameters  
//
  inline void SetHorn1LongRescale(double r) {fHorn1LongRescale = r;}  
  inline double GetHorn1LongRescale() const {return fHorn1LongRescale;}  
  inline void SetHorn1RadialRescale(double r) {fHorn1RadialRescale = r;}  
  inline double GetHorn2LongRescale() const {return fHorn2LongRescale;}  
  inline void SetHorn2LongRescale(double r) {fHorn2LongRescale = r;}  
  inline double GetHorn2RadialRescale() {return fHorn2LongRescale;}  
  inline void SetHorn2RadialRescale(double r) {fHorn2RadialRescale = r;}  
  inline void SetHorn2RadialRescaleCst(double r) {fHorn2RadialRescaleCst = r;}  
  inline double GetHorn2LongPosition() const { return fHorn2LongPosition; }
  inline void SetHorn2LongPosition(double l) { fHorn2LongPosition = l; }

  inline bool GetUseMarsTargetHorns() const { return fUseMarsTargetHorns;} 
  inline void SetUseMarsTargetHorns( bool v) { fUseMarsTargetHorns=v;}

  inline G4String GetMarsTargetHornsGDMLFilename() const { return fMarsTargetHornsGDMLFilename; }
  inline void SetMarsTargetHornsGDMLFilename(G4String &name) { fMarsTargetHornsGDMLFilename=name; }

  inline G4String GetAbsorberGDMLFilename() const { return fAbsorberGDMLFilename; }
  inline void SetAbsorberGDMLFilename(G4String &name) { fAbsorberGDMLFilename=name; }
//
// more public getter to support the Magnetic field 
//
  inline double GetHorn1NeckZPosition() const { return fHorn1NeckZPosition; } // in Drawing coordinate system 
  inline double GetHorn1NeckLength() const { return fHorn1NeckLength; } //  ... but rescaled is asked for..
  inline double GetHorn1NeckOuterRadius() const { return fHorn1NeckOuterRadius; } // same coordinate system.. 
  inline double GetHorn1NeckInnerRadius() const { return fHorn1NeckInnerRadius; } // same coordinate system.. 
  inline double GetHorn1ZDEndNeckRegion() const { return fHorn1ZDEndNeckRegion; } // same coordinate system.. 
  inline double GetHorn1ZEndIC() const { return fHorn1ZEndIC; } // the Z end of the inner conductor, rescaled..
  inline double GetHorn1EffectiveLength() const { return  (fHorn1TopUpstrLength + fHorn1TopDownstrLength); }
  inline double GetHorn1DeltaZEntranceToZOrigin() const { return -fHorn1LongRescale*3.0*CLHEP::cm;} // To be checked!...
  inline double GetHorn1OuterTubeOuterRad() const {return  fHorn1OuterTubeOuterRad; }
  inline double GetHornsOuterTubeOuterRad(size_t iH) const {return  fHornsOuterTubeOuterRad[iH]; }
     //Same thing for Horn2 
  inline double GetHorn2NeckZPosition() const { return fHorn2NeckZPosition; } // in Drawing coordinate system 
  inline double GetHorn2NeckLength() const { return fHorn2NeckLength; } //  ... but rescaled is asked for..
  inline double GetHorn2NeckOuterRadius() const { return fHorn2NeckOuterRadius; } // same coordinate system.. 
  inline double GetHorn2NeckInnerRadius() const { return fHorn2NeckInnerRadius; } // same coordinate system.. 
  inline double GetHorn2ZEndIC() const { return fHorn2ZEndIC; } // the Z end of the inner conductor, rescaled..
  inline double GetHorn2ZEqnChange(size_t k) const {return fHorn2ZEqnChanges[k]; } 
  inline size_t GetHorn2ZEqnChangeNumEqn() const {return fHorn2ZEqnChanges.size(); } 
  inline double GetHorn2DeltaZEntranceToZOrigin() const {return fHorn2DeltaZEntranceToZOrigin; } 
  inline double GetHorn2OuterTubeOuterRad() const {return  fHorn2OuterTubeOuterRad; }
  inline bool GetDoInstallShield() const { return fDoInstallShield;} 
  inline void SetDoInstallShield( bool v) { fDoInstallShield=v;}
  void SegmentTarget(); // Check the target segmentation. Assume fixed Fin size. 
//      
  void RescaleHorn1Lengthwise();
  void RescaleHorn2Lengthwise();
  void RescaleHorn1Radially();
  void RescaleHorn2Radially();
  void ShiftHorn2Radially();
  // Custom stuff for the upstream part of Horn1 
  //
  // Custom Horn1, based on a G4Polycone
  //
  bool fUseHorn1Polycone; 
  bool fUseHornsPolycone; // Extension to arbitrary number of Horns
  int fUseHorn1PolyNumInnerPts; 
  std::vector<G4ThreeVector> fHorn1PolyListRinThickZVects;
  double fHorn1PolyOuterRadius;
  //
  // Additional Custom Horns, based on a G4Polycone  up to 5.  (set by the G4UI limits) 
  // August 2015. 
  //
  int fUseNumberOfHornsPoly; 
  std::vector<int> fUseHornsPolyNumInnerPts; 
  std::vector< std::vector<G4ThreeVector> > fHornsPolyListRinThickZVects;
  std::vector<double> fHornsPolyOuterRadius;
  std::vector<double> fHornsPolyZStartPos;
  //
  std::vector<bool>  fPolyconeHornsAreParabolic;  // October 2015: LoSecco, Horns can be parabolic.... 
  bool fPolyconeHorn1IsParabolic; // for consistency with the old Horn1 Optimization.
  //
  
  LBNEVolumePlacementData* CreateHorn1TopLevelUpstr();
  
  const LBNEVolumePlacementData* Find(const G4String &name, const char *motherName, const char *method) const ;
  
  inline double GetConductorRadiusHorn1(double zD, size_t eqn) const {
     if (eqn >= fHorn1Equations.size()) return -1.;
     return fHorn1Equations[eqn].GetVal(zD);
  }
   inline double GetConductorRadiusHorn2(double zD, size_t eqn) const {
     if (eqn >= fHorn2Equations.size()) return -1.;
     return fHorn2Equations[eqn].GetVal(zD);
  }
//
// 1.2 MW, Feb 2014
// 
  inline bool GetUse1p2MW() const { return fUse1p2MW; }
  inline void SetUse1p2MW(bool t) { fUse1p2MW = t; }
//
  inline bool GetUseRoundedTargetFins() const { return fUseRoundedTargetFins; }
  inline void SetUseRoundedTargetFins(bool t) { fUseRoundedTargetFins = t; }
  void adaptTargetFor1p2MW();
//
// August 2014, add arbitrary shaped Polycone. 
// 
  inline bool GetUseHorn1Polycone() const { return fUseHorn1Polycone; }
  inline int GetUseHorn1PolyNumInnerPts() const { return fUseHorn1PolyNumInnerPts; }
  inline double GetHorn1PolyInnerConductorRadius(size_t numPts) const {
      if (numPts >= static_cast<size_t>(fUseHorn1PolyNumInnerPts)) {
        G4Exception("GetHorn1PolyInnerConductorRadius",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  fHorn1PolyListRinThickZVects[numPts];
      return v[0];
  }
  inline double GetHorn1PolyInnerConductorThickness(size_t numPts) const {
      if (numPts >= static_cast<size_t>(fUseHorn1PolyNumInnerPts)) {
        G4Exception("GetHorn1PolyInnerConductorThickness",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  fHorn1PolyListRinThickZVects[numPts];
      return v[1];
  }
  inline double GetHorn1PolyInnerConductorZCoord(size_t numPts) const {
      if (numPts >= static_cast<size_t>(fUseHorn1PolyNumInnerPts)) {
        G4Exception("GetHorn1PolyInnerConductorZCoord",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  fHorn1PolyListRinThickZVects[numPts];
      return v[2];
  }
  inline double GetHorn1PolyOuterRadius() const { return fHorn1PolyOuterRadius;}
  //
  //Settors 
  //
  inline void SetUseHorn1Polycone(bool t) {
      
     fUseHorn1Polycone = t; 
     if (!t) return;
     fUseHornsPolycone = false; // not a typo, indicating new mode for defining the geometry. 
                              // we support the idea to have one and only horn, simple Polycone.
     fUseNumberOfHornsPoly = 1;
     fMotherHornsAllLengths.resize(1); 
     fMotherHornsAllRads.resize(1);
     fUseHornsPolyNumInnerPts.resize(1);
     fHornsPolyOuterRadius.resize(1);
     fHornsPolyListRinThickZVects.resize(1);
     fPolyconeHornsAreParabolic.resize(1); fPolyconeHornsAreParabolic[0] = false;
     fMotherHornsAllThick.resize(1);
     fHornsLength.resize(1);
     fHornsPolyZStartPos.resize(1); 
     fHornsPolyZStartPos[0] = 0.; // defining here the G4 world coordinate system with respect to the Horns
     fHornsOuterTubeOuterRad.resize(1);
     fRemoveDecayPipeSnout = true; 
  }
  inline void SetUseHorn1PolyNumInnerPts(int n)  { 
     fUseHorn1PolyNumInnerPts = n; 
     fHorn1PolyListRinThickZVects.resize(static_cast<size_t>(n));
  }
  inline void SetHorn1PolyInnerThreeVect(size_t iP, G4ThreeVector vVal) {
      if (fHorn1PolyListRinThickZVects.size() < iP) {
        size_t numMiss = iP - fHorn1PolyListRinThickZVects.size();
        for (size_t ipp = 0; ipp != numMiss; ipp++) { 
          G4ThreeVector vNew; vNew[0] = 40.;  vNew[1] = 2; vNew[2] = 400.;
	  fHorn1PolyListRinThickZVects.push_back(vNew); 
        }
      }
      for (size_t kv=0; kv != 3; kv++) 
	   fHorn1PolyListRinThickZVects[iP][kv] = vVal[kv];
  }
  inline void SetHorn1PolyOuterRadius(double r) { fHorn1PolyOuterRadius = r;}
  //
  // August 2015: extend this to an arbitrary number of Polycone horns  
  //
  inline int GetNumberOfHornsPolycone() const { return fUseNumberOfHornsPoly; }
  inline int GetUseHornsPolyNumInnerPts(size_t i) const {
    if ((i < 1) || (i > fUseHornsPolyNumInnerPts.size())) {
       G4Exception("GetUseHornsPolyNumInnerPts",  " ", FatalErrorInArgument, "Illegal Horn number");
    }   
    return fUseHornsPolyNumInnerPts[i-1]; 
  }
  inline double GetHornsPolyInnerConductorRadius(size_t iH, size_t numPts) const {
      if ((iH < 1) || (iH > fUseHornsPolyNumInnerPts.size())) {
       G4Exception("GetHornsPolyInnerConductorRadius",  " ", FatalErrorInArgument, "Illegal Horn number");
      }   
      std::vector<G4ThreeVector> data = fHornsPolyListRinThickZVects[(iH-1)];
      if (numPts >= data.size()) {
        G4Exception("GetHornsPolyInnerConductorRadius",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  data[numPts];
      return v[0];
  }
  inline double GetHornsPolyInnerConductorThickness(size_t iH, size_t numPts) const {
      if ((iH < 1) || (iH > fUseHornsPolyNumInnerPts.size())) {
       G4Exception("GetHornsPolyInnerConductorThickness",  " ", FatalErrorInArgument, "Illegal Horn number");
      }   
      std::vector<G4ThreeVector> data = fHornsPolyListRinThickZVects[(iH-1)];
      if (numPts >= data.size()) {
        G4Exception("GetHornsPolyInnerConductorThickness",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  data[numPts];
      return v[1];
  }
  inline double GetHornsPolyInnerConductorZCoord(size_t iH, size_t numPts) const {
     if ((iH < 1) || (iH > fUseHornsPolyNumInnerPts.size())) {
       G4Exception("GetHorn2PolyInnerConductorZCoord",  " ", FatalErrorInArgument, "Illegal Horn number");
      }   
      std::vector<G4ThreeVector> data = fHornsPolyListRinThickZVects[(iH-1)];
      if (numPts >= data.size()) {
        G4Exception("GetHornsPolyInnerConductorZCoord",  " ", FatalErrorInArgument, "Bad Index");
      }
      G4ThreeVector v =  data[numPts];
      return v[2];
  }
  inline double GetHornsPolyOuterRadius(size_t iH) const {
     if ((iH < 1) || (iH > fHornsPolyOuterRadius.size())) {
       std::cerr << " GetHornsPolyOuterRadius, iH = " << iH <<  " size of container.. " << fHornsPolyOuterRadius.size() << std::endl;
       G4Exception("GetHornsPolyOuterRadius",  " ", FatalErrorInArgument, "Illegal Horn number");
      }   
     
    return fHornsPolyOuterRadius[(iH-1)];
  }
  inline double GetHornsPolyZStartPos(size_t iH) const {
     if ((iH < 1) || (iH > fHornsPolyZStartPos.size())) {
       G4Exception("GetHornsPolyZStartPos",  " ", FatalErrorInArgument, "Illegal Horn number");
      }   
     
    return fHornsPolyZStartPos[(iH-1)];
  }
  //
  // Parabolic horns, as ( very ) segmented Polycone
  //
  inline bool IsHornPnParabolic(size_t numHorn) const {
    return fPolyconeHornsAreParabolic[numHorn-1]; // same convention as the other accessors. 
  }
  inline bool IsHorn1PlParabolic() const {
    return fPolyconeHorn1IsParabolic;
  }
  inline size_t GetHornParabolicNumInnerPts(size_t iH) const { return fMotherHornsAllLengths[iH].size();}
  inline double GetHornParabolicRZCoord(size_t iH, size_t k) const { return fMotherHornsAllLengths[iH][k];}
  inline double GetHornParabolicRIn(size_t iH, size_t k) const { return fMotherHornsAllRads[iH][k];}
  inline double GetHornParabolicThick(size_t iH, size_t k) const { return fMotherHornsAllThick[iH][k];}
  
  //
  //Settors 
  //
  inline void SetNumberOfHornsPolycone(int n) {
    if ((n < 1) || (n > 5)) {
         G4Exception("SetNumberOfHornsPolycone",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
    }
    if (n == 1) {
      std::cout << " We will be using one and only one simple Horn, Polycone style.  Assume it is Horn1 " << std::endl;
     std::cerr << " We will be using one and only one simple Horn, Polycone style.  Assume it is Horn1 " << std::endl;
    }
    fUseHorn1Polycone = false; // we will use the generic Placement even for Horn1, more consistent this way
    fUseHornsPolycone = true; // not a typo, indicating new mode for defining the geometry. 
                              // we support the idea to have one and only horn, simple Polycone.
    fUseNumberOfHornsPoly = n;
    fMotherHornsAllLengths.resize(n); 
    fMotherHornsAllRads.resize(n);
    fUseHornsPolyNumInnerPts.resize(n);
    fHornsPolyOuterRadius.resize(n);
    fHornsPolyListRinThickZVects.resize(n);
    fHornsLength.resize(n);
    fHornsPolyZStartPos.resize(n); 
    fHornsPolyZStartPos[0] = 0.; // defining here the G4 world coordinate system with respect to the Horns
    if (n > 1) { 
      for (size_t k=1; k != static_cast<size_t>(n); k++) { 
         fHornsPolyZStartPos[k] = -10000.*CLHEP::m; fHornsLength[k] = -1.;
       }
    } // used to check the initialization.
    fHornsOuterTubeOuterRad.resize(n);
    fPolyconeHornsAreParabolic.resize(n); 
    fMotherHornsAllThick.resize(n);
    for (size_t k=0; k != static_cast<size_t>(n); k++) fPolyconeHornsAreParabolic[k] = false; //default is false.. 
    fRemoveDecayPipeSnout = true; 
  }
  inline void SetPolyconeHornParabolic(size_t hornNumber, bool t) {
     if (hornNumber > 5) {
       G4Exception("SetPolyconeHornParabolic",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
     }
     if (fUseNumberOfHornsPoly < static_cast<int>(hornNumber)) 
       this->SetNumberOfHornsPolycone(hornNumber);
     std::cerr << " Setting index " <<  hornNumber-1 
                 << " Hornpara.., size  " << fPolyconeHornsAreParabolic.size() << std::endl;
     fPolyconeHornsAreParabolic[hornNumber-1] = t;
  }
  inline void SetPolyconeHorn1Parabolic( bool t) {
    fPolyconeHorn1IsParabolic = t;
  }
  inline void SetUseHornsPolyNumInnerPts(size_t hornNumber, size_t nElem)  { 
     if (hornNumber > 5) {
       G4Exception("SetUseHornsPolyNumInnerPts",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
     }
     fUseHornsPolyNumInnerPts[hornNumber-1] = nElem; 
     fHornsPolyListRinThickZVects[hornNumber-1].resize(nElem);
     fMotherHornsAllLengths[hornNumber-1].resize(nElem);
     fMotherHornsAllRads[hornNumber-1].resize(nElem);
  }
  inline void SetHornsPolyInnerThreeVect(size_t hornNumber, size_t iP, G4ThreeVector vVal) {
     if (hornNumber > 5) {
       G4Exception("SetUseHornsPolyInnerThreeVect",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
     } 
     if (fHornsPolyListRinThickZVects[hornNumber-1].size() < iP) {
        size_t numMiss = iP - fHornsPolyListRinThickZVects[hornNumber-1].size();
        for (size_t ipp = 0; ipp != numMiss; ipp++) { 
          G4ThreeVector vNew; vNew[0] = 40.;  vNew[1] = 2; vNew[2] = 400.;
	  fHornsPolyListRinThickZVects[hornNumber-1].push_back(vNew); 
        }
      }
      for (size_t kv=0; kv != 3; kv++) 
	   fHornsPolyListRinThickZVects[hornNumber-1][iP][kv] = vVal[kv];
  }
  inline void SetHornsPolyOuterRadius(size_t hornNumber, double r) {
     if (hornNumber > 5) {
       G4Exception("SetUseHornsPolyOuterRadius",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
     } 
     fHornsPolyOuterRadius[hornNumber-1] = r;
  }
  inline void SetHornsPolyZStartPos(size_t hornNumber, double z) {
     if (hornNumber > 5) {
       G4Exception("SetUseHornsPolyZStartPos",  " ", FatalErrorInArgument, "Valid Number are 1, 2, 3, 4 or 5 ");
     } 
     if ((hornNumber == 1) && (std::abs(z) > 5.0*CLHEP::mm)) {
       std::ostringstream mStrStr; mStrStr << " The Z start of the 1rst simple Horn must be at zero. Found value : " << z;
       std::string mStr(mStrStr.str()); 
       G4Exception("SetUseHornsPolyZStartPos",  " ", FatalErrorInArgument, mStr.c_str());
     } 
     
     fHornsPolyZStartPos[hornNumber-1] = z;
     
  }
  
private:
  // GUI Interface  
  
  // Messenger
  G4UImessenger *fPlacementMessenger;

  // maps, to map the name of the detector to its placement, rotation, and the
  // logical volume in which it is placed.
  
  std::map<G4String, LBNEVolumePlacementData> fSubVolumes;
  //
  // A rotation matrix we will use often, to implement the round corners  
  //
  G4RotationMatrix fRotVertical;

  // Useful lengths/dimensions, gotten from the messenger class, or set internally.  

  G4double fTotalLength;
  G4double fLengthOfRockDownstr;
  G4double fTargetHallZ;
  G4double fAbsorberHallZ;
  G4double fDecayHallZ;
  G4double fDecayPipeLength; 
  G4double fDecayPipeLengthCorrection;
  G4double fDecayPipeRadius;
  G4double fDecayPipeUpstrWindowThick;
  G4double fDecayPipeWallThick;
  G4double fDecayPipeLongPosition; 
  G4String fDecayPipeGas;
  
  G4double fWaterLayerThickInHorns;
  G4double fHorn1Length; 
  std::vector<double> fHornsLength; // Aug. 2015.  Extensions to multiple Simple Polycone Horns 
  G4double fTargetAndBaffleLengthApprox; 
  G4double fTargetAndPlugLengthApprox;
  G4double fHorn1DownstreamPlateLength; // Bulkhead, guess work at this point 
  //
  G4double fDistMCZeroToACTRN1Pts; // See drawing 8875.112-MD-363097, with Jim Hylen annotation. 
  // 
  G4double fBaffleInnerRadius;
  G4double fBaffleOuterRadius;
  G4double fBaffleLength;
  G4double fBaffleWindowThickness;
  G4double fBaffleZPosition; // With respect to 0. of TargetHallAndHorn1 center (MCZERO) 
  G4double fPlugInnerRadius;
  G4double fPlugOuterRadius;
  G4double fPlugLength;
  G4double fPlugZPosition; // With respect to 0. of Horn2 center (MCZERO) 
  std::string fPlugMaterial;
  //
  // Target description.  Variable name 
  // borrowed (inspired, actually) from LBNENumiDataInput, when applicable. 
  //
  G4double fTargetSLength; // total length from first fin Beryllium window end cap. 
  G4double fTargetSLengthGraphite;// total length for the graphite target per-se. **Settable via Messenger. 
  G4double fTargetDensity; // Density of the target
  G4double fTargetSpecificLambda; // Specific nuclear interaction length of the target material
  G4String fTargetMaterialName;
  G4double fTargetLengthIntoHorn;
                       // offset with respecto MCZERO, and/or the transition between UpstreamTarget Hall and Horn1 Hall
		       // **Settable via Messenger. 
  G4double fTargetLengthOutsideHorn; // defined from the above.
  G4double fTargetLengthOutsideExtra; // Only for the Pseudo Nova option 
  G4double fTargetSLengthDownstrEnd;
  G4double fTargetZOffsetStart; 
  // No fTargetX0, Y0:  These would be surveyed point, part of the alignement 
  // realm, i.e. handled in the Surveyor class.
  G4double fTargetFinWidth;
  G4double fTargetFinWidthRequired;
  G4double fTargetFinContainerWidth;
  G4double fTargetFinExtraWidth;
  G4double fTargetFinHeight;
  G4double fTargetFinLength; // 
  G4int fTargetNumFins;
  G4int fTargetNumFinsUpstr;
  G4int fTargetNumFinsInHorn;
  // Quynh, August 2014. 
  G4int fTargetNumSphere;
  G4double fTargetFinLengthSplitUpstr; // 
  G4double fTargetFinLengthSplitDwnstr; // 
  G4double fTargetFinSpacingLength; // Averaged over the whole length
  G4double fTargetUpstrUpstrMargin; 
  G4double fTargetUpstrDwnstrMargin; 
  G4String fTargetUpstrPlateMaterial;
  G4double fTargetUpstrPlateHoleRadius;
  G4double fTargetUpstrPlateOuterRadius;
  G4double fTargetUpstrPlateThick;
  G4String fTargetCanMaterial;
  G4double fTargetCanInnerRadius;
  G4double fTargetCanOuterRadius;
  G4double fTargetCanLength;
  G4String fTargetDownstrCanFlangeMaterial;
  G4double fTargetDownstrCanFlangeOuterRadius;
  G4double fTargetDownstrCanFlangeInnerRadius;
  G4double fTargetDownstrCanFlangeThick;
  G4String fTargetFlangeMaterial;
  G4double fTargetFlangeInnerRadius;
  G4double fTargetFlangeOuterRadius;
  G4double fTargetFlangeThick;
  G4String fTargetCTubeMaterial;
  G4double fTargetCTubeOuterRadius;
  G4double fTargetCTubeInnerRadius;
  G4double fTargetCTubeUpstrLength;
  G4double fTargetCTubeUpstrOffset;
  G4double fTargetCTubeUpstrRadCurve;
  G4double fTargetCTubeReturnLengthUpstr;
  G4int fTargetCTubeUpstrNumSegCurve;
  G4double fTargetCTubeReturnHOffset;
  G4double fTargetCTubeReturnLengthDownstr;// Obsolete.but keep for possible upgrade if need 
  // Replaced by an effective thicness Titanium + water effective thickness
  G4double fTargetCTubeReturnDownstrCutAngleStart; // The angle at which we start, from 90 dgree (x=0., y ~ 16.5 mm) 
  G4double fTargetCTubeReturnDownstrCutAngleSize; // The angular size of the wedge representing the volume
  G4double fTargetCTubeReturnDownstrRadInner;
  G4double fTargetCTubeReturnDownstrRadOuter;
  G4double fTargetCTubeReturnDownstrThickTitanium;
  G4double fTargetCTubeReturnDownstrThickWater;
  // 
  G4double fTargetCTubeReturnLengthUstr;
  G4double fTargetCTubeReturnLengthUpstrEnd;
  G4double fTargetDistFlangeToTargetStart;
  G4double fTargetAlignRingThick;
  G4double fTargetAlignRingInnerRadius;
  G4double fTargetAlignRingOuterRadius;
  G4double fTargetAlignRingCutAngle;
  G4String fTargetAlignRingMaterial;
  G4double fTargetAlignRingSpacing;
  G4int fMaxNumAlignRings;
  G4double fTargetBerylDownstrWindowThick;
  G4double fTargetBerylDownstrWindowRadius;
  G4double fTargetBerylUpstrWindowThick;
  G4double fTargetBerylUpstrWindowRadius;
  G4double fTargetHeContTubeThickness;
  G4double fTargetHeContTubeInnerRadius;
  G4double fTargetHeContTubeLengthUpstr;
  G4double fTargetHeContTubeLengthInHorn;
  G4double fTargetHeContTubeLength; // New August/Sept 2014. 
  //
  // derived quantities
  G4double fTargetZ0; // offset with respecto MCZERO 
  G4double fTargetZ0Upstr; // For the two sections of the target. 
  G4double fTargetZ0Downstr; // For the two sections of the target. 
  G4double fTargetSWidth;
  G4double fTargetSHeight;
  //
  // Variables that are part physicall part of Horn1 but located logically in G4, in UpstreamTargetAssembly. 
  // We decompose the Inner to Outer conductor transition into 5 section of cones. 
  //
  std::vector<G4double> fTargetHorn1InnerRadsUpstr;
  std::vector<G4double> fTargetHorn1InnerRadsDownstr;
  std::vector<G4double> fTargetHorn1TransThick;
  std::vector<G4double> fTargetHorn1Lengths;
  std::vector<G4double> fTargetHorn1ZPositions;
  //
  // December 2013: Request to support 1.2 MW target option.  1rst step is to provide 
  // a simple target, box and/or cylinder 
  //
  G4bool fUseSimpleTargetBox;  
  G4bool fConstructPlug; 
  G4bool fConstructPlugInHorn1; 
  G4bool fUseSimpleTargetCylinder;  
  G4double fSimpleTargetLength;
  G4double fSimpleTargetRadius; // not used if box. 
  G4double fSimpleTargetHeight; // not used if cylinder 
  G4double fSimpleTargetWidth; // not used if cylinder 
  G4double fTotalLengthUpstreamAssembly;
  G4double fTotalLengthTargetAndHorn1Hall;
//Quynh, August 2014 
  G4bool fUseMultiSphereTarget;  
  G4double fMultiSphereTargetLength; 
  G4double fMultiSphereTargetRadius;
// October 2015, 
//
  bool fRemoveTargetAlltogether;
  //
  // Horn1, excluding the target elements in side the Horn1. 
  //
  bool fFillHornsWithArgon; // Introduced on June 18 2015. default is false, 
                           // for sake of consistency with LBNF CD1-refresh
			   // Likely to change to true, or we will fill with Helium.. 
  G4int fHorn1RadialRescaleCnt; // A counter to avoid calling the Rescale method twice in a give job .
                                // Amit B., Paul L. March 2015. 
  G4int fHorn1LongRescaleCnt;				
  //
  // First, a rescaling factor with respect to current (NUMI based) design
  //
  G4double fHorn1RadialRescale;
  G4double fHorn1LongRescale;
  G4double fHorn1RadialSafetyMargin;
  G4double fHorn1RMinAllBG; // 1.2 MW upgrade, insterting the target fully into the horn. 
  G4double fHorn1RMaxAllBG; // 1.2 MW upgrade, insterting the target fully into the horn. 
  bool fHorn1RadiusBigEnough; // 1p2 MW design and optimization: set the inner conductor radius sufficiently large such that 
  // it does not interfere with the Target helium Container tube. 
  
  G4double fHorn1IOTransLength; // Transition Inner to Outer conductors. A container volume (TUBS)
  G4double fHorn1IOTransInnerRad; // Surveyable!. But physically attachached to the top level section. 
  G4double fHorn1IOTransOuterRad;
  
  std::vector<G4double> fHorn1UpstrInnerRadsUpstr;
  std::vector<G4double> fHorn1UpstrInnerRadsDownstr; 
  std::vector<G4double> fHorn1UpstrInnerRadsOuterUpstr;
  std::vector<G4double> fHorn1UpstrInnerRadsOuterDownstr; 
  std::vector<G4double> fHorn1UpstrLengths;
  std::vector<G4double> fHorn1UpstrZPositions;

  std::vector<G4double> fHorn1UpstrOuterIOTransInnerRads; 
  std::vector<G4double> fHorn1UpstrOuterIOTransThicks; 
  std::vector<G4double> fHorn1UpstrOuterIOTransLengths; 
  std::vector<G4double> fHorn1UpstrOuterIOTransPositions; 

  G4double fHorn1TopUpstrLength; // Upstream part of the inner conductor, container volume (TUBS), Surveyed. 
  G4double fHorn1TopUpstrInnerRad; // This volume envelopes the target. 
  G4double fHorn1TopUpstrOuterRad;
  G4double fZHorn1ACRNT1Shift;  // Z coordinate shift from the beginning of Horn1TopLevelUpstr and Z=0., drawing coordinate. 
  
  G4double fHorn1TopDownstrLength; // Do part of the inner conductor, container volume (TUBS), Surveyed. 
  G4double fHorn1TopDownstrOuterRad;
  G4double fHorn1NeckLength;
  G4double fHorn1NeckOuterRadius;
  G4double fHorn1NeckInnerRadius;
  G4double fHorn1ZDEndNeckRegion;
  G4double fHorn1NeckZPosition; // from the start of Horn1TopLevelUpstr
  G4double fHorn1ZEndIC; // Z coordinate of the end of the inner conductor, rescaled is need be.
                                              
  // The outer tube
  
  G4double fHorn1OuterTubeInnerRad; 
  G4double fHorn1OuterTubeOuterRad; 
  std::vector<double> fHornsOuterTubeOuterRad;
  G4double fHorn1OuterConnectorRad;  // rescaled radially as well. 
  G4double fHorn1OuterConnectorThick;
  G4double fHorn1OuterConnectorLength;
  G4double fHorn1OuterConnectorPosition;

  G4double fHorn1InnerConnectorRad; // dwonstream end connectors 
  G4double fHorn1InnerConnectorThick;
  G4double fHorn1InnerConnectorLength;
  G4double fHorn1InnerConnectorPosition;
  
  G4String fHorn1InnerCondMat; // so that we can study the relevance of thinning the conductor. 
  G4String fHorn1AllCondMat; // so that we can study the relevance of thinning the conductor. 
  
  std::vector<LBNEHornRadialEquation> fHorn1Equations;
  //
  // For the Nova-like  option, different volume hierarchy. 
  // 
  G4VPhysicalVolume* fTargetHorn1HallPhysPtr;
  G4PVPlacement* fHorn1HallPhysPtr;
  //
  // ==========================================================
  // 
  // Horn2 
  //
  G4int fHorn2RadialRescaleCnt; // A counter to avoid calling the Rescale method twice in a give job .
                                // Amit B., Paul L. March 2015. 
  G4int fHorn2LongRescaleCnt;				
  G4double fHorn2RadialRescale;
  G4double fHorn2RadialRescaleCst;  // Added Dec. 16 2014 
  G4double fHorn2LongRescale;
  G4double fHorn2LongPosition; // with data cards... 
  //
  G4double fHorn2Length; // Top level volume, surveyable. and rescal-able. 
  G4double fHorn2LengthNominal; // As found on drawing 8875.112-MD 363382, Revision E 
  G4double fHorn2Radius;
  G4double fHorn2OffsetIOTr1; // The distance between Z=0 in Horn2 Drawing coordinate and 
                              // the most upstream z location of the Inner Outer transition piece
			      
  // We will subdivide Horn2 along the parts on Drawing 8875.112MD - 363383
  //  
  // Part 1 (index 0 in the array below) : Transition Inner to outer conductor . Drawing 363382
  // Part2.... Up to 7. 
  //
  std::vector<G4double> fHorn2PartsLengths; 
  std::vector<G4double> fHorn2PartsRadii;
  
  G4double fHorn2LengthMargin;   
  //
  // We approximate the upstream inner to outer transition (part 1) as a bunch tubs 
  //
  std::vector<G4double> fHorn2UpstrOuterIOTransRadsOne; 
  std::vector<G4double> fHorn2UpstrOuterIOTransRadsTwo; 
  std::vector<G4double> fHorn2UpstrOuterIOTransLengths; 
  std::vector<G4double> fHorn2UpstrOuterIOTransPositions; // with respect to the start of the mother volume
  //
  // And one mulit-sections cone section, whoe raddi are set the equations..  
  //
  G4double fHorn2InnerIOTransLength;
  
  std::vector<LBNEHornRadialEquation> fHorn2Equations;

  // The outer tube
  
  G4double fHorn2OuterTubeInnerRad; 
  G4double fHorn2OuterTubeOuterRad; 
  G4double fHorn2OuterTubeOuterRadMax; // include downstream flange
  
// To be used in the coordinate transform for computing the magnetic field 

  std::vector<G4double> fHorn2ZEqnChanges; // Z Coordinates (Drawing ) where the equation setting the radius changes. 
  G4double fHorn2NeckLength;
  G4double fHorn2NeckOuterRadius;
  G4double fHorn2NeckInnerRadius;
  G4double fHorn2NeckZPosition; // from the start of Horn1TopLevelUpstr
  G4double fHorn2ZEndIC; // Z coordinate of the end of the inner conductor, rescaled is need be.
  G4double fHorn2DeltaZEntranceToZOrigin;
//
  G4String fHorn2InnerCondMat; // so that we can study the relevance of thinning the conductor. 
  G4String fHorn2AllCondMat; // so that we can study the relevance of thinning the conductor. 
//
//  Ferburary 7-10 2015: The optimum geometry prefers a longer Horn2, and placed further downstresm
// Extend the chase, optionally, and reduce the decay pipe length  See method ExtendChaseForHorn2 
//
   G4double fExtendChaseLength;
//
// March 10 2015: Implement the magentic field inside the decay pipe.. 
//
 
   G4double fRadiusMilindWire;
   G4double fCurrentMilindWire;

  G4double fWriteGDMLFile;

//
// August Sept 2015:, post CD1-Refresh, LBNF era:
//
   G4double fChaseWidthForLBNF;   
   
//
// ===========================================
//
//
// Decay Pipe Snout Window ( if Helium chosen) 
//
  G4double fDecayPipeWindowZLocation; 
  G4double fDecayPipeWindowRadiusAlum;
  G4double fDecayPipeWindowRadiusBeryl;
  G4double fDecayPipeWindowThickBeryl; // Alreay defined see fDecayPipeUpstrWindowThick
  G4double fDecayPipeWindowThickAlum;
// 
// Other dimensions in PlaceFinalDecayPipeSnout
//
//
// 1.2 MW option, February 
//
   G4bool fUse1p2MW;
//
// Improve accuracy of volumes in target, rounded fins, March 2014
//
   G4bool fUseRoundedTargetFins;
// 
// July 2014: a logical to declare the target completely out of Horn1, similar to the NuMI Medium energy 
// setting currently used for Nova. Set when fTargetLengthOutsideHorn is < entire target length. 
//  If true, Valid only for the 1.2 MW option.  
//
   G4bool fUsePseudoNova;
//    
//
// Connectors and flange downstream used only once,  so we declare and rescale them as we go  
  
  bool fUseMarsTargetHorns; // a flag to turn on Mars GDML geometry for target and horns

  G4String fMarsTargetHornsGDMLFilename; 

  G4String fAbsorberGDMLFilename; 
//
// Steel shielding surrounding the horns 
//
  bool fDoInstallShield; // a flag to install or removing these shielding block. 
   
     
  // a flag to check the geometry as it is constructed. 
  
  bool fCheckVolumeOverLapWC;
  
  // Store the mother volume at top of volume hierarchy, for book-keeping/debugging purposes. 
  
  const G4LogicalVolume* fTopLogicalVolume;
  //
  // August 2014: Decide to install the complete Horn1 as a polycone volume to avoid having to split it
  // 
  // But, we'd like to keep the existing code as such, so, we start by placing a flag. 
  bool fHorn1MotherIsPolycone;
  double fArbitraryOffsetHystericalOne;
  std::vector<double> fMotherHorn1AllRads;
  std::vector<double> fMotherHorn1AllLengths; // [0] is the most upstream length. 
  std::vector<double> fMotherHorn1AllThick; 
// Extension to multiple Horns 
  std::vector< std::vector<double> > fMotherHornsAllRads;
  std::vector< std::vector<double> > fMotherHornsAllThick;
  std::vector< std::vector<double> > fMotherHornsAllLengths; // [0] is the most upstream length.  
//
 // October 2014: logical flag to build (or not) the decay Pipe snout, such that we have more room to place 
 // the Horns 
 bool fRemoveDecayPipeSnout;
  bool fUseGDMLFile;
  //
  // This method was found in G4PVPlacement. It is a clone, where we just skip 
  // the error messages.   
  bool CheckOverlaps(const G4PVPlacement *phys, G4int nres, G4double eps, G4bool verbose) const; 
  //
  // More intialization done here (too keep the constructor code of reasonable length.. )
  //
  void DeclareHorn1Dims(); 
  void CheckHorn1InnerConductAndTargetRadii(); // A check prior to the CheckOverlap of Geant4.  In principle, 
                                               // simpler and therefore more exact or reliable. 
					       // If fail, issues a fatal G4Exception. 
  int GetNumberOfInnerHornSubSections(size_t eqn, double z1, double z2, int nMax) const;
  int GetNumberOfInnerHorn2SubSections(size_t eqn, double z1, double z2, int nMax) const;
   
  void Horn1InstallSpiderHanger(const G4String &name, double zFromStartInnerConduct, double zPos,
                                G4PVPlacement *vMother );					       
  void Horn2InstallSpiderHanger(const G4String &name, G4PVPlacement *vMother );					       
  void Horn1PlaceAWeld(const G4String &name, double z, 
                                const LBNEVolumePlacementData *plInfo,  G4PVPlacement *vMother );
									       
  void DeclareHorn2Dims(); 
  // 
  // 1.2 MW upgrade.. 
  // 
  void PlaceFinalUpstrTarget1p2MW(G4PVPlacement *mother);
  void PlaceFinalDownstrTarget1p2MW(G4PVPlacement *mother);
// 
// Simple target implementation.. 
// 
  void PlaceFinalUpstrTargetSimpleCylinder(G4PVPlacement *mother);  
  void PlaceFinalUpstrTargetSimpleBox(G4PVPlacement *mother);  
  void declareTargetFinVertCorners(LBNEVolumePlacementData &info);
//Quynh add Sphere target choice----------------------------
  void PlaceFinalUpstrMultiSphereTarget(LBNEVolumePlacementData *plHelium, G4PVPlacement *mother);
  //
  void setMotherVolumeForHorn1(); // Simplification of geometry, August/Sept 2013.  
  void setEntireTargetDims(); // Simplification of geometry, August/Sept 2013.  
  void DumpAllHornsPolyconeParameters(); // for debugging... 
  public: 
  //
  // Transfer of the radial information about the field to the 1p2 MW new design of Horn1 
  //
  inline double GetHorn1RMinAllBG() const {return fHorn1RMinAllBG;} // Big radius of inner conductor, inner radius if 1p2 MW
  inline double GetHorn1RMaxAllBG() const {return fHorn1RMaxAllBG;} 
  //
  inline void SetUsePseudoNova(bool t) {fUsePseudoNova = t;}
  inline bool GetUsePseudoNova() const {return fUsePseudoNova;}
  inline void SetTargetHorn1HallPhysPtr(G4VPhysicalVolume *p) { fTargetHorn1HallPhysPtr = p;}
  inline void SetHorn1HallPhysPtr(G4PVPlacement *p) { fHorn1HallPhysPtr = p;}
  //
  inline bool GetHorn1MotherIsPolycone() const {return fHorn1MotherIsPolycone;}
  
// July-August 2014: Multi-sphere target (Quynh )

  inline void SetUseMultiSphereTarget(bool t) { fUseMultiSphereTarget = t;} 
  inline void SetMultiSphereTargetLength (double r) {fMultiSphereTargetLength = r;} //Quynh. real target.(2) - July 16 2014
  inline void SetMultiSphereTargetRadius (double r) {fMultiSphereTargetRadius = r;} //Quynh, P.L., August 2014 
  inline bool GetUseMultiSphereTarget() const { return fUseMultiSphereTarget;}
  inline double GetMultiSphereTargetRadius() const {return fMultiSphereTargetRadius;}
  void AdaptForMultiSphereTarget();
  void PlaceFinalMultiSphereTarget(LBNEVolumePlacementData *plHelium, G4PVPlacement *mother); 
   
  inline bool GetRemoveDecayPipeSnout() const {return fRemoveDecayPipeSnout;}
  inline void SetRemoveDecayPipeSnout(bool t) { fRemoveDecayPipeSnout = t;}
  
  inline double GetRadiusMilindWire() const {return fRadiusMilindWire;}
  inline void SetRadiusMilindWire(double r) { fRadiusMilindWire = r;}
  inline double GetCurrentMilindWire() const {return fCurrentMilindWire;}
  inline void SetCurrentMilindWire(double r) { fCurrentMilindWire = r;}

  inline bool GetWriteGDMLFile() const {return fWriteGDMLFile;}
  inline void SetWriteGDMLFile(bool t) { fWriteGDMLFile = t;}

  // August 2015.. 
  inline double GetChaseWidthForLBNF() const { return fChaseWidthForLBNF;}

  // John Back, March 2016, target module options, which includes the SAT
  bool fUseTargetModule;
  inline void SetUseTargetModule(bool t) {fUseTargetModule = t;}
  inline bool GetUseTargetModule() const { return fUseTargetModule;}

  // The target module type
  enum TargetType {SAT = 0, Cylinder = 1, TwoCylinder = 2};
  int fTargetModuleType;
  void SetTargetModuleType(int i) {fTargetModuleType = i;}
  inline int GetTargetModuleType() const {return fTargetModuleType;}

  // Initialise the geometrical parameters for the target module
  void InitTargetModule();

  // Create and place the target module
  void PlaceTargetModule();

  // Create the individual target module volumes
  LBNEVolumePlacementData* CreateTargetVol(const G4String &volName);

  // Place the outer canister of the target module
  void PlaceTargetOuterCan();

  // Place the inner canister of the target module
  void PlaceTargetInnerCan();

  // Place the inner support rods of the target module
  void PlaceTargetSupport();

  // Place the actual target, e.g. cylinder or spheres
  void PlaceTargetObject();

  // The radius of the target object
  G4double fTargetRadius;
  inline void SetTargetRadius (double r) {fTargetRadius = r;}
  inline double GetTargetRadius() const {return fTargetRadius;}

  // The length of the target object
  G4double fTargetLength;
  inline void SetTargetLength (double l) {fTargetLength = l;}
  inline double GetTargetLength() const {return fTargetLength;}    

  // The diameter thickness of the supporting rods holding the target object
  G4double fTargetSupportD;

  // The total length of the supporting rods
  G4double fTargetSupportL;

  // The thickness of the Ti casings
  G4double fTargetCaseT;
  // The He gas gap between the two Ti casing cylinders
  G4double fTargetHeGap;
  // The inner radius of the outer casing
  G4double fTargetOutCaseInnerR;

  // The integer number of target objects (cylinder = 1, spheres = N)
  int fNumTargetObjects;

  // The length of the inner canister holding the spheres
  G4double fTargetInCaseL;

  // The length of the upstream start of the inner target canister
  G4double fTargetInCaseUpL;

  // The length of the downstream end of the inner target canister
  G4double fTargetInCaseDnL;

  // The length separation between the downstream end of the inner target canister
  // and the downstream end of the cylindrical section of the outer target canister
  G4double fTargetCaseDiffL;

  // The length of the outer target canister
  G4double fTargetOutCaseL;

  // The total length of the target module, including the end bulb of the outer canister
  G4double fTargetModuleTotL;

  // The fractional length of the target module outside Horn
  G4double fTargetFracOutHornL;
  inline void SetTargetFracOutHornL(double l) {fTargetFracOutHornL = l;}
  inline G4double GetTargetFracOutHornL() const {return fTargetFracOutHornL;}

  // The number of interaction lengths required for the target object
  G4double fTargetNLambda;
  inline void SetTargetNLambda(double N) {fTargetNLambda = N;}
  inline G4double GetTargetNLambda() const {return fTargetNLambda;}

  // May need to set the extra outside length depending on Horn1 set-up, for example
  // if UseHorn1Polycone = false (default)
  inline void SetTargetLengthOutsideExtra(double l) {fTargetLengthOutsideExtra = l;}
  inline G4double GetTargetLengthOutsideExtra() const {return fTargetLengthOutsideExtra;}

  // Helper function to find the number of spheres in the SAT given 
  // the required number of interaction lengths, taking into account
  // the finite size of the Gaussian beam
  G4double FindNTargetSpheres() const;

};

#endif


//
// LBNEStackingAction.hh
//
#ifndef LBNEStackingAction_H
#define LBNEStackingAction_H 1

#include <fstream>
#include "globals.hh"
#include "G4UserStackingAction.hh"
//#include "G4ThreeVector.hh"

class G4Track;
class LBNERunManager;

class LBNEStackingAction : public G4UserStackingAction
{
  public:
    LBNEStackingAction();
    virtual ~LBNEStackingAction();

    virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
    virtual void NewStage();
    virtual void PrepareNewEvent();


   void KillEMParticles(G4ClassificationOfNewTrack& classification, 
			const G4Track* aTrack);
   void KillltZeroPzParticles(G4ClassificationOfNewTrack& classification, 
			      const G4Track * aTrack);
   void KillThresholdParticles(G4ClassificationOfNewTrack& classification, 
			       const G4Track * aTrack);
//   void KillOutOfWorldParticles(G4ClassificationOfNewTrack& classification, 
//				const G4Track * aTrack);
   void KillUnimportantParticles(G4ClassificationOfNewTrack& classification, 
				 const G4Track * aTrack);
   void UnKillMuonAlcoveParticles(G4ClassificationOfNewTrack& classification, 
				 const G4Track * aTrack);


  private:
   LBNERunManager * pRunManager;
   mutable double fStackingKillingThreshold; 
   // 
   // Debugging where the big deficit in pion production with respect to Fluka
   //
   mutable std::ofstream fOutHadronAtVectex; // close by defaults.. 
   mutable std::ofstream fOutHadronAtVectexCl; // close by defaults.. Classified... Suspect bug!...
   mutable bool doImportanceWeightSelection; // True by default, as decided by g4numi
  public:
    void OpenHadronAtVertex() const; 
    inline void SetDoImportanceWeightSelection( bool t) const { doImportanceWeightSelection = t; } // Pseudo Const.. 
    inline bool GetDoImportanceWeightSelection() const {return doImportanceWeightSelection; }
    inline void SetStackingKillingThreshold(double v) const { fStackingKillingThreshold = v;} //Pseudo const 
    inline double GetStackingKillingThreshold() const { return fStackingKillingThreshold;}
   
};

#endif

//
// LBNERunAction.hh
//

#ifndef LBNERunAction_h
#define LBNERunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "LBNETrajectory.hh"

class G4Run;
class LBNERunActionMessenger;

class LBNERunAction : public G4UserRunAction
{
public:
  LBNERunAction();
  ~LBNERunAction();

  void BeginOfRunAction(const G4Run*);
  void EndOfRunAction(const G4Run*);

   void CheckOKToRun();
  
   void CheckMemoryUsage(double VMLimit) const;
  
private:
   int fVerboseLevel;
   LBNERunActionMessenger* runMessenger;
   // Global information regarding energy deposition in the target.
   mutable bool fDoComputeEDepInGraphite;
   mutable bool fDoComputeEDepInArgonGas;
   mutable double fMeanEDepInGraphite;
   mutable double fRMSEDepInGraphite;
   mutable double fMeanEDepInArgonGasHorn1;
   mutable double fRMSEDepInArgonGasHorn1;
   mutable double fMeanEDepInArgonGasHorn2;
   mutable double fRMSEDepInArgonGasHorn2;
   
public:
   inline void SetDebugLevel(bool t) { fVerboseLevel = t; }  
   inline bool GetDebugLevel() const { return fVerboseLevel; }  
   inline void SetDoComputeEDepInGraphite(bool t) const { fDoComputeEDepInGraphite = t;}
   inline bool GetDoComputeEDepInGraphite() const {return fDoComputeEDepInGraphite;}
   inline void storeEDepInGraphite(double edep)  const {  /* not really... */
      fMeanEDepInGraphite += edep;
      fRMSEDepInGraphite += edep*edep;
   }
   inline void SetDoComputeEDepInArgonGas(bool t) const { fDoComputeEDepInArgonGas = t;}
   inline bool GetDoComputeEDepInArgonGas() const {return fDoComputeEDepInArgonGas;}
   inline void storeEDepInArgonGas(double edepH1, double edepH2)  const {  /* not really... */
      fMeanEDepInArgonGasHorn1 += edepH1;
      fRMSEDepInArgonGasHorn1 += edepH1*edepH1;
      fMeanEDepInArgonGasHorn2 += edepH2;
      fRMSEDepInArgonGasHorn2 += edepH2*edepH2;
   }
};

#endif


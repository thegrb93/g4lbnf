//---------------------------------------------------------------------------// 
// $Id: LBNEDetectorConstruction.hh,v 1.5.2.22 2013/12/24 20:30:22 lebrun Exp $
//---------------------------------------------------------------------------// 

#ifndef LBNEDetectorConstruction_H
#define LBNEDetectorConstruction_H 1

#include <vector>

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "LBNEVolumePlacements.hh"
#include "G4Element.hh"

class G4VSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4VisAttributes;

class LBNEMagneticField;
class LBNEMagneticFieldIC;
class LBNEMagneticFieldOC;

class LBNEDetectorConstruction : public G4VUserDetectorConstruction
{
public:
  
  LBNEDetectorConstruction();
  ~LBNEDetectorConstruction();
 
  void Initialize();

  G4VPhysicalVolume* Construct();
  
  void CheckOverlaps();
    
  void InitializeSubVolumes();
  void InitializeMaterials();
  void SetConstructTarget(bool val) { fConstructTarget = val; }
  void SetSimulationType(G4String val) { fSimulationType = val; }
  void SetBeamlineAngle(G4double angle) { fBeamlineAngle = angle; }
  inline G4double GetBeamlineAngle() const { return fBeamlineAngle; }
  G4LogicalVolume* GetPreMuonDetectorLogical(){return PreTrackingPlaneLogical;}
  G4LogicalVolume* GetMuonDetectorLogical(){ return TrackingPlaneLogical; }
  G4LogicalVolume* GetSecondMuonDetectorLogical(){return SecondTrackingPlaneLogical;}
  G4LogicalVolume* GetStoppedMuonDetector() {return StoppedMuonDetectorLogical;}
  G4LogicalVolume* GetHadronMonitorLogical(){ return hadronMonitorLogical; }  
  G4LogicalVolume* GetHorn1PlaneLogical(){ return TrackingPlaneH1Logical;}  //Amit Bashyal
  G4LogicalVolume* GetHorn2PlaneLogical(){return TrackingPlaneH2Logical;}  //Amit Bashyal
  G4LogicalVolume* GetDecayPipePlaneLogical(){return TrackingPlaneDPLogical;}  //Amit Bashyal
  bool HasBeenConstructed() const {return fHasBeenConstructed; }

  //Set boolean for simple geometry of Absorber
  void SetConstructSimpAbsorber(G4bool aBool) { fConstructSimpAbsorber = aBool; }
  //Set boolean for simple geometry of Absorber
  void SetConstructSculptedAbsorber(G4bool aBool) { fConstructSculptedAbsorber = aBool; }
  //Disable construction of the spoiler
  void SetDisableSpoiler(G4bool aBool){ fDisableSpoiler = aBool;}
  //Disable scalloping on aluminum layers
  void SetDisableSculptedLayers(G4bool aBool){ fDisableSculptedLayers = aBool;}

private:

   bool fHasBeenConstructed; // The construct method has been called (only once per run...) 

  // Geometric instantiations
  LBNEVolumePlacements*                  fPlacementHandler;
   
  // Horn current value
  G4double fHornCurrent;
  G4double fSkinDepthInnerRad; // For systematic uncertainties on the Horn1 current 
  std::vector<double> fDeltaEccentricityIO; // For systematic transverse shift of the Inner vs Outer Conductor. 
  std::vector<double> fDeltaEllipticityI; // For systematic due to deformation of Inner conductor.
  std::vector<double> fCurrentEqualizerLongAbsLength; // For systematic studies of imperfect current equaliztion sections.  
  std::vector<double> fCurrentEqualizerQuadAmpl; // 
  std::vector<double> fCurrentEqualizerOctAmpl; // 
  std::vector<double> fCurrentMultiplier; // 

  // Dimensions of (world-volume) pertinent features
  G4double fRockRadius;
  G4double fRockLength;
  G4double fRockX;
  G4double fRockY;
  G4double fRockZ;
  G4double fBeamlineAngle;
  G4double fDecayPipeWallThickness;
  G4double fTargetHallX;
  G4double fTargetHallY;
  G4double fTargetHallZ;
  G4double fDecayHallShieldingX;
  G4double fDecayHallShieldingY;
  G4double fDecayHallShieldingZ;
  G4double fDecayHallZ;
  G4double fAbsorberHallX;
  G4double fAbsorberHallY;
  G4double fAbsorberHallZ;
  // Flags that can be set by messenger
  bool fConstructTarget;
  G4String fSimulationType;
  bool fConstructSimpAbsorber;
  bool fConstructSculptedAbsorber;
  bool fDisableSculptedLayers;
  bool fDisableSpoiler;
  // Define steel, concrete downstream width of absorber and simple geometry variables
  G4double fDwStrAbsSteelWidth; 
  G4double fDwStrAbsConcreteWidth;
 
  void PrintDetectorGeometry();
  void PrintDetectorGeometry(const G4String &desc, 
           const G4String &name);
  void GetWorldTransformation(G4VPhysicalVolume *physvol,
            G4RotationMatrix &WorldRotation,
            G4ThreeVector    &WorldTranslation);
  void PrintSolidDescription(const G4VSolid *solidvol,
           const G4LogicalVolume *logvol,
           G4RotationMatrix &WorldRotation,
           G4ThreeVector    &WorldTranslation);
//
// New v3.x construction.
//	   
  void ConstructUpstreamTarget(G4PVPlacement *phys); 

//
//LBNE stuff

  void ConstructLBNEHadronAbsorberSimple(G4VPhysicalVolume* vPhys); //simpler absorber
  void ConstructLBNEHadronAbsorberSculpted(G4VPhysicalVolume* vPhys); //simpler absorber
  void ConstructLBNEHadronAbsorber(G4VPhysicalVolume* vPhys);
  void ConstructLBNEShieldingHorn1(G4VPhysicalVolume* vPhys); // And target 
  void ConstructLBNEShieldingHorn2(G4PVPlacement* vPhys);
  void ConstructLBNEShieldingBetweenHorns(G4VPhysicalVolume* tunnel);
  void ConstructLBNFShielding(G4VPhysicalVolume* vPhys); // And target 
  void LBNEDetermineTargetHallShieldingClosestApproach(G4int ii);
  void ConstructLBNEHorn1TrackingPlane(G4VPhysicalVolume* tunnel); //Amit Bashyal	
  void ConstructLBNEHorn2TrackingPlane(G4VPhysicalVolume* tunnel); //Amit Bashyal	
  void ConstructLBNEDecayPipeTrackingPlane(G4VPhysicalVolume* tunnel); //Amit Bashyal
  G4double fTgtHallShield_closest_yplus;
  G4double fTgtHallShield_closest_yminus;
  G4double fTgtHallShield_closest_xplus;
  G4double fTgtHallShield_closest_xminus;
  
  G4RotationMatrix fRotBeamlineAngle;

  void ConstructLBNETargetHall();
  void ConstructLBNETarget();

  void ConstructLBNEHorns();
  G4double fTrackingPlane_halfwidth;
  G4double fTrackingPlane_halfheight;
  G4double fTrackingPlane_X0;
  G4double fTrackingPlane_Y0;
  void ConstructLBNEHorn(G4int nhorn, G4int nparts, G4int jstart);
  G4double LBNEphornRgivenZ(G4double a, G4double b, G4double c, G4double z);
  
  void DropMarsTargetHorns(G4VPhysicalVolume *mother);
//
//


//
//Specifically Exact Numi Target
//This automatically constructs the NUMI baffle and budal monitor
   void ConstructNUMITarget(); //budal mon is constructed in this function
   void ConstructNUMIBaffle();
//
//

   ///use for testing gometry stuff
   void ConstructTesting();
  

   void InitializeMaterialsPostPreIdle();

 
//
  G4int GetMaterialCode(const G4String &matName);
  G4Material* GetMaterial(G4String matName);
  G4VisAttributes* GetMaterialVisAttrib(G4String matName);
  void DestroyMaterials();
  
  // Messenger
  G4UImessenger *fDetectorMessenger;
  // elements pointers
  G4Element *elC;
  G4Element *elN;
  G4Element *elO;
  
  // Materials
  G4Material* Vacuum;
  G4Material* DecayPipeVacuum;
  G4Material* NumiDecayPipeHelium;
  G4Material* Air;
  G4Material* Water;
  G4Material* He;
  G4Material* Beryllium;
  G4Material* C;
  G4Material* Al;
  G4Material* Ar;
  G4Material* Pb;
  G4Material* Fe;
  G4Material* CT852;
  G4Material* Steel316;
  G4Material* Titanium;
  G4Material* Concrete;
  G4Material* Shotcrete;
  G4Material* Rebar_Concrete;
  G4Material* Target;
  G4Material* HeliumTarget;
  G4Material* DolomiteRock;
  G4Material* DoloStone;
  G4Material* MaqShale;
  G4Material* Chert;
  G4Material* Pyrite;
  G4Material* MaqSiltstone;
  G4Material* var_Al;
  G4Material* var_Stl;
  G4Material* Slab_Stl;
  G4Material* Blu_Stl;
  G4Material* n1018_Stl;
  G4Material* A500_Stl;
  G4Material* M1018_Stl;
  G4Material* Alumina;
  G4Material* HeGas;
  G4Material* Drywall;
  G4Material* Paraffin;
  G4Material* graphiteBaffle;
  G4Material* DefaultMaterial;



  // Logical volumes
  //
  G4LogicalVolume* ROCK_log;
  G4LogicalVolume* TRGT_lv;
  // G4LogicalVolume* lvTUNE;
  G4LogicalVolume* BLK_log[100]; 
  G4LogicalVolume* CShld_log[16];
  G4LogicalVolume* TGAR_log;
  G4LogicalVolume* Horn_PM_lv[8];
  G4LogicalVolume* LVCPipe[20];
  G4LogicalVolume* LVCPipeW[20];
  G4LogicalVolume* HadrBox_log;
  G4LogicalVolume* ShldBox_log;
  G4LogicalVolume* PreTrackingPlaneLogical;
  G4LogicalVolume* TrackingPlaneLogical;
  //second tracking plane logical volume
  G4LogicalVolume* SecondTrackingPlaneLogical;
  //for Stopped Muon Detection
  G4LogicalVolume* StoppedMuonDetectorLogical;
  G4LogicalVolume* hadronMonitorLogical;
  G4LogicalVolume* TrackingPlaneH1Logical;
  G4LogicalVolume* TrackingPlaneH2Logical;
  G4LogicalVolume* TrackingPlaneDPLogical;
  // Physical volumes
  //
  G4VPhysicalVolume* fRock;
  G4VPhysicalVolume* fRvTUNE;
  G4VPhysicalVolume* TGAR;
  G4VPhysicalVolume* TRGT;
  G4VPhysicalVolume* PHORN[8];
  G4VPhysicalVolume* PVCPipe[20];
  G4VPhysicalVolume* CNT[20];
  G4VPhysicalVolume* HadrBox;
  G4VPhysicalVolume* ShldBox;
  
  //Solids
  //
  G4VSolid* BLK_solid[100];
  G4VSolid* CShld_solid[16];
  G4VSolid* Horn_PM[8];

 //
 // For the implementation of perfect focusing.  : The z coord where it is applied. 
 // Default value is very large (but a valid double precision) 
 //
 double fZCoordForPerfectFocusing; 
 mutable double fRCoordOutOfTarget;  // Mutable, because set as we are building the geometry.. 
   
 public:
 
  inline void SetHornCurrent(G4double val) { fHornCurrent = val; }
  inline double GetHornCurrent() const {return fHornCurrent; }
  inline void SetSkinDepthInnerRad(G4double val) { fSkinDepthInnerRad = val; }
  inline void SetDeltaEccentricityIO(size_t kH, G4double val) { if (kH < 3) fDeltaEccentricityIO[kH] = val;}
  inline void SetDeltaEllipticityI(size_t kH, G4double val) { if (kH < 3) fDeltaEllipticityI[kH] = val;}
  
  inline void SetZCoordForPerfectFocusing(double z) {fZCoordForPerfectFocusing = z;}
  inline double GetZCoordForPerfectFocusing() const {return fZCoordForPerfectFocusing;}
  inline void SetRCoordOutOfTarget(double r) const {fRCoordOutOfTarget = r;} // Pseudo Const, G4User inheritance too strict.. 
  inline double GetRCoordOutOfTarget() const {return fRCoordOutOfTarget;}

   // Define downstream width of concrete and steel for simple geometry of absorber
  inline void SetDwStrAbsConcreteWidth(G4double l) { fDwStrAbsConcreteWidth = l;}
  inline void SetDwStrAbsSteelWidth(G4double l) { fDwStrAbsSteelWidth = l;}
  
  inline double GetDwStrAbsConcreteWidth() const { return fDwStrAbsConcreteWidth;}
  inline double GetDwStrAbsSteelWidth() const { return fDwStrAbsSteelWidth;}

   inline void SetCurrentEqualizerLongAbsLength(size_t kH, double v) {if (kH < 3) fCurrentEqualizerLongAbsLength[kH] = v;}
   inline void SetCurrentEqualizerQuadAmpl(size_t kH, double v) {if (kH < 3) fCurrentEqualizerQuadAmpl[kH] = v;}
   inline void SetCurrentEqualizerOctAmpl(size_t kH, double v) {if (kH < 3) fCurrentEqualizerOctAmpl[kH] = v;}
   inline void SetCurrentMultipiler(size_t kH, double v) {if (kH < 3) fCurrentMultiplier[kH] = v;}
  
 private:
   void checkMaterialInHAGDML(const G4LogicalVolume *lVol) const;

};
#endif


//
// LBNEAnalysis.hh
//
// Modified Jul 2005 by A. MArino to make data_t and hadmmtuple_t classes

#ifndef LBNEANALYSIS_HH
#define LBNEANALYSIS_HH

#include "globals.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryPoint.hh"
#include "dk2nu/tree/dk2nu.h"
#include "dk2nu/tree/dkmeta.h"
//root
#include "TSystem.h"

//G4
#include "G4ios.hh"
#include "G4TrajectoryContainer.hh"

#include <map>

class G4ParticleDefinition;
class G4Step;
class TFile;
class TTree;
class G4Track;
class LBNEDataNtp_t;
class LBNEPrimaryGeneratorAction;
typedef std::vector<G4String>vstring_t; //A. Bashyal for ppfx...

const int kMaxP = 10000;
const int HkMaxP = 100000000;

class LBNEAnalysis
{
public:

   LBNEAnalysis();
   ~LBNEAnalysis();
   
   //void book();
   //void finish();
   G4bool CreateOutput();
   G4bool CreateDk2NuOutput();
   void CloseOutput();
   void CloseDk2NuOutput();
   void FillNeutrinoNtuple(const G4Track& track,const std::vector<G4VTrajectory*>& nuHistory);
   void FillTrackingNtuple(const G4Track& track, LBNETrajectory* currTrajectory);
   void TrackThroughGeometry(const LBNETrajectory* TrackTrajectory);      
   void fillDkMeta(); // From runAction, with input from PrimaryGenerator Action


   void FillAlcoveTrackingPlaneData(const G4Step& aStep);

   void FillTrackingPlaneData(const G4Step& aStep);  //---- for tracking planes
   void FillTrackingPlaneH1Data(const G4Step& aStep);
   void FillTrackingPlaneH2Data(const G4Step& aStep);
   void FillTrackingPlaneDPData(const G4Step& aStep);
   void FillTargetOutputData(const G4Step& aStep);

   LBNETrajectory* GetParentTrajectory(G4int parentID);
   LBNETrajectory* GetTrajectory(G4int trackID);
   static LBNEAnalysis* getInstance();
   
  void ResetEvent();  //---needed for tracking planes
   void FillEvent();
   void SetCount(G4int count);
   G4int GetCount();
   void SetEntry(G4int entry);
   G4int GetEntry();
   G4double GetDistanceInVolume(LBNETrajectory* wanted_traj,G4String wanted_vol); //For ppfx ...A. Bashyal
   
private:

   
private:
   static LBNEAnalysis* instance;
  
   bool fDk2NuDetectorFileRead; 
   
   G4double x;
   G4double y;
   G4double z;
  //----- variables to store the tracking plane info
  //  for each track it makes one entry per particle per tracking plane. There
  //  can also be multiple particles per event. 
   int fTrackID[kMaxP];
   int fParticlePDG[kMaxP];
   double fNImpWt[kMaxP];
   double fParticleX[kMaxP];
   double fParticleY[kMaxP];
   double fParticleZ[kMaxP];
   double fParticleEnergy[kMaxP];
   double fParticleMass[kMaxP];
   double fParticleDX[kMaxP];
   double fParticleDY[kMaxP];
   double fParticleDZ[kMaxP];
   int fNParticles;
// Variables to store the Horn 1 tracking plane info. Amit Bashyal
 int fH1TrackID;
   int fH1ParticlePDG;
   double fH1NImpWt;
   int fH1DParticlePDG;
   int fH1DTrackID;
   double fH1ParticleX;
   double fH1ParticleY;
   double fH1ParticleZ;
   double fH1ParticleEnergy;
   double fH1ParticleMass;
   double fH1ParticleDX;
   double fH1ParticleDY;
   double fH1ParticleDZ;
   double fH1ParticlePX;
   double fH1ParticlePY;
   double fH1ParticlePZ;
   double fH1ParticlePXPZ;
   double fH1ParticlePYPZ;
   double fH1PProductionX;
   double fH1PProductionY;
   double fH1PProductionZ;
   double fH1PProductionDX;
   double fH1PProductionDY;
   double fH1PProductionDZ;
   std::string fH1CProcess;
   G4ThreeVector NuMomentum;
   int fH1NParticles;
   
  //----- variables to store the tracking plane info in Horn2 Plane Amit Bashyal
  //  for each track it makes one entry per particle per tracking plane. There
  //  can also be multiple particles per event. 
   int fH2TrackID;
   int fH2ParticlePDG;
   double fH2NImpWt;
   double fH2ParticleX;
   double fH2ParticleY;
   double fH2ParticleZ;
   double fH2ParticleEnergy;
   double fH2ParticleMass;
   double fH2ParticleDX;
   double fH2ParticleDY;
   double fH2ParticleDZ;
   double fH2ParticlePX;
   double fH2ParticlePY;
   double fH2ParticlePZ;
   double fH2ParticlePXPZ;
   double fH2ParticlePYPZ;
   double fH2PProductionX;
   double fH2PProductionY;
   double fH2PProductionZ;
   double fH2PProductionDX;
   double fH2PProductionDY;
   double fH2PProductionDZ;
   G4String fH2CProcess;
   int fH2CNProcess;
   int fH2NParticles;

//Variables to be stored in the Decay Pipe Tracking Plane. All the Variables are
// not used as they are not useful for this study.  Amit Bashyal
  int fDPTrackID;
  int fDPParentID;
   int fDPParticlePDG;
   double fDPNImpWt;
   double fDPParticleX;
   double fDPParticleY;
   double fDPParticleZ;
   double fDPParticleEnergy;
   double fDPParticleMass;
   double fDPParticleDX;
   double fDPParticleDY;
   double fDPParticleDZ;
   double fDPParticlePX;
   double fDPParticlePY;
   double fDPParticlePZ;
   double fDPParticlePXPZ;
   double fDPParticlePYPZ;
   double fDPPProductionX;
   double fDPPProductionY;
   double fDPPProductionZ;
   double fDPPProductionDX;
   double fDPPProductionDY;
   double fDPPProductionDZ;
   int fDPNParticles;
   //Variables to Study the Particles coming out of Target Amit Bashyal
   
   int fTTrackID;
   int fTParentID;
   double fTParticleX;
   double fTParticleY;
   double fTParticleZ;
   int fTParticlePDG;
   double fTParticleEnergy;
   double fTParticlePX;
   double fTParticlePY;
   double fTParticlePZ;
   int fTNParticles;
  
  G4double noProtons;


   //Variables to study particles in the sculpted absorber tracking plane
   int fMuRunNo;
   int fMuEvtNo;
   int fMuNParticles;
   int fMuTrackID[kMaxP];
   int fMuParentID[kMaxP];
   int fMuPDG[kMaxP];
   double fMuNimpWt[kMaxP];
   double fMuX[kMaxP];
   double fMuY[kMaxP];
   double fMuZ[kMaxP];
   double fMuT[kMaxP];
   double fMuStartX[kMaxP];
   double fMuStartY[kMaxP];
   double fMuStartZ[kMaxP];
   double fMuStartE[kMaxP];
   double fMuMass[kMaxP];
   double fMuEnergy[kMaxP];
   double fMuPX[kMaxP];
   double fMuPY[kMaxP];
   double fMuPZ[kMaxP];
   double fMuTheta[kMaxP];
   double fMudEdx[kMaxP];
   double fMudEdx_ion[kMaxP];
   double fMuDE[kMaxP];
   double fMuDEion[kMaxP];
//   double fMuDEnonion[kMaxP];
   int fMuNSteps[kMaxP];





 TTree* fHorn1TrackingTree; //Tree for Horn1 Tracking Plane Data
  TTree* fHorn2TrackingTree;
  TTree* fDPTrackingTree;
  TTree* fTargetOutputTree; //Tree for Horn2 Tracking Plane Data
  TTree* fAlcoveTrackingTree; 

// What it should be..    
 //  std::string nuNtupleFileName;
   char nuNtupleFileName[1024];
   char nuNtupleFileNameDK2Nu[1024]; // New file to support standard complete 
   //
   //need this or get a seg fault don't know why
   //
   std::map<int, int> code;
   //
   
   TFile* fOutFile;
   TTree* fOutTree;

   TFile* nuNtuple;

   TFile* fOutFileDk2Nu; // The new (dk2nu, r487) NTuple file 
   TTree* fOutTreeDk2Nu;
   TTree* fOutTreeDk2NuMeta;

  TTree* fTrackingTree; // tree for tracking plane data

   
   bsim::Dk2Nu*  fDk2Nu;
   bsim::DkMeta*  fDkMeta;
            
   LBNEDataNtp_t *fLBNEOutNtpData; // Not used for Dk2Nu, we rely solely on G4Trajectory and G4TrajectoryPt
  LBNEDataNtp_t *fTrackingPlaneData; // not used?

   
   
   G4int fcount;
   G4int fentry;
   G4int nGenAbs = 3;
   G4int nVolAbs = 4;
   
   std::vector<G4double> fXdet_near;
   std::vector<G4double> fYdet_near;
   std::vector<G4double> fZdet_near;
   std::vector<G4double> fXdet_far;
   std::vector<G4double> fYdet_far;
   std::vector<G4double> fZdet_far;
   std::vector<G4String> fDetNameNear;
   std::vector<G4String> fDetNameFar;
   
     std::vector<bsim::Traj>vec_traj; //A. Bashyal
   int tar_trackID=-1;
   int dk_trackID;
   int nVintTot;
   int nVdblTot;
   G4double dist_IC1[3];
   G4double dist_IC2[3];
   G4double dist_DPIP[3];
   G4double dist_DVOL[3];
   vstring_t VolVdblName;
   vstring_t VolAbsName;
   vstring_t VolVintName;
   vstring_t GenAbsName;
   
   void setDetectorPositions(); // Obsollete if read from Dk2Nu file, as per Robert (Robert, not Rob), Hatcher, Sept 2014. 

};
#endif 

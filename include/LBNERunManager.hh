//
// LBNERunManager.hh
//

#ifndef LBNERunManager_h
#define LBNERunManager_h 1

#include "G4RunManager.hh"
#include "LBNERunAction.hh"
#include "LBNEEventAction.hh"
#include "LBNETrackingAction.hh"
#include "LBNESteppingAction.hh"
#include "LBNEStackingAction.hh"

#include "LBNEPrimaryGeneratorAction.hh"

class LBNERunManager : public G4RunManager
{
public:
   LBNERunManager();
   virtual ~LBNERunManager();
   
   virtual void InitializeGeometry();
   virtual void InitializePhysics();
   virtual void BeamOn(G4int n_event,const char* macroFile=0,G4int n_select=-1);
   inline G4int GetNumberOfEvents(){
      return numberOfEventToBeProcessed;
   }
   
   G4int nEvents;
   
   
protected:
   bool fGeometryIntializedHere;
   bool fPhysicsInitializedHere;
   G4String fPhysicsListName; // such that we can write it onto the Dk2Nu Meta tree at a later point. 
//   G4String fPhysicsListNameDefault; //a default value, for possible later usage. P.L., Sept 2014. 
   LBNEEventAction *fLBNEEventAction;
   LBNESteppingAction *fLBNESteppingAction;
   LBNEStackingAction *fLBNEStackingAction ;
   LBNETrackingAction *fLBNETrackingAction;
   LBNERunAction *fLBNERunAction;
   
//   LBNEPrimaryGeneratorAction * primaryGeneratorAction;
  
   G4String fMarsOrFlukaInputFileName;
   G4String fAsciiOutputFileName;
   G4String fNptOutputFileName;
   G4String fDk2NuOutputFileName;
   G4String fDetectorLocationFileName;
   bool fUseFluka;
   bool fUseMars;
   bool fCreateOutput;
   bool fCreateDk2NuOutput;
   bool fCreateAsciiOutput;
   bool fCreateTrkPlaneOutput;
   bool fCreateTrkPlaneH1Output; //Amit Bashyal
   bool fCreateTrkPlaneH2Output;  //Amit Bashyal
   bool fCreateTrkPlaneDPOutput;   //Amit Bashyal
   bool fCreateTrackingOutput;
   bool fCreateTrackingTargetOutASCII;
   bool fCreateTargetOutput;  //Amit Bashyal
   bool fCreateAlcoveTrackingOutput;//Jeremy Lopez
   int fVerboseLevel;   
  
public:

   inline void SetNptInputFileName(G4String &aName) { fMarsOrFlukaInputFileName = aName; }
   
   inline LBNEPrimaryGeneratorAction* GetLBNEPrimaryGeneratorAction()
      { return dynamic_cast<LBNEPrimaryGeneratorAction*>(userPrimaryGeneratorAction); }
   
   inline LBNESteppingAction* GetLBNESteppingManager() 
      { return dynamic_cast<LBNESteppingAction*> (userSteppingAction); }
      
   inline void SetUseFlukaInput(bool t) {  fUseFluka = t; }
   inline void SetUseMarsInput(bool t) {  fUseMars = t; }
   
   inline bool GetUseFlukaInput() const {return  fUseFluka;} 
   inline bool GetUseMarsInput() const {return  fUseMars;} 
   inline G4String GetNptInputFileName() const {return fMarsOrFlukaInputFileName;}
   
   inline void SetOutputNtpFileName(G4String &aName) {fNptOutputFileName = aName;} 
   inline G4String  GetOutputNtpFileName() const {return fNptOutputFileName;} 
   inline void SetOutputDk2NuFileName(G4String &aName) {fDk2NuOutputFileName = aName;} 
   inline G4String  GetOutputDk2NuFileName() const {return fDk2NuOutputFileName;} 
   inline void SetOutputASCIIFileName(G4String &aName) {fAsciiOutputFileName = aName;} 
   inline G4String  GetOutputASCIIFileName() const {return fAsciiOutputFileName;} 
   
   inline bool GetCreateOutput() const { return fCreateOutput; }
   inline void SetCreateOutput(bool t) { fCreateOutput = t;}
   inline bool GetCreateDk2NuOutput() const { return fCreateDk2NuOutput; }
   inline void SetCreateDk2NuOutput(bool t) { fCreateDk2NuOutput = t;}
   inline bool GetCreateASCIIOutput() const { return fCreateAsciiOutput; }
   inline void SetCreateASCIIOutput(bool t) { fCreateAsciiOutput = t;}
   inline bool GetCreateTrkPlaneOutput() const { return fCreateTrkPlaneOutput; }
   inline void SetCreateTrkPlaneOutput(bool t) { fCreateTrkPlaneOutput = t;}
   inline bool GetCreateTrackingTargetOutASCII() const { return fCreateTrackingTargetOutASCII; }
   inline void SetCreateTrackingTargetOutASCII(bool t) { 
      fCreateTrackingTargetOutASCII = t;
      if (t) {
        const LBNESteppingAction *stepAct 
	= reinterpret_cast<const LBNESteppingAction *>(G4RunManager::GetUserSteppingAction());
	stepAct->InitiateHadronFluxFromTargetASCII();
        const LBNETrackingAction *trackAct 
	= reinterpret_cast<const LBNETrackingAction *>(G4RunManager::GetUserTrackingAction());
	trackAct->OpenHadronAtVertex();
        const LBNEStackingAction *stackAct 
	= reinterpret_cast<const LBNEStackingAction *>(G4RunManager::GetUserStackingAction());
	stackAct->OpenHadronAtVertex();
      }
   }
   inline G4String GetPhysicsListName() const {return fPhysicsListName; }
   inline void SetPhysicsListName(G4String &t) {fPhysicsListName = t;}
   inline G4String GetDetectorLocationFileName() const { return fDetectorLocationFileName; }
   inline void SetDetectorLocationFileName(G4String &sDetLocName) { fDetectorLocationFileName = sDetLocName;}
    inline bool GetCreateTargetOutput() const {return fCreateTargetOutput;}
   inline void SetCreateTargetOutput(bool t) {fCreateTargetOutput = t;}
    inline bool GetCreateTrkPlaneH1Output() const {return fCreateTrkPlaneH1Output;}
   inline bool GetCreateTrkPlaneH2Output() const {return fCreateTrkPlaneH2Output;}
   inline bool GetCreateTrkPlaneDPOutput() const {return fCreateTrkPlaneDPOutput;}
   inline void SetCreateTrkPlaneH1Output (bool t) {fCreateTrkPlaneH1Output = t;}     
   inline void SetCreateTrkPlaneH2Output (bool t) {fCreateTrkPlaneH2Output = t;}
   inline void SetCreateTrkPlaneDPOutput (bool t) {fCreateTrkPlaneDPOutput = t;}
   inline bool GetCreateAlcoveTrackingOutput() const {return fCreateAlcoveTrackingOutput;}
   inline void SetCreateAlcoveTrackingOutput (bool t) {fCreateAlcoveTrackingOutput = t;}     
// Why keeping two event counters ? One in the Base class (numberOfEventToProcessed) and nEvent here...  
//  Note available in v4.9.3...   
   inline void SetNumberOfEventsLBNE( int n ) {nEvents = n;}
   inline int GetNumberOfEventsLBNE() { return nEvents;}
      
   inline int GetVerboseLevel() const {return fVerboseLevel; }   
      
};

#endif

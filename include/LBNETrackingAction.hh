//
// LBNETrackingAction.hh
//
#ifndef LBNETrackingAction_h
#define LBNETrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "LBNETrajectory.hh"

class LBNERunManager;
class LBNEPrimaryGeneratorAction;
class G4EventManager;
class LBNEEventAction;

class LBNETrackingAction : public G4UserTrackingAction
{
  public:
    LBNETrackingAction();
    virtual ~LBNETrackingAction();

    virtual void PreUserTrackingAction(const G4Track*);
    virtual void PostUserTrackingAction(const G4Track*);

   void SetPreLBNETrackInformation(const G4Track* aTrack);
   void SetPostLBNETrackInformation(const G4Track* aTrack);
   void AnalyzeIfNeutrino(const G4Track* aTrack);

  private:
   LBNERunManager *pRunManager;
   LBNEPrimaryGeneratorAction *NPGA;
   G4EventManager *EvtManager;
   LBNEEventAction *LBNEEvtAct;
   // 
   // Debugging where the big deficit in pion production with respect to Fluka
   //
   mutable std::ofstream fOutHadronAtVectex; // close by defaults.. 
   mutable bool doImportanceWeightSelection; // True by default, as decided by g4numi
  public:
    void OpenHadronAtVertex() const; 
    inline void SetDoImportanceWeightSelection( bool t) const { doImportanceWeightSelection = t; } // Pseudo Const.. 
    inline bool GetDoImportanceWeightSelection() const {return doImportanceWeightSelection; }
   
};
#endif

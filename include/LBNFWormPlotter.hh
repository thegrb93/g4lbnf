//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: LBNFWormPlotter.hh 77479 2013-11-25 10:01:22Z gcosmo $
//
//

#ifndef LBNFWormPlotter_H
#define LBNFWormPlotter_H 1

// class description:
//
// LBNFWormPlotter

#include "globals.hh"

#include "G4ThreeVector.hh"
#include "G4Colour.hh"

#include <vector>

class G4EventManager;
class G4RayShooter;
class LBNFWormPlotMessenger;

class LBNFWormPlotter {

public:

    LBNFWormPlotter();

    virtual ~LBNFWormPlotter();
    
    void CreatePlot(const G4String& fileName);
    // The main entry point which triggers ray tracing. "fileName" is output
    // file name, and it must contain extention (e.g. myFigure.jpg). This
    // method is available only if Geant4 is at Idle state.
    
    G4ThreeVector GetXAxis() const {return xAxis;}
    G4ThreeVector GetYAxis() const {return yAxis;}
    G4ThreeVector GetZAxis() const {return zAxis;}
    
    G4double GetXMin() const {return xMin;}
    G4double GetXMax() const {return xMax;}
    G4double GetYMin() const {return yMin;}
    G4double GetYMax() const {return yMax;}  
    G4double GetZ0() const {return z0;}
    
    G4String GetXName() const {return xName;}
    G4String GetYName() const {return yName;}
    
    void SetXAxis(const G4ThreeVector& vect) {xAxis = vect.unit();}
    void SetYAxis(const G4ThreeVector& vect) {yAxis = vect.unit();}
    
    void SetXName(const G4String& word) {xName = word;}
    void SetYName(const G4String& word) {yName = word;}
    
    void SetXMin(const G4double& val) {xMin = val;}
    void SetXMax(const G4double& val) {xMax = val;}
    void SetYMin(const G4double& val) {yMin = val;}
    void SetYMax(const G4double& val) {yMax = val;}
    void SetZ0(const G4double& val) {z0 = val;}
    
protected:  
    
    G4ThreeVector xAxis;
    G4ThreeVector yAxis;
    G4ThreeVector zAxis;
    G4double xMin;
    G4double xMax;
    G4double yMin;
    G4double yMax;
    G4double z0;
    G4bool gotBounds;
    G4double ABSq;
    G4double ADSq;
    G4ThreeVector AB;
    G4ThreeVector AD;
    G4ThreeVector PointA;
    
    G4String xName;
    G4String yName;
    
    G4RayShooter * theRayShooter;
    LBNFWormPlotMessenger * theMessenger;
    G4EventManager * theEventManager;
    
    std::vector<G4ThreeVector> findIntersections(const G4ThreeVector& rayPosition,
						 const G4ThreeVector& rayDirection) const;
    
    G4bool insidePlot(const G4ThreeVector& thePoint) const;
    
};

#endif

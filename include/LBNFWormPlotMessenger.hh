//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: LBNFWormPlotMessenger.hh 74050 2013-09-20 09:38:19Z gcosmo $
//
//

// class description:
//
//  This is a concrete class of G4UImessenger. This class defines various
// UI commands which are unique to G4RayTracer.
//

#ifndef LBNFWormPlotMessenger_HH
#define LBNFWormPlotMessenger_HH 1

#include "G4UImessenger.hh"
class G4UIdirectory;

class G4UIcmdWith3Vector;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;

class LBNFWormPlotter;

class LBNFWormPlotMessenger : public G4UImessenger {

public:
    
    static LBNFWormPlotMessenger* GetInstance(LBNFWormPlotter* wPlot);  // Singleton constructor.
    virtual ~LBNFWormPlotMessenger();
    
    virtual G4String GetCurrentValue(G4UIcommand * command);
    virtual void SetNewValue(G4UIcommand * command,G4String newValue);
    
private:
    
    LBNFWormPlotMessenger(LBNFWormPlotter* wPlot);
    
    static LBNFWormPlotMessenger* fpInstance;
    
    LBNFWormPlotter* theWorm;
    
    G4UIdirectory* wormDir;
    G4UIcmdWith3Vector* xAxisCmd;
    G4UIcmdWithAString* xNameCmd;
    G4UIcmdWith3Vector* yAxisCmd;
    G4UIcmdWithAString* yNameCmd;
    
    G4UIcmdWithADoubleAndUnit* xMinCmd;
    G4UIcmdWithADoubleAndUnit* xMaxCmd;
    G4UIcmdWithADoubleAndUnit* yMinCmd;
    G4UIcmdWithADoubleAndUnit* yMaxCmd;
    
    G4UIcmdWithADoubleAndUnit* z0Cmd;
    
    G4UIcmdWithAString* fileCmd;
    
};

#endif




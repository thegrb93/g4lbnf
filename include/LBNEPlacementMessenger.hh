#ifndef LBNEPLACEMENTMESSENGER_H
#define LBNEPLACEMENTMESSENGER_H 1

#include <vector>

//#include "G4VUserVolumePlacement.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4PVPlacement.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
//
// Geant User Interface utility for handling the nominal paramtof the geometry. 
//
class LBNEPlacementMessenger: public G4UImessenger {

public:
  static LBNEPlacementMessenger* Instance();
  ~LBNEPlacementMessenger();
  void SetNewValue(G4UIcommand* command, G4String cmd); // Will set data in the singleton LBNEVolumePlacements
     
private:
   LBNEPlacementMessenger();
  static LBNEPlacementMessenger* fInstance;
    
//  G4UIdirectory*                fLBNEDir;
//  G4UIdirectory*                fdetDir;

  G4UIcmdWithADoubleAndUnit* fWaterLayerThickInHorn;
  G4UIcmdWithADoubleAndUnit* fHorn1Length;
  G4UIcmdWithADoubleAndUnit* fBaffleLength;
  G4UIcmdWithADoubleAndUnit* fBaffleInnerRadius;
  G4UIcmdWithADoubleAndUnit* fBaffleZPosition;
  G4UIcmdWithADoubleAndUnit* fPlugLength;
  G4UIcmdWithADoubleAndUnit* fPlugInnerRadius;
  G4UIcmdWithADoubleAndUnit* fPlugOuterRadius;
  G4UIcmdWithADoubleAndUnit* fPlugZPosition;
  G4UIcmdWithABool* fConstructPlug;
  G4UIcmdWithABool* fConstructPlugInHorn1;
//
  G4UIcmdWithADoubleAndUnit* fTargetSLengthGraphite; 
  G4UIcmdWithADoubleAndUnit* fTargetFinWidth; 
  G4UIcmdWithADoubleAndUnit* fTargetLengthIntoHorn;
  G4UIcmdWithADoubleAndUnit* fTargetLengthOutsideHorn;
  G4UIcmdWithADoubleAndUnit* fTargetBerylCapThickness;
  G4UIcmdWithAString* fTargetMaterial;
  G4UIcmdWithAString* fPlugMaterial;
  G4UIcmdWithADoubleAndUnit* fTargetDensity;
//
// December 2013: Simple target. 
//
  G4UIcmdWithABool* fUseSimpleTargetBox;
  G4UIcmdWithABool* fUseSimpleTargetCylinder;
  // The length will be set by the cmd fTargetSLengthGraphite
  G4UIcmdWithADoubleAndUnit* fSimpleTargetRadius; // not used if box. 
  G4UIcmdWithADoubleAndUnit* fSimpleTargetHeight; // not used if cylinder 
  G4UIcmdWithADoubleAndUnit* fSimpleTargetWidth; // not used if cylinder 
  G4UIcmdWithADoubleAndUnit* fSimpleTargetLength; // used if either cylinder or box
  
//August 2014, Quynh added
  G4UIcmdWithABool* fUseMultiSphereTarget; 
  G4UIcmdWithADoubleAndUnit* fMultiSphereTargetRadius;

  
  G4UIcmdWithADouble* fHorn1RadialRescale;   
  G4UIcmdWithADouble* fHorn1LongRescale; 
  G4UIcmdWithAString* fHorn1InnerCondMat;
  G4UIcmdWithAString* fHorn1AllCondMat;
  G4UIcmdWithADoubleAndUnit* fHorn1RadialSafetyMargin;
  G4UIcmdWithABool* fHorn1RadiusBigEnough;  
//  
  G4UIcmdWithADouble* fHorn2RadialRescale; 
// December 16 2014:  The mapping will now be Rnew = rOld* fHorn2RadialRescale + fHorn2RadialRescaleCst  
  G4UIcmdWithADoubleAndUnit* fHorn2RadialRescaleCst;
  G4UIcmdWithADouble* fHorn2LongRescale; 
  G4UIcmdWithADoubleAndUnit* fHorn2LongPosition; // from the origin MCZero to the middle of Horn 2 
  G4UIcmdWithAString* fHorn2InnerCondMat;
  G4UIcmdWithAString* fHorn2AllCondMat;
//
  G4UIcmdWithADoubleAndUnit* fDecayPipeLength;
  G4UIcmdWithADoubleAndUnit* fDecayPipeRadius;
  G4UIcmdWithADoubleAndUnit* fDecayPipeLongPosition; // From the target 
  G4UIcmdWithADoubleAndUnit* fDecayPipeUpstreamWindowThickness;
  G4UIcmdWithAString* fDecayPipeGas; // Air or Helium supported 
//
// March 10-15 Placing a magnetic field inside the decay pipe.  
// Assuming coming from a wire. Suggested by Millind Diwan 
//
  G4UIcmdWithADoubleAndUnit* fRadiusMilindWire;
  G4UIcmdWithADoubleAndUnit* fCurrentMilindWire;
  
  G4UIcmdWithADoubleAndUnit* fLengthOfRockDownstr;

  G4UIcmdWithABool* fUseMarsTargetHorns; // if true (default is false), we use MARS target and horns rather than usual g4lbne geometry

   G4UIcmdWithAString* fMarsTargetHornsGDMLFilename;

   G4UIcmdWithAString* fAbsorberGDMLFilename;
 
  G4UIcmdWithABool* fWriteGDMLFile; 

  G4UIcmdWithABool* fInstallShield; // if true (default), we place roated chunk of steel surround Horns
//
// February 2014: 1.2 MW option. 
// 
  G4UIcmdWithABool* fUse1p2MW; 
//
// March 2014: implementing round corners.  Only for the 1.2 MW target.  
//
   G4UIcmdWithABool* fUseRoundedTargetFins; 
//
// August, September 2014: Adding the Polycone Horn1 volume, with a maximum of 20 inner points on the Z-plane.
//
  G4UIcmdWithABool* fUseHorn1Polycone; 
  G4UIcmdWithAnInteger* fUseHorn1PolyNumInnerPts; 
  std::vector<G4UIcmdWith3VectorAndUnit*> fHorn1PolyListRinThickZVects;
  G4UIcmdWithADoubleAndUnit* fHorn1PolyOuterRadius;
//
// October 2014: Remove the decay pipe snout to leave more space to extend Horns.. 
//  
  G4UIcmdWithABool* fRemoveDecayPipeSnout; 
//
// October 2015: Remove the target (and break the Hadron dump, but that part will be be implemented.. ) 
//  
  G4UIcmdWithABool* fRemoveTargetAlltogether; 
//
// August 2015:  add more Polycones Horn1. We limit ourself to 5 
//
  G4UIcmdWithAnInteger* fUseNumberOfHornsPoly; // from 1 to 5 
                                               // if one 1, revert to the version Sept 2014, we keep Horn2. 
  
  G4UIcmdWithAnInteger* fUseHorn2PolyNumInnerPts; 
  std::vector<G4UIcmdWith3VectorAndUnit*> fHorn2PolyListRinThickZVects;
  G4UIcmdWithADoubleAndUnit* fHorn2PolyOuterRadius;
  G4UIcmdWithADoubleAndUnit* fHorn2PolyZStartPos;
 
  G4UIcmdWithAnInteger* fUseHorn3PolyNumInnerPts; 
  std::vector<G4UIcmdWith3VectorAndUnit*> fHorn3PolyListRinThickZVects;
  G4UIcmdWithADoubleAndUnit* fHorn3PolyOuterRadius;
  G4UIcmdWithADoubleAndUnit* fHorn3PolyZStartPos;
  
  G4UIcmdWithAnInteger* fUseHorn4PolyNumInnerPts; 
  std::vector<G4UIcmdWith3VectorAndUnit*> fHorn4PolyListRinThickZVects;
  G4UIcmdWithADoubleAndUnit* fHorn4PolyOuterRadius;
  G4UIcmdWithADoubleAndUnit* fHorn4PolyZStartPos;
  
  G4UIcmdWithAnInteger* fUseHorn5PolyNumInnerPts; 
  std::vector<G4UIcmdWith3VectorAndUnit*> fHorn5PolyListRinThickZVects;
  G4UIcmdWithADoubleAndUnit* fHorn5PolyOuterRadius;
  G4UIcmdWithADoubleAndUnit* fHorn5PolyZStartPos;
  
  G4UIcmdWithAnInteger* fSetPolyconeHornParabolic; // from 1 to 5.  Set the corresponding horn to parabolic shape, ala LoSecco.  
  
  void SetMyUnitsAndConditions(G4UIcmdWithADoubleAndUnit *cmd, double value);

  // John Back, March 2016, target module (e.g. SAT) options
  G4UIcmdWithABool* fUseTargetModule;
  G4UIcmdWithAnInteger* fTargetModuleType; // The target module type
  G4UIcmdWithADoubleAndUnit* fTargetRadius; // Target object radius
  G4UIcmdWithADoubleAndUnit* fTargetLength; // Target object length
  G4UIcmdWithADouble* fTargetNLambda; // Target object length via nucl int length
  G4UIcmdWithADouble* fTargetFracOutHornL; // Target module length fraction outside Horn1
  G4UIcmdWithADoubleAndUnit* fTargetLengthOutsideExtra; // Target module z shift

};
#endif

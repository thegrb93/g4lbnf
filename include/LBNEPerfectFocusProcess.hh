//
// Implement Perfect focusing as a G4VProcess. 
//
#ifndef LBNEPerfectFocusProcess_H
#define LBNEPerfectFocusProcess_H 1
#include "globals.hh"
#include "G4VDiscreteProcess.hh"
#include "G4ParticleDefinition.hh"
#include "G4Step.hh"
#include "G4Track.hh"
//
// Purely mnemonics at this point, as we will not change the number of interaction length 
// Nor the step legnth.. 
//
class  LBNEPerfectFocusParticleChange: public G4VParticleChange {
public:

    LBNEPerfectFocusParticleChange();
    ~LBNEPerfectFocusParticleChange();
  
};

// 
// The real thing: instant and perfect focusing. 
//
class  LBNEPerfectFocusProcess: public G4VDiscreteProcess {
public:
  
  LBNEPerfectFocusProcess(const G4String &aName="pFocus", G4ProcessType   aType =  fGeneral  );
  virtual ~LBNEPerfectFocusProcess();
  
  
  LBNEPerfectFocusParticleChange* PostStepDoIt (const G4Track &track, const G4Step &stepData);
 
  G4double PostStepGetPhysicalInteractionLength (const G4Track &track, G4double previousStepSize, 
                                                   G4ForceCondition *condition);

  bool IsApplicable(const G4ParticleDefinition&);
  void StartTracking(G4Track *aTrack);
  G4double GetMeanFreePath(const G4Track&, G4double, G4ForceCondition*);

private:

  // hide assignment operator as private
  LBNEPerfectFocusProcess(const LBNEPerfectFocusProcess&);
  LBNEPerfectFocusProcess& operator = (const LBNEPerfectFocusProcess &right);

//  
// No data member, as there are no internal parameters to this process.. 
// Except one, the Z at which the perfect focusing occurs.. 
// Late December 2014, implement the perfect focusing process exiting the target region, 
//  triggered by fZCoordForPerfectFocusing< 0. 
//
   double fZCoordForPerfectFocusing;
   double fRCoordOutOfTargetSq;
   mutable double fZAtTrigger; // The z position where we change the mean free path to trigger the focusing.
   mutable double fZStart; // The z Startup position of the track. 
   mutable double fRAtTriggerSq; // The r position where we change the mean free path to trigger the focusing.
   mutable double fRStartSq; // The z Startup position of the track. 
   mutable LBNEPerfectFocusParticleChange* fParticleChange; // We got to manage the memory oursefl here.. 
//
   double GetFreePath(const G4Track &track, G4double previousStepSize); // to avoid cloning code in Phys. Inter Length and mean Free path. 
};

#endif

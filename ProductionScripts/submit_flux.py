##################################################
##
## submit_flux.py
##
## Laura Fields
## Aug 2012
##
## Submits g4lbne flux jobs using macro, input, number of
## protons, number of jobs and output directory
## specified by user
##
###################################################

from optparse import OptionParser
import sys, os, subprocess

###################################################
##
## Helper Functions
##
###################################################
def remove_file(myfile):
        print "REMOVING",myfile
#    if myfile.startswith("/pnfs/"):
        os.system("ifdh rm "+myfile)



###################################################
#
# Determine default g4lbne directory
# (the directory this script is in)
#
###################################################
scriptdir = os.path.abspath(sys.argv[0]+"/../..")
print "default g4lbne directory:",scriptdir

###################################################
#
# Setup parser that reads in options
#
###################################################

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)

parser.add_option("-g", "--g4lbne_dir", dest="g4lbne_dir",
                  help="g4lbne directory", default=scriptdir)
parser.add_option("-p", "--physics_list", dest="physics_list",
                  help="Geant4 Physics List", default="QGSP_BERT");
parser.add_option("-m", "--macro", dest="macro",
                  help="Template Macro", default="Nominal")
parser.add_option("-d", "--mode", dest="mode",
                  help="neutrino or antineutrino", default="neutrino")
parser.add_option("-n", "--n_events", dest="n_events",
                  help="Number of events per job", default=100000)
parser.add_option("-f", "--first_job", dest="first_job",
                  help="First job number", default="1")
parser.add_option("-l", "--last_job", dest="last_job",
                  help="Last job number", default="1")
parser.add_option("-o", "--output_dir", dest="output_dir",
                  help="Output directory", default="/pnfs/dune/scratch/users/$USER/fluxfiles/g4lbne")
parser.add_option("-a", "--macro_dir", dest="macro_dir",
                  help="Macro directory", default="macros")
parser.add_option("-v", "--interactive", dest="interactive",action="store_true",
                  help="Run interactively", default=False)
parser.add_option("-s","--osg",dest="osg",action="store_true",help="Option to run on OSG.  Does not work yet", default=False) 
parser.add_option("-t", "--notuples", dest="notuples",action="store_true",help="Don't write ntuple output",default=False)
parser.add_option("-x","--detx",dest="detx",action="append",help="X position in cm from MCZero of position where you want flux calculated (optional -- ND and FD fluxes are always calculated) Can be used multiple times",type="float")
parser.add_option("-y","--dety",dest="dety",action="append",help="See detx usage",type="float")
parser.add_option("-z","--detz",dest="detz",action="append",help="see detz usage",type="float")


(options, args) = parser.parse_args()

# Expand environment variables if necessary
if options.g4lbne_dir.find("$USER")>=0:
    options.g4lbne_dir = options.g4lbne_dir.replace("$USER",os.getenv("USER"));
if options.output_dir.find("$USER")>=0:
    options.output_dir = options.output_dir.replace("$USER",os.getenv("USER"));

###################################################
#
# Determine G4LBNE Version
#
###################################################
version="v3r4p3"


###################################################
#
# Check validity of specified options
#
###################################################
g4lbne_executable=options.g4lbne_dir+"/g4lbnf";
	
if not os.path.exists(g4lbne_executable):
    print "ERROR: I can't find a g4lbne executable at "+g4lbne_executable
    sys.exit()

g4lbne_setup=options.g4lbne_dir+"/setups/setup_g4lbne_fnal.sh"
if options.osg:
	g4lbne_setup = options.g4lbne_dir+"/setups/setup_g4lbne_cvmfs.sh"

if not os.path.exists(g4lbne_setup):
    print "ERROR: I can't find a g4lbne setup script at "+g4lbne_setup
    sys.exit()
    
macro_template = options.macro_dir+"/"+options.macro+".mac"
if options.macro.startswith("Optimizations"):
    splitmacro = options.macro.split("-")
    print splitmacro
    macro_template = options.macro_dir+"/Optimizations/"+splitmacro[1]+"/"+splitmacro[1]+"_"+splitmacro[2]+".mac"
if not os.path.exists(macro_template):
    print "ERROR: I can't find a macro at "+macro_template
    sys.exit()

histomaker_macro=options.g4lbne_dir+"/ProductionScripts/makeFluxHistograms.C";
if not os.path.exists(histomaker_macro):
    print "ERROR: I can't find a histogram macro at "+histomaker_macro
    sys.exit()

#input_card = options.g4lbne_dir+"/inputs/"+options.input+".input"
#if not os.path.exists(macro_template):
#    print "I can't find an input card at at "+input_card
#    sys.exit()

physics_list = options.physics_list
print "Using GEANT4 physics list",physics_list

if options.detx == None: options.detx = []
if options.dety == None: options.dety = []
if options.detz == None: options.detz = []

if len(options.detx) != len(options.dety) or len(options.detx) != len(options.detz) or len(options.dety) != len(options.detz):
    print "ERROR: You must specify an equal number of x,y & z detector positions"
    sys.exit()

for pos in range(len(options.detx)):
    print "Flux will be calculated at position: (",options.detx[pos],",",options.dety[pos],",",options.detz[pos],")"

###################################################
#
# Print options
#
###################################################
if options.interactive: 
    print "Running",str(int(options.last_job)-int(options.first_job)+1),"jobs starting with JobID",options.first_job,"."
    print "Submitting",str(int(options.last_job)-int(options.first_job)+1),"jobs starting with JobID",options.first_job,"."
print "Using the g4lbne installed at",options.g4lbne_dir
print "Each job will have",options.n_events,"protons on target."
print "Using macro template:",macro_template
print "Output will be written to:",options.output_dir
if options.notuples:
    print "Will only write histogram output -- no nutples"

###################################################
#
# Create Directories
#
###################################################
# osg needs macros to be on pnfs
# keep a copy on /dune/app for record keeping too
output_macro_dir = "/dune/app/users/"+os.getenv("USER")+"/fluxfiles/g4lbne/"+version+"/"+physics_list+"/"+options.macro+"/"+str(options.mode)+"/macros/"
output_macro_dir2 = "/pnfs/dune/scratch/users/"+os.getenv("USER")+"/fluxfiles/g4lbne/"+version+"/"+physics_list+"/"+options.macro+"/"+str(options.mode)+"/macros/"
flux_dir = options.output_dir+"/"+version+"/"+physics_list+"/"+options.macro+"/"+str(options.mode)+"/flux/"
log_dir = options.output_dir+"/"+version+"/"+physics_list+"/"+options.macro+"/"+str(options.mode)+"/logfiles/"

print "Creating",output_macro_dir
if not os.path.exists(output_macro_dir):
    os.makedirs(output_macro_dir)
print "Creating",output_macro_dir2
if not os.path.exists(output_macro_dir2):
    os.makedirs(output_macro_dir2)
print "Creating",flux_dir
if not os.path.exists(flux_dir):
    os.makedirs(flux_dir)
if not os.path.exists(flux_dir+"/histos/"):
    os.makedirs(flux_dir+"/histos/")
print "Creating",log_dir
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

if not os.path.exists(output_macro_dir):
    print "ERROR: could not create macro directory: "+output_macro_dir
    sys.exit()

if not os.path.exists(output_macro_dir2):
    print "ERROR: could not create macro directory: "+output_macro_dir2
    sys.exit()

if not os.path.exists(flux_dir):
    print "ERROR: could not create flux directory: "+flux_dir
    sys.exit()

if not os.path.exists(log_dir):
    print "ERROR: could not create log directory: "+log_dir
    sys.exit()

os.chmod(output_macro_dir,0777)
os.chmod(output_macro_dir2,0777)
os.chmod(flux_dir,0777)
os.chmod(flux_dir+"/histos/",0777)
os.chmod(log_dir,0777)

temp_dir = " "
if not options.interactive:
    temp_dir = "$_CONDOR_SCRATCH_DIR/"

###################################################
#
# Loop over requested jobs
#
###################################################
file_prefix = "g4lbne_"+version+"_"+physics_list+"_"+options.macro+"_"+str(options.mode)
macros_to_pnfs = ""
for i in range(int(options.first_job),int(options.last_job)+1):

    if 1>0:
        # Write macro file
        oldmac = open(macro_template)    
        new_macro_filename = output_macro_dir+os.path.basename(macro_template).replace(".mac","_"+str(i).zfill(3)+".mac")
        new_macro_filename2 = output_macro_dir2+os.path.basename(macro_template).replace(".mac","_"+str(i).zfill(3)+".mac")

        newmac = open(new_macro_filename, 'w')
        horn_current_set = False
        for s in oldmac.xreadlines():
            if s.find("/LBNE/output/OutputNtpFileName")>=0:
                newmac.write("/LBNE/output/OutputNtpFileName "+temp_dir+file_prefix+"\n")
            elif s.find("/LBNE/output/OutputDk2NuFileName")>=0:
                newmac.write("/LBNE/output/OutputDk2NuFileName "+temp_dir+file_prefix+"\n")
            elif s.find("/LBNE/rndm/setRndmSeed")>=0:
                newmac.write("/LBNE/rndm/setRndmSeed "+str(i)+"\n")
            elif s.find("/LBNE/run/setRunID")>=0:
                newmac.write("/LBNE/run/setRunID "+str(i)+"\n")
            elif s.find("/LBNE/run/NEvents")>=0:
                newmac.write("/LBNE/run/NEvents "+str(options.n_events)+"\n")
            elif s.find("/LBNE/det/seHornCurrent")>=0:
                    splitline = s.split()
                    if options.mode=="antineutrino" and not splitline[1].strip().startswith("-"):
                        newmac.write("/LBNE/det/seHornCurrent "+"-"+splitline[1]+" \n")
                    elif options.mode=="neutrino" and splitline[1].startswith("-"):
                        newmac.write("/LBNE/det/seHornCurrent "+splitline[1][1:]+" \n")
                    else:
                        newmac.write(s)
		    horn_current_set = True
            else:
                newmac.write(s);

        if not horn_current_set:
            print "ERROR: The macro "+macro_template+" does set the horn current; please add a line such as /LBNE/det/seHornCurrent 200 kA to your macro template"
            sys.exit()

	newmac.close()

	if options.osg:
		if os.path.exists(new_macro_filename2):
			os.system("ifdh rm "+new_macro_filename2)
		macros_to_pnfs += new_macro_filename+" "

# If running on osg, the macros need to be on pnfs, so copy them
if options.osg:
	print "Copying to",os.path.dirname(new_macro_filename2)
	os.system("ifdh cp -D "+macros_to_pnfs+" "+os.path.dirname(new_macro_filename2))

# Write the script that will run on the grid
if 1>0:

    script_filename = output_macro_dir+os.path.basename(macro_template).replace(".mac",".sh")
    wrapper = open(script_filename, 'w')

    wrapper.write("source "+g4lbne_setup+" \n\n")

    on_grid = "true";
    if(options.interactive):
        on_grid = "false";

    offset = options.first_job
    wrapper.write("export run_number=$(($PROCESS+"+offset+")) \n")
    wrapper.write("export padded_run_number=`printf \"%03g\" $(($PROCESS+"+offset+"))` \n")
    wrapper.write("export padded_run_number5=`printf \"%05g\" $(($PROCESS+"+offset+"))` \n")
    wrapper.write("\nenv\n\n")
    new_macro_filename = output_macro_dir+os.path.basename(macro_template).replace(".mac","_${padded_run_number}.mac")
    new_macro_filename2 = output_macro_dir2+os.path.basename(macro_template).replace(".mac","_${padded_run_number}.mac")

    local_macro_path = "$_CONDOR_SCRATCH_DIR/"+os.path.basename(new_macro_filename)

    if options.interactive:
	    wrapper.write(g4lbne_executable+" --physicslist "+physics_list+" "+new_macro_filename+'\n\n')
    else:
	    if options.osg:
		    wrapper.write("ifdh cp "+ new_macro_filename2+" "+local_macro_path+" \n\n")
	    else:

		    wrapper.write("ifdh cp "+ new_macro_filename+" "+local_macro_path+" \n\n")
	    wrapper.write("if [ -f "+local_macro_path+" ];\n")
	    wrapper.write("then\n")
	    wrapper.write("   "+g4lbne_executable+" --physicslist "+physics_list+" "+local_macro_path+'\n')
	    wrapper.write("else\n")
	    wrapper.write('   echo "'+local_macro_path+' does not exit.  Exiting."\n')
	    wrapper.write("   exit 1 \n")
	    wrapper.write("fi \n\n")




    # copy the output
    ntuple_output = file_prefix+"_${padded_run_number}.root";
    dk2nu_output = file_prefix+"_${padded_run_number5}.dk2nu.root";
    ndhisto_output = "histos_"+file_prefix+"_${run_number}_LBNEND.root";
    fdhisto_output = "histos_"+file_prefix+"_${run_number}_LBNEFD.root";
    ndfmchisto_output = "histos_"+file_prefix+"_${run_number}_LBNEND_fastmc.root";
    fdfmchisto_output = "histos_"+file_prefix+"_${run_number}_LBNEFD_fastmc.root";
    ndglobeshisto_output = "histos_"+file_prefix+"_${run_number}_LBNEND_globes_flux.txt";
    fdglobeshisto_output = "histos_"+file_prefix+"_${run_number}_LBNEFD_globes_flux.txt";
    userhisto_output = []
    userfmchisto_output = []
    userglobeshisto_output = []
    for pos in range(len(options.detx)):
        userhisto_output.append("histos_"+file_prefix+"_${run_number}_USER"+str(pos)+".root")
        userfmchisto_output.append("histos_"+file_prefix+"_${run_number}_USER"+str(pos)+"_fastmc.root")
        userglobeshisto_output.append("histos_"+file_prefix+"_${run_number}_USER"+str(pos)+"_globes_flux.txt")
    
    if not options.notuples:
	    wrapper.write("if [ -f "+temp_dir+ntuple_output+" ];\n")
	    wrapper.write("then\n")
	    wrapper.write("   ifdh cp  "+ temp_dir+ntuple_output+" "+flux_dir+ntuple_output+"\n")
	    wrapper.write("fi \n")

	    wrapper.write("if [ -f "+temp_dir+ntuple_output+" ];\n")
	    wrapper.write("then\n")
	    wrapper.write("   ifdh cp "+ temp_dir+dk2nu_output+" "+flux_dir+dk2nu_output+"\n")
	    wrapper.write("fi \n")



    # make histograms
    wrapper.write('root -q -b '+histomaker_macro+'\\(\\\"'+os.getenv("USER")+'\\\",\\\"'+version+'\\\",\\\"'+options.macro+'\\\",\\\"'+str(options.mode)+'\\\",\\\"LBNEND\\\",\\\"'+physics_list+'\\\",\\\"1\\\",\\\"${run_number}\\\",\\\"'+str(options.n_events)+'\\\",\\\"'+on_grid+'\\\"\\)\n\n');
    wrapper.write('root -q -b '+histomaker_macro+'\\(\\\"'+os.getenv("USER")+'\\\",\\\"'+version+'\\\",\\\"'+options.macro+'\\\",\\\"'+str(options.mode)+'\\\",\\\"LBNEFD\\\",\\\"'+physics_list+'\\\",\\\"1\\\",\\\"${run_number}\\\",\\\"'+str(options.n_events)+'\\\",\\\"'+on_grid+'\\\"\\)\n\n');
    wrapper.write('root -q -b '+histomaker_macro+'\\(\\\"'+os.getenv("USER")+'\\\",\\\"'+version+'\\\",\\\"'+options.macro+'\\\",\\\"'+str(options.mode)+'\\\",\\\"USER0\\\",\\\"'+physics_list+'\\\",\\\"1\\\",\\\"${run_number}\\\",\\\"'+str(options.n_events)+'\\\",\\\"'+on_grid+'\\\"\\)\n\n');

    wrapper.write("ls -lrt $_CONDOR_SCRATCH_DIR\n")

    outfiles = temp_dir+ndhisto_output+" "+temp_dir+fdhisto_output+" "+temp_dir+ndfmchisto_output+" "+temp_dir+fdfmchisto_output+" "+temp_dir+ndglobeshisto_output+" "+temp_dir+fdglobeshisto_output+" "
    for pos in range(len(options.detx)):
            outfiles = outfiles+temp_dir+userhisto_output[pos]+" "+temp_dir+userfmchisto_output[pos]+" "+temp_dir+userglobeshisto_output[pos]

    wrapper.write("ifdh cp -D "+outfiles+" "+flux_dir+"/histos/"+"\n")
    wrapper.write("echo 'ALL DONE'\n")
    wrapper.close();
    
    os.chmod(script_filename,0777);
    
    # remove old files to prevent permissions conflicts
    logfile = log_dir + ntuple_output.split("/")[-1].replace(".root",".log").replace("_${padded_run_number}","_\$PROCESS");
    if os.path.exists(logfile):
        os.remove(logfile)
    if os.path.exists(ntuple_output):
        os.remove(ntuple_output);
    if os.path.exists(dk2nu_output):
        os.remove(dk2nu_output);
    if os.path.exists(ndhisto_output):
        os.remove(ndhisto_output);
    if os.path.exists(fdhisto_output):
        os.remove(fdhisto_output);
    if os.path.exists(ndfmchisto_output):
        os.remove(ndfmchisto_output);
    if os.path.exists(fdfmchisto_output):
        os.remove(fdfmchisto_output);
    if os.path.exists(ndglobeshisto_output):
        os.remove(ndglobeshisto_output);
    if os.path.exists(fdglobeshisto_output):
        os.remove(fdglobeshisto_output);
    for pos in range(len(options.detx)):
        if os.path.exists(userhisto_output[pos]):
            os.remove(userhisto_output[pos])
        if os.path.exists(userfmchisto_output[pos]):
            os.remove(userfmchisto_output[pos])
        if os.path.exists(userglobeshisto_output[pos]):
            os.remove(userglobeshisto_output[pos])
        
    print "logfile",logfile

    # Submit the job
    if options.interactive:
	    print "type "+script_filename+" to execute"
    elif options.osg:
#	    os.system("jobsub_submit.py -N "+str(int(options.last_job)-int(options.first_job)+1)+" --resource-provides=usage_model=OFFSITE --OS=SL6 --disk=9000MB --site=SU-OG --group=dune -L "+logfile+" file:///"+script_filename);
	    os.system("jobsub_submit.py -N "+str(int(options.last_job)-int(options.first_job)+1)+" --resource-provides=usage_model=OFFSITE --OS=SL6 --disk=9000MB --site=Nebraska,Wisconsin,FNAL,MWT2,Clemson,Cornell --expected-lifetime 4h --group=dune -L "+logfile+" file:///"+script_filename);
#	    os.system("jobsub_submit.py --resource-provides=usage_model=OFFSITE --OS=SL6 --disk=9000MB --site=Nebraska,Caltech,Wisconsin,MWT2 --group=dune -L "+logfile+" file:///"+script_filename);
#	    os.system("jobsub_submit.py --resource-provides=usage_model=OFFSITE --OS=SL6 --disk=9000MB --site=FNAL --group=dune -L "+logfile+" file:///"+script_filename);
    else:
	    os.system("jobsub_submit.py -N "+str(int(options.last_job)-int(options.first_job)+1)+" --resource-provides=usage_model=OPPORTUNISTIC --OS=SL6 --group=dune -L "+logfile+" file:///"+script_filename);

    print "DONE"
    
  

import os


for root, dirs, files in os.walk("/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/"):
    for name in root:

        fullpath = root

        splitdir = fullpath.split("/")

        if len(splitdir) != 11:
            continue

        if not os.path.exists(fullpath+"/flux/"):
            print "WARNING: Path without flux directory:",fullpath
            continue


        n_tuples = 0
        n_histos = 0
        for root, dirs, files in os.walk(fullpath+"/flux/"):
            for file in files:
                if(file.startswith("g4lbne_")):
                   n_tuples = n_tuples + 1
                if(file.startswith("histos_")):
                   n_histos = n_histos + 1

        version = splitdir[7]
        plist = splitdir[8]
        macro = splitdir[9]
        current = splitdir[10]

        if(n_tuples != 250 or n_histos != 6):
            # skip simulations we don't care about anymore
            if (splitdir[9]=="700kW" or ("CylindricalTarget" in splitdir[9])):
                continue
            print "WARNING: "+plist+"/"+macro+"/"+current+" ntuples: "+str(n_tuples)+" nhistos: "+str(n_histos)
            continue

        oppcurrent = "-"+current
        if current.startswith("-"):
            oppcurrent = current[1:]

        if not os.path.exists(fullpath.replace(current,oppcurrent)):
            print "WARNING: "+plist+"/"+macro+"/"+current+" ntuples: "+str(n_tuples)+" has no opposite current directory"
        

#!/bin/sh
# Minimal script to copy a file from grid cloud, process it... On the grid
#
#set -x
echo " RunCyl, v1_1, My date, there " `date`
uname -a
g++ --version
#
echo  " condor scrach is " ${_CONDOR_SCRATCH_DIR}
cd ${_CONDOR_SCRATCH_DIR}
echo "Check, I am now on " `pwd`
#
IFDH_OPTION=""
#
GROUP=`id -gn`
#
SCRATCH_DIR="/pnfs/fermilab/volatile"
echo -e "\nSetting up NEXT UPS DB : "
#source /grid/fermiapp/products/next/setups.sh
source /grid/fermiapp/products/next/setups.sh
setup ifdhc v1_6_2
echo " Product ifdh ok ? " $IFDHC_DIR 
#
#echo " Trying to process " ${NUMCONC} " root files" 
voms-proxy-info --all
voms-proxy-init -rfc -noregen -voms fermilab:/fermilab/Role=Analysis 
setup ifdhc v1_6_2 
export CPN_LOCK_GROUP=gpcf
echo " After voms-proxy.. Processing " ${NUMCONC} " root files" 
echo " Other arguments are : " 
echo " CLUSTER " ${CLUSTER}
echo " PROCESS " ${PROCESS}
ls -l /lbne/app/users/lebrun/fluka/install/flukahp
#
# One must have this is ifdhc product is used! 
#
export CPN_LOCK_GROUP=gpcf
echo "Check, I am now on " `pwd`
source /grid/fermiapp/products/common/etc/setups.sh
source /grid/fermiapp/products/lbne/setup
let "myran=${CLUSTER}/10 +${PROCESS}"
sed -i  s/RanCra/${myran}.0/ ./CylTargetProd_${CLUSTER}_${PROCESS}.inp
echo " Random number will be " ${myran}
echo " Run g4lbne now at " `date`
cp /lbne/app/users/lebrun/work2/dev2/repo/macros/CylTargetNoFlukaProd.mac ./runProd.mac
sed -i s/RanCra/${myran}/ ./runProd.mac
sed -i s/genProd/${CLUSTER}_${PROCESS}/ ./runProd.mac
sed -i s/theFlukaName/outProd_${CLUSTER}_${PROCESS}.dat/ ./runProd.mac
source /lbne/app/users/lebrun/work2/dev2/repo/setups/setup_g4lbne_fnal.sh
time /lbne/app/users/lebrun/work2/dev2/repo/g4lbneProd2 ./runProd.mac >& g4lbne_${CLUSTER}_${PROCESS}.lis
#
cp ./g4lbne_${CLUSTER}_${PROCESS}.lis /lbne/data/users/lebrun/fluka/
cp ./OutNFD_*.dk2nu.root /lbne/data/users/lebrun/fluka/
#

void makeFluxHistograms(std::string user = "beam", std::string version = "v3r0p1", std::string macro = "Nominal", std::string current = "200kA", std::string location
= "LBNEFD", std::string physics_list = "QGSP_BERT", std::string n_files = "250", std::string start_index = "1", std::string pot_per_file = "100000",std::string
			on_grid = "false",std::string user_position_x = "0",std::string user_position_y = "0",std::string user_position_z = "0", std::string dk2nu="false") {

  std::string G4LBNEWORKDIR = getenv("G4LBNEWORKDIR");
  std::string DK2NU = getenv("DK2NU");


  gROOT->ProcessLine((".X "+DK2NU+"/scripts/load_dk2nu.C").c_str());

  gROOT->ProcessLine((".L "+G4LBNEWORKDIR+"/ProductionScripts/eventRates.C+").c_str());

  //gROOT->ProcessLine((".L test_read_dk2nu.C+"));

  std::string command = "eventRates t(\""+user+"\", \""+version+"\", \""+macro+"\", \""+current+"\", \""+location+"\", \""+physics_list+"\", "+n_files+", "+start_index+", "+pot_per_file+",   "+on_grid+", "+user_position_x+", "+user_position_y+", "+user_position_z+", "+dk2nu+")";

  std::cout<<"Executing: "<<command<<std::endl;

   gROOT->ProcessLine(command.c_str());


   gROOT->ProcessLine("t.Loop()");

}

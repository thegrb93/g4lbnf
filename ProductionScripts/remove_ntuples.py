import sys,os

filepath = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r0p10/"

# Make sure user really wants to do this
input_variable = raw_input ("Deleting all ntuples in path "+filepath+"\n\t Are you sure [Y/n]: ")

if input_variable != "Y":
   print "You didn't enter 'Y' -- quitting"
   sys.exit()

# Walk over files, find ntuples, delete them
for dirpath, subdirs, files in os.walk(filepath):
    for file in files:
        if file.startswith("g4lbne") and file.endswith(".root"):
            print file
            os.remove(os.path.join(dirpath,file))

print "All done."

#! /bin/bash
#
# arguments:
#     CLUSTER : The cluster number, A user-defined number
#     NUMJOB: the number of nexus output files to concatenate 
if [ $# -eq 1 ]; then
    export NUMJOBS=$1 
else
    echo "Usage : "
    echo "  $ runCyl_submit.sh NUMJOB "
    exit
fi

#source /grid/fermiapp/products/next/setups.sh
#setup jobsub_client

#jobsub_submit -G fermilab -M -e SEED_BASE -N $QUEUECOUNT file://$PWD/run_nexus.sh &> run_nexus.log
#jobsub_submit -G fermilab -M -e SEED_BASE -N $QUEUECOUNT --opportunistic file://$PWD/run_nexus.sh &> run_nexus.log
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runCyl.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCyl.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT1.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT2.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runCylT2.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT2.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT3F.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT3.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT3_3.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT4_4.sh
#jobsub_submit -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT5.sh
#jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT6.sh
#jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT4_4.sh
#jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT3_3.sh
#jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylT5.sh
# Feb. 3 14:31 
#jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runCylT2Thin.sh
# Feb. 3 14:32 
jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runCylT2Thick.sh
# Feb. 3 14:35 
jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylThick.sh
# Feb. 3 14:35
jobsub_submit --use_gftp -G lbne -N ${NUMJOBS}   --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC  file://$PWD/runNoFlukaCylThin.sh

exit

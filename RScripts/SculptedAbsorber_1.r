#
# Some studies of the muon energy loss patterns in the Sculpted Abosrber. 
#
dirPnfs <- "/pnfs/lbne/scratch/users/lebrun/LBNFSculptHA1d/";
dirScratch <- "/scratch/lbne/lebrun/"
jtitleHA <- "MuonFluxAtSculptedAbsorber_SculptedAbsorberProd1_";
#jCluster <- 1757280;
#jCluster <- 1817848;
jCluster <- 2015073;
#
# copy first on /scratch
system("date");
numFiles <- 25;
for (kf in 1:numFiles) {    
  cmd <- sprintf("ifdh cp %s%s%d_%d.txt %s", dirPnfs, jtitleHA, jCluster, (kf-1), dirScratch);
  system(cmd);
}

system("date");
for (kf in 1:numFiles) {  
  afNameHA <- sprintf("%s%s%d_%d.txt", dirScratch, jtitleHA, jCluster, (kf-1));
  if(kf == 1) {
   tab5rowsHA <- read.table(afNameHA, header = TRUE, nrows = 5);
   classesHA <- sapply(tab5rowsHA, class);
  }
#  mDTmp <- read.table(afNamePR, h=T, colClasses = classesPR);
  mDTmp <- read.table(afNameHA, h=T);
  if(kf == 1) {
    mD <- mDTmp;
  } else {
    mD <- merge(mD, mDTmp, all=T);
  }
}
system("date");
write.table(mD, "/lbne/data/users/lebrun/SculptedAbsorber/MDProd1_2015073.tab");
mDIn <- subset(mD, (mD$slice == -2));
mDSl3 <- subset(mD, (mD$slice == 3));
mDSl8 <- subset(mD, (mD$slice == 8));
rrSl8 <- sqrt(mDSl8$x*mDSl8$x + mDSl8$y*mDSl8$y);
mDEnd <- subset(mD, (mD$slice == 30));
rrIn <- sqrt(mDIn$x*mDIn$x + mDIn$y*mDIn$y);
rrEnd <- sqrt(mDEnd$x*mDEnd$x + mDEnd$y*mDEnd$y);
# Look for muons that are detect at slice 30, but not slice -2 
createdMuonBit <- rep(0, times=length(mD$x));
#createdSliceM2 <- rep(0, times=length(mD$x));
i <- 1;
nTotp1 <- length(mD$x)+1;
nTot <- length(mD$x);
done <- 0;
while(i < nTotp1) {
#   msg <- sprintf(" I line evt %d tr %d slide %d ", mD$evt[i] , mD$tr[i], mD$slice[i]);
#   print(msg);
#   if (mD$evt[i] > 50) break;
   gotSliceM2 <- 0; gotSliceE <- 0;
   if ((i == 1) && (mD$slice[i] == -2)) gotSliceM2 <- 1;
   newTrack <- 0;
   if ((i > 1) && (mD$tr[i-1] != mD$tr[i]) && (mD$evt[i-1] == mD$evt[i])) newTrack <- 1;
   if ((i > 1) && (mD$evt[i-1] != mD$evt[i])) newTrack <- 1;
   if ((newTrack == 1) && (mD$slice[i] == -2)) gotSliceM2 <- 1;
   if (mD$slice[i] ==  -2) gotSliceM2 <- 1;
   for (j in (i+1:length(mD$x))) {
     if (j >= nTot) { done <- 1; break; }
     newTrackJ <- 0;
     if ((j > 1) && (mD$tr[j-1] != mD$tr[j]) && (mD$evt[j-1] == mD$evt[j])) newTrackJ <- 1;
     if ((j > 1) && (mD$evt[j-1] != mD$evt[j])) newTrackJ <- 1;
#     msg <- sprintf(" J line evt %d tr %d slide %d NewTrJ %d gotSliceM2 %d ", mD$evt[j] , mD$tr[j], mD$slice[j], newTrackJ, gotSliceM2 );
#     print(msg);
     if (newTrackJ == 1) {
       i <- j;
       break;
     }
     if ((gotSliceM2 == 0) && (mD$slice[j] > 2)) {
        createdMuonBit[j] <- 1;
#	msg <- sprintf("  -- Delayed muon, evt %d tr %d slide %d ", mD$evt[j] , mD$tr[j], mD$slice[j]);
#	print(msg);
     }  
   }
   if (done == 1) break;
}
#
mDCr <- subset(mD, (createdMuonBit == 1));
write.table(mDCr, "/lbne/data/users/lebrun/SculptedAbsorber/MDCrProd1_2015073.tab");
mDCrEnd <- subset(mDCr, (mDCr$slice == 30));
mDCrSl3 <- subset(mDCr, (mDCr$slice == 3));
rrCrEnd <- sqrt(mDCrEnd$x*mDCrEnd$x + mDCrEnd$y*mDCrEnd$y);
#
mDSl5 <- subset(mD, (mD$slice == 5));
deltaEk4to5 <- rep(-1., time=length(mDSl5$ek));
rCoord4to5 <- rep(-1.e10, time=length(mDSl5$ek)); 
done <- 0;
k5 <- 1;
for (i in 2:length(mD$ek)) {
    if (mD$slice[i] != 5) next;
    im1 <- i-1;
    if (mD$slice[im1] != 4) next;
    if (mD$evt[im1] != mD$evt[i]) next;
    if (mD$tr[im1] != mD$tr[i]) next;
    deltaEk4to5[k5] <- mD$ek[im1] - mD$ek[i];
    xx45 <- 0.5*(mD$x[i] + mD$x[im1]);
    yy45 <- 0.5*(mD$y[i] + mD$y[im1]);
    rCoord4to5[k5] <- sqrt(xx45*xx45 + yy45*yy45);
    k5 <- k5+1;
    if ((i %% 100) == 0)  print(sprintf("At i %d", i));
}
deltaEk4to5 <- deltaEk4to5[1:k5-1]; 
rCoord4to5 <- rCoord4to5[1:k5-1];
#
#plot(rCoord4to5,  deltaEk4to5, type='p', pch='.');
#plot(rCoord4to5,  deltaEk4to5, type='p', pch='.', ylim=c(0., 0.25), xlab = " R from beam axis", ylab = "Energy Loss (GeV)")
#   
mDSl12 <- subset(mD, (mD$slice == 12));
deltaEk11to12 <- rep(-1., time=length(mDSl12$ek));
rCoord11to12 <- rep(-1.e10, time=length(mDSl12$ek)); 
xCoord11to12 <- rep(-1.e10, time=length(mDSl12$ek)); 
done <- 0;
k12 <- 1;
for (i in 2:length(mD$ek)) {
    if (mD$slice[i] != 12) next;
    im1 <- i-1;
    if (mD$slice[im1] != 11) next;
    if (mD$evt[im1] != mD$evt[i]) next;
    if (mD$tr[im1] != mD$tr[i]) next;
    deltaEk11to12[k12] <- mD$ek[im1] - mD$ek[i];
    xx1112 <- 0.5*(mD$x[i] + mD$x[im1]);
    yy1112 <- 0.5*(mD$y[i] + mD$y[im1]);
    rCoord11to12[k12] <- sqrt(xx1112*xx1112 + yy1112*yy1112);
    xCoord11to12[k12] <- xx1112;
    k12 <- k12+1;
    if ((i %% 100) == 0)  print(sprintf("At i %d", i));
}
deltaEk11to12 <- deltaEk11to12[1:k12-1]; 
rCoord11to12 <- rCoord11to12[1:k12-1];
xCoord11to12 <- xCoord11to12[1:k12-1];
#

#plot(rCoord11to12,  deltaEk11to12, type='p', pch='.', ylim=c(0., 0.25), xlab = " R from beam axis", ylab = "Energy Loss (GeV)");
#
   

#
#  First, run the C++ ./SculptedAbsorberCompress C++ script.  Runs much faster than R 
#  second, doing some plots of what is oberved in the back..  
#
library(plotrix);
mDCentered <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1e_Sl30_2048727.txt", h=T)
mDCrCentered <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1e_Sl30Cr_2048727.txt", h=T);
hhCeXAll <- weighted.hist(mDCentered$x, mDCentered$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCeCrXAll <- weighted.hist(mDCrCentered$x, mDCrCentered$w, seq(from=-200., to = 200., by = 10.), plot=F);
#
# Offset by 25 mRad... 
#
#mD <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1f_Sl30_2071904.txt", h=T)
#mDCr <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1f_Sl30Cr_2071904.txt", h=T)
#
# Offset by 50 mRad... 
#
mD <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1g_Sl30_2109228.txt", h=T)
mDCr <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNFSculptHA1g_Sl30Cr_2109228.txt", h=T)
mDL <- subset(mD , (mD$ek < 0.5)); 
mDLL <- subset(mD , (mD$ek < 0.1)); 
mDCrL <- subset(mD , (mD$ek < 0.5)); 
mDCrLL <- subset(mD , (mD$ek < 0.1)); 
hhXAll <- weighted.hist(mD$x, mD$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCrXAll <- weighted.hist(mDCr$x, mDCr$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhXAllCoordPos <- hhXAll$mids[21:40]
hhXAllValuesPos <- hhXAll$counts[21:40]
hhXAllValuesNeg <- rep(0., times=20);
for (k in 1:20) { hhXAllValuesNeg[k] <- hhXAll$counts[21-k]; }
hhCrXAll <- weighted.hist(mDCr$x, mDCr$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCrXAllValuesPos <- hhCrXAll$counts[21:40]
hhCrXAllValuesNeg <- hhCrXAll$counts[1:20]
for (k in 1:20) { hhCrXAllValuesNeg[k] <- hhCrXAll$counts[21-k]; }
#
hhYAll <- weighted.hist(mD$y, mD$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCrYAll <- weighted.hist(mDCr$y, mDCr$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhYAllCoordPos <- hhYAll$mids[21:40]
hhYAllValuesPos <- hhYAll$counts[21:40]
hhYAllValuesNeg <- rep(0., times=20);
for (k in 1:20) { hhYAllValuesNeg[k] <- hhYAll$counts[21-k]; }
hhCrYAll <- weighted.hist(mDCr$y, mDCr$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCrYAllValuesPos <- hhCrYAll$counts[21:40];
hhCrYAllValuesNeg <- hhCrYAll$counts[1:20];
for (k in 1:20) { hhCrYAllValuesNeg[k] <- hhCrYAll$counts[21-k]; }
#
poT <- 50*150000;
#
plot(hhXAll$mids, hhXAll$counts, type='b', pch=16, col="blue", ylim=c(0., 60000.), xlab="X [cm]", ylab="Counts/10cm/7.5MPOT" );
lines(hhCrXAll$mids, 5*hhCrXAll$counts, type='b', pch=17, col="red");
legend(x="topright", ncol=1, c("All Muons", "Muons from HA, X5"), pch=c(16, 17), col=c("blue", "red")); 
#
#
plot(hhXAllCoordPos, hhXAllValuesNeg, type='b', pch=16, col="cyan", ylim=c(0., 60000.), xlab="X [cm]", ylab="Counts/10cm/7.5MPOT" );
lines(hhXAllCoordPos, hhXAllValuesPos, type='b', pch=17, col="darkblue");
lines(hhXAllCoordPos, 5.0*hhCrXAllValuesNeg, type='b', pch=18, col="magenta");
lines(hhXAllCoordPos, 5.0*hhCrXAllValuesPos, type='b', pch=15, col="darkred");
legend(x="topright", ncol=1,
      c("All, X < 0.", "All, X > 0. ", "from HA, X5, X < 0.", "from HA, X5, X > 0."), 
       pch=c(16, 17, 18, 15), col=c("cyan", "darkblue", "magenta", "darkred")); 
#
plot(hhXAll$mids, hhCrXAll$counts/hhXAll$counts, type='b', pch=15, col="black", xlab="X [cm]", ylab="Ratio" );
#
hhYAll <- weighted.hist(mD$y, mD$w, seq(from=-200., to = 200., by = 10.), plot=F);
hhCrYAll <- weighted.hist(mDCr$y, mDCr$w, seq(from=-200., to = 200., by = 10.), plot=F);
plot(hhYAll$mids, hhYAll$counts, type='b', pch=16, col="blue", ylim=c(0., 60000.), xlab="Y [cm]", ylab="Counts/10cm/7.5MPOT" );
lines(hhCrYAll$mids, 5*hhCrYAll$counts, type='b', pch=17, col="red");
#
#
plot(hhYAllCoordPos, hhYAllValuesNeg, type='b', pch=16, col="cyan", ylim=c(0., 60000.), xlab="Y [cm]", ylab="Counts/10cm/7.5MPOT" );
lines(hhYAllCoordPos, hhYAllValuesPos, type='b', pch=17, col="darkblue");
lines(hhYAllCoordPos, 5.0*hhCrYAllValuesNeg, type='b', pch=18, col="magenta");
lines(hhYAllCoordPos, 5.0*hhCrYAllValuesPos, type='b', pch=15, col="darkred");
legend(x="topright", ncol=1,
      c("All, Y < 0.", "All, Y > 0. ", "from HA, X5, Y < 0.", "from HA, X5, Y > 0."), 
       pch=c(16, 17, 18, 15), col=c("cyan", "darkblue", "magenta", "darkred")); 
#
plot(hhYAll$mids, hhCrYAll$counts/hhYAll$counts, type='b', pch=15, col="black", xlab="Y [cm]", ylab="Ratio" );
#
rrEnd <- sqrt(mD$x*mD$x + mD$y*mD$y);
mDRIn <- subset(mD, (rrEnd < 20.));
mDRMed <- subset(mD, ((rrEnd > 20.) & (rrEnd < 100.)));
mDROut <- subset(mD, (rrEnd > 100.));
#
rrCrEnd <- sqrt(mDCr$x*mDCr$x + mDCr$y*mDCr$y);
mDCrRIn <- subset(mDCr, (rrCrEnd < 20.));
mDCrRMed <- subset(mDCr, ((rrCrEnd > 20.) & (rrCrEnd < 100.)));
mDCrROut <- subset(mDCr, (rrCrEnd > 100.));
#
hhEkRIn <-  weighted.hist(mDRIn$ek, mDRIn$w, breaks=seq(0., 100., by=0.25), plot=F);
hhEkRMed <-  weighted.hist(mDRMed$ek, mDRMed$w, breaks=seq(0., 100., by=0.25), plot=F);
hhEkROut <-  weighted.hist(mDROut$ek, mDROut$w, breaks=seq(0., 100., by=0.25), plot=F);
#
hhCrEkRIn <-  weighted.hist(mDCrRIn$ek, mDCrRIn$w, breaks=seq(0., 100., by=0.25), plot=F);
hhCrEkRMed <-  weighted.hist(mDCrRMed$ek, mDCrRMed$w, breaks=seq(0., 100., by=0.25), plot=F);
hhCrEkROut <-  weighted.hist(mDCrROut$ek, mDCrROut$w, breaks=seq(0., 100., by=0.25), plot=F);
#
SIn <- 0.2*0.2*pi;
SMed <- 1.0*pi - SIn;
SOut <- 4.0*pi - SMed;
#
ymax <- max(max(hhEkRIn$counts/(poT*SIn)), max(hhEkRIn$counts/(poT*SMed)), max(hhEkROut$counts/(poT*SOut)));
plot(hhEkRIn$mids, hhEkRIn$counts/(poT*SIn), type='b', pch=15, col="red", log='y', 
        xlab="Kinetic Energy [GeV]", ylab="Counts/250MeV/PoT/m^2", xlim=c(0., 10.), ylim=c(1.0e-5, ymax) )
lines(hhEkRMed$mids, hhEkRMed$counts/(poT*SMed), type='b', pch=16, col="magenta");
lines(hhEkROut$mids, hhEkROut$counts/(poT*SOut), type='b', pch=17, col="blue");
legend(x="topright", ncol=1, c("R < 20 cm", " 20 < R < 100 cm ", " R > 1 m "), 
                    pch=c(15, 16, 17), col=c("red", "magenta", "blue")); 

ymax <- max(max(hhCrEkRIn$counts/(poT*SIn)), max(hhCrEkRIn$counts/(poT*SMed)), max(hhCrEkROut$counts/(poT*SOut)));
plot(hhCrEkRIn$mids, hhCrEkRIn$counts/(poT*SIn), type='b', pch=15, col="red", log='y', 
        xlab="Kinetic Energy [GeV]", ylab="Counts/250MeV/PoT/m^2", xlim=c(0., 10.), ylim=c(1.0e-7, ymax) )
lines(hhCrEkRMed$mids, hhCrEkRMed$counts/(poT*SMed), type='b', pch=16, col="magenta");
lines(hhCrEkROut$mids, hhCrEkROut$counts/(poT*SOut), type='b', pch=17, col="blue");
legend(x="topright", ncol=1, c("R < 20 cm", " 20 < R < 100 cm ", " R > 1 m "), 
                    pch=c(15, 16, 17), col=c("red", "magenta", "blue")); 


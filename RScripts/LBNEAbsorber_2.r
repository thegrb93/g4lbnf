#
# Read the tables produced by SculptedAbsorber 1, and make weighted histograms of momentum 
#
library("plotrix");
mD <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNEAbsorbV1a_Sl30_2181709.txt", h=T)
mDCr <- read.table("/lbne/data/users/lebrun/SculptedAbsorber/MuonFluxAtSculptedAbsorber_LBNEAbsorbV1a_Sl30Cr_2181709.txt", h=T)
rrD <- sqrt(mD$x*mD$x + mD$y*mD$y);
rrDCr <- sqrt(mDCr$x*mDCr$x + mDCr$y*mDCr$y);
poT <- (200-76)*150000; # 76 files missing 
#
hhR <-  weighted.hist(rrD, mD$w, breaks=seq(0., 400., by=10.), plot=F);
hhRCr <-  weighted.hist(rrDCr, mDCr$w, breaks=seq(0., 400., by=10.), plot=F);
plot(hhR$mids, hhR$counts/(poT*pi*hhR$mids*10), type='p', pch=16, col="blue",
        xlab="R [cm]", ylab="Counts/cmsq/PoT", xlim=c(0., 380.), ylim=c(0., 3.0e-6))
lines(hhRCr$mids, 4*hhRCr$counts/(poT*pi*hhRCr$mids*10), type='p', pch=17, col="red");
legend(x="topright", ncol=1, c("All", "Created in HA, X4"), pch=c(16,17), col=c("blue", "red")); 
#
hhX <-  weighted.hist(mD$x, mD$w, breaks=seq(-200., 200., by=10.), plot=F);
hhXCr <-  weighted.hist(mDCr$x, mDCr$w, breaks=seq(-200., 200., by=10.), plot=F);
plot(hhX$mids, hhX$counts*7.5e6/(poT), type='b', pch=16, col="blue",
        xlab="X [cm]", ylab="Counts/10cm/(7.5 miilion PoT)", xlim=c(-200, 200.), ylim=c(0., 20000.))
lines(hhXCr$mids, 5.*hhXCr$counts*7.5e6/(poT), type='b', pch=17, col="red");
legend(x="topright", ncol=1, c("All", "Created in HA, X5"), pch=c(16,17), col=c("blue", "red")); 
#
plot(hhX$mids, hhXCr$counts/hhX$counts, type='b', pch=15, col="black",
        xlab="X [cm]", ylab="Ratio, Counts/10cm/(7.5 miilion PoT)", xlim=c(-200, 200.))

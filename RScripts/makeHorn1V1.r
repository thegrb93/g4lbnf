#
# A semi parabolic Horn1
#
zz <- rep(0., times=15);
rr <- rep(20., times=15);
tt <- rep(2.5, times=15);
rr[1] <- 40. ;  
zz[2] <- 12.5;
rr[2] <- 19.0;
zz[3] <- 850.; # The end of the neck
rr[3] <- 19.0;
zStep <- (3500. - 850.0)/15.;
rrStep = 9.5;
for (i in 4:15) {
  rr[i] = rr[i-1] + ((14 - i)*(14 - i)/12)*rrStep; 
  zz[i] = 850 + i*zStep;
  tt[i] = 2.5 - (((i-3)*(i-3))/12.)*0.06; 
}
plot(zz, rr, type='b', pch='*', ylim=c(0., max(rr)))
lines(zz, tt*100., type='b', pch='o', lwd=2, col="green");
hStr1 <- "/LBNE/det/Horn1PolyPt";
hStr2 <- "RinThickZ";
for (i in 1:length(zz)) {
  tStr <- sprintf("%s%d%s  %7.3f %7.3f %10.2f ", hStr1, (i-1), hStr2, rr[i], tt[i], zz[i]);
  print(tStr);
}
aPdf <- pdf("./Horn1SemiParabolic_1.pdf", width=7., height=5., family="serif", useDingbats=F);
plot(zz, rr, type='b', pch='*', ylim=c(0., max(rr)))
lines(zz, tt*100., type='b', pch='o', lwd=2, col="green");
legend(x="topleft", ncol=1, c("Inner-inner radius (mm)", "Thickness X 100"), lty=c(1,2), pch=c('*', 'o'), col=c("black", "green")); 
dev.off();

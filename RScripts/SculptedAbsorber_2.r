#
# Read the tables produced by SculptedAbsorber 1, and make weighted histograms of momentum 
#
library("plotrix");
afName <- "/lbne/data/users/lebrun/SculptedAbsorber/MDProd1_2015073.tab";
afNameCR <- "/lbne/data/users/lebrun/SculptedAbsorber/MDCrProd1_2015073.tab";
tab5rowsHA <- read.table(afName, header = TRUE, nrows = 5);
classesHA <- sapply(tab5rowsHA, class);
#mD <- read.table(afName, colClasses = classesHA, header = TRUE);
#mDCr <- read.table(afNameCR, colClasses = classesHA, header = TRUE);
mD <- read.table(afName,  header = TRUE);
mDCr <- read.table(afNameCR, header = TRUE);
mDIn <- subset(mD, (mD$slice == -2));
mDEnd <- subset(mD, (mD$slice == 30));
mDCrEnd <- subset(mDCr, (mDCr$slice == 30));
rrIn <- sqrt(mDIn$x*mDIn$x + mDIn$y*mDIn$y);
rrEnd <- sqrt(mDEnd$x*mDEnd$x + mDEnd$y*mDEnd$y);
rrCrEnd <- sqrt(mDCrEnd$x*mDCrEnd$x + mDCrEnd$y*mDCrEnd$y);
poT <- 25*150000;
#
hhEkIn <-  weighted.hist(mDIn$ek, mDIn$w, breaks=seq(0., 150., by=0.1), plot=F);
hhEkEnd <-  weighted.hist(mDEnd$ek, mDEnd$w, breaks=seq(0., 150., by=0.1), plot=F);
plot(hhEkIn$mids, hhEkIn$counts/poT, type='p', pch=16, col="blue",
        xlab="Kinetic Energy [GeV]", ylab="Counts/100MeV/PoT", xlim=c(0., 10.))
lines(hhEkEnd$mids, 4.0*hhEkEnd$counts/poT, type='p', pch=17, col="red");
legend(x="topright", ncol=1, c("Front of HA", "Back of HA, X 4"), pch=c(16,17), col=c("blue", "red")); 
#
mDInRIn <- subset(mDIn, (rrIn < 200.));
mDInRMed1 <- subset(mDIn, ((rrIn > 200.) & (rrIn < 500.) ));
mDInRMed2 <- subset(mDIn, ((rrIn > 500.) & (rrIn < 1000.) ));
mDInROut <- subset(mDIn, (rrIn > 1000.));
#
mDEndRIn <- subset(mDEnd, (rrEnd < 200.));
mDEndRMed1 <- subset(mDEnd, ((rrEnd > 200.) & (rrEnd < 500.)));
mDEndRMed2 <- subset(mDEnd, ((rrEnd > 500.) & (rrEnd < 1000.)));
mDEndROut <- subset(mDEnd, (rrEnd > 1000.));
#
hhEkInRIn <-  weighted.hist(mDInRIn$ek, mDInRIn$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkInRMed1 <-  weighted.hist(mDInRMed1$ek, mDInRMed1$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkInRMed2 <-  weighted.hist(mDInRMed2$ek, mDInRMed2$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkInROut <-  weighted.hist(mDInROut$ek, mDInROut$w, breaks=seq(0., 100., by=0.1), plot=F);
#
plot(hhEkInRIn$mids, 100.0*hhEkInRIn$counts/poT, type='p', pch=15, col="red",
        xlab="Kinetic Energy [GeV]", ylab="Counts/100MeV/PoT/S", xlim=c(0., 10.), )
lines(hhEkInRMed1$mids, 19.05*hhEkInRMed1$counts/poT, type='p', pch=16, col="magenta");
lines(hhEkInRMed2$mids, 5.3333*hhEkInRMed2$counts/poT, type='p', pch=17, col="blue");
lines(hhEkInROut$mids, 1.333*hhEkInROut$counts/poT, type='p', pch=18, col="cyan");
legend(x="topright", ncol=2, c("R < 20 cm", " 20 < R < 50 cm ", " 0.5 < R < 1. m", " R > 1 m "), 
                    pch=c(15, 16, 17, 18), col=c("red", "magenta", "blue", "cyan")); 

#
hhEkEndRIn <-  weighted.hist(mDEndRIn$ek, mDEndRIn$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkEndRMed1 <-  weighted.hist(mDEndRMed1$ek, mDEndRMed1$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkEndRMed2 <-  weighted.hist(mDEndRMed2$ek, mDEndRMed2$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkEndROut <-  weighted.hist(mDEndROut$ek, mDEndROut$w, breaks=seq(0., 100., by=0.1), plot=F);
#
plot(hhEkEndRIn$mids, 100.0*hhEkEndRIn$counts/poT, type='p', pch=15, col="red",
        xlab="Kinetic Energy [GeV]", ylab="Counts/100MeV/PoT/S", xlim=c(0., 10.), )
lines(hhEkEndRMed1$mids, 19.05*hhEkEndRMed1$counts/poT, type='p', pch=16, col="magenta");
lines(hhEkEndRMed2$mids, 5.3333*hhEkEndRMed2$counts/poT, type='p', pch=17, col="blue");
lines(hhEkEndROut$mids, 1.333*hhEkEndROut$counts/poT, type='p', pch=18, col="cyan");
legend(x="topright", ncol=2, c("R < 20 cm", " 20 < R < 50 cm ", " 0.5 < R < 1. m", " R > 1 m "), 
                    pch=c(15, 16, 17, 18), col=c("red", "magenta", "blue", "cyan")); 
#

mDCrEndRIn <- subset(mDCrEnd, (rrCrEnd < 200.));
mDCrEndRMed1 <- subset(mDCrEnd, ((rrCrEnd > 200.) & (rrCrEnd < 500.)));
mDCrEndRMed2 <- subset(mDCrEnd, ((rrCrEnd > 500.) & (rrCrEnd < 1000.)));
mDCrEndROut <- subset(mDCrEnd, (rrCrEnd > 1000.));
#
#
hhEkCrEndRIn <-  weighted.hist(mDCrEndRIn$ek, mDCrEndRIn$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkCrEndRMed1 <-  weighted.hist(mDCrEndRMed1$ek, mDCrEndRMed1$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkCrEndRMed2 <-  weighted.hist(mDCrEndRMed2$ek, mDCrEndRMed2$w, breaks=seq(0., 100., by=0.1), plot=F);
hhEkCrEndROut <-  weighted.hist(mDCrEndROut$ek, mDCrEndROut$w, breaks=seq(0., 100., by=0.1), plot=F);
#
plot(hhEkCrEndRIn$mids, 100.0*hhEkCrEndRIn$counts/poT, type='p', pch=15, col="red",
        xlab="Kinetic Energy [GeV]", ylab="Counts/100MeV/PoT/S", xlim=c(0., 10.), )
lines(hhEkCrEndRMed1$mids, 19.05*hhEkCrEndRMed1$counts/poT, type='p', pch=16, col="magenta");
lines(hhEkCrEndRMed2$mids, 5.3333*hhEkCrEndRMed2$counts/poT, type='p', pch=17, col="blue");
lines(hhEkCrEndROut$mids, 1.333*hhEkCrEndROut$counts/poT, type='p', pch=18, col="cyan");
legend(x="topright", ncol=2, c("R < 20 cm", " 20 < R < 50 cm ", " 0.5 < R < 1. m", " R > 1 m "), 
                    pch=c(15, 16, 17, 18), col=c("red", "magenta", "blue", "cyan")); 
#


import ROOT

# Helper function that makes a error histogram out of a bunch of universes
# by calculating rms of universes in each bin
def MakeErrorHistogram(universes,asFrac=True):
    n_universes = len(universes)
    avg_hist = universes[0].Clone(universes[0].GetName()+"_avg")
    error_hist = universes[0].Clone(universes[0].GetName()+"_error")
    for i in range(n_universes):
        universes[i].Print()
        avg_hist.Add(universes[i])
    avg_hist.Scale(1/float(n_universes))

    # Calculate Error = RMS of universes
    for j in range(error_hist.GetNbinsX()+2):
        error = 0
        for i in range(n_universes):
            error += 1/float(n_universes)*ROOT.TMath.Power(universes[i].GetBinContent(j)-avg_hist.GetBinContent(j),2)
        error_hist.SetBinContent(j,ROOT.TMath.Sqrt(error))
    if asFrac:
        error_hist.Divide(avg_hist)
    return error_hist

# Open ppfx output file
in_file = ROOT.TFile("g4numi_mipp0_na490_0_120_14_dk2nudune.root")

# Get the central value flux out
cv_flux = in_file.Get("nom/hcv_numu")

# Get the error universes out
n_universes = 100
total_error_universes = []
thin_target_univereses = []
mipp_numu_universes = []
attenuation_universes = []
other_univereses = []

for i in range(n_universes):

    total_error_universes.append(in_file.Get("numu_total/htotal_numu_"+str(i)))

error_universes = MakeErrorHistogram(total_error_universes)

c1 = ROOT.TCanvas()
error_universes.Draw()
c1.Print("total_error.eps")
c1.Print("total_error.png")


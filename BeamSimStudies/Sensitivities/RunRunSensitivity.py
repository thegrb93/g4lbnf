import sys,os

if len(sys.argv)<5:
    print "Usage:",sys.argv[0]," <macro> <version> <proton_energy_in_GeV> <user_who_made_the_flux>"
    sys.exit()

macro = sys.argv[1]
version = sys.argv[2]
energy = sys.argv[3]
user = sys.argv[4]

fhc_file = "/dune/data/users/"+user+"/fluxfiles/g4lbne/"+version+"/QGSP_BERT/"+macro+"/neutrino/flux/histos_g4lbne_"+version+"_QGSP_BERT_"+macro+"_neutrino_LBNEFD_globes_flux.txt"
rhc_file = "/dune/data/users/"+user+"/fluxfiles/g4lbne/"+version+"/QGSP_BERT/"+macro+"/antineutrino/flux/histos_g4lbne_"+version+"_QGSP_BERT_"+macro+"_antineutrino_LBNEFD_globes_flux.txt"

pwd = os.getenv("PWD")

os.system("cd /dune/app/users/ljf26/lblpwgtools/code/sensitivity; ./RunSensitivity.sh -n "+macro+" -f "+fhc_file+" -r "+rhc_file+" -m "+energy+" -s 1; cd "+pwd)


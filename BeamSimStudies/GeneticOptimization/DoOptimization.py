import Optimizations, subprocess, re, math
"""
DoOptimization.py
Laura Fields
August 2014

This is the main control script for running the optimization.  
It does several tasks:

- Itentifies new configurations that are ready to be generated
- Crates g4lbne macros for each new configuration and submits them
- Identifies configurations that have completed and merges output histos
  (This step is done first so that the merged histos are available 
   for making future generations)

"""


import glob,sys, os 

if len(sys.argv) < 2:
    print "Usage: "+sys.argv[0]+" <optname>"
    sys.exit()

optname = sys.argv[1]

optimization = Optimizations.Optimization(optname)

##########################################
#
# Identify completed configurations and merge histograms
#
##########################################

configs = []
config_iter = optimization.getLastGoodConfig()+1
n_missing_configs = 0
while n_missing_configs < 15:
    temp_histo_path = "/pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optname+"-"+str(config_iter)

    if optimization.rhc_parameters_float_separate:
        temp_histo_path = "/pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optname+"FHC-"+str(config_iter)
    if os.path.exists(temp_histo_path):
        configs.append(temp_histo_path)
    else:
        n_missing_configs = n_missing_configs + 1

    config_iter = config_iter + 1

print "Found",len(configs),"optimization histogram directories past the last good one"

if len(configs)>100:
    print "Found more than 100 configurations past the last good one."
    print "This is a problem that needs manual intervention (like manually changing the recorded last good configuration)... quitting"
    sys.exit()

configs_to_merge = []

for config in configs:

    # Skip over any configurations that were generated for a parameter scan
    if "Scan" in  config: 
        continue

    fhc_files = glob.glob(config+"/*/flux/histos/histos_*_neutrino_*_"+optimization.detector_location_name+"_fastmc.root")

    rhc_files = glob.glob(config.replace("FHC","RHC")+"/*/flux/histos/histos_*_antineutrino_*_"+optimization.detector_location_name+"_fastmc.root")

    configuration_number = config.split("/")[len(config.split("/"))-1].split("-")[2]

    print configuration_number,len(fhc_files),len(rhc_files)

    config = config.replace("/pnfs/lbne/scratch",optimization.output_location)
        
    if len(fhc_files) >= 4 and len(rhc_files) >= 4:
        merged_file = glob.glob(config+"/*/flux/histos_*_"+optimization.detector_location_name+"_fastmc.root")

        if optimization.rhc_parameters_float_separate:
            merged_file = merged_file+glob.glob(config.replace("FHC","RHC")+"/*/flux/histos_*_"+optimization.detector_location_name+"_fastmc_merged.root")

        if not len(merged_file) == 2 :
            configs_to_merge.append((configuration_number))
        
print "Merging",len(configs_to_merge),"configurations"
for config_number in configs_to_merge:
    if optimization.rhc_parameters_float_separate:
        os.system("python ../../ProductionScripts/merge_histograms.py --mode neutrino --n 100000  -m Optimizations-"+optname+"FHC-"+str(config_number)+" -l "+optimization.detector_location_name+" --input_dir /pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne --output_dir "+optimization.output_location+"users/ljf26/fluxfiles/g4lbne -p "+optimization.physics_list)
        os.system("python ../../ProductionScripts/merge_histograms.py --mode antineutrino --n 100000  -m Optimizations-"+optname+"RHC-"+str(config_number)+" -l "+optimization.detector_location_name+" --input_dir /pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne --output_dir "+optimization.output_location+"users/ljf26/fluxfiles/g4lbne -p "+optimization.physics_list)

    else:
        os.system("python ../../ProductionScripts/merge_histograms.py --mode neutrino --n 100000  -m Optimizations-"+optname+"-"+str(config_number)+" -l "+optimization.detector_location_name+" --input_dir /pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne  --output_dir "+optimization.output_location+"users/ljf26/fluxfiles/g4lbne -p "+optimization.physics_list)
        os.system("python ../../ProductionScripts/merge_histograms.py --mode antineutrino --n 100000  -m Optimizations-"+optname+"-"+str(config_number)+" -l "+optimization.detector_location_name+" --input_dir /pnfs/lbne/scratch/users/ljf26/fluxfiles/g4lbne  --output_dir "+optimization.output_location+"users/ljf26/fluxfiles/g4lbne -p "+optimization.physics_list)

##########################################
#
# Identify completed configurations that can be used for next generation
#
##########################################
# search for merged histos
configs_merged = [] 
n_dirs_with_merged_histos = 0
for config in configs:
    if "Scan" in  config: 
        continue

    config = config.replace("/pnfs/lbne/scratch",optimization.output_location)

    merged_file = glob.glob(config+"/*/flux/histos_*_"+optimization.detector_location_name+"_fastmc.root")

    configuration_number = config.split("/")[len(config.split("/"))-1].split("-")[2]
    
    if len(merged_file) > 0 :
        n_dirs_with_merged_histos = n_dirs_with_merged_histos + 1
        configs_merged.append((configuration_number))

print "Found ",len(configs_merged),"merged configurations"
configs_merged.sort(key=float)

##########################################
#
# Determine whether a new generation is ready to start
#
##########################################
n_good_configs_in_range = 0

first_config = optimization.getLastGoodConfig()+1
last_config = first_config+optimization.generation_size

if len(configs_merged)>optimization.generation_size-10:

    first_config = first_config + optimization.generation_size
    last_config = last_config + optimization.generation_size
else: 
    print "Not enough completed configurations to do another generation"
        
##########################################
#
# Make Macro files for next generation
#
##########################################
print "first last ",first_config,last_config

if(first_config == 1 and len(configs)==0):
    print "Making first generation macro files...."

if(len(configs_merged)>optimization.generation_size-10 or len(configs)==0):
    optimization.populateMacros(first_config,last_config)

    # store a record of the last complete configuration
    f = open(optimization.last_completed_config_file,"w")
    f.write(str(first_config-1))
    f.close()

##########################################
#
# Submit g4lbne jobs for next generation
#
##########################################
resubmit_last_generation = False


if(len(configs_merged) > (optimization.generation_size-10) or len(configs)==0 or resubmit_last_generation):
    current_directory = os.getcwd()
    os.chdir("../..")
    for i in range(int(first_config),int(last_config)):

        # look up off axis angle for this configuration
        detx = str(0)
        dety = str(0)
        detz = str(0)

        theta_y = optimization.getParameterValue("OffAxisAngleY",i)
        
        if(theta_y != -1):
            detx = str(0)
            dety = str(math.sin(theta_y)*129700000.0);
            detz = str(math.cos(theta_y)*129700000.0);
        
        if optimization.rhc_parameters_float_separate:
            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode neutrino -m Optimizations-"+optname+"FHC-"+str(i)+" --detx "+detx+" --dety "+dety+" --detz "+detz+" -p "+optimization.physics_list)
            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode antineutrino -m Optimizations-"+optname+"RHC-"+str(i)+" --detx "+detx+" --dety "+dety+" --detz "+detz+" -p "+optimization.physics_list)
#            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode neutrino -m Optimizations-"+optname+"FHC-"+str(i)+" --output_dir /pnfs/lbne/scratch/users/$USER/fluxfiles/g4lbne -p "+optimization.physics_list)
#            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode antineutrino -m Optimizations-"+optname+"RHC-"+str(i)+" --output_dir /pnfs/lbne/scratch/users/$USER/fluxfiles/g4lbne -p "+optimization.physics_list)
        else:
            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode neutrino -m Optimizations-"+optname+"-"+str(i)+" --detx "+detx+" --dety "+dety+" --detz "+detz+" -p "+optimization.physics_list)
            os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --notuples --mode antineutrino -m Optimizations-"+optname+"-"+str(i)+" --detx "+detx+" --dety "+dety+" --detz "+detz+" -p "+optimization.physics_list)
#            os.system("python ProductionScripts/submit_flux.py  -n 100000 -f 1 -l 5 --notuples --mode antineutrino -m Optimizations-"+optname+"-"+str(i)+" --output_dir /pnfs/lbne/scratch/users/$USER/fluxfiles/g4lbne -p "+optimization.physics_list)
#            os.system("python ProductionScripts/submit_flux.py  -n 100000 -f 1 -l 5 --notuples --mode antineutrino -m Optimizations-"+optname+"-"+str(i)+" --output_dir /pnfs/lbne/scratch/users/$USER/fluxfiles/g4lbne -p "+optimization.physics_list)
    os.chdir("BeamSimStudies/GeneticOptimization")


import os, random, sys
import subprocess, ROOT
from array import array
import OptimizationUtils

class Optimization:
    
    def __init__(self,optimizationName):

        if not optimizationName in ["CP_run5","CP_run7","CP_run8","CP_run9","CP_run10","CP_run11","CP_run12","CP_run13","CP_run14","CP_run15","CP_run16","CP_run17","CP_run18","CP_run19","CP_run20"]:
            print "I don't recognize the optimization name:",optimizationName
            sys.exit()

        self.optname = optimizationName
        self.parameter_names = []
        self.parameter_units = []
        self.parameter_lower_limits = []
        self.parameter_upper_limits = []
        self.macro_commands = []
        self.macro_set_stages = []
        self.roulette_wheel = []
        self.roulette_wheel_start_iter = -1;
        self.roulette_wheel_stop_iter = -1;
        self.knobturns = []

        self.physics_list = "QGSP_BERT"
        if self.optname in ["CP_run8"]:
            self.physics_list = "FTFP_BERT"

        self.detector_location_name = "LBNEFD"
        if self.optname in ["CP_run5"]:
            self.g4lbne_version = "v3r3p5"
        elif self.optname in ["CP_run7"]:
            self.g4lbne_version = "v3r3p9"
        elif self.optname in ["CP_run8"]:
            self.g4lbne_version = "v3r3p8"
        elif self.optname in ["CP_run9","CP_run10","CP_run11","CP_run12","CP_run13","CP_run14"]:
            self.g4lbne_version = "v3r3p9"
        elif self.optname in ["CP_run15","CP_run16","CP_run17"]:
            self.g4lbne_version = "v3r4"
        elif self.optname in ["CP_run18","CP_run19","CP_run20"]:
            self.g4lbne_version = "v3r4p1"

        if(self.optname=="CP_run5"):
            self.seed_offset = 50002
        if(self.optname=="CP_run7"):
            self.seed_offset = 49850
        if(self.optname=="CP_run8"):
            self.seed_offset = 39448
        if(self.optname=="CP_run9"):
            self.seed_offset = 29339
        if(self.optname=="CP_run10"):
            self.seed_offset = 34876
        if(self.optname=="CP_run11"):
            self.seed_offset = 49494
        if(self.optname=="CP_run12"):
            self.seed_offset = 49494
        if(self.optname=="CP_run13"):
            self.seed_offset = 49494
        if(self.optname=="CP_run14"):
            self.seed_offset = 9375
        if(self.optname=="CP_run15"):
            self.seed_offset = 2902277
        if(self.optname=="CP_run16"):
            self.seed_offset = 3937
        if(self.optname=="CP_run17"):
            self.seed_offset =  49494
        if(self.optname=="CP_run18"):
            self.seed_offset =  94875394
        if(self.optname=="CP_run19"):
            self.seed_offset =  94875392924
        if(self.optname=="CP_run20"):
            self.seed_offset =  3
    
        self.output_location = "/lbne/data2/"
        if self.optname in ["CP_run5"]:
            self.output_location = "/lbne/data/"

        self.last_completed_config_file = "/lbne/data/users/ljf26/Optimizations/"+self.optname+"_last_completed_config.dat"

        self.generation_size = 100

        self.rhc_parameters_float_separate = False
        if(self.optname=="CP_run4" or self.optname=="CP_run7" or self.optname=="CP_run8"):
            self.rhc_parameters_float_separate = True
    
        if(optimizationName=="CP_run5"):

            self.parameter_names.append("Horn1ICRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#Horn1ICRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(35)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#Horn1ICRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(75)
            self.macro_commands.append("#Horn1ICRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICRadius4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#Horn1ICRadius4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1OCRadius")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(800)
            self.macro_commands.append("#Horn1OCRadius")
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("Horn1ICLength1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(800)
            self.parameter_upper_limits.append(2500)
            self.macro_commands.append("#Horn1ICLength1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength5")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength6")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength6")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn1ICLength7")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#Horn1ICLength7")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("Horn2LongRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2LongRescale");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("Horn2RadialRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescale");
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("FHCHorn2RadialRescaleCst")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(-78);
            self.parameter_upper_limits.append(100);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescaleCst");
            self.macro_set_stages.append("PreInit")            
            
            self.parameter_names.append("Horn2LongPosition")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(3.0);
            self.parameter_upper_limits.append(15.0);
            self.macro_commands.append("/LBNE/det/Horn2LongPosition");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("GraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(1.75);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("GraphiteTargetFinWidth")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/GraphiteTargetFinWidth");
            self.macro_set_stages.append("PreInit")
            
            self.parameter_names.append("HornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(200);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(60);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")            

            self.parameter_names.append("OffAxisAngle")
            self.parameter_units.append("mrad");
            self.parameter_lower_limits.append(0);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("#OffAxisAngle")
            self.macro_set_stages.append("PreInit")            
        
        if(optimizationName=="CP_run7" or optimizationName=="CP_run8"):

            self.parameter_names.append("FHCHorn1ICRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#FHCHorn1ICRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(35)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#FHCHorn1ICRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(75)
            self.macro_commands.append("#FHCHorn1ICRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICRadius4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#FHCHorn1ICRadius4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1OCRadius")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(800)
            self.macro_commands.append("#FHCHorn1OCRadius")
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("FHCHorn1ICLength1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(800)
            self.parameter_upper_limits.append(2500)
            self.macro_commands.append("#FHCHorn1ICLength1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength5")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength6")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength6")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn1ICLength7")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#FHCHorn1ICLength7")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCHorn2LongRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2LongRescale");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("FHCHorn2RadialRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescale");
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("FHCHorn2RadialRescaleCst")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(-78);
            self.parameter_upper_limits.append(100);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescaleCst");
            self.macro_set_stages.append("PreInit")            
            
            self.parameter_names.append("FHCHorn2LongPosition")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(3.0);
            self.parameter_upper_limits.append(15.0);
            self.macro_commands.append("/LBNE/det/Horn2LongPosition");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("FHCGraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("FHCGraphiteTargetFinWidth")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/GraphiteTargetFinWidth");
            self.macro_set_stages.append("PreInit")
            
            self.parameter_names.append("FHCHornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("FHCProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")            

            self.parameter_names.append("RHCHorn1ICRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#RHCHorn1ICRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(35)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#RHCHorn1ICRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(75)
            self.macro_commands.append("#RHCHorn1ICRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICRadius4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#RHCHorn1ICRadius4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1OCRadius")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(800)
            self.macro_commands.append("#RHCHorn1OCRadius")
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("RHCHorn1ICLength1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(800)
            self.parameter_upper_limits.append(2500)
            self.macro_commands.append("#RHCHorn1ICLength1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength4")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength5")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength6")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength6")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn1ICLength7")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#RHCHorn1ICLength7")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCHorn2LongRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2LongRescale");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("RHCHorn2RadialRescale")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescale");
            self.macro_set_stages.append("PreInit")            

            self.parameter_names.append("RHCHorn2RadialRescaleCst")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(-78);
            self.parameter_upper_limits.append(100);
            self.macro_commands.append("/LBNE/det/Horn2RadialRescaleCst");
            self.macro_set_stages.append("PreInit")            
            
            self.parameter_names.append("RHCHorn2LongPosition")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(3.0);
            self.parameter_upper_limits.append(15.0);
            self.macro_commands.append("/LBNE/det/Horn2LongPosition");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("RHCGraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("RHCGraphiteTargetFinWidth")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/GraphiteTargetFinWidth");
            self.macro_set_stages.append("PreInit")
            
            self.parameter_names.append("RHCHornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("RHCProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")            

        if(optimizationName=="CP_run15" or optimizationName=="CP_run19"):

            self.parameter_names.append("HornALength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(1000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornALength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornAF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0.01)
            self.parameter_upper_limits.append(0.99)
            self.macro_commands.append("#HornAF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornARadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornARadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornARadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornBLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF5")
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("HornBRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(150)
            self.macro_commands.append("#HornBRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornBRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornBRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(17000)
            self.macro_commands.append("#HornBLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornCLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(150)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornCRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornCRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(4000)
            self.parameter_upper_limits.append(19000)
            self.macro_commands.append("#HornCLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("GraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2.0);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("GraphiteTargetFinWidth")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/GraphiteTargetFinWidth");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("BeamSigma")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(1.6);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/generator/beamSigmaX");
            self.macro_set_stages.append("Idle")
            
            self.parameter_names.append("HornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")  

        if(optimizationName=="CP_run20"):

            self.parameter_names.append("HornALength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(1000)
            self.parameter_upper_limits.append(6000)
            self.macro_commands.append("#HornALength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornAF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0.01)
            self.parameter_upper_limits.append(0.99)
            self.macro_commands.append("#HornAF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornARadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornARadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#HornARadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(6000)
            self.macro_commands.append("#HornBLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF5")
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("HornBRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(150)
            self.macro_commands.append("#HornBRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornBRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#HornBRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(30000)
            self.macro_commands.append("#HornBLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(6000)
            self.macro_commands.append("#HornCLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(150)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornCRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(1000)
            self.macro_commands.append("#HornCRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(4000)
            self.parameter_upper_limits.append(40000)
            self.macro_commands.append("#HornCLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("GraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(3.0);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("GraphiteTargetFinWidth")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/GraphiteTargetFinWidth");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("BeamSigma")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(1.6);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/generator/beamSigmaX");
            self.macro_set_stages.append("Idle")
            
            self.parameter_names.append("HornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")  


        if(optimizationName=="CP_run16" or optimizationName=="CP_run18"):

            self.parameter_names.append("HornALength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(1000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornALength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornAF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0.01)
            self.parameter_upper_limits.append(0.99)
            self.macro_commands.append("#HornAF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornARadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornARadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornARadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornBLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornBRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornBRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(17000)
            self.macro_commands.append("#HornBLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornCLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornCRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornCRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(4000)
            self.parameter_upper_limits.append(19000)
            self.macro_commands.append("#HornCLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("SimpleTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2.0);
            self.macro_commands.append("/LBNE/det/SimpleTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("SimpleTargetRadius")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(6);
            self.parameter_upper_limits.append(20);
            self.macro_commands.append("/LBNE/det/SimpleTargetRadius");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("BeamSigma")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(1.6);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/generator/beamSigmaX");
            self.macro_set_stages.append("Idle")
            
            self.parameter_names.append("HornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")  

        if(optimizationName=="CP_run17"):

            self.parameter_names.append("HornALength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(1000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornALength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornAF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0.01)
            self.parameter_upper_limits.append(0.99)
            self.macro_commands.append("#HornAF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornARadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornARadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornARadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornARadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornBLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornBF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornBRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(200)
            self.macro_commands.append("#HornBRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(200)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornBRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornBLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(17000)
            self.macro_commands.append("#HornBLongPosition")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLength")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(2000)
            self.parameter_upper_limits.append(4500)
            self.macro_commands.append("#HornCLength")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF1")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF2")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF3")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF4")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF4")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCF5")
            self.parameter_units.append("")
            self.parameter_lower_limits.append(0)
            self.parameter_upper_limits.append(1)
            self.macro_commands.append("#HornCF5")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius1")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius1")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius2")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(20)
            self.parameter_upper_limits.append(50)
            self.macro_commands.append("#HornCRadius2")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadius3")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(50)
            self.parameter_upper_limits.append(550)
            self.macro_commands.append("#HornCRadius3")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCRadiusOC")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(550)
            self.parameter_upper_limits.append(650)
            self.macro_commands.append("#HornCRadiusOC")
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("HornCLongPosition")
            self.parameter_units.append("mm")
            self.parameter_lower_limits.append(4000)
            self.parameter_upper_limits.append(19000)
            self.macro_commands.append("#HornCLongPosition")
            self.macro_set_stages.append("PreInit")

            # Seems that sphere target gets length from "GraphiteTargetLength""
            # even if its not graphite
            self.parameter_names.append("GraphiteTargetLength")
            self.parameter_units.append("m");
            self.parameter_lower_limits.append(0.5);
            self.parameter_upper_limits.append(2.0);
            self.macro_commands.append("/LBNE/det/GraphiteTargetLength");
            self.macro_set_stages.append("PreInit")
        
            self.parameter_names.append("MultiSphereTargetRadius")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(9);
            self.parameter_upper_limits.append(15);
            self.macro_commands.append("/LBNE/det/MultiSphereTargetRadius");
            self.macro_set_stages.append("PreInit")

            self.parameter_names.append("BeamSigma")
            self.parameter_units.append("mm");
            self.parameter_lower_limits.append(1.6);
            self.parameter_upper_limits.append(2.5);
            self.macro_commands.append("/LBNE/generator/beamSigmaX");
            self.macro_set_stages.append("Idle")
            
            self.parameter_names.append("HornCurrent")
            self.parameter_units.append("kA");
            self.parameter_lower_limits.append(150);
            self.parameter_upper_limits.append(300);
            self.macro_commands.append("/LBNE/det/seHornCurrent")
            self.macro_set_stages.append("PreInit")        

            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")  

        if(optimizationName == "power_test"):
            self.parameter_names.append("ProtonEnergy")
            self.parameter_units.append("GeV");
            self.parameter_lower_limits.append(40);
            self.parameter_upper_limits.append(130);
            self.macro_commands.append("/LBNE/primary/protonMomentum")
            self.macro_set_stages.append("Idle")            
        
        if(optimizationName=="antinufrac_test"):
            self.parameter_names.append("AntinuFraction")
            self.parameter_units.append("");
            self.parameter_lower_limits.append(0);
            self.parameter_upper_limits.append(1);
            # This doesn't actually do anything to the macro
            # but store it there so it can be looked up with everything else
            self.macro_commands.append("#AntiNu Fraction");
            self.macro_set_stages.append("PreInit");
            
        self.h_nom_fhcnumu = None
        self.h_nom_fhcnumubar = None
        self.h_nom_rhcnumu = None
        self.h_nom_rhcnumubar = None

    def getEnergy(self,iteration,mode):
        if not self.rhc_parameters_float_separate:
            power_iter = self.getParameterIndex("ProtonEnergy")
            if power_iter == -1: return 120.0
            params = self.readVariationFromMacros(iteration)
            return params[power_iter]
        else:
            power_iter = self.getParameterIndex(mode+"ProtonEnergy")
            if power_iter == -1: return 120.0
            params = self.readVariationFromMacros(iteration)
            return params[power_iter]

    def getNominalFitness(self,type="cp"):

        fitness = []
        fiterror = []

        if self.optname=="HE_test":
            nomdir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/"+self.physics_list+"/Nominal"
            nomfile = nomdir+"/200kA/flux/histos_g4lbne_v3r2p4_"+self.physics_list+"_Nominal_200kA_LBNEFD_fastmc.root"
            if not os.path.exists(nomfile):
                print "WARNING: could not extract nominal fitness"
            else:
                tempfile = ROOT.TFile(nomfile)
                temphisto = tempfile.Get("numu_flux")
                fiterror = [ROOT.Double(0)];
                fitness = [temphisto.IntegralAndError(5,40,fiterror)] # 0 to 10 GeV
                tempfile.Close()
        elif self.optname.startswith("ME_"):
            nomdir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/"+self.physics_list+"/Nominal"
            nomfile = nomdir+"/200kA/flux/histos_g4lbne_v3r2p4_"+self.physics_list+"_Nominal_200kA_LBNEFD_fastmc.root"
            if not os.path.exists(nomfile):
                print "WARNING: could not extract nominal fitness"
            else:
                tempfile = ROOT.TFile(nomfile)
                temphisto = tempfile.Get("numu_flux")
                fiterror = [ROOT.Double(0)];
                fitness = [temphisto.IntegralAndError(9,32,fiterror[0])] 
                tempfile.Close()
        else:
            FMC_SENSIT = "/lbne/data/users/lblpwg_tools/FastMC_Data/outputs/ljf26/Sensitivity_Plots"
            nh_cp_file = ROOT.TFile(FMC_SENSIT+"/ProtonP120GeV_nh_"+type+"_histos.root") 
            ih_cp_file = ROOT.TFile(FMC_SENSIT+"/ProtonP120GeV_ih_"+type+"_histos.root") 

            nh_sensitivity = OptimizationUtils.GetPercentile(nh_cp_file.Get("h2"),75)
            ih_sensitivity = OptimizationUtils.GetPercentile(ih_cp_file.Get("h2"),75)
            nh_cp_file.Close()
            ih_cp_file.Close()

            fiterror = [0,0,0]
            fitness = [(nh_sensitivity+ih_sensitivity) / 2,nh_sensitivity,ih_sensitivity]
        return [fitness,fiterror]

    def getLastGoodConfig(self):
        try:
            f = open(self.last_completed_config_file)
            lines = f.readlines()
            f.close()
            return int(lines[0].strip())
        except Exception,e:
            return 0;

    def getFitness(self,iteration,type="cp"):

        fitness_names = []
        fitnesses = [];
        fiterrors = [];

        mode_prefix = ""
        if(self.rhc_parameters_float_separate):
            mode_prefix = "FHC"
            
        fitness_filename = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+mode_prefix+"-"+str(int(iteration))+"/neutrino/flux/fitness_"+type+".txt"

        if(os.path.exists(fitness_filename)):
            try:
                lines = open(fitness_filename).readlines()
                fitness_string = lines[0]
                fitnesses = fitness_string.split()
                fiterror_string = lines[1]
                fiterrors = fiterror_string.split()
                for i in range(len(fitnesses)):
                    fitnesses[i] = float(fitnesses[i].strip())
                    fiterrors[i] = float(fiterrors[i].strip())
            except Exception,e :
                print "Warning: could not extract fitness from",fitness_filename

            if fitnesses == [] and fiterrors == []:
                os.remove(fitness_filename)
    
        if(len(fitnesses)==0):
            
            if self.optname=="HE_test":
                rootfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"-"+str(int(iteration))+"/200kA/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"-"+str(i)+"_200kA_LBNEFD_fastmc.root"
                tempfile = ROOT.TFile(rootfile)
                if tempfile.IsZombie():
                    return [-1,-1]
                temphisto = tempfile.Get("numu_cceventrate")
                fiterror = 0;
                fitness = temphisto.IntegralAndError(5,40,fiterror) # 0 to 10 GeV
                tempfile.Close()
                fitness_names.append("standard")
                fitnesses.append(fitness)
                fiterrors.append(fiterror)
    
            elif self.optname=="ME_run1":
                rootfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"-"+str(int(iteration))+"/200kA/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"-"+str(int(iteration))+"_200kA_LBNEFD_fastmc.root"

                tempfile = ROOT.TFile(rootfile)
                if tempfile.IsZombie() or not tempfile.GetListOfKeys().Contains("numu_flux") :
                    print "Found Zombie"
                    return [["standard"],[-1],[-1]]
                temphisto = tempfile.Get("numu_flux")

            
                # look up power for this configuration
                pot_scale = OptimizationUtils.GetPowerPOTScaleFactor(self.getEnergy(iteration,"FHC"))
                temphisto.Scale(pot_scale)

                fiterror = ROOT.Double(0);
                fitness = temphisto.IntegralAndError(9,32,fiterror) # 1 to 4 GeV
                tempfile.Close()
                fitness_names.append("standard")
                fitnesses.append(fitness)
                fiterrors.append(fiterror)

            else:


                if(self.rhc_parameters_float_separate):
                    varfhcfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"FHC-"+str(int(iteration))+"/200kA/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"FHC-"+str(int(iteration))+"_200kA_LBNEFD_fastmc.root"
                    varrhcfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"RHC-"+str(int(iteration))+"/-200kA/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"RHC-"+str(int(iteration))+"_-200kA_LBNEFD_fastmc.root"                
                else:
                    varfhcfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"-"+str(int(iteration))+"/neutrino/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"-"+str(int(iteration))+"_neutrino_LBNEFD_fastmc.root"
                    varrhcfile = self.output_location+"users/ljf26/fluxfiles/g4lbne/"+self.g4lbne_version+"/"+self.physics_list+"/Optimizations-"+self.optname+"-"+str(int(iteration))+"/antineutrino/flux/histos_g4lbne_"+self.g4lbne_version+"_"+self.physics_list+"_Optimizations-"+self.optname+"-"+str(int(iteration))+"_antineutrino_LBNEFD_fastmc.root"                

                # scale to appropriate POT for the energy of for this configuration
                fhcenergy = self.getEnergy(iteration,"FHC")
                rhcenergy = self.getEnergy(iteration,"RHC")

                # look up antinu fraction for this configuration
                antinufrac = self.getParameterValue("AntinuFraction",iteration)
                if(antinufrac == -1): antinufrac = 0.5

                [fitness_names,fitnesses,fiterrors] = OptimizationUtils.ComputeMean75PercentSensitivityAndError(varfhcfile,varrhcfile,fhcenergy,rhcenergy,antinufrac,type)

            # Write new fitness file
            if(fitnesses[0]!=-1):
                fitnessfile = open(fitness_filename,"w")
                for i in range(len(fitnesses)):
                    fitnessfile.write(str(fitnesses[i])+" ")
                fitnessfile.write("\n");
                for i in range(len(fiterrors)):
                    fitnessfile.write(str(fiterrors[i])+" ")
                fitnessfile.write("\n");
                fitnessfile.close()

        return [fitness_names,fitnesses,fiterrors]

    def createRouletteWheel(self,start_iter, stop_iter):
        self.readVariationsFromMacros()
        self.roulette_wheel = []
    
        total_fitness = 0

        print "Creating Roulette wheel for iterations ",start_iter,stop_iter
        for i in range(start_iter,stop_iter):
            fitness = self.getFitness(i)[1][0]
            if(fitness == -1):
                continue        
            if fitness != 0:
                self.roulette_wheel.append([i,fitness])
 

        # find minimum fitness
        min_fitness = 1e40
        for entry in self.roulette_wheel:
            if entry[1] < min_fitness:
                min_fitness = entry[1]
        

        # subtract off minimum fitness
        print "Roulette_wheel",self.roulette_wheel
        for entry in self.roulette_wheel:

            entry[1] = pow(entry[1] - min_fitness,2)
            total_fitness = total_fitness + entry[1]


        # normalize to a total probability of 1
        for entry in self.roulette_wheel:
            entry[1] = entry[1]/total_fitness

        total_prob = 0
        for entry in self.roulette_wheel:
            total_prob = total_prob + entry[1]
        print "Total prob: ",total_prob

        self.roulette_wheel_start_iter = start_iter
        self.roulette_wheel_stop_iter = stop_iter


    def drawRouletteWheel(self):
        canv = ROOT.TCanvas("MyCanvas","MyCanvas")
        self.createRouletteWheel(1,self.generation_size);
        vals = []
        names = []
        cols = []
        for entry in self.roulette_wheel:
            vals.append(entry[1])
            if(entry[0]%10==0):
                names.append(array('c',str(entry[0])+"\0"))
            else:
                names.append(array('c',"\0"))
            cols.append(entry[0]%10+1)
        a = array( 'l', map( lambda x: x.buffer_info()[0], names ) )
        pie1 = ROOT.TPie("pie1","Roulette Wheel",len(vals),array('f',vals),array('i',cols),a);
        pie1.GetSlice(1).SetFillStyle(3031);
        pie1.SetAngle3D(60);
        #pie1.SetLabelsOffset(-.1);
        pie1.Draw("3d t nol")
        canv.Print("roulette_wheel.eps");
        
        


    def pickRandomParent(self):
        random_number = random.random()
        total_roulette = 0
        
        for entry in self.roulette_wheel:
            total_roulette = total_roulette+entry[1]
            if total_roulette > random_number:
                return entry[0]
        print "ERROR: reached end of roulette wheel without picking an entry... something is wrong"
        sys.exit()

    def getKnobturns(self):
        if self.knobturns == []:
            self.readVariationsFromMacros()
        return self.knobturns
        
        

    def twoPointCrossover(self):

        mom_iter = self.pickRandomParent()
        dad_iter = self.pickRandomParent()

        parent_parameters = self.getKnobturns()

        mom_parameters = []
        dad_parameters = []
        for entry in parent_parameters:
            if entry[0]== mom_iter:
                mom_parameters = entry[1]
            if entry[0]== dad_iter:
                dad_parameters = entry[1]
        if len(mom_parameters)==0 or len(dad_parameters)==0:
            print "ERROR: couldn't find mom or dad parameters... something is wrong"
            sys.exit()

        n_genes = len(self.parameter_names)
        position_a = 0.0
        position_b = 0.0
        while position_a == position_b:
            position_a = random.randint(1,n_genes)
            position_b = random.randint(position_a,n_genes)

        child_parameters = []
        for j in range(0,n_genes):
            if j < position_a or j>= position_b:
                child_parameters.append(mom_parameters[j])
            else:
                child_parameters.append(dad_parameters[j])
        if len(child_parameters) != n_genes:
            print "ERROR: child doesn't have the right number of genes... something is wrong"
            sys.exit()
    
        child_parameters = self.mutate(child_parameters)

        return child_parameters

    def mutate(self,chromosome):
        print "starting mutate for chromosome with "+str(len(chromosome))+"genes"

        for i in range (0,len(chromosome)):
            gene = chromosome[i]
            mutation_probability = 0.1
            mutation_amplitude = 0.05;
            gaussian_width = (self.parameter_upper_limits[i]-self.parameter_lower_limits[i])*mutation_amplitude
            gaussian_mean = 0
            
            throw = random.random()
            if(throw < mutation_probability):
                chromosome[i] = gene + random.gauss(gaussian_mean,gaussian_width);
                while(chromosome[i]<self.parameter_lower_limits[i] or
                      chromosome[i]>self.parameter_upper_limits[i]):
                    chromosome[i] = gene + random.gauss(gaussian_mean,gaussian_width);
        return chromosome

    def populateMacros(self,low_iter,high_iter):
        for i in range(low_iter,high_iter):

            random.seed(i+self.seed_offset)
            
            macro_output = "../../macros/Optimizations/"+self.optname+"/"+self.optname+"_"+str(i)+".mac"
            
            if self.rhc_parameters_float_separate:
                macro_output = "../../macros/Optimizations/"+self.optname+"FHC/"+self.optname+"FHC_"+str(i)+".mac"
                macro_outputRHC =  "../../macros/Optimizations/"+self.optname+"RHC/"+self.optname+"RHC_"+str(i)+".mac"

            if high_iter>self.generation_size+1 and self.roulette_wheel_start_iter != (i-((i-1)%self.generation_size)-self.generation_size) and self.roulette_wheel_stop_iter != (i-((i-1)%self.generation_size)):
                self.createRouletteWheel((i-((i-1)%self.generation_size)-self.generation_size),(i-((i-1)%self.generation_size)))

            macro_template = "../../macros/NoSnout.mac"
        
            if(not os.path.exists(macro_template)):
                print "I can't find macro template NoSnout.mac"

            # Write macro file

            n_macros_to_write = 1
            if self.rhc_parameters_float_separate:
                n_macros_to_write = 2

            for macro_num in range(n_macros_to_write):

             macro_name = macro_output
             if(macro_num==1):
                 macro_name = macro_outputRHC

             print "Making",macro_name

             if not os.path.exists(os.path.dirname(macro_name)):
                 os.makedirs(os.path.dirname(macro_name))

             good_configuration_found = False;
             n_configurations_tried = 0;
             if (macro_num == 0 and os.path.exists(macro_output)):
                 print "    ",os.path.basename(macro_output),"already exists... skipping"
                 continue
             if (macro_num == 1 and os.path.exists(macro_outputRHC)):
                 print "    ",os.path.basename(macro_outputRHC),"already exists... skipping"
                 continue

             while not good_configuration_found:                

                # Get genetic values for parmeters
                new_values = []
                print i
                if(i<self.generation_size+1): # random values
                    for par_iter in range(0,len(self.parameter_names)):
                        new_values.append(random.uniform(self.parameter_lower_limits[par_iter],self.parameter_upper_limits[par_iter]))
                else:
                    new_values = self.twoPointCrossover()


                parameter_prefix = ""
                if(self.rhc_parameters_float_separate):
                    parameter_prefix = "FHC"
                    if(macro_num==1):
                        parameter_prefix = "RHC"
                                

                if self.optname in ["CP_run11","CP_run15","CP_run16","CP_run17","CP_run18","CP_run19","CP_run20"]:
                    HornBF1 = new_values[self.getParameterIndex(parameter_prefix+"HornBF1")];
                    HornBF2 = new_values[self.getParameterIndex(parameter_prefix+"HornBF2")];
                    HornBF3 = new_values[self.getParameterIndex(parameter_prefix+"HornBF3")];
                    HornBF4 = new_values[self.getParameterIndex(parameter_prefix+"HornBF4")];
                    HornBF5 = new_values[self.getParameterIndex(parameter_prefix+"HornBF5")];

                    Btot = HornBF1+HornBF2+HornBF3+HornBF4+HornBF5
                    HornBF1 = HornBF1 / Btot
                    HornBF2 = HornBF2 / Btot
                    HornBF3 = HornBF3 / Btot
                    HornBF4 = HornBF4 / Btot
                    HornBF5 = HornBF5 / Btot

                    new_values[self.getParameterIndex(parameter_prefix+"HornBF1")] = HornBF1
                    new_values[self.getParameterIndex(parameter_prefix+"HornBF2")] = HornBF2
                    new_values[self.getParameterIndex(parameter_prefix+"HornBF3")] = HornBF3
                    new_values[self.getParameterIndex(parameter_prefix+"HornBF4")] = HornBF4
                    new_values[self.getParameterIndex(parameter_prefix+"HornBF5")] = HornBF5

                    HornCF1 = new_values[self.getParameterIndex(parameter_prefix+"HornCF1")];
                    HornCF2 = new_values[self.getParameterIndex(parameter_prefix+"HornCF2")];
                    HornCF3 = new_values[self.getParameterIndex(parameter_prefix+"HornCF3")];
                    HornCF4 = new_values[self.getParameterIndex(parameter_prefix+"HornCF4")];
                    HornCF5 = new_values[self.getParameterIndex(parameter_prefix+"HornCF5")];
                
                    Ctot = HornCF1+HornCF2+HornCF3+HornCF4+HornCF5
                    HornCF1 = HornCF1 / Ctot
                    HornCF2 = HornCF2 / Ctot
                    HornCF3 = HornCF3 / Ctot
                    HornCF4 = HornCF4 / Ctot
                    HornCF5 = HornCF5 / Ctot

                    new_values[self.getParameterIndex(parameter_prefix+"HornCF1")] = HornCF1
                    new_values[self.getParameterIndex(parameter_prefix+"HornCF2")] = HornCF2
                    new_values[self.getParameterIndex(parameter_prefix+"HornCF3")] = HornCF3
                    new_values[self.getParameterIndex(parameter_prefix+"HornCF4")] = HornCF4
                    new_values[self.getParameterIndex(parameter_prefix+"HornCF5")] = HornCF5

                oldmac = open(macro_template)    
                newmac = open(macro_name, 'w')
                
                for s in oldmac.xreadlines():

                    if s.find("/LBNE/det/seHornCurrent")>=0:
                        if not ("HornCurrent" in self.parameter_names or
                                "FHCHornCurrent" in self.parameter_names):
                            newmac.write(s)
                        for j in range(0,len(self.parameter_names)):
                            if self.rhc_parameters_float_separate:
                                if (self.parameter_names[j].startswith("FHC") and macro_num==1):
                                    continue
                                if (self.parameter_names[j].startswith("RHC") and macro_num==0):
                                    continue

                            parameter = self.parameter_names[j]
                            macro_set_stage = self.macro_set_stages[j]

                            if macro_set_stage == "PreInit":

                                    value = new_values[j]

                                    parameter = self.parameter_names[j]
                        
                                    if("MultiSphereTargetRadius" in parameter):
                                        newmac.write("/LBNE/det/UseMultiSphereTarget \n")
                                        newmac.write("/LBNE/det/TargetMaterial Beryllium\n")  
                                    if("SimpleTargetLength" in parameter):
                                        newmac.write("/LBNE/det/UseSimpleCylindricalTarget True\n")

                                    newmac.write(self.macro_commands[j]+" "+str(value)+" "+self.parameter_units[j]+"\n")
                                
                            # setup special horn shape

                    
                        if self.optname in ["CP_run5","CP_run7","CP_run8","CP_run13"]:
                            Horn1OCRadius = new_values[self.getParameterIndex(parameter_prefix+"Horn1OCRadius")];
                            Horn1ICRadius1 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICRadius1")];
                            Horn1ICRadius2 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICRadius2")];
                            Horn1ICRadius3 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICRadius3")];
                            Horn1ICRadius4 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICRadius4")];
                            Horn1ICLength1 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength1")];
                            Horn1ICLength2 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength2")];
                            Horn1ICLength3 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength3")];
                            Horn1ICLength4 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength4")];
                            Horn1ICLength5 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength5")];
                            Horn1ICLength6 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength6")];
                            Horn1ICLength7 = new_values[self.getParameterIndex(parameter_prefix+"Horn1ICLength7")];
                            newmac.write("/LBNE/det/UseHorn1Polycone True \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn1Polycone 10 \n")
                            newmac.write("/LBNE/det/Horn1PolyPt0RinThickZ "+str(Horn1OCRadius)+"  2.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt1RinThickZ "+str(Horn1ICRadius1)+"  2.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt2RinThickZ "+str(Horn1ICRadius1)+"  2.000  "+str(1.00+Horn1ICLength1)+" mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt3RinThickZ "+str(Horn1ICRadius2)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt4RinThickZ "+str(Horn1ICRadius2)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt5RinThickZ "+str(Horn1ICRadius3)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt6RinThickZ "+str(Horn1ICRadius3)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt7RinThickZ "+str(Horn1ICRadius4)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt8RinThickZ "+str(Horn1ICRadius4)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6+Horn1ICLength7)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt9RinThickZ "+str(Horn1OCRadius)+"  2.000  "+str(2.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6+Horn1ICLength7)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyOuterRadius "+str(Horn1OCRadius)+" mm\n")
                    
                            newmac.write("/LBNE/det/TargetLengthOutsideHorn 0 m\n")

                        elif self.optname in ["CP_run9","CP_run10","CP_run11","CP_run14","CP_run15","CP_run16","CP_run17","CP_run18","CP_run19","CP_run20"]:


                            HornARadiusOC = new_values[self.getParameterIndex(parameter_prefix+"HornARadiusOC")];
                            HornARadius1 = new_values[self.getParameterIndex(parameter_prefix+"HornARadius1")];
                            HornARadius2 = new_values[self.getParameterIndex(parameter_prefix+"HornARadius2")];
                            HornALength = new_values[self.getParameterIndex(parameter_prefix+"HornALength")];
                            HornAF1 = new_values[self.getParameterIndex(parameter_prefix+"HornAF1")];
                            HornBRadiusOC = new_values[self.getParameterIndex(parameter_prefix+"HornBRadiusOC")];
                            HornBRadius1 = new_values[self.getParameterIndex(parameter_prefix+"HornBRadius1")];
                            HornBRadius2 = new_values[self.getParameterIndex(parameter_prefix+"HornBRadius2")];
                            HornBRadius3 = new_values[self.getParameterIndex(parameter_prefix+"HornBRadius3")];
                            HornBLength = new_values[self.getParameterIndex(parameter_prefix+"HornBLength")];
                            HornBF1 = new_values[self.getParameterIndex(parameter_prefix+"HornBF1")];
                            HornBF2 = new_values[self.getParameterIndex(parameter_prefix+"HornBF2")];
                            HornBF3 = new_values[self.getParameterIndex(parameter_prefix+"HornBF3")];
                            HornBF4 = new_values[self.getParameterIndex(parameter_prefix+"HornBF4")];
                            HornBF5 = new_values[self.getParameterIndex(parameter_prefix+"HornBF5")];


                            HornCRadiusOC = new_values[self.getParameterIndex(parameter_prefix+"HornCRadiusOC")];
                            HornCRadius1 = new_values[self.getParameterIndex(parameter_prefix+"HornCRadius1")];
                            HornCRadius2 = new_values[self.getParameterIndex(parameter_prefix+"HornCRadius2")];
                            HornCRadius3 = new_values[self.getParameterIndex(parameter_prefix+"HornCRadius3")];
                            HornCLength = new_values[self.getParameterIndex(parameter_prefix+"HornCLength")];
                            HornCF1 = new_values[self.getParameterIndex(parameter_prefix+"HornCF1")];
                            HornCF2 = new_values[self.getParameterIndex(parameter_prefix+"HornCF2")];
                            HornCF3 = new_values[self.getParameterIndex(parameter_prefix+"HornCF3")];
                            HornCF4 = new_values[self.getParameterIndex(parameter_prefix+"HornCF4")];
                            HornCF5 = new_values[self.getParameterIndex(parameter_prefix+"HornCF5")];

                            HornBLongPosition = new_values[self.getParameterIndex(parameter_prefix+"HornBLongPosition")]
                            HornCLongPosition = new_values[self.getParameterIndex(parameter_prefix+"HornCLongPosition")]

                            newmac.write("/LBNE/det/NumberOfHornsPolycone 3 \n")
                            newmac.write("/LBNE/det/Horn2PolyZStartPos "+str(HornBLongPosition)+" \n")
                            newmac.write("/LBNE/det/Horn3PolyZStartPos "+str(HornCLongPosition)+" \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn1Polycone 5 \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn2Polycone 8 \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn3Polycone 8 \n")

                            newmac.write("/LBNE/det/Horn1PolyPt0RinThickZ "+str(HornARadiusOC-15)+"  2.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt1RinThickZ "+str(HornARadius1)+"  2.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt2RinThickZ "+str(HornARadius1)+"  2.000  "+str(1.00+HornALength*HornAF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt3RinThickZ "+str(HornARadius2)+"  2.000  "+str(1.00+HornALength)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt4RinThickZ "+str(HornARadiusOC-15)+"  2.000  "+str(2.00+HornALength)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyOuterRadius "+str(HornARadiusOC)+" mm\n")

                            newmac.write("/LBNE/det/Horn2PolyPt0RinThickZ "+str(HornBRadiusOC-15)+"  2.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt1RinThickZ "+str(HornBRadius1)+"  2.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt2RinThickZ "+str(HornBRadius1)+"  2.000  "+str(1.00+HornBLength*HornBF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt3RinThickZ "+str(HornBRadius2)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt4RinThickZ "+str(HornBRadius2)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2+HornBF3))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt5RinThickZ "+str(HornBRadius3)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2+HornBF3+HornBF4))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt6RinThickZ "+str(HornBRadius3)+"  2.000  "+str(1.00+HornBLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt7RinThickZ "+str(HornBRadiusOC-15)+"  2.000  "+str(2.00+HornBLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyOuterRadius "+str(HornBRadiusOC)+" mm\n")
                
                            newmac.write("/LBNE/det/Horn3PolyPt0RinThickZ "+str(HornCRadiusOC-15)+"  4.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt1RinThickZ "+str(HornCRadius1)+"  4.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt2RinThickZ "+str(HornCRadius1)+"  4.000  "+str(1.00+HornCLength*HornCF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt3RinThickZ "+str(HornCRadius2)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt4RinThickZ "+str(HornCRadius2)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2+HornCF3))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt5RinThickZ "+str(HornCRadius3)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2+HornCF3+HornCF4))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt6RinThickZ "+str(HornCRadius3)+"  4.000  "+str(1.00+HornCLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt7RinThickZ "+str(HornCRadiusOC-15)+"  4.000  "+str(2.00+HornCLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyOuterRadius "+str(HornCRadiusOC)+" mm\n")

                            newmac.write("/LBNE/det/TargetLengthOutsideHorn 0 m\n")
                  
                            if self.optname=="CP_run19":
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 1 \n")                 
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 2 \n")                 
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 3 \n")                 

                    elif s.find("/run/initialize")>=0:
                        newmac.write(s)
                        for j in range(0,len(self.parameter_names)):
                            parameter = self.parameter_names[j]

                            if self.rhc_parameters_float_separate:
                                if (self.parameter_names[j].startswith("FHC") and
                                    macro_num==1):
                                    continue
                                if (self.parameter_names[j].startswith("RHC") and
                                    macro_num==0):
                                    continue
                        
                            macro_set_stage = self.macro_set_stages[j]
                            if(macro_set_stage == "Idle"):
                                value = new_values[j]
                                newmac.write(self.macro_commands[j]+" "+str(value)+" "+self.parameter_units[j]+"\n")
                            # Also scale beam width to match target fin size
                            if("GraphiteTargetFinWidth" in parameter):
                                # Scale nominal size (1.7 mm) based on
                                # change in fin width from nominal (.10 m)
                                if self.optname in ["CP_run5","CP_run7","CP_run8","CP_run13"]:
                                       new_beam_size = 1.7 * new_values[j]/10
                                       newmac.write("/LBNE/generator/beamSigmaX "+str(new_beam_size)+" mm \n");
                                       newmac.write("/LBNE/generator/beamSigmaY "+str(new_beam_size)+" mm \n");
                            
                            if parameter == "BeamSigma":
                                temp = self.macro_commands[j].replace("SigmaX","SigmaY")
                                newmac.write(temp+" "+str(value)+" "+self.parameter_units[j]+"\n")
                                

                    elif s.find("/LBNE/run/NEvents")>=0: # set to small number of events for now (because we're gonna test it interactively)
                        newmac.write("/LBNE/run/NEvents 10 \n")
                    else:
                        newmac.write(s);
            
                newmac.close();
                oldmac.close();
        
                # Make sure the macro is good
                n_configurations_tried = n_configurations_tried + 1

                parameter_prefix = ""
                if(self.rhc_parameters_float_separate):
                    parameter_prefix = "FHC"
                    if(macro_num==1):
                        parameter_prefix = "RHC"

                                   
                if(self.optname=="CP_run5" or self.optname=="CP_run6" or self.optname=="CP_run7" or self.optname=="CP_run8" or self.optname=="CP_run13"):
                    horn1length = self.getParameterValue(parameter_prefix+"Horn1ICLength1",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength2",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength3",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength4",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength5",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength6",i)+self.getParameterValue(parameter_prefix+"Horn1ICLength7",i)
                    horn2pos = self.getParameterValue(parameter_prefix+"Horn2LongPosition",i)*1000 # convert to mm
                    horn2length = 3.63 * self.getParameterValue(parameter_prefix+"Horn2LongRescale",i) * 1000 # convert to mm
                    if(horn2pos < horn1length):
                        print "Warning: Configuration not valid because horn2 position is less than horn1 length.. rethrowing"
                        continue

                if(self.optname=="CP_run9" or self.optname=="CP_run10"):
                    HornALength = self.getParameterValue("HornALength",i)
                    HornBLength = self.getParameterValue("HornBLength",i)
                    HornCLength = self.getParameterValue("HornCLength",i)
                    HornBLongPosition = self.getParameterValue("HornBLongPosition",i)
                    HornCLongPosition = self.getParameterValue("HornCLongPosition",i)
                    HornBF1 = self.getParameterValue("HornBF1",i);
                    HornBF2 = self.getParameterValue("HornBF2",i);
                    HornBF3 = self.getParameterValue("HornBF3",i);
                    HornBF4 = self.getParameterValue("HornBF4",i);
                    HornCF1 = self.getParameterValue("HornCF1",i);
                    HornCF2 = self.getParameterValue("HornCF2",i);
                    HornCF3 = self.getParameterValue("HornCF3",i);
                    HornCF4 = self.getParameterValue("HornCF4",i);
            
                    if HornBF1+HornBF2+HornBF3+HornBF4>0.99:
                        print "Failing BF"
                        print HornBF1,HornBF2,HornBF3,HornBF4
                        continue
                    if HornCF1+HornCF2+HornCF3+HornCF4>0.99:
                        print HornCF1,HornCF2,HornCF3,HornCF4
                        print "Failing CF"
                        continue
                    if HornBLongPosition < HornALength:
                        print "Failing HornB Start"
                        continue
                    if HornCLongPosition < HornBLength + HornBLongPosition:
                        print "Failing HornC Start"
                        continue
                    if HornCLongPosition + HornCLength > 21000:
                        print "Failing HornC End"
                        continue
    
                if(self.optname in ["CP_run11","CP_run15","CP_run16","CP_run17","CP_run18","CP_run19","CP_run20"]):
                    HornALength = self.getParameterValue("HornALength",i)
                    HornBLength = self.getParameterValue("HornBLength",i)
                    HornCLength = self.getParameterValue("HornCLength",i)
                    HornBLongPosition = self.getParameterValue("HornBLongPosition",i)
                    HornCLongPosition = self.getParameterValue("HornCLongPosition",i)
                    if HornBLongPosition < HornALength:
                        print "Failing HornB Start"
                        continue
                    if HornCLongPosition < HornBLength + HornBLongPosition:
                        print "Failing HornC Start"
                        continue
                    if HornCLongPosition + HornCLength > 21000:
                        print "Failing HornC End"
                        continue

                if(self.optname in ["CP_run20"]):
                    HornALength = self.getParameterValue("HornALength",i)
                    HornBLength = self.getParameterValue("HornBLength",i)
                    HornCLength = self.getParameterValue("HornCLength",i)
                    HornBLongPosition = self.getParameterValue("HornBLongPosition",i)
                    HornCLongPosition = self.getParameterValue("HornCLongPosition",i)
                    if HornBLongPosition < HornALength:
                        print "Failing HornB Start"
                        continue
                    if HornCLongPosition < HornBLength + HornBLongPosition:
                        print "Failing HornC Start"
                        continue

                if(self.optname=="CP_run12"):
                    HornALength = self.getParameterValue("HornAICLength1",i)+self.getParameterValue("HornAICLength2",i)
                    HornBLength = self.getParameterValue("HornBICLength1",i)+self.getParameterValue("HornBICLength2",i)+self.getParameterValue("HornBICLength3",i)+self.getParameterValue("HornBICLength4",i)+self.getParameterValue("HornBICLength5",i)
                    HornCLength = self.getParameterValue("HornCICLength1",i)+self.getParameterValue("HornCICLength2",i)+self.getParameterValue("HornCICLength3",i)+self.getParameterValue("HornCICLength4",i)+self.getParameterValue("HornCICLength5",i)
                    HornBLongPosition = self.getParameterValue("HornBLongPosition",i)
                    HornCLongPosition = self.getParameterValue("HornCLongPosition",i)
                    if HornBLongPosition < HornALength:
                        print "Failing HornB Start"
                        continue
                    if HornCLongPosition < HornBLength + HornBLongPosition:
                        print "Failing HornC Start"
                        continue
                    if HornCLongPosition + HornCLength > 21000:
                        print "Failing HornC End"
                        continue

                
                # make sure it runs
                if os.path.exists("temp.txt"):
                    os.remove("temp.txt")
                os.chdir("../..");
            
                os.system("pwd")
            
                mode_prefix = ""
                if(self.rhc_parameters_float_separate):
                    if macro_num == 0:
                        mode_prefix = "FHC"
                    else:
                        mode_prefix = "RHC"
                macro_status_code = os.system("./g4lbnf macros/Optimizations/"+self.optname+mode_prefix+"/"+os.path.basename(macro_name)+" >& BeamSimStudies/GeneticOptimization/temp"+self.optname+".txt")
                
                has_overlap = False
                for line in open("BeamSimStudies/GeneticOptimization/temp"+self.optname+".txt"):
                    if "Overlap" in line:
                        has_overlap = True
                os.chdir("BeamSimStudies/GeneticOptimization")

                if macro_status_code==0 and not has_overlap:
                    good_configuration_found = True
                    print "Found good configuration after",n_configurations_tried,"tries."
                else:
                    print "g4lbne returned a nonzero status code for this macro... will try again"
                    print "Here is the error output:"
                    os.system("cat temp.txt");
                    print "Number of configurations tried:",n_configurations_tried

                    os.remove(macro_name)
                


    def getParameterIndex(self,parameter_name) :
        for i in range(0,len(self.parameter_names)):
            if self.parameter_names[i]==parameter_name:
                return i
        return -1

    def getParameterValue(self,parameter_name,iteration) :
        val = -1
        val_iter = self.getParameterIndex(parameter_name)
        if val_iter == -1:
            return val

        params = self.readVariationFromMacros(iteration)

        return params[val_iter]

    def readVariationFromMacros(self,iteration) :
        
        if not self.rhc_parameters_float_separate:

            macro = "../../../"+self.g4lbne_version+"/macros/Optimizations/"+self.optname+"/"+self.optname+"_"+str(int(iteration))+".mac"

            return_data = []
            if not os.path.exists(macro):
                return return_data

            tempknobturns = []
            for i in range(0,len(self.parameter_names)):

                parameter = self.parameter_names[i]

                macro_command = self.macro_commands[i]
                tempfile = open(macro)
                lines = tempfile.readlines()
                tempfile.close()

                par_found = False
                for line in lines:
                    if line.startswith(macro_command+" "):

                        par_value = line.split()[1]
                        if par_value == "Fraction":
                            par_value = line.split()[2]
                        par_found = True
                        tempknobturns.append(float(par_value))
                if not par_found:
                    print "ERROR: Macro file",macro,"does not contain a line that changes",parameter," -- Something is wrong -- Quitting."
                    sys.exit()
                return_data = tempknobturns
            
        else:

            return_data = []

            tempknobturns = []

            for mode in ["FHC","RHC"]:
              macro = "../../../"+self.g4lbne_version+"/macros/Optimizations/"+self.optname+mode+"/"+self.optname+mode+"_"+str(int(iteration))+".mac"
              if not os.path.exists(macro):
                  continue

              tempfile = open(macro)
              lines = tempfile.readlines()
              tempfile.close()

              for i in range(0,len(self.parameter_names)):
                if not self.parameter_names[i].startswith(mode):
                    continue
                parameter = self.parameter_names[i]
                macro_command = self.macro_commands[i]
              
                par_found = False
                for line in lines:
                    if line.startswith(macro_command+" "):
                        par_value = line.split()[1]
                        if par_value == "Fraction":
                            par_value = line.split()[2]
                        par_found = True

                        return_data.append(float(par_value))
                        break

        return return_data

    def readVariationsFromMacros(self) :

        mode_prefix = ""
        if(self.rhc_parameters_float_separate):
            mode_prefix = "FHC"

        # Search for populated macros
        n_configs_not_found = 0
        n_configs = 1
        macros = []

        while n_configs_not_found < 100:
            macro = "../../../"+self.g4lbne_version+"/macros/Optimizations/"+self.optname+mode_prefix+"/"+self.optname+mode_prefix+"_"+str(n_configs)+".mac"
    
            if os.path.exists(macro):
                macros.append(macro)
            else:
                n_configs_not_found = n_configs_not_found + 1
            n_configs = n_configs + 1

        # loop over populated macros and extract data
        return_data = []
        for macro in macros:

            if "vrml" in macro or "Scan" in macro:
                continue

            iteration = float(macro.split("_")[len(macro.split("_"))-1].split(".")[0])
            tempknobturns = self.readVariationFromMacros(iteration)
            return_data.append([iteration,tempknobturns])

        self.knobturns = return_data

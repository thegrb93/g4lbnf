import ROOT, random, array, sys, os, math

def ConvertGraphToHisto(new_bins,pGraph):

    name = "h_"+pGraph.GetName();
    histo = ROOT.TH1D(name,name,len(new_bins)-1,array.array('d',new_bins));

    pGraph.Sort();

    if pGraph.GetN() != len(new_bins)-1:
        print "OptimizationUtils.ConvertGraphToHisto: WARNING converting graph with",pGraph.GetN(),"points to histogram with",len(new_bins)-1,"bins"

    for i in range(0,len(new_bins)):
        x  = ROOT.Double(0.0)
        y   = ROOT.Double(0.0)
        pGraph.GetPoint(i,x,y);
        histo.SetBinContent(i+1,y)
        histo.SetBinError(i+1,0)

    return histo

def GetPercentile(histo,X):
    n = histo.GetNbinsX();
    x = []
    y = []
    
    for i in range(0,n):
        x.append(histo.GetBinCenter(i+1));
        y.append(histo.GetBinContent(i+1));
    y_array = array.array('d',y)

    percentiles = array.array('d',[0.0]);
    probs = array.array('d',[1-float(X)/100.0])

    ROOT.TMath.Quantiles(n,1,y_array,percentiles,probs,False);
    return percentiles[0]

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)



def GetFractionAtMoreThanXSigma(histo, X):
    n = histo.GetNbinsX();

    greaterThanX = 0.0;
    total = 0.0;

    for i in range(0,n):
        total = total + float(histo.GetBinWidth(i+1));
        if(histo.GetBinContent(i+1)>=X):
            greaterThanX = greaterThanX + float(histo.GetBinWidth(i+1));


    return greaterThanX/total;

def GetPowerPOTScaleFactor(energy):
    if(energy >= 120):
        return 120/energy
    if(energy < 60):
        return 1.71e21/1e21; # limited to 0.7 s cycle time
    # linear interpolate
    energies = [60,70,80,90,100,110,120]
    potperyear = [1.71e21,1.5e21,1.33e21,1.23e21,1.14e21,1.07e21,1e21]
    for i in range(0,len(energies)):
        if energy < energies[i]:
            interp_high = i
            interp_low = i-1
            break

    scale_factor =  (potperyear[interp_high]+(potperyear[interp_low]-potperyear[interp_high])/(energies[interp_high]-energies[interp_low])*(energies[interp_high]-energy))/1e21
    return scale_factor


def MakeSumw2(histo):
    if histo.GetSumw2N() == 0:
        histos.Sumw2();

def ComputeMean75PercentSensitivityAndError(varfhcfile,varrhcfile, fhcvarenergy=120.0,rhcvarenergy = 120.0, antinufrac=0.5,quantity="cp"):

    nomfhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_200kA_LBNEFD_fastmc.root"
                
    nomrhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/-200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_-200kA_LBNEFD_fastmc.root"

    # change sensitivity type string to get file names right
    if quantity == "cp":
        quantity = "cp_75thpercentile"
    if quantity == "mh":
        quantity = "mh_minimum"

    # Have to use same bins that were used for the FMC bin-by-bin variation study
    new_bins = [0.0,0.5,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,15.0,20.0,120.0]

    null_return = [[-1,-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1,-1]]

    # Open files with flux histograms and make sure they are good
    nomfhctfile = ROOT.TFile(nomfhcfile)
    nomrhctfile = ROOT.TFile(nomrhcfile)
    varfhctfile = ROOT.TFile(varfhcfile)
    varrhctfile = ROOT.TFile(varrhcfile)

    fhcnumu_nh_weightfile = ROOT.TFile("sensitivity_plots/fhcnumu_nh_"+quantity+"_fixedScale.root");
    fhcnumu_ih_weightfile = ROOT.TFile("sensitivity_plots/fhcnumu_ih_"+quantity+"_fixedScale.root");
    rhcnumu_nh_weightfile = ROOT.TFile("sensitivity_plots/rhcnumu_nh_"+quantity+"_fixedScale.root");
    rhcnumu_ih_weightfile = ROOT.TFile("sensitivity_plots/rhcnumu_ih_"+quantity+"_fixedScale.root");
    fhcnumubar_nh_weightfile = ROOT.TFile("sensitivity_plots/fhcnumubar_nh_"+quantity+"_fixedScale.root");
    fhcnumubar_ih_weightfile = ROOT.TFile("sensitivity_plots/fhcnumubar_ih_"+quantity+"_fixedScale.root");
    rhcnumubar_nh_weightfile = ROOT.TFile("sensitivity_plots/rhcnumubar_nh_"+quantity+"_fixedScale.root");
    rhcnumubar_ih_weightfile = ROOT.TFile("sensitivity_plots/rhcnumubar_ih_"+quantity+"_fixedScale.root");
    fhcnuenuebar_nh_weightfile = ROOT.TFile("sensitivity_plots/fhcnuenuebar_nh_"+quantity+"_fixedScale.root");
    fhcnuenuebar_ih_weightfile = ROOT.TFile("sensitivity_plots/fhcnuenuebar_ih_"+quantity+"_fixedScale.root");
    rhcnuenuebar_nh_weightfile = ROOT.TFile("sensitivity_plots/rhcnuenuebar_nh_"+quantity+"_fixedScale.root");
    rhcnuenuebar_ih_weightfile = ROOT.TFile("sensitivity_plots/rhcnuenuebar_ih_"+quantity+"_fixedScale.root");

    files_to_check = [nomfhctfile,nomrhctfile,varfhctfile,varrhctfile]
    for a_file in files_to_check:
        if(a_file.IsZombie()):
            print "Optimizationutils::ComputeMeanPercentCPSensitivity: Could not open",a_file.GetName()
            return null_return
        
    # Get Flux histograms for nominal case and variation
    nom_fhcnumu = nomfhctfile.Get("numu_flux").Rebin(len(new_bins)-1,"h_nomfhcnumu_flux",array.array('d',new_bins))
    nom_fhcnumubar = nomfhctfile.Get("numubar_flux").Rebin(len(new_bins)-1,"h_nomfhcnumubar_flux",array.array('d',new_bins))
    nom_fhcnue = nomfhctfile.Get("nue_flux").Rebin(len(new_bins)-1,"h_nomfhcnue_flux",array.array('d',new_bins))
    nom_fhcnuebar = nomfhctfile.Get("nuebar_flux").Rebin(len(new_bins)-1,"h_nomfhcnuebar_flux",array.array('d',new_bins))

    nom_rhcnumu = nomrhctfile.Get("numu_flux").Rebin(len(new_bins)-1,"h_nomrhcnumu_flux",array.array('d',new_bins))
    nom_rhcnumubar = nomrhctfile.Get("numubar_flux").Rebin(len(new_bins)-1,"h_nomrhcnumubar_flux",array.array('d',new_bins))
    nom_rhcnue = nomrhctfile.Get("nue_flux").Rebin(len(new_bins)-1,"h_nomrhcnue_flux",array.array('d',new_bins))
    nom_rhcnuebar = nomrhctfile.Get("nuebar_flux").Rebin(len(new_bins)-1,"h_nomrhcnuebar_flux",array.array('d',new_bins))

    var_fhcnumu = varfhctfile.Get("numu_flux").Rebin(len(new_bins)-1,"h_fhcnumu_flux",array.array('d',new_bins))
    var_fhcnumubar = varfhctfile.Get("numubar_flux").Rebin(len(new_bins)-1,"h_fhcnumubar_flux",array.array('d',new_bins))
    var_fhcnue = varfhctfile.Get("nue_flux").Rebin(len(new_bins)-1,"h_fhcnue_flux",array.array('d',new_bins))
    var_fhcnuebar = varfhctfile.Get("nuebar_flux").Rebin(len(new_bins)-1,"h_fhcnuebar_flux",array.array('d',new_bins))

    var_rhcnumu = varrhctfile.Get("numu_flux").Rebin(len(new_bins)-1,"h_rhcnumu_flux",array.array('d',new_bins))
    var_rhcnumubar = varrhctfile.Get("numubar_flux").Rebin(len(new_bins)-1,"h_rhcnumubar_flux",array.array('d',new_bins))
    var_rhcnue = varrhctfile.Get("nue_flux").Rebin(len(new_bins)-1,"h_rhcnue_flux",array.array('d',new_bins))
    var_rhcnuebar = varrhctfile.Get("nuebar_flux").Rebin(len(new_bins)-1,"h_rhcnuebar_flux",array.array('d',new_bins))

    nom_histos = [nom_fhcnumu, nom_fhcnumubar, nom_fhcnue, nom_fhcnuebar, nom_rhcnumu, nom_rhcnumubar, nom_rhcnue, nom_rhcnuebar]
    var_histos = [var_fhcnumu, var_fhcnumubar, var_fhcnue, var_fhcnuebar, var_rhcnumu, var_rhcnumubar, var_rhcnue, var_rhcnuebar]

    # Rebin makes the last bin screwy sometimes, so fix these bins
    # Fluxes should be small (<10^-10) postive numbers
    for histo in nom_histos+var_histos:
        for i in range(0,histo.GetNbinsX()):
            if(histo.GetBinContent(i+1) != histo.GetBinContent(i+1)  or 
               histo.GetBinContent(i+1)<-10000 or histo.GetBinContent(i+1)>1):
                print "WARNING: the",i+1,"th bin in",histo.GetName(),"is",histo.GetBinContent(i+1),"... Resetting it to zero"
                histo.SetBinContent(i+1,0)
                histo.SetBinError(i+1,0)


    # Make sure errors get propagated
    for histo in nom_histos+var_histos:
        MakeSumw2(histo)

    # Scale variations for powers different than 120
    fhc_scale_factor = GetPowerPOTScaleFactor(fhcvarenergy)
    rhc_scale_factor = GetPowerPOTScaleFactor(rhcvarenergy)

    # Scale variations for antinu fraction other than 0.5
    antinu_scale = antinufrac/0.5*rhc_scale_factor;
    nu_scale = (1.0-antinufrac)/0.5*fhc_scale_factor;

    var_fhcnumu.Scale(nu_scale);
    var_fhcnumubar.Scale(nu_scale);
    var_fhcnue.Scale(nu_scale);
    var_fhcnuebar.Scale(nu_scale);

    var_rhcnumu.Scale(antinu_scale);
    var_rhcnumubar.Scale(antinu_scale);
    var_rhcnue.Scale(antinu_scale);
    var_rhcnuebar.Scale(antinu_scale);

    # Compute change in sensitivity
    fhcnumu = var_fhcnumu.Clone()
    fhcnumu.Add(nom_fhcnumu,-1)
    fhcnumu.Divide(nom_fhcnumu)
    #fhcnumu.Scale(1/0.1)
    fhcnumu.Scale(100)
    fhcnumu_ih = fhcnumu.Clone()
    fhcnumu_nh = fhcnumu.Clone()

    fhcnumubar = var_fhcnumubar.Clone()
    fhcnumubar.Add(nom_fhcnumubar,-1)
    fhcnumubar.Divide(nom_fhcnumubar)
    fhcnumubar.Scale(100)
    fhcnumubar_ih = fhcnumubar.Clone()
    fhcnumubar_nh = fhcnumubar.Clone()
 
    fhcnuenuebar = var_fhcnue.Clone()
    fhcnuenuebar.Add(nom_fhcnue,-1)
    fhcnuenuebar.Divide(nom_fhcnue)
    fhcnuenuebar.Scale(100)
    fhcnuenuebar_ih = fhcnuenuebar.Clone()
    fhcnuenuebar_nh = fhcnuenuebar.Clone()
 
    rhcnumu = var_rhcnumu.Clone()
    rhcnumu.Add(nom_rhcnumu,-1)
    rhcnumu.Divide(nom_rhcnumu)
    rhcnumu.Scale(100)
    rhcnumu_ih = rhcnumu.Clone()
    rhcnumu_nh = rhcnumu.Clone()

    rhcnumubar = var_rhcnumubar.Clone()
    rhcnumubar.Add(nom_rhcnumubar,-1)
    rhcnumubar.Divide(nom_rhcnumubar)
    rhcnumubar.Scale(100)
    rhcnumubar_ih = rhcnumubar.Clone()
    rhcnumubar_nh = rhcnumubar.Clone()

    rhcnuenuebar = var_rhcnuebar.Clone()
    rhcnuenuebar.Add(nom_rhcnuebar,-1)
    rhcnuenuebar.Divide(nom_rhcnuebar)
    rhcnuenuebar.Scale(100)
    rhcnuenuebar_ih = rhcnuenuebar.Clone()
    rhcnuenuebar_nh = rhcnuenuebar.Clone()
    
    tot_fhcnumu_ih = 0;
    tot_fhcnumu_nh = 0;
    tot_fhcnumubar_ih = 0;
    tot_fhcnumubar_nh = 0;
    tot_fhcnuenuebar_ih = 0;
    tot_fhcnuenuebar_nh = 0;
    tot_rhcnumu_ih = 0;
    tot_rhcnumu_nh = 0;
    tot_rhcnumubar_ih = 0;
    tot_rhcnumubar_nh = 0;
    tot_rhcnuenuebar_ih = 0;
    tot_rhcnuenuebar_nh = 0;

    err_fhcnumu_ih = 0;
    err_fhcnumu_nh = 0;
    err_fhcnumubar_ih = 0;
    err_fhcnumubar_nh = 0;
    err_fhcnuenuebar_ih = 0;
    err_fhcnuenuebar_nh = 0;
    err_rhcnumu_ih = 0;
    err_rhcnumu_nh = 0;
    err_rhcnumubar_ih = 0;
    err_rhcnumubar_nh = 0;
    err_rhcnuenuebar_ih = 0;
    err_rhcnuenuebar_nh = 0;

    for i in range(fhcnumu.GetNbinsX()):

        fhcnumu_ih.SetBinContent(i+1,fhcnumu_ih_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnumu.GetBinContent(i+1)+100));
        if(fhcnumu.GetBinContent(i+1) != 0):
            fhcnumu_ih.SetBinError(i+1,fhcnumu.GetBinError(i+1)/fhcnumu.GetBinContent(i+1)*fhcnumu_ih.GetBinContent(i+1));
        rhcnumu_ih.SetBinContent(i+1,rhcnumu_ih_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnumu.GetBinContent(i+1)+100));
        if(rhcnumu.GetBinContent(i+1) != 0):
            rhcnumu_ih.SetBinError(i+1,rhcnumu.GetBinError(i+1)/rhcnumu.GetBinContent(i+1)*rhcnumu_ih.GetBinContent(i+1));

        fhcnumubar_ih.SetBinContent(i+1,fhcnumubar_ih_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnumubar.GetBinContent(i+1)+100));
        if(fhcnumubar.GetBinContent(i+1)!=0):
            fhcnumubar_ih.SetBinError(i+1,fhcnumubar.GetBinError(i+1)/fhcnumubar.GetBinContent(i+1)*fhcnumubar_ih.GetBinContent(i+1));
        rhcnumubar_ih.SetBinContent(i+1,rhcnumubar_ih_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnumubar.GetBinContent(i+1)+100));
        if(rhcnumubar.GetBinContent(i+1)!=0):
            rhcnumubar_ih.SetBinError(i+1,rhcnumubar.GetBinError(i+1)/rhcnumubar.GetBinContent(i+1)*rhcnumubar_ih.GetBinContent(i+1));

        fhcnuenuebar_ih.SetBinContent(i+1,fhcnuenuebar_ih_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnuenuebar.GetBinContent(i+1)+100));
        if(fhcnuenuebar.GetBinContent(i+1)!=0):
            fhcnuenuebar_ih.SetBinError(i+1,fhcnuenuebar.GetBinError(i+1)/fhcnuenuebar.GetBinContent(i+1)*fhcnuenuebar_ih.GetBinContent(i+1));
        rhcnuenuebar_ih.SetBinContent(i+1,rhcnuenuebar_ih_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnuenuebar.GetBinContent(i+1)+100));
        if(rhcnuenuebar.GetBinContent(i+1)!=0):
            rhcnuenuebar_ih.SetBinError(i+1,rhcnuenuebar.GetBinError(i+1)/rhcnuenuebar.GetBinContent(i+1)*rhcnuenuebar_ih.GetBinContent(i+1));

        fhcnumu_nh.SetBinContent(i+1,fhcnumu_nh_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnumu.GetBinContent(i+1)+100));
        if(fhcnumu.GetBinContent(i+1)!=0):
            fhcnumu_nh.SetBinError(i+1,fhcnumu.GetBinError(i+1)/fhcnumu.GetBinContent(i+1)*fhcnumu_nh.GetBinContent(i+1));
        rhcnumu_nh.SetBinContent(i+1,rhcnumu_nh_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnumu.GetBinContent(i+1)+100));
        if(rhcnumu.GetBinContent(i+1)!=0):
            rhcnumu_nh.SetBinError(i+1,rhcnumu.GetBinError(i+1)/rhcnumu.GetBinContent(i+1)*rhcnumu_nh.GetBinContent(i+1));

        fhcnumubar_nh.SetBinContent(i+1,fhcnumubar_nh_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnumubar.GetBinContent(i+1)+100));
        if(fhcnumubar.GetBinContent(i+1)!=0):
            fhcnumubar_nh.SetBinError(i+1,fhcnumubar.GetBinError(i+1)/fhcnumubar.GetBinContent(i+1)*fhcnumubar_nh.GetBinContent(i+1));
        rhcnumubar_nh.SetBinContent(i+1,rhcnumubar_nh_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnumubar.GetBinContent(i+1)+100));
        if(rhcnumubar.GetBinContent(i+1)!=0):
            rhcnumubar_nh.SetBinError(i+1,rhcnumubar.GetBinError(i+1)/rhcnumubar.GetBinContent(i+1)*rhcnumubar_nh.GetBinContent(i+1));

        fhcnuenuebar_nh.SetBinContent(i+1,fhcnuenuebar_nh_weightfile.Get("Graph;"+str(i+1)).Eval(fhcnuenuebar.GetBinContent(i+1)+100));
        if(fhcnuenuebar.GetBinContent(i+1)!=0):
            fhcnuenuebar_nh.SetBinError(i+1,fhcnuenuebar.GetBinError(i+1)/fhcnuenuebar.GetBinContent(i+1)*fhcnuenuebar_nh.GetBinContent(i+1));
        rhcnuenuebar_nh.SetBinContent(i+1,rhcnuenuebar_nh_weightfile.Get("Graph;"+str(i+1)).Eval(rhcnuenuebar.GetBinContent(i+1)+100));
        if(rhcnuenuebar.GetBinContent(i+1)!=0):
            rhcnuenuebar_nh.SetBinError(i+1,rhcnuenuebar.GetBinError(i+1)/rhcnuenuebar.GetBinContent(i+1)*rhcnuenuebar_nh.GetBinContent(i+1));

        tot_fhcnumu_ih += fhcnumu_ih.GetBinContent(i+1)
        tot_fhcnumu_nh += fhcnumu_nh.GetBinContent(i+1)
        tot_fhcnumubar_ih += fhcnumubar_ih.GetBinContent(i+1)
        tot_fhcnumubar_nh += fhcnumubar_nh.GetBinContent(i+1)
        tot_fhcnuenuebar_ih += fhcnuenuebar_ih.GetBinContent(i+1)
        tot_fhcnuenuebar_nh += fhcnuenuebar_nh.GetBinContent(i+1)
        
        tot_rhcnumu_ih += rhcnumu_ih.GetBinContent(i+1)
        tot_rhcnumu_nh += rhcnumu_nh.GetBinContent(i+1)
        tot_rhcnumubar_ih += rhcnumubar_ih.GetBinContent(i+1)
        tot_rhcnumubar_nh += rhcnumubar_nh.GetBinContent(i+1)
        tot_rhcnuenuebar_ih += rhcnuenuebar_ih.GetBinContent(i+1)
        tot_rhcnuenuebar_nh += rhcnuenuebar_nh.GetBinContent(i+1)

        err_fhcnumu_ih +=math.pow(fhcnumu_ih.GetBinError(i+1),2)
        err_fhcnumu_nh +=math.pow(fhcnumu_nh.GetBinError(i+1),2)
        err_fhcnumubar_ih +=math.pow(fhcnumubar_ih.GetBinError(i+1),2)
        err_fhcnumubar_nh +=math.pow(fhcnumubar_nh.GetBinError(i+1),2)
        err_fhcnuenuebar_ih +=math.pow(fhcnuenuebar_ih.GetBinError(i+1),2)
        err_fhcnuenuebar_nh +=math.pow(fhcnuenuebar_nh.GetBinError(i+1),2)

        err_rhcnumu_ih +=math.pow(rhcnumu_ih.GetBinError(i+1),2)
        err_rhcnumu_nh +=math.pow(rhcnumu_nh.GetBinError(i+1),2)
        err_rhcnumubar_ih +=math.pow(rhcnumubar_ih.GetBinError(i+1),2)
        err_rhcnumubar_nh +=math.pow(rhcnumubar_nh.GetBinError(i+1),2)
        err_rhcnuenuebar_ih +=math.pow(rhcnuenuebar_ih.GetBinError(i+1),2)
        err_rhcnuenuebar_nh +=math.pow(rhcnuenuebar_nh.GetBinError(i+1),2)

    err_fhcnumu_ih = math.sqrt(err_fhcnumu_ih)
    err_fhcnumu_nh = math.sqrt(err_fhcnumu_nh)
    err_fhcnumubar_ih = math.sqrt(err_fhcnumubar_ih)
    err_fhcnumubar_nh = math.sqrt(err_fhcnumubar_nh)
    err_fhcnuenuebar_ih = math.sqrt(err_fhcnuenuebar_ih)
    err_fhcnuenuebar_nh = math.sqrt(err_fhcnuenuebar_nh)

    err_rhcnumu_ih = math.sqrt(err_rhcnumu_ih)
    err_rhcnumu_nh = math.sqrt(err_rhcnumu_nh)
    err_rhcnumubar_ih = math.sqrt(err_rhcnumubar_ih)
    err_rhcnumubar_nh = math.sqrt(err_rhcnumubar_nh)
    err_rhcnuenuebar_ih = math.sqrt(err_rhcnuenuebar_ih)
    err_rhcnuenuebar_nh = math.sqrt(err_rhcnuenuebar_nh)


    # Get nominal 75% CP sensitivities
    if(quantity=="cp_75thpercentile"): 
        quantity = "cp"
    if(quantity=="mh_minimum"):
        quantity = "mh"
    FMC_SENSIT = "/lbne/data/users/lblpwg_tools/FastMC_Data/outputs/ljf26/Sensitivity_Plots"
    nh_cp_file = ROOT.TFile(FMC_SENSIT+"/ProtonP120GeV_nh_"+quantity+"_histos.root") 
    ih_cp_file = ROOT.TFile(FMC_SENSIT+"/ProtonP120GeV_ih_"+quantity+"_histos.root") 

    nh_sensitivity = GetPercentile(nh_cp_file.Get("h2"),75)
    ih_sensitivity = GetPercentile(ih_cp_file.Get("h2"),75)

    # Calculate estimated sensitivity given changes in flux
    fitness_types = ["standard","fhcnumu","fhcnumubar","fhcnuenuebar","rhcnumu","rhcnumubar","rhcnuenuebar","standard_nh","standard_ih"]
    nh_fitness = [0,0,0,0,0,0,0]
    ih_fitness = [0,0,0,0,0,0,0]
    nh_fiterror = [0,0,0,0,0,0,0]
    ih_fiterror = [0,0,0,0,0,0,0]
    # standard
    nh_fitness[0] = nh_sensitivity+ tot_fhcnumu_nh + tot_fhcnumubar_nh + tot_rhcnumu_nh + tot_rhcnumubar_nh + tot_fhcnuenuebar_nh + tot_rhcnuenuebar_nh;
    ih_fitness[0] = ih_sensitivity+ tot_fhcnumu_ih + tot_fhcnumubar_ih + tot_rhcnumu_ih + tot_rhcnumubar_ih + tot_fhcnuenuebar_ih + tot_rhcnuenuebar_ih;
    nh_fiterror[0] = ROOT.TMath.Sqrt(ROOT.TMath.Power(err_fhcnumu_nh,2)+ROOT.TMath.Power(err_fhcnumubar_nh,2)+ROOT.TMath.Power(err_rhcnumu_nh,2)+ROOT.TMath.Power(err_rhcnumubar_nh,2)+ROOT.TMath.Power(err_fhcnuenuebar_nh,2)+ROOT.TMath.Power(err_rhcnuenuebar_nh,2))
    ih_fiterror[0] = ROOT.TMath.Sqrt(ROOT.TMath.Power(err_fhcnumu_ih,2)+ROOT.TMath.Power(err_fhcnumubar_ih,2)+ROOT.TMath.Power(err_rhcnumu_ih,2)+ROOT.TMath.Power(err_rhcnumubar_ih,2)+ROOT.TMath.Power(err_fhcnuenuebar_ih,2)+ROOT.TMath.Power(err_rhcnuenuebar_ih,2))

    nh_fitness[1] = tot_fhcnumu_nh
    ih_fitness[1] = tot_fhcnumu_ih
    nh_fiterror[1] = err_fhcnumu_nh
    ih_fiterror[1] = err_fhcnumu_ih

    nh_fitness[2] = tot_fhcnumubar_nh
    ih_fitness[2] = tot_fhcnumubar_ih
    nh_fiterror[2] = err_fhcnumubar_nh
    ih_fiterror[2] = err_fhcnumubar_ih

    nh_fitness[3] = tot_fhcnuenuebar_nh
    ih_fitness[3] = tot_fhcnuenuebar_ih
    nh_fiterror[3] = err_fhcnuenuebar_nh
    ih_fiterror[3] = err_fhcnuenuebar_ih

    nh_fitness[4] = tot_rhcnumu_nh
    ih_fitness[4] = tot_rhcnumu_ih
    nh_fiterror[4] = err_rhcnumu_nh
    ih_fiterror[4] = err_rhcnumu_ih

    nh_fitness[5] = tot_rhcnumubar_nh
    ih_fitness[5] = tot_rhcnumubar_ih
    nh_fiterror[5] = err_rhcnumubar_nh
    ih_fiterror[5] = err_rhcnumubar_ih

    nh_fitness[6] = tot_rhcnuenuebar_nh
    ih_fitness[6] = tot_rhcnuenuebar_ih
    nh_fiterror[6] = err_rhcnuenuebar_nh
    ih_fiterror[6] = err_rhcnuenuebar_ih

    fitness = []
    fiterror = []
    for i in range(len(nh_fitness)):
        fitness.append((nh_fitness[i]+ih_fitness[i])/2.0)
        fiterror.append(ROOT.TMath.Sqrt(nh_fiterror[i]*nh_fiterror[i]+ih_fiterror[i]*ih_fiterror[i])/2.0)

    fitness.append(nh_fitness[0])
    fitness.append(ih_fitness[0])
    fiterror.append(nh_fiterror[0])
    fiterror.append(ih_fiterror[0])

    fhcnumu_nh_weightfile.Close()
    fhcnumubar_nh_weightfile.Close()
    fhcnuenuebar_nh_weightfile.Close()
    rhcnumu_nh_weightfile.Close()
    rhcnumubar_nh_weightfile.Close()
    rhcnuenuebar_nh_weightfile.Close()
    rhcnumu_nh_weightfile.Close()
    rhcnumubar_nh_weightfile.Close()
    rhcnuenuebar_nh_weightfile.Close()
    rhcnumu_nh_weightfile.Close()
    rhcnumubar_nh_weightfile.Close()
    rhcnuenuebar_nh_weightfile.Close()

    nomfhctfile.Close()
    nomrhctfile.Close()
    varfhctfile.Close()
    varrhctfile.Close()

    return [fitness_types,fitness,fiterror]

def AddPlotLabel(label,x,y,size=0.05,color=1,font=62,align=22,angle=0):

  latex = ROOT.TLatex( x, y, label );
  latex.SetNDC();
  latex.SetTextSize(4);
  latex.SetTextColor(color);
  latex.SetTextFont(font);
  latex.SetTextAlign(align);
  latex.SetTextAngle(angle);
  latex.DrawClone();


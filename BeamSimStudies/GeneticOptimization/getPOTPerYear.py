import OptimizationUtils, sys

if len(sys.argv) < 2: 
    print "Usage:",sys.argv[0]," <Proton_Energy>"
    sys.exit()

proton_energy = sys.argv[1]

print 1e21 * OptimizationUtils.GetPowerPOTScaleFactor(float(proton_energy))

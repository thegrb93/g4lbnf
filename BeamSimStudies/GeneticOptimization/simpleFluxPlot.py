import ROOT, sys, os


##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 1:
    print "Please specify a flux file or directory path"
    sys.exit()

fluxpath = sys.argv[1]



##### MAKE VECTOR OF FLUX FILES ####

fluxfiles = []
if os.path.isdir(fluxpath):
    print "The path you specified ("+fluxpath+") appears to be a directory... looking for flux files in that directory."

    for root,dirs,files in os.walk(fluxpath):
        for file in files:
            temppath = os.path.join(root,file)
            
            if file.startswith("g4lbne_") and file.endswith(".root"):
                fluxfiles.append(temppath)
    print "Found ",len(fluxfiles)," flux files to consider"
else:
    print "Drawing fluxes from file:",fluxpath
    fluxfiles.append(fluxpath)


##### CREATE A CHAIN OF FLUX NTUPLES #####            

FluxChain = ROOT.TChain("nudata")
for file in fluxfiles:
    FluxChain.Add(file)


canv = ROOT.TCanvas("NeutrinoEnergyFar","NeutrinoEnergyFar")
canv.cd()
H_NeutrinoEnergyFar = ROOT.TH1F("H_NeutrinoEnergyFar","H_NeutrinoEnergyFar",40,0,20);
FluxChain.Draw("NenergyF[0]>>H_NeutrinoEnergyFar","Nimpwt/3.1415*NWtFar[0]")
#FluxChain.Draw("Nimpwt");

canv.Print("NeutrinoEnergyFar.png");
canv.Print("NeutrinoEnergyFar.eps");

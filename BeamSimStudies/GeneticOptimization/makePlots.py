
import ROOT, glob, sys, os, re
from array import array
import Optimizations, OptimizationUtils

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 1:
    print "Please specify an optimization name"
    sys.exit()

optimization_name = sys.argv[1]

max_configurations = 1000

if len(sys.argv)>2: 
    max_configurations = int(sys.argv[2])

##### Find processed configurations and calculate fitnesses #####

doSubFitnesses = True

configurations = []
fitnesses = []
fhcnumufitnesses = []
fhcnumubarfitnesses = []
rhcnumufitnesses = []
rhcnumubarfitnesses = []
fhcnuenuebarfitnesses = []
rhcnuenuebarfitnesses = []
nhfitnesses = []
ihfitnesses = []
mhfitnesses = []
fitness_errors = []
fhcnumufitness_errors = []
rhcnumufitness_errors = []
fhcnumubarfitness_errors = []
rhcnumubarfitness_errors = []
fhcnuenuebarfitness_errors = []
rhcnuenuebarfitness_errors = []
nhfitness_errors = []
ihfitness_errors = []
mhfitness_errors = []
proton_energies = []
zero_errors = []

optimization = Optimizations.Optimization(optimization_name)

fluxpath = optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/"

fluxdirs = glob.glob(fluxpath+"*")
if optimization.rhc_parameters_float_separate:
    fluxdirs = glob.glob(fluxpath+"*FHC*")

for dir in natural_sort(fluxdirs):

    if os.path.basename(dir).startswith("Optimizations-"+optimization_name):
        configuration = dir.split("/")[9].split("-")[2]
        
        if "Scan" in configuration: continue

        if float(configuration) < max_configurations:

            [tfitness_names,tfitnesses,tfiterrors] = optimization.getFitness(float(configuration))

            if tfitnesses[0]==-1: continue
            print configuration,tfitnesses[0]
            [fitness,fitness_error] = [tfitnesses[0],tfiterrors[0]]
            if optimization_name.startswith("CP"):
                
                [fhcnumufitness,fhcnumufitness_error] = [tfitnesses[1],tfiterrors[1]]
                [fhcnumubarfitness,fhcnumubarfitness_error] = [tfitnesses[2],tfiterrors[2]]
                [fhcnuenuebarfitness,fhcnuenuebarfitness_error] = [tfitnesses[3],tfiterrors[3]]
                [rhcnumufitness,rhcnumufitness_error] = [tfitnesses[4],tfiterrors[4]]
                [rhcnumubarfitness,rhcnumubarfitness_error] = [tfitnesses[5],tfiterrors[5]]
                [rhcnuenuebarfitness,rhcnuenuebarfitness_error] = [tfitnesses[6],tfiterrors[6]]
                [nhfitness,nhfitness_error] = [tfitnesses[7],tfiterrors[7]]
                [ihfitness,ihfitness_error] = [tfitnesses[8],tfiterrors[8]]
                [tfitness_names,tfitnesses,tfiterrors] = optimization.getFitness(float(configuration),"mh")
                [mhfitness,mhfitness_error] = [tfitnesses[0],tfiterrors[0]]

            # veto cases where fitness wasn't calculated or 
            # errors are absurdly large
            if(float(fitness)>0 and float(fitness_error) < 5):
        
                configurations.append(float(configuration))
                fitnesses.append(fitness)
                fitness_errors.append(fitness_error)
                if(optimization_name.startswith("CP_")):

                   fhcnumufitnesses.append(fhcnumufitness)
                   fhcnumufitness_errors.append(fhcnumufitness_error)
                   rhcnumufitnesses.append(rhcnumufitness)
                   rhcnumufitness_errors.append(rhcnumufitness_error)
                   fhcnumubarfitnesses.append(fhcnumubarfitness)
                   fhcnumubarfitness_errors.append(fhcnumubarfitness_error)
                   rhcnumubarfitnesses.append(rhcnumubarfitness)
                   rhcnumubarfitness_errors.append(rhcnumubarfitness_error)
                   fhcnuenuebarfitnesses.append(fhcnuenuebarfitness)
                   fhcnuenuebarfitness_errors.append(fhcnuenuebarfitness_error)
                   rhcnuenuebarfitnesses.append(rhcnuenuebarfitness)
                   rhcnuenuebarfitness_errors.append(rhcnuenuebarfitness_error)
                   ihfitnesses.append(ihfitness)
                   ihfitness_errors.append(ihfitness_error)
                   nhfitnesses.append(nhfitness)
                   nhfitness_errors.append(nhfitness_error)
                   mhfitnesses.append(mhfitness)
                   mhfitness_errors.append(mhfitness_error)
                else:
                   fhcnumufitnesses.append(0)
                   fhcnumufitness_errors.append(0)
                   rhcnumufitnesses.append(0)
                   rhcnumufitness_errors.append(0)
                   fhcnumubarfitnesses.append(0)
                   fhcnumubarfitness_errors.append(0)
                   rhcnumubarfitnesses.append(0)
                   rhcnumubarfitness_errors.append(0)
                   fhcnuenuebarfitnesses.append(0)
                   fhcnuenuebarfitness_errors.append(0)
                   rhcnuenuebarfitnesses.append(0)
                   rhcnuenuebarfitness_errors.append(0)
                   ihfitnesses.append(0)
                   ihfitness_errors.append(0)
                   nhfitnesses.append(0)
                   nhfitness_errors.append(0)
                   mhfitnesses.append(0)
                   mhfitness_errors.append(0)
                zero_errors.append(0.0)

##### Get nominal fitness errors ####
nominal_fitnesses = []
nominal_fitness_error = 0
[nominal_fitness,nominal_fitness_error] = optimization.getNominalFitness("cp")
nominal_fitnesses.append(nominal_fitness[0]); # standard
nominal_fitnesses.append(0); # fhcnumu
nominal_fitnesses.append(0); # fhcnumubar
nominal_fitnesses.append(0); # fhcnuenuebar
nominal_fitnesses.append(0); # rhcnumu
nominal_fitnesses.append(0); # rhcnumubar
nominal_fitnesses.append(0); # rhcnuenuebar
if optimization_name.startswith("CP_"):
    nominal_fitnesses.append(nominal_fitness[1]) #nh
    nominal_fitnesses.append(nominal_fitness[2]) #ih
print "NOMINAL STANDARD FITNESS: ",nominal_fitness[0]

[nominal_fitness,nominal_fitness_error] = optimization.getNominalFitness("mh") # mh
nominal_fitnesses.append(nominal_fitness[0]);


# Read in knob turns from macros
configurations2 = []
parameter_values = []

parameter_histos = []

n_parameters = len(optimization.parameter_names);
for i in range(0,n_parameters):
    parameter_name = optimization.parameter_names[i]
    parameter_values.append([])
    parameter_histos.append(ROOT.TH1F("h_parameters_"+str(i),parameter_name,10,optimization.parameter_lower_limits[i],optimization.parameter_upper_limits[i]));

knobturns = optimization.getKnobturns()

for configuration in configurations:
    if float(configuration)>max_configurations: continue
    configuration_found = False
    for j in range(0,len(knobturns)):
        if knobturns[j][0]==configuration:
            configuration_found = True
            configurations2.append(knobturns[j][0])
            for i in range(0,n_parameters):
                parameter_histos[i].Fill(knobturns[j][1][i])
                parameter_values[i].append(float(knobturns[j][1][i]))
    if not configuration_found:
        print "ERROR: configuration:",configuration," found in flux directories but can't find macros to look up knobturns... quitting."
        sys.exit()

if configurations != configurations2:
    print "WARNING: vector of configurations matched to fitness and vector of configurations matched to knobturns is different.  This will likely cause plots to be incorrect"
    

# Make TGraphs
canv = ROOT.TCanvas("MyCanvas","MyCanvas")
#split data into chunks to make tgraphs of different colors for different configurations
split_configurations = [[]]
split_fitnesses = [[]]
split_fhcnumufitnesses = [[]]
split_rhcnumufitnesses = [[]]
split_fhcnumubarfitnesses = [[]]
split_rhcnumubarfitnesses = [[]]
split_fhcnuenuebarfitnesses = [[]]
split_rhcnuenuebarfitnesses = [[]]
split_ihfitnesses = [[]]
split_nhfitnesses = [[]]
split_mhfitnesses = [[]]
split_zero_errors = [[]]
split_fitness_errors = [[]]
split_fhcnumufitness_errors = [[]]
split_rhcnumufitness_errors = [[]]
split_fhcnumubarfitness_errors = [[]]
split_rhcnumubarfitness_errors = [[]]
split_fhcnuenuebarfitness_errors = [[]]
split_rhcnuenuebarfitness_errors = [[]]
split_ihfitness_errors = [[]]
split_nhfitness_errors = [[]]
split_mhfitness_errors = [[]]
split_parameter_values = []
for j in range(0,n_parameters):
    split_parameter_values.append([[]])
gen_end = optimization.generation_size
gen_number = 0
for i in range(0,len(configurations)):
    if(configurations[i]>gen_end):
        gen_end = gen_end+optimization.generation_size
        gen_number = gen_number+1
        split_configurations.append([])
        split_fitnesses.append([])
        split_fhcnumufitnesses.append([])
        split_rhcnumufitnesses.append([])
        split_fhcnumubarfitnesses.append([])
        split_rhcnumubarfitnesses.append([])
        split_fhcnuenuebarfitnesses.append([])
        split_rhcnuenuebarfitnesses.append([])
        split_ihfitnesses.append([])
        split_nhfitnesses.append([])
        split_mhfitnesses.append([])
        split_zero_errors.append([])
        split_fitness_errors.append([])
        split_fhcnumufitness_errors.append([])
        split_rhcnumufitness_errors.append([])
        split_fhcnumubarfitness_errors.append([])
        split_rhcnumubarfitness_errors.append([])
        split_fhcnuenuebarfitness_errors.append([])
        split_rhcnuenuebarfitness_errors.append([])
        split_ihfitness_errors.append([])
        split_nhfitness_errors.append([])
        split_mhfitness_errors.append([])
        for j in range(0,n_parameters):
            split_parameter_values[j].append([])
    split_configurations[gen_number].append(float(configurations[i]))

    split_fitnesses[gen_number].append(float(fitnesses[i]))
    split_fhcnumufitnesses[gen_number].append(float(fhcnumufitnesses[i]))
    split_rhcnumufitnesses[gen_number].append(float(rhcnumufitnesses[i]))
    split_fhcnumubarfitnesses[gen_number].append(float(fhcnumubarfitnesses[i]))
    split_rhcnumubarfitnesses[gen_number].append(float(rhcnumubarfitnesses[i]))
    split_fhcnuenuebarfitnesses[gen_number].append(float(fhcnuenuebarfitnesses[i]))
    split_rhcnuenuebarfitnesses[gen_number].append(float(rhcnuenuebarfitnesses[i]))
    split_ihfitnesses[gen_number].append(float(ihfitnesses[i]))
    split_nhfitnesses[gen_number].append(float(nhfitnesses[i]))
    split_mhfitnesses[gen_number].append(float(mhfitnesses[i]))
    
    split_zero_errors[gen_number].append(float(zero_errors[i]))
    split_fitness_errors[gen_number].append(float(fitness_errors[i]))
    split_fhcnumufitness_errors[gen_number].append(float(fhcnumufitness_errors[i]))
    split_rhcnumufitness_errors[gen_number].append(float(rhcnumufitness_errors[i]))
    split_fhcnumubarfitness_errors[gen_number].append(float(fhcnumubarfitness_errors[i]))
    split_rhcnumubarfitness_errors[gen_number].append(float(rhcnumubarfitness_errors[i]))
    split_fhcnuenuebarfitness_errors[gen_number].append(float(fhcnuenuebarfitness_errors[i]))
    split_rhcnuenuebarfitness_errors[gen_number].append(float(rhcnuenuebarfitness_errors[i]))
    split_ihfitness_errors[gen_number].append(float(ihfitness_errors[i]))
    split_nhfitness_errors[gen_number].append(float(nhfitness_errors[i]))
    split_mhfitness_errors[gen_number].append(float(mhfitness_errors[i]))
    for j in range(0,n_parameters):
        split_parameter_values[j][gen_number].append(float(parameter_values[j][i]))

print "BLAH"
print configurations
print split_configurations

colors = [39,222,9]

fitness_types = ["Nu_mu Flux"]
if optimization_name.startswith("CP"):
    fitness_types = ["CP","fhcnumu","fhcnumubar","fhcnuenuebar","rhcnumu","rhcnumubar","rhcnuenuebar","ih","nh","mh"]
for type_iter in range(len(fitness_types)):
    fitness_vs_configurations = []
    
    for i in range(0,len(split_configurations)):
        
        if fitness_types[type_iter]=="CP" or fitness_types[type_iter]=="Nu_mu Flux":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_fitnesses[i]),array('d',split_zero_errors[i]),array('d',split_fitness_errors[i])));

        if fitness_types[type_iter]=="fhcnumu":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_fhcnumufitnesses[i]),array('d',split_zero_errors[i]),array('d',split_fhcnumufitness_errors[i])));
        if fitness_types[type_iter]=="fhcnumubar":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_fhcnumubarfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_fhcnumubarfitness_errors[i])));
        if fitness_types[type_iter]=="fhcnuenuebar":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_fhcnuenuebarfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_fhcnuenuebarfitness_errors[i])));
        if fitness_types[type_iter]=="rhcnumu":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_rhcnumufitnesses[i]),array('d',split_zero_errors[i]),array('d',split_rhcnumufitness_errors[i])));
        if fitness_types[type_iter]=="rhcnumubar":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_rhcnumubarfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_rhcnumubarfitness_errors[i])));
        if fitness_types[type_iter]=="rhcnuenuebar":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_rhcnuenuebarfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_rhcnuenuebarfitness_errors[i])));
        if fitness_types[type_iter]=="ih":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_ihfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_ihfitness_errors[i])));
        if fitness_types[type_iter]=="nh":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_nhfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_nhfitness_errors[i])));
        if fitness_types[type_iter]=="mh":
            fitness_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[i]),array('d',split_configurations[i]),array('d',split_mhfitnesses[i]),array('d',split_zero_errors[i]),array('d',split_mhfitness_errors[i])));

        x_max = max_configurations + 100
        
        fitness_vs_configurations[i].GetXaxis().SetLimits(0.0,x_max)
        fitness_vs_configurations[i].GetXaxis().SetTitle("Configuration")
        fitness_vs_configurations[i].GetYaxis().SetTitle(fitness_types[type_iter]+" Fitness")
        fitness_vs_configurations[i].GetXaxis().CenterTitle()
        fitness_vs_configurations[i].GetYaxis().CenterTitle()
        fitness_vs_configurations[i].SetMarkerColor(colors[i%len(colors)]);
        fitness_vs_configurations[i].SetLineColor(colors[i%len(colors)]);
    
        if(i==0):
            fitness_vs_configurations[i].SetTitle("");
            fitness_vs_configurations[i].Draw("ap");
            a_line = ROOT.TLine(0,nominal_fitnesses[type_iter],x_max,nominal_fitnesses[type_iter])
            a_line.SetLineWidth(2)

            if(fitness_types[type_iter]=="CP" or fitness_types[type_iter]=="Nu_mu Flux"):
                fitness_vs_configurations[i].SetMaximum(max(fitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(fitnesses)*0.8)
            if(fitness_types[type_iter]=="fhcnumu"):
                fitness_vs_configurations[i].SetMaximum(max(fhcnumufitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(fhcnumufitnesses))
            if(fitness_types[type_iter]=="fhcnumubar"):
                fitness_vs_configurations[i].SetMaximum(max(fhcnumubarfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(fhcnumubarfitnesses))
            if(fitness_types[type_iter]=="fhcnuenuebar"):
                fitness_vs_configurations[i].SetMaximum(max(fhcnuenuebarfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(fhcnuenuebarfitnesses))
            if(fitness_types[type_iter]=="rhcnumu"):
                fitness_vs_configurations[i].SetMaximum(max(rhcnumufitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(rhcnumufitnesses))
            if(fitness_types[type_iter]=="rhcnumubar"):
                fitness_vs_configurations[i].SetMaximum(max(rhcnumubarfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(rhcnumubarfitnesses))
            if(fitness_types[type_iter]=="rhcnuenuebar"):
                fitness_vs_configurations[i].SetMaximum(max(rhcnuenuebarfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(min(rhcnuenuebarfitnesses))
            if(fitness_types[type_iter]=="ih"):
                fitness_vs_configurations[i].SetMaximum(max(ihfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(0)
            if(fitness_types[type_iter]=="nh"):
                fitness_vs_configurations[i].SetMaximum(max(nhfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(0)
            if(fitness_types[type_iter]=="mh"):
                fitness_vs_configurations[i].SetMaximum(max(mhfitnesses)*1.1)
                fitness_vs_configurations[i].SetMinimum(3)

            a_line.SetLineStyle(2);
            a_line.Draw("SAME");
            fitness_vs_configurations[i].Draw("psame");

        else:
            fitness_vs_configurations[i].Draw("psame");

    #latex = ROOT.TLatex( 0.67, 0.67, "Nominal Configuration" );
    #latex.SetNDC()
    #latex.SetTextSize(0.035);
    #latex.Draw()
    canv.Print("plots/"+optimization_name+"_"+fitness_types[type_iter]+"_fitness_vs_configuration.eps");
    canv.Print("plots/"+optimization_name+"_"+fitness_types[type_iter]+"_fitness_vs_configuration.png");


ROOT.gStyle.SetMarkerStyle(2);

best_parameter_values = []
best_parameter_errors = []
for i in range(0,n_parameters):
    best_parameter_values.append([])
    best_parameter_errors.append([])

for j in range(0,len(fitnesses)):

#    if(fitnesses[j]>2.4): 
        for i in range(0,n_parameters):
            best_parameter_values[i].append(parameter_values[i][j])
            best_parameter_errors[i].append(0)
        

for i in range(0,n_parameters):

    parameter_vs_configurations = []
    y_low = optimization.parameter_lower_limits[i]-(optimization.parameter_upper_limits[i]-optimization.parameter_lower_limits[i])*0.05;
    y_low = optimization.parameter_upper_limits[i]+(optimization.parameter_upper_limits[i]-optimization.parameter_lower_limits[i])*0.05;
    frame = ROOT.TH2D("frame","frame",100,0,max_configurations+100,100,optimization.parameter_lower_limits[i],optimization.parameter_upper_limits[i]);
    frame.SetTitle(optimization.parameter_names[i]);
    frame.Draw();
    ROOT.gPad.Update()
    title = ROOT.gPad.GetPrimitive("title");
    title.SetBorderSize(0);

    for j in range(0,len(split_configurations)):

        parameter_vs_configurations.append(ROOT.TGraphErrors(len(split_configurations[j]),array('d',split_configurations[j]),array('d',split_parameter_values[i][j]),array('d',split_zero_errors[j]),array('d',split_zero_errors[j])));
        parameter_vs_configurations[j].GetXaxis().SetLimits(0.0,max_configurations+100)
#parameter_vs_configuration.GetHistogram().SetMaximum(0.8e-18);
        y_axis_title = optimization.parameter_names[i]
        if(optimization.parameter_units[i]!=""):
            y_axis_title = y_axis_title+" ("+optimization.parameter_units[i]+")"
        frame.GetXaxis().SetTitle("Configuration")
        frame.GetYaxis().SetTitle(y_axis_title)
        frame.GetXaxis().CenterTitle()
        frame.GetYaxis().CenterTitle()
        parameter_vs_configurations[j].SetMarkerColor(colors[j%len(colors)]);
        parameter_vs_configurations[j].SetLineColor(colors[j%len(colors)]);
        parameter_vs_configurations[j].Draw("psame");

    parameter_name = optimization.parameter_names[i]
    canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_configuration.eps");
    canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_configuration.png");

    fitness_types = ["Nu_mu Flux"]
    if optimization_name.startswith("CP_"):
        fitness_types = ["CP","fhcnumu","fhcnumubar","fhcnuenuebar","rhcnumu","rhcnumubar","rhcnuenuebar","nh","ih","mh"]
    for fitness_type in fitness_types:
        if fitness_type=="CP" or fitness_type=="Nu_mu Flux":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',fitnesses),array('d',zero_errors),array('d',fitness_errors))
        if fitness_type=="fhcnumu":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',fhcnumufitnesses),array('d',zero_errors),array('d',fhcnumufitness_errors))
        if fitness_type=="fhcnumubar":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',fhcnumubarfitnesses),array('d',zero_errors),array('d',fhcnumubarfitness_errors))
        if fitness_type=="fhcnuenuebar":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',fhcnuenuebarfitnesses),array('d',zero_errors),array('d',fhcnuenuebarfitness_errors))
        if fitness_type=="rhcnumu":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',rhcnumufitnesses),array('d',zero_errors),array('d',rhcnumufitness_errors))
        if fitness_type=="rhcnumubar":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',rhcnumubarfitnesses),array('d',zero_errors),array('d',rhcnumubarfitness_errors))
        if fitness_type=="rhcnuenuebar":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',rhcnuenuebarfitnesses),array('d',zero_errors),array('d',rhcnuenuebarfitness_errors))
        if fitness_type=="mh":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',mhfitnesses),array('d',zero_errors),array('d',mhfitness_errors))
        if fitness_type=="ih":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',ihfitnesses),array('d',zero_errors),array('d',ihfitness_errors))
        if fitness_type=="nh":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',nhfitnesses),array('d',zero_errors),array('d',nhfitness_errors))
        fitness_vs_parameter.GetYaxis().SetTitle(fitness_type+" Fitness")
        x_axis_title = optimization.parameter_names[i]
        if(optimization.parameter_units[i]!=""):
            x_axis_title = x_axis_title+" ("+optimization.parameter_units[i]+")"
        fitness_vs_parameter.GetXaxis().SetTitle(x_axis_title);
        fitness_vs_parameter.GetXaxis().CenterTitle()
        fitness_vs_parameter.GetYaxis().CenterTitle()
        fitness_vs_parameter.Draw("AP");
        canv.Print("plots/"+optimization_name+"_"+fitness_type+"_fitness_vs_"+parameter_name+".eps")
        canv.Print("plots/"+optimization_name+"_"+fitness_type+"_fitness_vs_"+parameter_name+".png")

    """
    for j in range(i+1,n_parameters):
        parameter_vs_parameter = ROOT.TGraphErrors(len(best_parameter_values[i]),array('d',best_parameter_values[i]),array('d',best_parameter_values[j]),array('d',best_parameter_errors[i]),array('d',best_parameter_errors[j]))
        x_axis_title = optimization.parameter_names[i]
        y_axis_title = optimization.parameter_names[j]
        if(optimization.parameter_units[i]!=""):
            x_axis_title = x_axis_title+" ("+optimization.parameter_units[i]+")"
        if(optimization.parameter_units[j]!=""):
            y_axis_title = y_axis_title+" ("+optimization.parameter_units[j]+")"
        parameter_vs_parameter.GetXaxis().SetTitle(x_axis_title);
        parameter_vs_parameter.GetYaxis().SetTitle(y_axis_title);
        parameter_vs_parameter.GetXaxis().CenterTitle()
        parameter_vs_parameter.GetYaxis().CenterTitle()
        parameter_vs_parameter.Draw("AP");
        canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_"+optimization.parameter_names[j]+".eps")
        canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_"+optimization.parameter_names[j]+".png")
   """

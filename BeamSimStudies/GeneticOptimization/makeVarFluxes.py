import ROOT, os, array, sys, OptimizationUtils, shutil

templatefhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_200kA_LBNEFD_fastmc.root"        
templaterhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/-200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_-200kA_LBNEFD_fastmc.root"
for flux in ["fhc_numu","fhc_numubar","fhc_nue","rhc_numu","rhc_numubar","rhc_nuebar"]:
    for var in ["0","50","90","100","110","200","500","1000"]:
        print flux,var
        nu_type = flux.split("_")[1]
        mode = flux.split("_")[0]
        newfile = ROOT.TFile("temp/"+mode+"_"+flux+var+".root","RECREATE")
        if(flux.startswith("fhc")):
               oldfile = ROOT.TFile(templatefhcfile)
        else:
               oldfile = ROOT.TFile(templaterhcfile)
        dirList = ROOT.gDirectory.GetListOfKeys()
        newfile.cd()
        for stuff in dirList:
            if "flux" in stuff.GetName() and "osc" not in stuff.GetName():
                temp = oldfile.Get(stuff.GetName()).Clone()
                if ((nu_type=="numu" and stuff.GetName()=="numu_flux") or (nu_type=="numubar" and stuff.GetName()=="numubar_flux") or ("nue" in nu_type and "nue" in stuff.GetName())):
                    temp.Scale(float(var)/100.0)
                temp.Write()
        newfile.Close()
        oldfile.Close()
        if(flux.startswith("fhc")):
            newrhcfile = "temp/rhc_"+flux+var+".root"
            shutil.copyfile(templaterhcfile,newrhcfile)
        else:
            newfhcfile = "temp/fhc_"+flux+var+".root"
            shutil.copyfile(templatefhcfile,newfhcfile)


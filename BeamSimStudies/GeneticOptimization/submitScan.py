import ROOT, glob, sys, os, re
from array import array
import Optimizations, OptimizationUtils

##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 2:
    print "Please specify an optimization name and configuration number"
    sys.exit()

optimization_name = sys.argv[1]
configuration = int(sys.argv[2])

optimization = Optimizations.Optimization(optimization_name)
macro_template = "../../macros/NoSnout.mac"

##### Loop over Parameters that were optimized ####
n_scan_points = 30

#print optimization.parameter_names
#sys.exit()

for par_iter in range(len(optimization.parameter_names)):
    if par_iter < 2: continue

    lower_limit = optimization.parameter_lower_limits[par_iter]
    upper_limit = optimization.parameter_upper_limits[par_iter]

#    if optimization.parameter_names[par_iter] not in ["FHCHorn1ICRadius1","FHCHorn1ICRadius2","FHCHorn1ICRadius4","FHCHorn1OCRadius","FHCHorn1ICLength1","FHCHorn1ICLength3","FHCHorn1ICLength4","FHCHorn1ICLength5","FHCHorn2LongRescale","FHCHorn2RadialRescale","FHCHorn2RadialRescaleCst","FHCProtonEnergy","RHCHorn1ICRadius1","RHCHorn2RadialRescaleCst"]:
#        continue

    # Loop over values to scan for this parameter
    for scan_iter in range(n_scan_points+1):

        scan_value = lower_limit + scan_iter*(upper_limit-lower_limit)/n_scan_points

        # Create new macro files
        macro_output = "../../macros/Optimizations/"+optimization.optname+"/"+optimization.optname+"_"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)+".mac"
        macro_outputRHC = "../../macros/Optimizations/"+optimization.optname+"/"+optimization.optname+"_"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)+".mac"
        if optimization.rhc_parameters_float_separate:
            macro_output = "../../macros/Optimizations/"+optimization.optname+"FHC/"+optimization.optname+"FHC_"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)+".mac"
            macro_outputRHC = "../../macros/Optimizations/"+optimization.optname+"RHC/"+optimization.optname+"RHC_"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)+".mac"


        oldmac = open(macro_template)
        oldmac_lines = oldmac.readlines()
        oldmac.close()
        newmac = open(macro_output, 'w')    
        newmacRHC = open(macro_outputRHC, 'w')    

        par_values = optimization.readVariationFromMacros(configuration)
        par_values[par_iter] = scan_value

        modes = ["FHC"]
        if optimization.rhc_parameters_float_separate:
            modes = ["FHC","RHC"]

        for mode in modes:
         for s in oldmac_lines:
            output_file = newmac
            if mode=="RHC":
                output_file = newmacRHC

            if s.find("/LBNE/det/seHornCurrent")>=0:
                if not mode+"HornCurrent" in optimization.parameter_names and not "HornCurrent" in optimization.parameter_names:
                    output_file.write(s)
                for j in range(0,len(optimization.parameter_names)):
                    if optimization.rhc_parameters_float_separate:
                        if (optimization.parameter_names[j].startswith("FHC") and
                            mode=="RHC"):
                            continue
                        if (optimization.parameter_names[j].startswith("RHC") and
                            mode=="FHC"):
                            continue
                    parameter = optimization.parameter_names[j]
                    macro_set_stage = optimization.macro_set_stages[j]

                    if macro_set_stage == "PreInit":
                        value = par_values[j]
                        output_file.write(optimization.macro_commands[j]+" "+str(value)+" "+optimization.parameter_units[j]+"\n")

                        if("MultiSphereTargetRadius" in parameter):
                            output_file.write("/LBNE/det/UseMultiSphereTarget\n")
                            output_file.write("/LBNE/det/TargetMaterial Beryllium\n")  
                        if("SimpleTargetLength" in parameter):
                            output_file.write("/LBNE/det/UseSimpleCylindricalTarget\n")

                prefix = ""
                if optimization.rhc_parameters_float_separate:
                    prefix = mode

                    # setup special horn shape
                if optimization.optname in ["CP_run5","CP_run7","CP_run8","CP_run13"]:
                    Horn1OCRadius = par_values[optimization.getParameterIndex(prefix+"Horn1OCRadius")];
                    Horn1ICRadius1 = par_values[optimization.getParameterIndex(prefix+"Horn1ICRadius1")];
                    Horn1ICRadius2 = par_values[optimization.getParameterIndex(prefix+"Horn1ICRadius2")];
                    Horn1ICRadius3 = par_values[optimization.getParameterIndex(prefix+"Horn1ICRadius3")];
                    Horn1ICRadius4 = par_values[optimization.getParameterIndex(prefix+"Horn1ICRadius4")];
                    Horn1ICLength1 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength1")];
                    Horn1ICLength2 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength2")];
                    Horn1ICLength3 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength3")];
                    Horn1ICLength4 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength4")];
                    Horn1ICLength5 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength5")];
                    Horn1ICLength6 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength6")];
                    Horn1ICLength7 = par_values[optimization.getParameterIndex(prefix+"Horn1ICLength7")];
                    output_file.write("/LBNE/det/UseHorn1Polycone True \n")
                    output_file.write("/LBNE/det/NumInnerPtsHorn1Polycone 10 \n")
                    output_file.write("/LBNE/det/Horn1PolyPt0RinThickZ "+str(Horn1OCRadius)+"  2.000  0.00 mm\n");
                    output_file.write("/LBNE/det/Horn1PolyPt1RinThickZ "+str(Horn1ICRadius1)+"  2.000  1.00 mm\n");
                    output_file.write("/LBNE/det/Horn1PolyPt2RinThickZ "+str(Horn1ICRadius1)+"  2.000  "+str(1.00+Horn1ICLength1)+" mm\n");
                    output_file.write("/LBNE/det/Horn1PolyPt3RinThickZ "+str(Horn1ICRadius2)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt4RinThickZ "+str(Horn1ICRadius2)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt5RinThickZ "+str(Horn1ICRadius3)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt6RinThickZ "+str(Horn1ICRadius3)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt7RinThickZ "+str(Horn1ICRadius4)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt8RinThickZ "+str(Horn1ICRadius4)+"  2.000  "+str(1.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6+Horn1ICLength7)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyPt9RinThickZ "+str(Horn1OCRadius)+"  2.000  "+str(2.00+Horn1ICLength1+Horn1ICLength2+Horn1ICLength3+Horn1ICLength4+Horn1ICLength5+Horn1ICLength6+Horn1ICLength7)+" mm\n")
                    output_file.write("/LBNE/det/Horn1PolyOuterRadius "+str(Horn1OCRadius)+" mm\n")
                    
                    output_file.write("/LBNE/det/TargetLengthOutsideHorn 0 m\n")
                elif optimization.optname in ["CP_run9","CP_run10","CP_run11","CP_run14","CP_run15","CP_run16","CP_run17","CP_run18","CP_run19"]:


                            HornARadiusOC = par_values[optimization.getParameterIndex(prefix+"HornARadiusOC")];
                            HornARadius1 = par_values[optimization.getParameterIndex(prefix+"HornARadius1")];
                            HornARadius2 = par_values[optimization.getParameterIndex(prefix+"HornARadius2")];
                            HornALength = par_values[optimization.getParameterIndex(prefix+"HornALength")];
                            HornAF1 = par_values[optimization.getParameterIndex(prefix+"HornAF1")];
                            HornBRadiusOC = par_values[optimization.getParameterIndex(prefix+"HornBRadiusOC")];
                            HornBRadius1 = par_values[optimization.getParameterIndex(prefix+"HornBRadius1")];
                            HornBRadius2 = par_values[optimization.getParameterIndex(prefix+"HornBRadius2")];
                            HornBRadius3 = par_values[optimization.getParameterIndex(prefix+"HornBRadius3")];
                            HornBLength = par_values[optimization.getParameterIndex(prefix+"HornBLength")];
                            HornBF1 = par_values[optimization.getParameterIndex(prefix+"HornBF1")];
                            HornBF2 = par_values[optimization.getParameterIndex(prefix+"HornBF2")];
                            HornBF3 = par_values[optimization.getParameterIndex(prefix+"HornBF3")];
                            HornBF4 = par_values[optimization.getParameterIndex(prefix+"HornBF4")];
                            HornBF5 = par_values[optimization.getParameterIndex(prefix+"HornBF5")];


                            HornCRadiusOC = par_values[optimization.getParameterIndex(prefix+"HornCRadiusOC")];
                            HornCRadius1 = par_values[optimization.getParameterIndex(prefix+"HornCRadius1")];
                            HornCRadius2 = par_values[optimization.getParameterIndex(prefix+"HornCRadius2")];
                            HornCRadius3 = par_values[optimization.getParameterIndex(prefix+"HornCRadius3")];
                            HornCLength = par_values[optimization.getParameterIndex(prefix+"HornCLength")];
                            HornCF1 = par_values[optimization.getParameterIndex(prefix+"HornCF1")];
                            HornCF2 = par_values[optimization.getParameterIndex(prefix+"HornCF2")];
                            HornCF3 = par_values[optimization.getParameterIndex(prefix+"HornCF3")];
                            HornCF4 = par_values[optimization.getParameterIndex(prefix+"HornCF4")];
                            HornCF5 = par_values[optimization.getParameterIndex(prefix+"HornCF5")];

                            HornBLongPosition = par_values[optimization.getParameterIndex(prefix+"HornBLongPosition")]
                            HornCLongPosition = par_values[optimization.getParameterIndex(prefix+"HornCLongPosition")]

                            newmac.write("/LBNE/det/NumberOfHornsPolycone 3 \n")
                            newmac.write("/LBNE/det/Horn2PolyZStartPos "+str(HornBLongPosition)+" \n")
                            newmac.write("/LBNE/det/Horn3PolyZStartPos "+str(HornCLongPosition)+" \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn1Polycone 5 \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn2Polycone 8 \n")
                            newmac.write("/LBNE/det/NumInnerPtsHorn3Polycone 8 \n")

                            newmac.write("/LBNE/det/Horn1PolyPt0RinThickZ "+str(HornARadiusOC-15)+"  2.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt1RinThickZ "+str(HornARadius1)+"  2.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt2RinThickZ "+str(HornARadius1)+"  2.000  "+str(1.00+HornALength*HornAF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn1PolyPt3RinThickZ "+str(HornARadius2)+"  2.000  "+str(1.00+HornALength)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyPt4RinThickZ "+str(HornARadiusOC-15)+"  2.000  "+str(2.00+HornALength)+" mm\n")
                            newmac.write("/LBNE/det/Horn1PolyOuterRadius "+str(HornARadiusOC)+" mm\n")

                            newmac.write("/LBNE/det/Horn2PolyPt0RinThickZ "+str(HornBRadiusOC-15)+"  2.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt1RinThickZ "+str(HornBRadius1)+"  2.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt2RinThickZ "+str(HornBRadius1)+"  2.000  "+str(1.00+HornBLength*HornBF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn2PolyPt3RinThickZ "+str(HornBRadius2)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt4RinThickZ "+str(HornBRadius2)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2+HornBF3))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt5RinThickZ "+str(HornBRadius3)+"  2.000  "+str(1.00+HornBLength*(HornBF1+HornBF2+HornBF3+HornBF4))+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt6RinThickZ "+str(HornBRadius3)+"  2.000  "+str(1.00+HornBLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyPt7RinThickZ "+str(HornBRadiusOC-15)+"  2.000  "+str(2.00+HornBLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn2PolyOuterRadius "+str(HornBRadiusOC)+" mm\n")
                
                            newmac.write("/LBNE/det/Horn3PolyPt0RinThickZ "+str(HornCRadiusOC-15)+"  4.000  0.00 mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt1RinThickZ "+str(HornCRadius1)+"  4.000  1.00 mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt2RinThickZ "+str(HornCRadius1)+"  4.000  "+str(1.00+HornCLength*HornCF1)+" mm\n");
                            newmac.write("/LBNE/det/Horn3PolyPt3RinThickZ "+str(HornCRadius2)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt4RinThickZ "+str(HornCRadius2)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2+HornCF3))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt5RinThickZ "+str(HornCRadius3)+"  4.000  "+str(1.00+HornCLength*(HornCF1+HornCF2+HornCF3+HornCF4))+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt6RinThickZ "+str(HornCRadius3)+"  4.000  "+str(1.00+HornCLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyPt7RinThickZ "+str(HornCRadiusOC-15)+"  4.000  "+str(2.00+HornCLength)+" mm\n")
                            newmac.write("/LBNE/det/Horn3PolyOuterRadius "+str(HornCRadiusOC)+" mm\n")

                            newmac.write("/LBNE/det/TargetLengthOutsideHorn 0 m\n")
                  
                            if optimization.optname=="CP_run19":
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 1 \n")                 
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 2 \n")                 
                                newmac.write("/LBNE/det/SetPolyconeHornParabolic 3 \n")                 



            elif s.find("/run/initialize")>=0:
                output_file.write(s)
                for j in range(0,len(optimization.parameter_names)):

                    if (optimization.parameter_names[j].startswith("FHC") and
                        mode=="RHC"):
                        continue
                    if (optimization.parameter_names[j].startswith("RHC") and
                        mode=="FHC"):
                        continue

                    parameter = optimization.parameter_names[j]

                    macro_set_stage = optimization.macro_set_stages[j]
                    if(macro_set_stage == "Idle"):
                        value = par_values[j]
                        output_file.write(optimization.macro_commands[j]+" "+str(value)+" "+optimization.parameter_units[j]+"\n")
                        # Also scale beam width to match target fin size
                        # For optimizations other than "CP_test", which
                        # was started without this scaling and shouldn't be
                        # changed in the middle of the optimization
                    if(parameter==prefix+"GraphiteTargetFinWidth"):
                      if optimization.optname in ["CP_run5","CP_run7","CP_run8","CP_run13"]:
                        # Scale nominal size (1.7 mm) based on
                        # change in fin width from nominal (.10 m)
                        new_beam_size = 1.7 * par_values[j]/10
                        output_file.write("/LBNE/generator/beamSigmaX "+str(new_beam_size)+" mm \n");
                        output_file.write("/LBNE/generator/beamSigmaY "+str(new_beam_size)+" mm \n");
                    if parameter == "BeamSigma":
                                temp = optimization.macro_commands[j].replace("SigmaX","SigmaY")
                                newmac.write(temp+" "+str(value)+" "+optimization.parameter_units[j]+"\n")
                
        
            else:
                output_file.write(s);
    
         output_file.close();
        
         # submit jobs
         os.chdir("../..")
         print "prefix",prefix
         print "opt parnames",optimization.parameter_names[par_iter]
         if not optimization.rhc_parameters_float_separate:
             os.system("python ProductionScripts/submit_flux.py  -n 100000 -f 1 -l 5 --mode neutrino --notuples -m Optimizations-"+optimization.optname+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
             os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode antineutrino --notuples -m Optimizations-"+optimization.optname+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
             os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode neutrino --notuples -p FTFP_BERT -m Optimizations-"+optimization.optname+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
             os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode antineutrino --notuples -p FTFP_BERT -m Optimizations-"+optimization.optname+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
         elif mode=="FHC":
              os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode neutrino --notuples -m Optimizations-"+optimization.optname+mode+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))            
              os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode neutrino --notuples -p FTFP_BERT -m Optimizations-"+optimization.optname+mode+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
         elif mode=="RHC":
              os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode antineutrino --notuples -m Optimizations-"+optimization.optname+mode+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))            
              os.system("python ProductionScripts/submit_flux.py -n 100000 -f 1 -l 5 --mode antineutrino --notuples -p FTFP_BERT -m Optimizations-"+optimization.optname+mode+"-"+str(configuration)+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))

         os.chdir("BeamSimStudies/GeneticOptimization")


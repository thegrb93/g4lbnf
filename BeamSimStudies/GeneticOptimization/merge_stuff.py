import os

if __name__ == "__main__":
    for i in range(501,601):
        os.system("python ../../ProductionScripts/merge_histograms.py --n 100000  -m Optimizations-HE_test-"+str(i))

def merge_one(optname,i):
    os.system("python ../../ProductionScripts/merge_histograms.py -c 200 --n 100000  -m Optimizations-"+optname+"-"+str(i))
    os.system("python ../../ProductionScripts/merge_histograms.py -c -200 --n 100000  -m Optimizations-"+optname+"-"+str(i))
    os.system("python ../../ProductionScripts/merge_histograms.py -c 200 --n 100000  -m Optimizations-"+optname+"FHC-"+str(i))
    os.system("python ../../ProductionScripts/merge_histograms.py -c -200 --n 100000  -m Optimizations-"+optname+"RHC-"+str(i))
    

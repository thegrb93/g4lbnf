\contentsline {section}{\numberline {1}introduction}{2}{section.1}
\contentsline {section}{\numberline {2}CP Sensitivity Estimator}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Comparisons of Sensitivity Estimator with Fast MC Sensitivities}{4}{subsection.2.1}
\contentsline {section}{\numberline {3}Beam Simulation}{12}{section.3}
\contentsline {section}{\numberline {4}Optimization Algorithm}{13}{section.4}
\contentsline {section}{\numberline {5}Results of Optimization}{14}{section.5}
\contentsline {section}{\numberline {6}Parameter Scan}{25}{section.6}
\contentsline {section}{\numberline {7}Appendix: Alternate optimization using the FTFP\_BERT physics list}{34}{section.7}

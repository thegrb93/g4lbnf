import ROOT, glob, sys, os
from array import array
import Optimizations, OptimizationUtils

##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 1:
    print "Please specify an optimization name"
    sys.exit()

optimization_name = sys.argv[1]

max_configurations = 3200

if len(sys.argv)>2: 
    max_configurations = int(sys.argv[2])

##### Find processed configurations and calculate fitnesses #####

configurations = []
fitnesses = []
fitness_errors = []
proton_energies = []
zero_errors = []

n_var_plots = 1;

optimization = Optimizations.Optimization(optimization_name)

fluxpath = optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/"
fluxdirs = glob.glob(fluxpath+"*")
if optimization.rhc_parameters_float_separate:
    fluxdirs=glob.glob(fluxpath+"*FHC*")

for dir in fluxdirs:

    if "Scan" in dir: 
        continue

    if os.path.basename(dir).startswith("Optimizations-"+optimization_name):
        configuration = dir.split("/")[9].split("-")[2]

        if float(configuration) >max_configurations: continue
        [fitness_types,tfitnesses,fiterrors] = optimization.getFitness(float(configuration))
        fitness = tfitnesses[0]
        fitness_error = fiterrors[0]
        print "AAAA",configuration, fitness, fitness_error
        
        if(fitness>0):
            configurations.append(float(configuration))
            fitnesses.append(fitness)
            fitness_errors.append(fitness_error)
            zero_errors.append(0.0)

# Git list of list indices ordered by fitness
ordered_indices = [i[0] for i in sorted(enumerate(fitnesses), key=lambda x:x[1],reverse=True)]

knobturns = optimization.getKnobturns()

for i in range(0,4):
    config_number = configurations[ordered_indices[i]]

    print "config_number",config_number

    knob_index = -1
    for j in range(0,len(knobturns)):
        if float(knobturns[j][0])==float(config_number):
            knob_index = j
            break
    if knob_index == -1:
        print "ERROR: Could not find knobs for configuration",config_number
    knob_names = optimization.parameter_names

    print "\n\nFitness rank:",i+1
    print "Configuration number:",int(config_number)
    all_fitnesses = optimization.getFitness(int(config_number))
    print "Fitnesses:"
    print "  ",all_fitnesses
    print "MH Fitnesses"
    print " ",optimization.getFitness(int(config_number),"mh")
    #print fitnesses[ordered_indices[i]]
    for j in range(0,len(knobturns[knob_index][1])):
        print "     ",optimization.parameter_names[j],knobturns[knob_index][1][j]

# Get Nominal Flux
nom_flux_file_fhc = ROOT.TFile("/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_200kA_LBNEFD_fastmc.root")
nom_flux_file_rhc = ROOT.TFile("/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/QGSP_BERT/Nominal/-200kA/flux/histos_g4lbne_v3r2p4_QGSP_BERT_Nominal_-200kA_LBNEFD_fastmc.root")

nom_flux = []
nom_flux.append(nom_flux_file_fhc.Get("numu_flux"))
nom_flux.append(nom_flux_file_fhc.Get("numubar_flux"))
nom_flux.append(nom_flux_file_fhc.Get("nue_flux"))
nom_flux.append(nom_flux_file_fhc.Get("nuebar_flux"))
nom_flux.append(nom_flux_file_rhc.Get("numu_flux"))
nom_flux.append(nom_flux_file_rhc.Get("numubar_flux"))
nom_flux.append(nom_flux_file_rhc.Get("nue_flux"))
nom_flux.append(nom_flux_file_rhc.Get("nuebar_flux"))

# Get Optimized Fluxes
n_fluxes_to_plot = 4
var_flux_files_fhc = []
var_flux_files_rhc = []
var_fluxes = []
new_bins = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]
for i in range(0,n_fluxes_to_plot):            

    configuration = configurations[ordered_indices[i]]
    print "configuration",configuration
    if optimization.rhc_parameters_float_separate:
        var_flux_files_fhc.append(ROOT.TFile(optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optimization_name+"FHC-"+str(int(configuration))+"/200kA/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+optimization.physics_list+"_Optimizations-"+optimization_name+"FHC-"+str(int(configuration))+"_200kA_"+optimization.detector_location_name+"_fastmc.root"))
        var_flux_files_rhc.append(ROOT.TFile(optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optimization_name+"RHC-"+str(int(configuration))+"/-200kA/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+optimization.physics_list+"_Optimizations-"+optimization_name+"RHC-"+str(int(configuration))+"_-200kA_"+optimization.detector_location_name+"_fastmc.root"))
    else:
        var_flux_files_fhc.append(ROOT.TFile(optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optimization_name+"-"+str(int(configuration))+"/neutrino/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+optimization.physics_list+"_Optimizations-"+optimization_name+"-"+str(int(configuration))+"_neutrino_"+optimization.detector_location_name+"_fastmc.root"))
        var_flux_files_rhc.append(ROOT.TFile(optimization.output_location+"users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+optimization.physics_list+"/Optimizations-"+optimization_name+"-"+str(int(configuration))+"/antineutrino/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+optimization.physics_list+"_Optimizations-"+optimization_name+"-"+str(int(configuration))+"_antineutrino_"+optimization.detector_location_name+"_fastmc.root"))

    var_fluxes.append([])
    var_fluxes[i].append(var_flux_files_fhc[i].Get("numu_flux"))
    var_fluxes[i].append(var_flux_files_fhc[i].Get("numubar_flux"))
    var_fluxes[i].append(var_flux_files_fhc[i].Get("nue_flux"))
    var_fluxes[i].append(var_flux_files_fhc[i].Get("nuebar_flux"))
    var_fluxes[i].append(var_flux_files_rhc[i].Get("numu_flux"))
    var_fluxes[i].append(var_flux_files_rhc[i].Get("numubar_flux"))
    var_fluxes[i].append(var_flux_files_rhc[i].Get("nue_flux"))
    var_fluxes[i].append(var_flux_files_rhc[i].Get("nuebar_flux"))
    fhcenergy = optimization.getEnergy(configuration,"FHC")
    rhcenergy = optimization.getEnergy(configuration,"RHC")
    print "Energies",fhcenergy,rhcenergy

    # Assume 7.5e13 protons / cycle and 1.2 s cycle time @120 GeV
    # Scale other energies as a function of power

    scale_factor_fhc = 7.5e13/1.2*OptimizationUtils.GetPowerPOTScaleFactor(fhcenergy)
    scale_factor_rhc = 7.5e13/1.2*OptimizationUtils.GetPowerPOTScaleFactor(rhcenergy)
    print scale_factor_fhc
    for j in range(0,4):
        var_fluxes[i][j] = var_fluxes[i][j].Rebin(len(new_bins)-1,"var_flux_"+str(j),array('d',new_bins))
        var_fluxes[i][j].Scale(scale_factor_fhc)
        var_fluxes[i][j].Scale(1.0,"width") # per GeV
    for j in range(4,8):
        var_fluxes[i][j] = var_fluxes[i][j].Rebin(len(new_bins)-1,"var_flux_"+str(j),array('d',new_bins))
        var_fluxes[i][j].Scale(scale_factor_rhc)
        var_fluxes[i][j].Scale(1.0,"width") # per GeV


for j in range(0,8):
    nom_flux[j] = nom_flux[j].Rebin(len(new_bins)-1,"nom_flux",array('d',new_bins))
    nom_flux[j].Scale(7.5e13/1.2,"width") # per GeV

# Plot all fluxes
    canv = ROOT.TCanvas("MyCanvas","MyCanvas")
    maxes = [nom_flux[j].GetMaximum()]
#for var_flux in var_fluxes:
#    maxes.append[var_flux.GetMaximum()]
    plot_max = max(maxes)*1.7

    colors = [2,4,8,6]
    nom_flux[j].SetMaximum(plot_max)
    nom_flux[j].SetTitle("")
    nom_flux[j].Draw()

    frame = ROOT.TH2D("frame","frame",100,0,8,100,0,plot_max);
    frame.GetXaxis().SetTitle(nom_flux[j].GetXaxis().GetTitle())
    frame.GetYaxis().SetTitle(nom_flux[j].GetYaxis().GetTitle().replace("POT","s"))
    frame.GetXaxis().CenterTitle()
    frame.GetYaxis().CenterTitle()
    frame.SetTitle("")
    frame.Draw()
    
    nom_flux[j].Draw("HSAME")

#    for i in range(0,len(var_fluxes)):
    for i in range(0,n_var_plots):
        var_fluxes[i][j].SetLineColor(colors[i])
        if(i==0):
            var_fluxes[i][j].SetLineWidth(4);
        else:
            var_fluxes[i][j].SetLineWidth(2);
        var_fluxes[i][j].Draw("HSAME")

    nom_flux[j].Draw("HSAME")

    leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
    leg.SetBorderSize(0);
    leg.SetFillStyle(0);
    leg.AddEntry(nom_flux[j],"Nominal","l");
    labels = []
    labels.append("Optimized (#"+str(int(configurations[ordered_indices[0]]))+")")
    labels.append("2nd Best (#"+str(int(configurations[ordered_indices[1]]))+")")
    labels.append("3rd Best (#"+str(int(configurations[ordered_indices[2]]))+")")
    labels.append("4th Best (#"+str(int(configurations[ordered_indices[3]]))+")")
        
    for i in range(0,n_var_plots):
        leg.AddEntry(var_fluxes[i][j],labels[i],"l")
    leg.Draw()

    flavor_strings = ["numu_fhc","numubar_fhc","nue_fhc","nuebar_fhc","numu_rhc","numubar_rhc","nue_rhc","nuebar_rhc"]
    canv.Print("plots/"+optimization_name+"_"+flavor_strings[j]+"_best_fluxes.eps");
    canv.Print("plots/"+optimization_name+"_"+flavor_strings[j]+"_best_fluxes.png");



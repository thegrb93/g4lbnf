import sys,shutil,os,glob
import Optimizations

##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 1:
    print "Please specify an optimization name"
    sys.exit()

optname = sys.argv[1]

optimization = Optimizations.Optimization(optname)

temp_dir = optname+"_temp"

if os.path.exists(temp_dir):
    shutil.rmtree(temp_dir)

os.makedirs(temp_dir)

tarfile = temp_dir+".tar.gz"
if os.path.exists(tarfile):
    os.remove(tarfile)


os.makedirs(temp_dir+"/fitness_vs_configuration/")
os.makedirs(temp_dir+"/fitness_vs_configuration/eps")
os.makedirs(temp_dir+"/fitness_vs_configuration/png")

fitness_types = ["Standard","fhcnumu","fhcnumubar","fhcnuenuebar","rhcnumu","rhcnumubar","rhcnuenuebar","ih","nh","mh"]
for type in fitness_types:
    subdir = temp_dir+"/fitness_vs_configuration/"
    file_prefix = optname+"_"+type+"_fitness_vs_configuration"
    shutil.copyfile("plots/"+file_prefix+".eps",subdir+"/eps/"+file_prefix+".eps")
    shutil.copyfile("plots/"+file_prefix+".png",subdir+"/png/"+file_prefix+".png")

os.makedirs(temp_dir+"/best_fluxes/")
os.makedirs(temp_dir+"/best_fluxes/eps")
os.makedirs(temp_dir+"/best_fluxes/png")
versions = ["numu_fhc","numubar_fhc","nue_fhc","nuebar_fhc","numu_rhc","numubar_rhc","nue_rhc","nuebar_rhc"]
for version in versions:
    subdir = temp_dir+"/best_fluxes/"
    file_prefix = optname+"_"+version+"_best_fluxes"
    shutil.copyfile("plots/"+file_prefix+".eps",subdir+"/eps/"+file_prefix+".eps")
    shutil.copyfile("plots/"+file_prefix+".png",subdir+"/png/"+file_prefix+".png")

os.makedirs(temp_dir+"/fitness_vs_parameter/")
os.makedirs(temp_dir+"/fitness_vs_parameter/eps")
os.makedirs(temp_dir+"/fitness_vs_parameter/png")
params = optimization.parameter_names
for param in params:
    for fitness_type in fitness_types:
        subdir = temp_dir+"/fitness_vs_parameter/"
        file_prefix = optname+"_"+fitness_type+"_fitness_vs_"+param
        shutil.copyfile("plots/"+file_prefix+".eps",subdir+"/eps/"+file_prefix+".eps")
        shutil.copyfile("plots/"+file_prefix+".png",subdir+"/png/"+file_prefix+".png")

os.makedirs(temp_dir+"/parameter_vs_configuration/")
os.makedirs(temp_dir+"/parameter_vs_configuration/eps")
os.makedirs(temp_dir+"/parameter_vs_configuration/png")
params = optimization.parameter_names
for param in params:
    for fitness_type in fitness_types:
        subdir = temp_dir+"/parameter_vs_configuration/"
        file_prefix = optname+"_"+param+"_vs_configuration"
        shutil.copyfile("plots/"+file_prefix+".eps",subdir+"/eps/"+file_prefix+".eps")
        shutil.copyfile("plots/"+file_prefix+".png",subdir+"/png/"+file_prefix+".png")

os.makedirs(temp_dir+"/parameter_vs_parameter/")
os.makedirs(temp_dir+"/parameter_vs_parameter/eps")
os.makedirs(temp_dir+"/parameter_vs_parameter/png")
params = optimization.parameter_names
for param1 in params:
    for param2 in params:
        subdir = temp_dir+"/parameter_vs_parameter/"
        file_prefix = optname+"_"+param1+"_vs_"+param2
        if(os.path.exists("plots/"+file_prefix+".eps")):
            shutil.copyfile("plots/"+file_prefix+".eps",subdir+"/eps/"+file_prefix+".eps")
            shutil.copyfile("plots/"+file_prefix+".png",subdir+"/png/"+file_prefix+".png")

os.makedirs(temp_dir+"/visualizations/")
os.makedirs(temp_dir+"/visualizations/eps")
os.makedirs(temp_dir+"/visualizations/png")
params = optimization.parameter_names

subdir = temp_dir+"/visualizations/"
file_prefix = optname
eps_to_copy = glob.glob("vis_plots/*"+file_prefix+"*.eps")
png_to_copy = glob.glob("vis_plots/*"+file_prefix+"*.png")
print eps_to_copy
for eps_file in eps_to_copy:
    shutil.copyfile(eps_file,subdir+"/eps/"+os.path.basename(eps_file))

for png_file in png_to_copy:
    shutil.copyfile(png_file,subdir+"/png/"+os.path.basename(png_file))

os.makedirs(temp_dir+"/scans/")
os.makedirs(temp_dir+"/scans/eps")
os.makedirs(temp_dir+"/scans/png")
params = optimization.parameter_names

subdir = temp_dir+"/scans/"
file_prefix = optname
eps_to_copy = glob.glob("scan_plots/*"+file_prefix+"*.eps")
png_to_copy = glob.glob("scan_plots/*"+file_prefix+"*.png")
print eps_to_copy
for eps_file in eps_to_copy:
    shutil.copyfile(eps_file,subdir+"/eps/"+os.path.basename(eps_file))

for png_file in png_to_copy:
    shutil.copyfile(png_file,subdir+"/png/"+os.path.basename(png_file))

os.system("tar cvzf " + tarfile+ " " + temp_dir)


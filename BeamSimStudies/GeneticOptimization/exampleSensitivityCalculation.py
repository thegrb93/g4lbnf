import OptimizationUtils


nominal_fhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r3p2/QGSP_BERT/Nominal/200kA/flux/histos_g4lbne_v3r3p2_QGSP_BERT_Nominal_200kA_LBNEFD_fastmc.root"
nominal_rhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r3p2/QGSP_BERT/Nominal/-200kA/flux/histos_g4lbne_v3r3p2_QGSP_BERT_Nominal_-200kA_LBNEFD_fastmc.root"
nominal_sensitivity = OptimizationUtils.ComputeMean75PercentSensitivityAndError(nominal_fhcfile,nominal_rhcfile, 120.0, 120.0,0.5, "cp")
print "Nominal Sensitivity: ",nominal_sensitivity[1][0]

varied_fhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r3p6/QGSP_BERT/CP_run5_9116_80GeV/200kA/flux/histos_g4lbne_v3r3p6_QGSP_BERT_CP_run5_9116_80GeV_200kA_LBNEFD_fastmc.root"
varied_rhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r3p6/QGSP_BERT/CP_run5_9116_80GeV/-200kA/flux/histos_g4lbne_v3r3p6_QGSP_BERT_CP_run5_9116_80GeV_-200kA_LBNEFD_fastmc.root"

varied_sensitivity = OptimizationUtils.ComputeMean75PercentSensitivityAndError(varied_fhcfile,varied_rhcfile, 80.0, 80.0,0.5, "cp")
print "Varied Sensitivity: ",varied_sensitivity[1][0]



import ROOT, os, array, sys

variations = ["ProtonP120GeV","Optimizations_CP_run3_7862","Optimizations_CP_run3_8186"]
var_names = ["Nom v3r2p4","7862","8186"]
var_colors = [8,4,2]

var_histos = []

c1 = ROOT.TCanvas("c1");

leg = ROOT.TLegend(0.4,0.6,0.6,0.8);
leg.SetFillStyle(0);
leg.SetBorderSize(0);

i=0
for variation in variations:
    print variation
    nh_cp_file = ROOT.TFile("/lbne/data/users/lblpwg_tools/FastMC_Data/outputs/ljf26/Sensitivity_Plots/"+variation+"_nh_cp_histos.root"); 
    var_histos.append(nh_cp_file.Get("h2"))
    var_histos[i].SetDirectory(0)
    var_histos[i].SetLineWidth(3)
    var_histos[i].SetFillStyle(0)
    var_histos[i].GetYaxis().SetTitleOffset(0.95)
    var_histos[i].SetLineColor(var_colors[i])
    if i==0:
        var_histos[i].SetMaximum(var_histos[i].GetMaximum()*2)
        var_histos[i].DrawCopy("")
    else:
        var_histos[i].DrawCopy("same")
    leg.AddEntry(var_histos[i],var_names[i],"l")
    i = i+1

leg.Draw()
c1.Print("sensitivity_comps.eps")
c1.Print("sensitivity_comps.png")

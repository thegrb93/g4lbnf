import ROOT, glob, sys, os, re
from array import array
import Optimizations, OptimizationUtils

physics_list = "QGSP_BERT"

# options: cp, mh, cp_fhcnumu, cp_fhcnumubar, cp_fhcnuenuebar, cp_rhcnumu, cp_rhcnumubar, cp_rhcnuenuebar
fitness_type = "cp"

##### PARSE INPUT ARGUMENT #####

if not len(sys.argv) > 2:
    print "Please specify an optimization name and configuration number"
    sys.exit()

optimization_name = sys.argv[1]
configuration = sys.argv[2]

optimization = Optimizations.Optimization(optimization_name)

#### merge configs if need ####

n_scan_points = 30

configs_already_merged = []
configs_merged_now = []
configs_not_finished = []

for par_iter in range(len(optimization.parameter_names)):

    configs_already_merged.append([])
    configs_merged_now.append([])
    configs_not_finished.append([])

    #if "HornALength" not in optimization.parameter_names[par_iter]:
    #    continue
    
    lower_limit = optimization.parameter_lower_limits[par_iter]
    upper_limit = optimization.parameter_upper_limits[par_iter]

    # Loop over values to scan for this parameter
    for scan_iter in range(n_scan_points+1):
        scan_value = lower_limit + scan_iter*(upper_limit-lower_limit)/n_scan_points
        fhc_flux_dir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value);
        rhc_flux_dir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value);
        if(optimization.rhc_parameters_float_separate):
            fhc_flux_dir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/Optimizations-"+optimization_name+"FHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value);
            rhc_flux_dir = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/Optimizations-"+optimization_name+"RHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value);

        merged_file = glob.glob(fhc_flux_dir+"/*/flux/histos_*_LBNEFD_fastmc.root")

        if not len(merged_file) == 2 :

            temp_fhc_dir = fhc_flux_dir.replace("/lbne/data/users/","/pnfs/lbne/scratch/users/")
            temp_rhc_dir = rhc_flux_dir.replace("/lbne/data/users/","/pnfs/lbne/scratch/users/")

            fhc_files = glob.glob(temp_fhc_dir+"/neutrino/flux/histos/histos_*_neutrino_*_LBNEFD_fastmc.root")
            rhc_files = glob.glob(temp_rhc_dir+"/antineutrino/flux/histos/histos_*_antineutrino_*_LBNEFD_fastmc.root")
        
            print "TEMP_FHC_FLUX_DIR",temp_fhc_dir+"/neutrino/flux/histos/histos_*_neutrino_*_LBNEFD_fastmc.root"

            if len(fhc_files) == 5 and len(rhc_files) == 5:
            
                print "Merging",optimization.parameter_names[par_iter],scan_value

                if not optimization.rhc_parameters_float_separate:
                    os.system("python ../../ProductionScripts/merge_histograms.py --mode neutrino --n 100000  -p "+physics_list+" -m Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
                    os.system("python ../../ProductionScripts/merge_histograms.py --mode antineutrino --n 100000 -p "+physics_list+" -m Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
                else :
                    os.system("python ../../ProductionScripts/merge_histograms.py --mode neutrino --n 100000  -p "+physics_list+" -m Optimizations-"+optimization_name+"FHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
                    os.system("python ../../ProductionScripts/merge_histograms.py --mode antineutrino --n 100000 -p "+physics_list+" -m Optimizations-"+optimization_name+"RHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value))
                configs_merged_now[par_iter].append(scan_value)
            else:
                print "Not all files finished for ",optimization.parameter_names[par_iter],scan_value
                configs_not_finished[par_iter].append(scan_value)
        else:
            configs_already_merged[par_iter].append(scan_value)            


for par_iter in range(len(optimization.parameter_names)):
    
    #if "HornALength" not in optimization.parameter_names[par_iter]:
    #    continue


    par_name = optimization.parameter_names[par_iter]
    lower_limit = optimization.parameter_lower_limits[par_iter]
    upper_limit = optimization.parameter_upper_limits[par_iter]
    
    # Loop over values to scan for this parameter
    par_values = []
    fit_values = []
    zero_errors = []
    fit_errors = []

    for scan_iter in range(n_scan_points+1):
        scan_value = lower_limit + scan_iter*(upper_limit-lower_limit)/n_scan_points

        macro_name_fhc = "Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)
        macro_name_rhc = "Optimizations-"+optimization_name+"-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)
        if optimization.rhc_parameters_float_separate:
            macro_name_fhc = "Optimizations-"+optimization_name+"FHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)
            macro_name_rhc = "Optimizations-"+optimization_name+"RHC-"+configuration+"_Scan_"+optimization.parameter_names[par_iter]+"_"+str(scan_value)

        nomfhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/"+physics_list+"/Nominal/neutrino/flux/histos_g4lbne_v3r2p4_"+physics_list+"_Nominal_neutrino_LBNEFD_fastmc.root"
                
        nomrhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/v3r2p4/"+physics_list+"/Nominal/antineutrino/flux/histos_g4lbne_v3r2p4_"+physics_list+"_Nominal_antineutrino_LBNEFD_fastmc.root"
        
        varfhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/"+macro_name_fhc+"/neutrino/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+physics_list+"_"+macro_name_fhc+"_neutrino_LBNEFD_fastmc.root"
        varrhcfile = "/lbne/data/users/ljf26/fluxfiles/g4lbne/"+optimization.g4lbne_version+"/"+physics_list+"/"+macro_name_rhc+"/antineutrino/flux/histos_g4lbne_"+optimization.g4lbne_version+"_"+physics_list+"_"+macro_name_rhc+"_antineutrino_LBNEFD_fastmc.root"

        # look up power for this configuration
        rhcpower = 0
        fhcpower = 0
        if(par_name != "ProtonEnergy" and 
           par_name != "FHCProtonEnergy" and 
           par_name != "RHCProtonEnergy"):
            power_iter = optimization.getParameterIndex("ProtonEnergy")
            
            fhcpower = optimization.getEnergy(configuration,"FHC")
            rhcpower = optimization.getEnergy(configuration,"RHC")
        elif par_name == "ProtonEnergy":
            fhcpower = scan_value
            rhcpower = scan_value
        elif par_name == "FHCProtonEnergy":
            fhcpower = scan_value
            rhcpower = optimization.getEnergy(configuration,"RHC")
        elif par_name == "RHCProtonEnergy":
            rhcpower = scan_value
            fhcpower = optimization.getEnergy(configuration,"FHC")

        # look up antinu fraction for this configuration
        antinufrac = optimization.getParameterValue("AntinuFraction",configuration)
        if(antinufrac == -1): 
            antinufrac = 0.5
        print antinufrac
    
        # Get whatever fitness the user asked for
        [fitness_names,fitnesses,fiterrors] = OptimizationUtils.ComputeMean75PercentSensitivityAndError(varfhcfile,varrhcfile,fhcpower,rhcpower,antinufrac,"cp")
        if(fitness_type=="mh"):
            [fitness_names,fitnesses,fiterrors] = OptimizationUtils.ComputeMean75PercentSensitivityAndError(varfhcfile,varrhcfile,fhcpower,rhcpower,antinufrac,"mh")
        if fitness_type in ["mh","cp"]:
            t_fitness = fitnesses[0]
            t_error = fiterrors[0]
        if fitness_type =="cp_fhcnumu":
            t_fitness = fitnesses[1]
            t_error = fiterrors[1]
        if fitness_type =="cp_fhcnumubar":
            t_fitness = fitnesses[2]
            t_error = fiterrors[2]
        if fitness_type =="cp_fhcnuenuebar":
            t_fitness = fitnesses[3]
            t_error = fiterrors[3]
        if fitness_type =="cp_rhcnumu":
            t_fitness = fitnesses[4]
            t_error = fiterrors[4]
        if fitness_type =="cp_rhcnumubar":
            t_fitness = fitnesses[5]
            t_error = fiterrors[5]
        if fitness_type =="cp_rhcnuenuebar":
            t_fitness = fitnesses[6]
            t_error = fiterrors[6]

        print t_fitness

#        if t_fitness >0:
        if 1>0:
            par_values.append(scan_value)
            fit_values.append(t_fitness)
            if(fiterrors[0]<10):
                fit_errors.append(t_error)
            else:
                fit_errors.append(0);
            zero_errors.append(0)
        
# Make TGraphs
    canv = ROOT.TCanvas("MyCanvas","MyCanvas")
    print par_values
    print zero_errors
    print fit_values
    print fit_errors
    if len(par_values)==0: 
        continue
    ROOT.gStyle.SetMarkerStyle(2);
#    fitness_vs_par = ROOT.TGraphErrors(len(par_values),array('d',par_values),array('d',zero_errors),array('d',fit_values),array('d',fit_errors));
    fitness_vs_par = ROOT.TGraphErrors(len(par_values),array('d',par_values),array('d',fit_values),array('d',zero_errors),array('d',fit_errors));
    fitness_vs_par.GetXaxis().SetTitle(par_name);
    fitness_vs_par.GetYaxis().SetTitle("Fitness");
    fitness_vs_par.SetMinimum(1.5);
    fitness_vs_par.SetMaximum(2.0);
    if(fitness_type=="mh"):
        fitness_vs_par.SetMinimum(5.0);
        fitness_vs_par.SetMaximum(6.5);
    if(fitness_type in ["cp_fhcnumu","cp_fhcnumubar","cp_fhcnuenuebar","cp_rhcnumu","cp_rhcnumubar","cp_rhcnuenuebar"]):
        fitness_vs_par.SetMinimum(-0.1);
        fitness_vs_par.SetMaximum(0.4);
    ROOT.gPad.SetGrid(1);
    fitness_vs_par.SetTitle("")
    fitness_vs_par.Draw("AP")

    # Draw line at value chosen by optimization
    optimized_value = optimization.getParameterValue(optimization.parameter_names[par_iter],configuration)

    my_line = ROOT.TLine(optimized_value,fitness_vs_par.GetMinimum(),optimized_value,fitness_vs_par.GetMaximum())
    my_line.SetLineColor(2);
    my_line.Draw();

    canv.Print("scan_plots/"+optimization_name+"_"+configuration+"_Scan_"+par_name+"_"+physics_list+"_"+fitness_type+".eps")
    canv.Print("scan_plots/"+optimization_name+"_"+configuration+"_Scan_"+par_name+"_"+physics_list+"_"+fitness_type+".png")


"""
        if fitness_type=="mh":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',mhfitnesses),array('d',zero_errors),array('d',mhfitness_errors))
        if fitness_type=="ih":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',ihfitnesses),array('d',zero_errors),array('d',ihfitness_errors))
        if fitness_type=="nh":
               fitness_vs_parameter = ROOT.TGraphErrors(len(configurations),array('d',parameter_values[i]),array('d',nhfitnesses),array('d',zero_errors),array('d',nhfitness_errors))
        fitness_vs_parameter.GetYaxis().SetTitle(fitness_type+" Fitness")
        x_axis_title = optimization.parameter_names[i]
        if(optimization.parameter_units[i]!=""):
            x_axis_title = x_axis_title+" ("+optimization.parameter_units[i]+")"
        fitness_vs_parameter.GetXaxis().SetTitle(x_axis_title);
        fitness_vs_parameter.GetXaxis().CenterTitle()
        fitness_vs_parameter.GetYaxis().CenterTitle()
        fitness_vs_parameter.Draw("AP");
        canv.Print("plots/"+optimization_name+"_"+fitness_type+"_fitness_vs_"+parameter_name+".eps")
        canv.Print("plots/"+optimization_name+"_"+fitness_type+"_fitness_vs_"+parameter_name+".png")


    for j in range(i+1,n_parameters):
        parameter_vs_parameter = ROOT.TGraphErrors(1000,array('d',parameter_values[i][-1000:]),array('d',parameter_values[j][-1000:]),array('d',zero_errors[-1000:]),array('d',zero_errors[-1000:]))
        x_axis_title = optimization.parameter_names[i]
        y_axis_title = optimization.parameter_names[j]
        if(optimization.parameter_units[i]!=""):
            x_axis_title = x_axis_title+" ("+optimization.parameter_units[i]+")"
        if(optimization.parameter_units[j]!=""):
            y_axis_title = y_axis_title+" ("+optimization.parameter_units[j]+")"
        parameter_vs_parameter.GetXaxis().SetTitle(x_axis_title);
        parameter_vs_parameter.GetYaxis().SetTitle(y_axis_title);
        parameter_vs_parameter.GetXaxis().CenterTitle()
        parameter_vs_parameter.GetYaxis().CenterTitle()
        parameter_vs_parameter.Draw("AP");
        canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_"+optimization.parameter_names[j]+".eps")
        canv.Print("plots/"+optimization_name+"_"+parameter_name+"_vs_"+optimization.parameter_names[j]+".png")

"""

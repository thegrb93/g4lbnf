*$ CREATE FLUSCW.FOR
*COPY FLUSCW
*                                                                      *
*=== fluscw ===========================================================*
*                                                                      *
      DOUBLE PRECISION FUNCTION FLUSCW ( IJ    , PLA   , TXX   , TYY   ,
     &                                   TZZ   , WEE   , XX    , YY    ,
     &                                   ZZ    , NREG  , IOLREG, LLO   ,
     &                                   NSURF )

      INCLUDE '(DBLPRC)'
      INCLUDE '(DIMPAR)'
      INCLUDE '(IOUNIT)'
*
*----------------------------------------------------------------------*
*                                                                      *
*     Copyright (C) 1989-2005      by    Alfredo Ferrari & Paola Sala  *
*     All Rights Reserved.                                             *
*                                                                      *
*     New version of Fluscw for FLUKA9x-FLUKA200x:                     *
*                                                                      *
*     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!     *
*     !!! This is a completely dummy routine for Fluka9x/200x. !!!     *
*     !!! The  name has been kept the same as for older  Fluka !!!     *
*     !!! versions for back-compatibility, even though  Fluscw !!!     *
*     !!! is applied only to estimators which didn't exist be- !!!     *
*     !!! fore Fluka89.                                        !!!     *
*     !!! User  developed versions  can be used for  weighting !!!     *
*     !!! flux-like quantities at runtime                      !!!     *
*     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!     *
*                                                                      *
*     Input variables:                                                 *
*                                                                      *
*           Ij = (generalized) particle code (Paprop numbering)        *
*          Pla = particle laboratory momentum (GeV/c) (if > 0),        *
*                or kinetic energy (GeV) (if <0 )                      *
*    Txx,yy,zz = particle direction cosines                            *
*          Wee = particle weight                                       *
*     Xx,Yy,Zz = position                                              *
*         Nreg = (new) region number                                   *
*       Iolreg = (old) region number                                   *
*          Llo = particle generation                                   *
*        Nsurf = transport flag (ignore!)                              *
*                                                                      *
*     Output variables:                                                *
*                                                                      *
*       Fluscw = factor the scored amount will be multiplied by        *
*       Lsczer = logical flag, if true no amount will be scored        *
*                regardless of Fluscw                                  *
*                                                                      *
*     Useful variables (common SCOHLP):                                *
*                                                                      *
*     Flux like binnings/estimators (Fluscw):                          *
*          ISCRNG = 1 --> Boundary crossing estimator                  *
*          ISCRNG = 2 --> Track  length     binning                    *
*          ISCRNG = 3 --> Track  length     estimator                  *
*          ISCRNG = 4 --> Collision density estimator                  *
*          ISCRNG = 5 --> Yield             estimator                  *
*          JSCRNG = # of the binning/estimator                         *
*                                                                      *
*----------------------------------------------------------------------*
*

      INCLUDE '(SCOHLP)'
      INCLUDE '(CASLIM)'
      INCLUDE '(FLKSTK)'
      INCLUDE '(GENSTK)'
      INCLUDE '(TRACKR)'

      double precision uPx, uPy, uPz
      double precision protonx, protony, protonz
      double precision protonpx, protonpy, protonpz
      double precision mywei, totp
      
      logical doWriteIt
      
      parameter (pmin1=0.1)
      parameter (pmin2=1.)

*      REAL*8 MASS
      INTEGER TYPE, typeP
      CHARACTER(len=255) :: clusterName
      CHARACTER(len=255) :: processName
      CHARACTER(len=255) :: fileName
      save protonx
      save protony
      save protonz
      save protonpx
      save protonpy
      save protonpz
      data protonx /0./
      data protony /0./
      data protonz /0./
      data protonpx /0./
      data protonpy /0./
      data protonpz /0./

      PARAMETER (NNTUPLE=22)
      REAL XTUP(nntuple)
      character NAMETU1*60, NAMETU2*60
      LOGICAL LFIRST
      SAVE LFIRST
      DATA LFIRST /.true./
   
      double precision protvx,protvy,protvz,protvid
      save protvx
      save protvy
      save protvz
      save protvid
      
* return message from first call
      if(LFIRST) then
         CALL getenv("CLUSTER", clusterName)      
         CALL getenv("PROCESS", processName)      
         fileName='outProd_'//TRIM(clusterName)//'_'//TRIM(processName)//'.dat'
         OPEN(UNIT=25, FILE=fileName,STATUS='NEW')
         write(LUNOUT,*)'FLUSCW called'
         LFIRST = .false.
c        1         2         3         4         5         6         7
C    67890123456789012345678901234567890123456789012345678901234567890
	 NAMETU1 = 
     &  ' X Y Z Px Py Pz G4Type Weight pGen GTypeP  PxP PyP PzP Vx   ' 
c  NAMETU1 has 15 variables 
	 NAMETU2 = 
     &  ' Vy Vz XPr YPr ZPr PxPr PyPr PzPr                           ' 
	 WRITE(25, *) NAMETU1, NAMETU2 
c         write(LUNOUT,*)'And qu it NOW '
c	 print *, ' I said, quit now... ' 
c	 stop
      endif
      

      if(ij.eq.1.and.NREG.eq.3.0.and.Iolreg.eq.2) then
         protonx = XX
         protony = YY
         protonz = ZZ
         protonpx = Ptrack*TXX
         protonpy = Ptrack*TYY
         protonpz = Ptrack*TZZ
      endif
            
      doWriteIt = .true.
c
c Escaping the simple target..Note: Geometry dependent!... 
c
      if(IOLREG.eq.3.0.and.NREG.eq.2.0) then
c      if(NREG.eq.4.0) then

******** translate fluka particle code to geant3
         call gcode(IJ,type)
         
	 IF ((TYPE .EQ. 39) .OR. (TYPE .EQ. 22)
     &      .OR. (abs(TYPE) .EQ. 11) .OR. (TYPE .EQ. 111))  doWriteIt = .false. 
         if (abs(Ptrack) < 0.050) doWriteIt = .false.    
c compute momentum in the 3 dirs
         uPx = Ptrack*TXX
         uPy = Ptrack*TYY
         uPz = Ptrack*TZZ

******** hbook***

         totp=sqrt(uPx*uPx+uPy*uPy+uPz*uPz) ! calculate momentum        

         if ((totp.gt.30.) .or. (totp .lt. pmin1)) then  ! calculate weight
            mywei = 1.
         else
            mywei = 30./totp
         endif

*         if ((randy.lt.(1./mywei)).and.(totp.ge.pmin1)) then
            
            xtup(8) = REAL(mywei)

            xtup(1) = REAL(XX)
            xtup(2) = REAL(YY)
            xtup(3) = REAL(ZZ)
            xtup(4) = REAL(uPx)
            xtup(5) = REAL(uPy)
            xtup(6) = REAL(uPz)
            xtup(7) = REAL(TYPE)
*            xtup(8) = WEE
            xtup(9) = REAL(LLO)
************ parent info
c           check first if it was a decay otherwise
c           it was a inelastic interaction
            if (ISPUSR(2).ne.0) then
c              translate fluka particle code to geant3
               call gcode(ISPUSR(2), typeP)
	       xtup(11) = real(typeP)
c               xtup(10) = ISPUSR(2) ! type
               xtup(11) = REAL(SPAUSR(5)) ! px
               xtup(12) = REAL(SPAUSR(6)) ! py
               xtup(13) = REAL(SPAUSR(7)) ! pz
c              SPAUSR(8) is momentum
            else
c              translate fluka particle code to geant3 (G4 now, P.L., Jan 2015) 
               call gcode(ISPUSR(1), typeP)
               xtup(10) = REAL(typeP) ! type
               xtup(11) = REAL(SPAUSR(1)) ! px
               xtup(12) = REAL(SPAUSR(2)) ! py
               xtup(13) = REAL(SPAUSR(3)) ! pz
c              SPAUSR(4) is momentum
            endif

            if (SPAUSR(11).ne.0) then
               protvid = ISPUSR(3)
               protvx = SPAUSR(9)
               protvy = SPAUSR(10)
               protvz = SPAUSR(11)
            endif
            
c            write(*,*) ' id:',protvid,' x:',protvx,
c     &                 ' y:',protvy,' z:',protvz

c            write(*,*) ' id:',xtup(7),' monpz:',xtup(13),
c     &                 ' pz:',xtup(6)

            xtup(14)= -999. ! primary vertex x
            xtup(15)= -999. ! primary vertex y
            xtup(16)= -999. ! primary vertex z

            xtup(17)= REAL(protonx)
            xtup(18)= REAL(protony)
            xtup(19)= REAL(protonz)
            xtup(20)= REAL(protonpx)
            xtup(21)= REAL(protonpy)
            xtup(22)= REAL(protonpz)

*            CALL HFN(100,xtup)

*         endif
*****************

c         write(25,200)EvtCount,IJ, XX, YY, ZZ, uPx, uPy,
c     + uPz,Etrack,WEE

********* write: event number, position and momentum of
*********        outgoing particle, ID, weight, generation,
*********        position and direction of incomming proton,
*********        mother info for interaction(position and
*********        kinetic energy), mother info for decay.

            if (doWriteIt) write(25,200)
     &        xtup(1),xtup(2),xtup(3),xtup(4),xtup(5),
     &        xtup(6), INT(xtup(7)),xtup(8),xtup(9),xtup(10),
     &        xtup(11),xtup(12),xtup(13),xtup(14),xtup(15),
     &        xtup(16),xtup(17),xtup(18),xtup(19),xtup(20),
     &        xtup(21),xtup(22)
c     &        NCASE,XX, YY, ZZ, uPx, uPy, uPz, TYPE, WEE, LLO,
c     &        protonx,protony,protonz,protonu,protonv,
c     &        ISPUSR(1),SPAUSR(1),SPAUSR(2),SPAUSR(3),SPAUSR(4),
c     &        ISPUSR(2),SPAUSR(5),SPAUSR(6),SPAUSR(7),SPAUSR(8)

      endif

 200  format(6(1x,F11.6),(1x,I8),(1x,F11.6),
     + (1x,F5.1),(1x,F5.1),12(1x,F11.6),(1x,I7))

c 200  format(I8,I3,F10.5,F10.5,F10.5,E14.6,E14.6,E14.6,F8.1,
c     + F8.1,E12.4)
c 200  format(F11.6,F11.6,F11.6,F11.6,F11.6,F11.6,I2,F3.1,I2)



*
      FLUSCW = ONEONE
      LSCZER = .FALSE.
      RETURN
*=== End of function Fluscw ===========================================*
      END

      SUBROUTINE GCODE(IJ,GTYPE)
      
      INTEGER IJ,GTYPE
C!------------------------------------------------------------
C
C return geant3 particle code

      
*  Translation of particle type from Fluka to GEANT
*-------- IJ - Fluka number, GTYPE - GEANT Number
* -- Upgrade directly to Geant4.. Paul Lebrun, January 2015. 
* From the manual: 
*
c PROTON              1          Proton                  2212
c APROTON             2          Antiproton             -2212
c ELECTRON            3          Electron                  11
c POSITRON            4          Positron                 -11
c NEUTRIE             5          Electron Neutrino         12
c ANEUTRIE            6          Electron Antineutrino    -12
c PHOTON              7          Photon                    22
c NEUTRON             8          Neutron                 2112
c ANEUTRON            9          Antineutron            -2112
c MUON+              10          Positive Muon            -13
c MUON-              11          Negative Muon             13
c KAONLONG           12          Kaon-zero long           130
c PION+              13          Positive Pion            211
c PION-              14          Negative Pion           -211
c KAON+              15          Positive Kaon            321
c KAON-              16          Negative Kaon           -321
c LAMBDA             17          Lambda                  3122
c ALAMBDA            18          Antilambda             -3122
c KAONSHRT           19          Kaon zero short          310
c SIGMA-             20          Negative Sigma          3112
c SIGMA+             21          Positive Sigma          3222
c SIGMAZER           22          Sigma-zero              3212
c PIZERO             23          Pion-zero                111
c KAONZERO           24          Kaon-zero                311
c AKAONZER           25          Antikaon-zero           -311
c Reserved           26               ---                 ---
c NEUTRIM            27          Muon neutrino             14
c ANEUTRIM           28          Muon antineutrino        -14
c Blank              29               ---                 ---
c Reserved           30               ---                 ---
c ASIGMA-            31          Antisigma-minus        -3222
c ASIGMAZE           32          Antisigma-zero         -3212
c ASIGMA+            33          Antisigma-plus         -3112
c XSIZERO            34          Xi-zero                 3322
c AXSIZERO           35          Antixi-zero            -3322
c XSI-               36          Negative Xi             3312
c AXSI+              37          Positive Xi            -3312
c OMEGA-             38          Omega-minus             3334
c AOMEGA+            39          Antiomega              -3334
c Reserved           40               ---                 ---
c TAU+               41          Positive Tau             -15
c TAU-               42          Negative Tau              15
c NEUTRIT            43          Tau neutrino              16
c ANEUTRIT           44          Tau antineutrino         -16
c D+                 45          D-plus                   411
c D-                 46          D-minus                 -411
c D0                 47          D-zero                   421
c D0BAR              48          AntiD-zero              -421
c DS+                49          D_s-plus                 431
c DS-                50          D_s-minus               -431
c LAMBDAC+           51          Lambda_c-plus           4122
c XSIC+              52          Xi_c-plus               4232
c XSIC0              53          Xi_c-zero               4112
c XSIPC+             54          Xi'_c-plus              4322
c XSIPC0             55          Xi'_c-zero              4312
c OMEGAC0            56          Omega_c-zero            4332
c ALAMBDC-           57          Antilambda_c-minus     -4122
c AXSIC-             58          AntiXi_c-minus         -4232
c AXSIC0             59          AntiXi_c-zero          -4132
c AXSIPC-            60          AntiXi'_c-minus        -4322
c AXSIPC0            61          AntiXi'_c-zero         -4312
c AOMEGAC0           62          AntiOmega_c-zero       -4332
c
c Reserved are defined as graviton. 
c
       INTEGER G4NUMBERS
       DIMENSION G4NUMBERS(62)
       SAVE G4NUMBERS
c        1         2         3         4         5         6         7
C    67890123456789012345678901234567890123456789012345678901234567890
       DATA G4NUMBERS/    2212, -2212, 11, -11, 12, -12, 22, 2112,
     a  -2112, -13, 13, 130, 211, -211, 321, -321, 3122, -3122, 310,
     b  3112, 3222, 3212, 111, 311,-311, 39, 14, -14, 39, 39, -3222,
     c  -3212, -3112, 3322, -3322, 3312, -3312, 3334, -3334, 39, -15,
     d  15, 16, -16, 411, -411, 421, -421, 431, -431, 4122, 4232, 4112,
     e  4322, 4312, 4332, -4122, -4232, -4132, -4322, -4312, -4332 /
      
      if (IJ .le. 0) then 
        GTYPE = 39
      else if (IJ .le. 62) then 
        GTYPE = G4NUMBERS(IJ)
      else  
        GTYPE = 39
      end if
      RETURN
      END  


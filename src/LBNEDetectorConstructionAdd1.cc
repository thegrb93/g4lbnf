#include <fstream>
#include <vector>
#include <string>
#include <sstream>

#include "LBNEDetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4VisAttributes.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVReplica.hh"
#include "G4AssemblyVolume.hh"
#include "LBNEMagneticField.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"

#include "G4RegionStore.hh"
#include "G4SolidStore.hh"
#include "G4GeometryManager.hh"
#include "G4FieldManager.hh"
#include "LBNEVolumePlacements.hh"
#include "LBNEDetectorMessenger.hh"
#include "LBNERunManager.hh"
#include "G4GDMLParser.hh"
#include "G4UIcmdWithADouble.hh"

#include "G4RunManager.hh"

#include "G4VisExtent.hh"

void LBNEDetectorConstruction::ConstructLBNEHadronAbsorberSculpted(G4VPhysicalVolume *mother)
{
   const double in = 25.4*CLHEP::mm;
   G4cout << "LBNF Absorber, Aluminium, Sculpted, January 2015  " << G4endl;
   const LBNEVolumePlacementData *plTop = 
         fPlacementHandler->Find(G4String("Absorber"), mother->GetLogicalVolume()->GetName(), 
	                         G4String("LBNEDetectorConstruction::ConstructLBNEHadronAbsorberSculpted"));
   const double halfHeight = plTop->fParams[1]/2.;
   const double halfWidth = plTop->fParams[0]/2.;
   const double airBufferLength = std::abs(halfHeight*std::tan(fBeamlineAngle)) + 5.0*CLHEP::mm; // to avoid wrong material in fron of the 
   // spoiler ( or Hadron Monitor) due to the rotation. 
   const double totalLength = 
      2.0*airBufferLength + 357.0*in/std::cos(fBeamlineAngle) + 36*in/std::cos(fBeamlineAngle) + 2.0*CLHEP::cm;
      //
      // Last term is the concrete wall  // Drawing received from Vladimir Sidorov, April 2015. 
   // See also LBNE doc-db 10095.  This does not include the ~ 2 feet of concrete at the back. 
   // August 2015: The ~ 2 feet was too inaccurate, and I left off the 60 inch of steel at the end. 
   // more orver, I also have to carve an airbuffer at the end.. 
   // The 36 inch is the correct thickness of the back wall. The 2 cm is extra gaps.. 
   // 
   // Start  the top volume 
   G4String topVStr("HadronAbsorberSculpTop");
   G4Box* topBox = new G4Box(topVStr, halfWidth-10., halfHeight-10., totalLength/2.);
   G4LogicalVolume* topVol = new G4LogicalVolume(topBox, G4Material::GetMaterial("Concrete"), topVStr);
   const LBNEVolumePlacementData *plDecayPipe = 
         fPlacementHandler->Find(G4String("Absorber"), G4String("DecayPipeHall"), 
	                         G4String("LBNEDetectorConstruction::ConstructLBNEHadronAbsorberSculpted"));
   const double zLocTop = plDecayPipe->fPosition[2] + plDecayPipe->fParams[2]/2. +  totalLength/2. + 15.0*CLHEP::mm; 
                         // last term is correction to avoid clashs with alcove or decay pipe..  
   std::cerr << " LBNEDetectorConstruction::ConstructLBNEHadronAbsorberSculpted, zLocTop " 
             << zLocTop << " Length of mother " << plTop->fParams[2] << std::endl 
	     << " Total length of Abosrber " << totalLength << " airbuffer lenfth " << airBufferLength << std::endl
	     << " ...........  pipe Lengtgh " << plDecayPipe->fParams[2] 
	     << " Pipe position " << plDecayPipe->fPosition[2] << std::endl;
   const G4ThreeVector locTopV(0., 0., zLocTop);
   // decay pipe and the muon alcove. 
   new G4PVPlacement((G4RotationMatrix *) 0, locTopV, 
                                                topVol, topVStr + std::string("_P"),
                                                mother->GetLogicalVolume(), false, 1, true);
   const double aFoot = 12.0*in;
   const double longSpacing = 5.0*CLHEP::mm;						
   const double tranSpacing = 10.0*CLHEP::mm;
   //
   // Construct a subtraction from a box aligned whose long. axis is aligned with the beam direction, 
   // and a rotated box.  The dimension is the decaypiperadius. 
   const double radPipe = fPlacementHandler->GetDecayPipeRadius();
   const double lengthCylBuffer = (halfHeight + radPipe)*std::abs(std::tan(fBeamlineAngle));
   const double shortLengthCylBuffer = (2.0*radPipe)*std::abs(std::tan(fBeamlineAngle));
   double zCurrentSl = - totalLength/2. + lengthCylBuffer/2. + 0.2*CLHEP::mm ;
   std::cerr << " lengthCylBuffer " << lengthCylBuffer << " shortLengthCylBuffer " 
            << shortLengthCylBuffer << "  .. zCurrentSl " << zCurrentSl << std::endl;
   {
     const G4String aAirBuffTStrT("HadronAbsorberSculpAibufferTop");     
     const G4String aAirBuffTStrS("HadronAbsorberSculpAibufferSub");     
     const G4String aAirBuffTStrD("HadronAbsorberSculpAibufferDiag");     
     G4Tubs* aTubeAirbufferA = new G4Tubs(aAirBuffTStrT, 0., radPipe, lengthCylBuffer/2., 0., 360.0*CLHEP::degree);     
     G4Box* aBoxAirbufferB = new G4Box(aAirBuffTStrS, radPipe, radPipe, 1.05*shortLengthCylBuffer/2.); // could a bit longer..
     G4ThreeVector shiftZ(0., 0.,  lengthCylBuffer/2.);   
     G4SubtractionSolid* aSectionSubAirbufferA = 
	     new G4SubtractionSolid(aAirBuffTStrD, aTubeAirbufferA, aBoxAirbufferB,  &fRotBeamlineAngle, shiftZ);
	     
     G4LogicalVolume* airBuffVol = new G4LogicalVolume(aSectionSubAirbufferA, G4Material::GetMaterial("Air"), aAirBuffTStrD);
     G4ThreeVector shiftZby2(0., 0., zCurrentSl );   
     new G4PVPlacement((G4RotationMatrix *) 0, shiftZby2, airBuffVol, aAirBuffTStrD+std::string("_P") , topVol, false, 1, true);    
   }
   zCurrentSl += lengthCylBuffer/2.  - shortLengthCylBuffer/2. +  aFoot/2. ;						
   for (size_t kSlice=0; kSlice != 23; kSlice++) {
     const double halfHSl = halfHeight - 2.0*CLHEP::cm - std::abs((aFoot/2.)*std::sin(fBeamlineAngle));
     const double halfWSl = halfWidth - 2.0*CLHEP::cm;
     std::ostringstream  aSlVStrStr;  aSlVStrStr << "HadronAbsorberSculpSlice-" << kSlice;
     std::ostringstream  akSlVStrStr;  akSlVStrStr << kSlice;
     const G4String aSlVStr(aSlVStrStr.str());     
     G4Box* aBoxSlice = new G4Box(aSlVStr, halfWSl, halfHSl, aFoot/2. + longSpacing/2.);     
     const G4ThreeVector locSliceV(0., 0., zCurrentSl);
     const G4String aSlVStrP = aSlVStr + std::string("_P");
     switch (kSlice) {
       case 0 : // The spoiler
         { 

             G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), topVStr);
             new G4PVPlacement(&fRotBeamlineAngle, locSliceV, 
                                                slVol, aSlVStr + std::string("_P"), topVol, false, 1, true);
    	     std::string spoilVAStr("HadronAbsorberSculpSpoilerAir");					
  	     const double halfWidthSpoil = 40.0*in/2.; const double halfHeightSpoil = 40.0*in/2.;				
             G4Box* spoilerBoxAir = new G4Box(spoilVAStr, halfWidthSpoil+tranSpacing/2., halfHeightSpoil+tranSpacing/2.,
	                                           aFoot/2. + longSpacing/2. - 0.1*CLHEP::mm);
             G4LogicalVolume* slSpoilAir = new G4LogicalVolume(spoilerBoxAir, G4Material::GetMaterial("Air"), spoilVAStr);
  	     const G4ThreeVector spV(0., 0., 0.);	 
	     new G4PVPlacement((G4RotationMatrix*) 0, spV, slSpoilAir, spoilVAStr + std::string("_P"), slVol , false, 1, true);					
           if (!fDisableSpoiler){
	     std::string spoilVStr("HadronAbsorberSculpSpoilerAl");					
	     G4Box* spoilerBoxAlum = new G4Box(spoilVStr, halfWidthSpoil, halfHeightSpoil, aFoot/2. - 0.2*CLHEP::mm);
             G4LogicalVolume* slSpoilAl = new G4LogicalVolume(spoilerBoxAlum, G4Material::GetMaterial("Aluminum"), spoilVAStr);
	     new G4PVPlacement((G4RotationMatrix *) 0, spV, slSpoilAl, spoilVStr + std::string("_P"), slSpoilAir, false, 1, true);
           }else{
             G4cout << "Skipping construction of the spoiler!" <<G4endl;
           }
	   break;					                                               
	 }					
       case 1 :
       case 2 :
       case 3 :
       case 4 :
       case 5 : // The Masks 
         {
           G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), topVStr);
           new G4PVPlacement(&fRotBeamlineAngle, locSliceV, 
                                                slVol, aSlVStr + std::string("_P"), topVol, false, 1, true);
	   std::string maskVAStr("HadronAbsorberSculpMaskAir-");	maskVAStr += 	akSlVStrStr.str();
	   const double halfWidthMask = (kSlice%2 == 1) ? 79.0*in/2. : 77.0*in/2.; 
	   const double halfHeightMask = 77.0*in/2.;				
	   G4Box* maskerBoxAir = new G4Box(maskVAStr, halfWidthMask+tranSpacing/2., halfHeightMask+tranSpacing/2.,
	                      aFoot/2. + longSpacing/2. - 0.1*CLHEP::mm);
           G4LogicalVolume* slMaskAir = new G4LogicalVolume(maskerBoxAir, G4Material::GetMaterial("Air"), maskVAStr);
	   const G4ThreeVector spV(0., 0., 0.);	 
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAir, maskVAStr + std::string("_P"), slVol , false, 1, true);					

	   std::string maskVAlStr("HadronAbsorberSculpMaskAl-");	maskVAlStr += akSlVStrStr.str();			
	   G4Box* maskerBoxAlum = new G4Box(maskVAlStr, halfWidthMask, halfHeightMask, aFoot/2. - 0.2*CLHEP::mm);
           G4LogicalVolume* slMaskAl = new G4LogicalVolume(maskerBoxAlum, G4Material::GetMaterial("Aluminum"), maskVAlStr);
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAl  , maskVAlStr + std::string("_P"), slMaskAir, false, 1, true);					

           double holeDiam = 30*in;
	   if ((kSlice == 3) || (kSlice == 4)) holeDiam = 20*in;
	   if ((kSlice == 5) || (kSlice == 6)) holeDiam = 12*in; // An educated guess !. 
	   std::string maskHVStr("HadronAbsorberSculpMaskHole-");	maskHVStr += akSlVStrStr.str();			
	   G4Tubs* maskerTubsHole = new G4Tubs(maskHVStr, 0., holeDiam/2. , aFoot/2. - 0.2*CLHEP::mm, 0., 360.0*CLHEP::degree);
           G4LogicalVolume* slMaskHole = new G4LogicalVolume(maskerTubsHole, G4Material::GetMaterial("Air"), maskHVStr);
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskHole  , maskHVStr + std::string("_P"), slMaskAl, false, 1, true);
	   break;					                                               
	 }					
       case 6 :
       case 7 :
       case 8 :
       case 9 :
       case 10 :
       case 11 :
       case 12 :
       case 13 :
       case 14 : // The Sculpted Aluminnum 
         { 
           G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), topVStr);
           new G4PVPlacement(&fRotBeamlineAngle, locSliceV, 
                                                slVol, aSlVStr + std::string("_P"), topVol, false, 1, true);
	   std::string maskVAStr("HadronAbsorberSculpSculpAir-");	maskVAStr += 	akSlVStrStr.str();			
	   const double halfWidthMask = (kSlice%2 == 1) ?  62.0*in/2. : 60.0*in/2.; 
	   const double halfHeightMask = 60.0*in/2.;				
	   G4Box* maskerBoxAir = new G4Box(maskVAStr, halfWidthMask+tranSpacing/2., halfHeightMask+tranSpacing/2., 
	                           aFoot/2. + longSpacing/2. - 0.1*CLHEP::mm);
           G4LogicalVolume* slMaskAir = new G4LogicalVolume(maskerBoxAir, G4Material::GetMaterial("Air"), maskVAStr);
	   const G4ThreeVector spV(0., 0., 0.);	 
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAir, maskVAStr + std::string("_P"), slVol , false, 1, true);					

	   std::string maskVBStr("HadronAbsorberSculpBoxAl-");	maskVBStr += 	akSlVStrStr.str();			
	   std::string maskVSStr("HadronAbsorberSculpSphAl-");	maskVSStr += 	akSlVStrStr.str();			
	   std::string maskVTmpStr("HadronAbsorberSculpSculpAlTmp-");	maskVTmpStr += akSlVStrStr.str();
	   std::string maskVFinalStr("HadronAbsorberSculpSculpAl-");	maskVFinalStr += akSlVStrStr.str();
           if (!fDisableSculptedLayers){

	     G4Box* maskerBoxAlum = new G4Box(maskVBStr, halfWidthMask, halfHeightMask, aFoot/2. - 0.2*CLHEP::mm);
	     const double radSphere = 6.89286*in; // Solving (d+6)^2 = R^2, d + 3.5 = R Docdb 10095, page 7 
             G4Sphere *aSphere= new G4Sphere (maskVSStr, 0., radSphere, 0., 360.*CLHEP::degree, 0., 360.*CLHEP::degree);

	     G4ThreeVector transLeft(0., 0., (-6.0*in - radSphere + 3.5*in)); // Docdb 10095, page 7 			
	     G4ThreeVector transRight(0., 0., (+6.0*in + radSphere - 3.5*in));			
	     G4SubtractionSolid* maskerTmpAlum = 
	       new G4SubtractionSolid(maskVTmpStr, maskerBoxAlum, aSphere,  (G4RotationMatrix *) 0, transLeft);
	     G4SubtractionSolid* maskerScupltedBoxAlum = 
	       new G4SubtractionSolid(maskVFinalStr, maskerTmpAlum, aSphere,  (G4RotationMatrix *) 0, transRight);
	     
             G4LogicalVolume* slMaskAl = new G4LogicalVolume(maskerScupltedBoxAlum, G4Material::GetMaterial("Aluminum"), maskVFinalStr);
	     new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAl  , maskVFinalStr + std::string("_P"), slMaskAir, false, 1, true);					
           }else{
             G4cout << "Only using simple aluminum panels with no scallops"<<G4endl;
	     G4Box* maskerBoxAlum = new G4Box(maskVFinalStr, halfWidthMask, halfHeightMask, aFoot/2. - 0.2*CLHEP::mm);
             G4LogicalVolume* slMaskAl = new G4LogicalVolume(maskerBoxAlum, G4Material::GetMaterial("Aluminum"), maskVFinalStr);
	     new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAl  , maskVFinalStr + std::string("_P"), slMaskAir, false, 1, true);					


           }
	   break;					                                               
         }
       case 15 :
       case 16 :
       case 17 :
       case 19 : // The solid aAl 
         {
     
           G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), topVStr);
           new G4PVPlacement(&fRotBeamlineAngle, locSliceV, 
                                                slVol, aSlVStr + std::string("_P"), topVol, false, 1, true);
	   std::string maskVAStr("HadronAbsorberSculpSolidAir-");	maskVAStr += 	akSlVStrStr.str();			
	   const double halfWidthMask = (kSlice%2 == 1) ?  62.0*in/2. : 60.0*in/2.; 
	   const double halfHeightMask = 60.0*in/2.;				
	   G4Box* maskerBoxAir = new G4Box(maskVAStr, halfWidthMask+tranSpacing/2., halfHeightMask+tranSpacing/2., 
	                                   aFoot/2. + longSpacing/2. - 0.1*CLHEP::mm);
           G4LogicalVolume* slMaskAir = new G4LogicalVolume(maskerBoxAir, G4Material::GetMaterial("Air"), maskVAStr);
	   const G4ThreeVector spV(0., 0., 0.);	 
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAir, maskVAStr + std::string("_P"), slVol , false, 1, true);
	   
	   std::string maskVStr("HadronAbsorberSculpSolidAl-");	maskVStr += akSlVStrStr.str();			
	   G4Box* maskerBoxAlum = new G4Box(maskVStr, halfWidthMask, halfHeightMask, aFoot/2. - 0.2*CLHEP::mm);
           G4LogicalVolume* slMaskAl = new G4LogicalVolume(maskerBoxAlum, G4Material::GetMaterial("Aluminum"), maskVStr);
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAl  , maskVStr + std::string("_P"), slMaskAir, false, 1, true);					
           break;
        }
       default : // The rest of it. Just Steel
        {
	
           G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), topVStr);
           new G4PVPlacement(&fRotBeamlineAngle, locSliceV, 
                                                slVol, aSlVStr + std::string("_P"), topVol, false, 1, true);
	   std::string maskVAStr("HadronAbsorberSculpEndAir-");	maskVAStr += 	akSlVStrStr.str();			
	   const double halfWidthMask = 62.0*in/2.; const double halfHeightMask = 60.0*in/2.;				
	   G4Box* maskerBoxAir = new G4Box(maskVAStr, halfWidthMask+tranSpacing/2., halfHeightMask+tranSpacing/2., 
	                  aFoot/2. + longSpacing/2. - 0.1*CLHEP::mm);
           G4LogicalVolume* slMaskAir = new G4LogicalVolume(maskerBoxAir, G4Material::GetMaterial("Air"), maskVAStr);
	   const G4ThreeVector spV(0., 0., 0.);	 
	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskAir, maskVAStr + std::string("_P"), slVol , false, 1, true);
	   
	   std::string maskVStr("HadronAbsorberSculpEndIron-");	maskVStr += akSlVStrStr.str();			
	   G4Box* maskerBoxSteel = new G4Box(maskVStr, halfWidthMask, halfHeightMask, aFoot/2. - 0.2*CLHEP::mm);
           G4LogicalVolume* slMaskIron = new G4LogicalVolume(maskerBoxSteel, G4Material::GetMaterial("Iron"), maskVStr);
     	   new G4PVPlacement((G4RotationMatrix *) 0, spV, slMaskIron  , maskVStr + std::string("_P"), slMaskAir, false, 1, true);					
           break;
	}   					
     } // type of slices (switch)
      zCurrentSl += aFoot + longSpacing + 2.0*CLHEP::mm;
   } // On longitudinal slices. 						  
   {
   // August 25 2015. 
   // We now the air-cooled steel at the end of HA. (approximated as ~ 4.5 of concrete. Let us do it right..
   // (or at least try to... ) 
   //
     const double lengthSteelLast = 60.0*in + 5*CLHEP::mm;
     const double halfHSl = halfHeight - 2.0*CLHEP::cm - std::abs((aFoot/2.)*std::sin(fBeamlineAngle));
     const double halfWSl = halfWidth - 2.0*CLHEP::cm;
     std::string ACIronVStr("HadronAbsorberSculpEndIronAC");    
     G4Box* aBoxSlice = new G4Box(ACIronVStr, halfWSl, halfHSl, lengthSteelLast/2. );
     G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Iron"), ACIronVStr);
     zCurrentSl += (-aFoot/2.) + lengthSteelLast/2. + 0.25*CLHEP::mm;
     G4ThreeVector posEndIron(0., 0., zCurrentSl); 
     new G4PVPlacement(&fRotBeamlineAngle, posEndIron, 
   					     slVol, ACIronVStr + std::string("_P"), topVol, false, 1, true);
     std::cerr << " Placing Air cooled Iron at Z = " << zCurrentSl << std::endl;
	zCurrentSl +=	lengthSteelLast/2. + longSpacing + 2.0*CLHEP::mm;			     
   }
   {
     const double lengthConcBackWall = 36.0*in + 5*CLHEP::mm;
     const double halfHSl = halfHeight - 2.0*CLHEP::cm - std::abs((aFoot/2.)*std::sin(fBeamlineAngle));
     const double halfWSl = halfWidth - 2.0*CLHEP::cm;
     std::string ACConcVStr("HadronAbsorberSculpBackWall");    
     G4Box* aBoxSlice = new G4Box(ACConcVStr, halfWSl, halfHSl, lengthConcBackWall/2. );
     G4LogicalVolume* slVol = new G4LogicalVolume(aBoxSlice, G4Material::GetMaterial("Concrete"), ACConcVStr);
     zCurrentSl += lengthConcBackWall/2. + 1.0*CLHEP::mm;
     G4ThreeVector posBackWall(0., 0., zCurrentSl); 
     std::cerr << " Placing Concrete Back wall at Z = " << zCurrentSl << std::endl;
     new G4PVPlacement(&fRotBeamlineAngle, posBackWall, 
   					     slVol, ACConcVStr + std::string("_P"), topVol, false, 1, true);
     zCurrentSl += lengthConcBackWall/2. + 1.0*CLHEP::mm;
   
   }
   //
   // Now we finish the carving the downstream air buffer,  to avoid extra concrete..
   // We assume a simple alvove, 1.5 m. radius.  
   //
   /*
   ** This seems to cause the complete loss of muons at the back wall, over a radius of 1.5 m... 
   ** There is an overlap.. to be fixed.. When we have a design for the muon wall.  
   {
     const double radAlcove = 1.5*CLHEP::m;
     std::cerr << " Final air buffer...  " << lengthCylBuffer << " shortLengthCylBuffer " 
            << shortLengthCylBuffer << "  .. zCurrentSl " << zCurrentSl << std::endl;
     {
       const G4String aAirBuffTStrT("HadronAbsorberSculpAibufferLastTop");     
       const G4String aAirBuffTStrS("HadronAbsorberSculpAibufferLastSub");     
       const G4String aAirBuffTStrD("HadronAbsorberSculpAibufferLastEff");     
       G4Tubs* aTubeAirbufferA = new G4Tubs(aAirBuffTStrT, 0., radAlcove, lengthCylBuffer/2., 0., 360.0*CLHEP::degree);     
       G4Box* aBoxAirbufferB = new G4Box(aAirBuffTStrS, radPipe, radAlcove, 1.05*shortLengthCylBuffer/2.); // could a bit longer..
       G4ThreeVector shiftZ(0., 0.,  lengthCylBuffer/2.);   
       G4SubtractionSolid* aSectionSubAirbufferA = 
	     new G4SubtractionSolid(aAirBuffTStrD, aTubeAirbufferA, aBoxAirbufferB,  &fRotBeamlineAngle, shiftZ);
	     
       G4LogicalVolume* airBuffVol = new G4LogicalVolume(aSectionSubAirbufferA, G4Material::GetMaterial("Air"), aAirBuffTStrD);
       G4ThreeVector shiftZby2(0., 0., zCurrentSl );   
       new G4PVPlacement((G4RotationMatrix *) 0, shiftZby2, airBuffVol, aAirBuffTStrD+std::string("_P") , topVol, false, 1, true);    
     }
   }
   */
   {
     const double heightAlcove = 6*CLHEP::m;
     const double lengthAlcove = 1.5 * CLHEP::m;
     std::cerr << " Final air buffer...  " << lengthAlcove << " height/width " 
            << heightAlcove << "  .. zCurrentSl " << zCurrentSl << "Total volume length: "<<totalLength<<" Half width: "<<halfWidth<<" Half Height: "<<halfHeight<<std::endl;
     {
       const G4String aAirBuffTStrT("HadronAbsorberSculpAirBufferBox");     
       const G4String aAirBuffTStrD("HadronAbsorberSculpAirBuffer");     
       G4Box* aBoxAirBufferA = new G4Box(aAirBuffTStrT, heightAlcove/2, heightAlcove/2, lengthAlcove/2);
	     
       G4LogicalVolume* airBuffVol = new G4LogicalVolume(aBoxAirBufferA, G4Material::GetMaterial("Air"), aAirBuffTStrD);
       zCurrentSl += (lengthAlcove/2 + 2*CLHEP::mm)/fabs(cos(fBeamlineAngle));
       G4ThreeVector shiftZby2(0., 0., zCurrentSl );
       new G4PVPlacement(&fRotBeamlineAngle, shiftZby2, airBuffVol, aAirBuffTStrD+std::string("_P") , topVol, false, 1, true);    

       //Add muon tracking plane
       G4Box* muonTrackingBox = new G4Box("MuonTrackingBox",heightAlcove*0.45,heightAlcove*0.45,1*CLHEP::cm);
       G4LogicalVolume* muonTrackingVol = new G4LogicalVolume(muonTrackingBox,G4Material::GetMaterial("Argon"),"MuonTrackingVol");
       G4ThreeVector shiftZ(0,0,-lengthAlcove/2 + 30*CLHEP::cm);//5 cm from the upstream face of the alcove volume
       G4RotationMatrix* rot = new G4RotationMatrix(fRotBeamlineAngle.inverse());
       G4cout <<"Upstream face: "<<-lengthAlcove/2<<G4endl;
       G4cout <<"Z Shift: "<<shiftZ.z()<<G4endl;
       G4cout <<"Distance: "<<heightAlcove*0.4<<G4endl;
       new G4PVPlacement(rot,shiftZ,muonTrackingVol,"MuonTrackingVol_P",airBuffVol,false,1,true);

     }
   }
  
}

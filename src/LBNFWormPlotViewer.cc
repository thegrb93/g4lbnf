//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: LBNFWormPlotViewer.cc 74050 2013-09-20 09:38:19Z gcosmo $

#include "LBNFWormPlotViewer.hh"

#include "G4ios.hh"
#include <sstream>

#include "G4VSceneHandler.hh"
#include "G4Scene.hh"
#include "LBNFWormPlotter.hh"
#include "G4UImanager.hh"

LBNFWormPlotViewer::LBNFWormPlotViewer(G4VSceneHandler& sceneHandler,
				       const G4String& name,
				       LBNFWormPlotter* aWorm):
    G4VViewer(sceneHandler, sceneHandler.IncrementViewCount(), name),
    fFileCount(0)
{
    theWorm = aWorm;
    if (!aWorm) theWorm = new LBNFWormPlotter;
}

LBNFWormPlotViewer::~LBNFWormPlotViewer() {}

void LBNFWormPlotViewer::SetView()
{
  // Do nothing here
}


void LBNFWormPlotViewer::ClearView() {}

void LBNFWormPlotViewer::DrawView() {

    std::ostringstream filename;
    filename << "g4WormPlot." << fShortName << '_' << fFileCount++ << ".jpeg";
    theWorm->CreatePlot(filename.str());

}

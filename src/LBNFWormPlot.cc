//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4RayTracer.cc 74050 2013-09-20 09:38:19Z gcosmo $

#include "LBNFWormPlot.hh"
#include "LBNFWormPlotSceneHandler.hh"
#include "LBNFWormPlotViewer.hh"
#include "LBNFWormPlotter.hh"

LBNFWormPlot::LBNFWormPlot():
    G4VGraphicsSystem("WormPlot",
		      "WormPlot",
		      "Geometry 2d projection plots",
		      G4VGraphicsSystem::twoD)
{
  thePlot = new LBNFWormPlotter();
}

LBNFWormPlot::~LBNFWormPlot()
{
  delete thePlot;
}

G4VSceneHandler* LBNFWormPlot::CreateSceneHandler (const G4String& name) {

    G4VSceneHandler* pScene = new LBNFWormPlotSceneHandler(*this, name);
    return pScene;

}

G4VViewer* LBNFWormPlot::CreateViewer (G4VSceneHandler& sceneHandler,
				       const G4String& name) {
    G4VViewer* pViewer = new LBNFWormPlotViewer(sceneHandler, name);
    if (pViewer) {
	if (pViewer->GetViewId() < 0) {
	    G4cout <<
		"LBNFWormPlot::CreateViewer: ERROR flagged by negative"
		" view id in LBNFWormPlotViewer creation."
		"\n Destroying view and returning null pointer."
		   << G4endl;
	    delete pViewer;
	    pViewer = 0;
	}
    } else {
	G4cout <<
	    "LBNFWormPlot::CreateViewer: ERROR: null pointer on new LBNFWormPlotViewer."
	       << G4endl;
    }

    return pViewer;

}

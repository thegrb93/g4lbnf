#include "LBNESteppingActionMessenger.hh"

#include "LBNESteppingAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "Randomize.hh"
#include "G4RunManager.hh"

LBNESteppingActionMessenger::LBNESteppingActionMessenger(LBNESteppingAction* RA)
  :SteppingAction (RA)
{
     //
  //LBNE/stepping
  //
  StepDir = new G4UIdirectory("/LBNE/stepping/");
  StepDir->SetGuidance("In Step analysis and  control.");
  
  SimulationNameCmd  = new G4UIcmdWithAString("/LBNE/stepping/name",this);
  SimulationNameCmd->SetGuidance("Name for the study in question");
  SimulationNameCmd->SetParameterName("Name",true);
  SimulationNameCmd->SetDefaultValue ("GeomPos101");
  SimulationNameCmd->AvailableForStates(G4State_Idle);
    

  OutputNtupleFileNameCmd  = new G4UIcmdWithAString("/LBNE/stepping/ntuplefilename",this);
  OutputNtupleFileNameCmd->SetGuidance("Root ntuple with positions of volume transitions ");
  OutputNtupleFileNameCmd->SetParameterName("NtupleFileName",true);
  OutputNtupleFileNameCmd->SetDefaultValue ("./steppingTuple.root");
  OutputNtupleFileNameCmd->AvailableForStates(G4State_Idle);

  OutputASCIIFileNameCmd  = new G4UIcmdWithAString("/LBNE/stepping/filename",this);
  OutputASCIIFileNameCmd->SetGuidance("Ascii file Name for a plain ASCII file with positions of the geantino  ");
  OutputASCIIFileNameCmd->SetParameterName("FileName",true);
  OutputASCIIFileNameCmd->SetDefaultValue ("./steppingActionOut.txt");
  OutputASCIIFileNameCmd->AvailableForStates(G4State_Idle);
    
  KeyVolumeForOutput  = new G4UIcmdWithAString("/LBNE/stepping/keyVolumeForOutput",this);
  KeyVolumeForOutput->SetGuidance("A volume that will trigger output running geantino propagation ");
  KeyVolumeForOutput->SetParameterName("keyVolumeForOutput",true);
  KeyVolumeForOutput->SetDefaultValue ("blank");
  KeyVolumeForOutput->AvailableForStates(G4State_Idle);
  
  KeyVolumeForOutputTo  = new G4UIcmdWithAString("/LBNE/stepping/keyVolumeForOutputTo",this);
  KeyVolumeForOutputTo->SetGuidance(
   "A volume that will trigger output running geantino propagation, second one Post or pre step  ");
  KeyVolumeForOutputTo->SetParameterName("keyVolumeForOutputTo ",true);
  KeyVolumeForOutputTo->SetDefaultValue ("blank");
  KeyVolumeForOutputTo->AvailableForStates(G4State_Idle);
  
 GenerateParticleOuputHorns  = new G4UIcmdWithABool("/LBNE/stepping/generateParticlesFluxThroughHorns",this);
 GenerateParticleOuputHorns->SetGuidance(
  "Save bulky ASCII file with 6D phase info for muons, for instance, for z < 15000. ");
 GenerateParticleOuputHorns->SetParameterName("ParticleFluxInfo",true);
 GenerateParticleOuputHorns->SetDefaultValue (false);
 GenerateParticleOuputHorns->AvailableForStates(G4State_Idle);
    
 GenerateMuonSculptedAbsorberFlux  = new G4UIcmdWithABool("/LBNE/stepping/GenerateMuonSculptedAbsorberFlux",this);
 GenerateMuonSculptedAbsorberFlux->SetGuidance(
  "Save bulky ASCII file with 6D phase info for muons, at the front/back of the sculpted muon absorber ");
 GenerateMuonSculptedAbsorberFlux->SetParameterName("MuonSculptedAbsorberFlux",true);
 GenerateMuonSculptedAbsorberFlux->SetDefaultValue (false);
 GenerateMuonSculptedAbsorberFlux->AvailableForStates(G4State_Idle);
    
 MuonSculptedAbsorberFluxFilename  = new G4UIcmdWithAString("/LBNE/stepping/MuonSculptedAbsorberFluxFilename",this);
 MuonSculptedAbsorberFluxFilename->SetGuidance(
  "Set filename for the muon absorber flux ASCII file ");
 MuonSculptedAbsorberFluxFilename->SetParameterName("Filename",true);
 MuonSculptedAbsorberFluxFilename->SetDefaultValue ("MuonFluxAtSculptedAbsorber.txt");
 MuonSculptedAbsorberFluxFilename->AvailableForStates(G4State_Idle);
    
  
}
LBNESteppingActionMessenger::~LBNESteppingActionMessenger() {

  delete SimulationNameCmd; 
  delete OutputASCIIFileNameCmd;
  delete OutputNtupleFileNameCmd;
  delete KeyVolumeForOutput;
  delete GenerateMuonSculptedAbsorberFlux;
  delete MuonSculptedAbsorberFluxFilename;
  delete StepDir;
}

void LBNESteppingActionMessenger::SetNewValue(G4UIcommand* command,G4String newValues) {

   if (command == SimulationNameCmd)
   { 
      G4cout << "\n---> Stepping Output info Data Set Name " << newValues << G4endl;
      SteppingAction->SetStudyGeantinoMode(newValues);
      
   } else if(command == OutputASCIIFileNameCmd) {
     SteppingAction->OpenAscii(newValues.c_str());
   } else if(command == OutputNtupleFileNameCmd) {
     SteppingAction->OpenNtuple(newValues.c_str());
   } else if(command == KeyVolumeForOutput) {
     SteppingAction->SetKeyVolumeForOutput(newValues);
   } else if(command == KeyVolumeForOutputTo) {
     SteppingAction->SetKeyVolumeForOutputTo(newValues);
   } else if(command == GenerateParticleOuputHorns) {
     G4UIcmdWithABool* cmd= reinterpret_cast<G4UIcmdWithABool*>(command); 
     SteppingAction->SetStudyParticleThroughHorns(cmd->GetNewBoolValue(newValues));
   } else if(command == GenerateMuonSculptedAbsorberFlux) {
     G4UIcmdWithABool* cmd= reinterpret_cast<G4UIcmdWithABool*>(command); 
     SteppingAction->SetGenerateMuonSculptedAbsorberFlux(cmd->GetNewBoolValue(newValues));
   }else if (command == MuonSculptedAbsorberFluxFilename){
     SteppingAction->SetMuonSculptedAbsorberFluxFilename(newValues);
   }
}



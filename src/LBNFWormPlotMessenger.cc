//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4WormPlotMessenger.cc 86973 2014-11-21 11:57:27Z gcosmo $
//
//
//

#include "LBNFWormPlotMessenger.hh"

#include "G4UIdirectory.hh"

#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4ThreeVector.hh"

#include "LBNFWormPlotter.hh"

LBNFWormPlotMessenger* LBNFWormPlotMessenger::fpInstance = 0;

LBNFWormPlotMessenger* LBNFWormPlotMessenger::GetInstance(LBNFWormPlotter* wPlot) {

  if (!fpInstance) fpInstance = new LBNFWormPlotMessenger(wPlot);
  return fpInstance;

}

LBNFWormPlotMessenger::LBNFWormPlotMessenger(LBNFWormPlotter* wPlot) {

    theWorm = wPlot;

    wormDir = new G4UIdirectory("/vis/wormPlot/");
    wormDir->SetGuidance("WormPlot commands.");
    
    xAxisCmd = new G4UIcmdWith3Vector("/vis/wormPlot/xAxis",this);
    xAxisCmd->SetGuidance("Define the xAxis direction vector");
    xAxisCmd->SetParameterName("x", "y", "z", true);
    xAxisCmd->SetDefaultValue(G4ThreeVector(0.0, 0.0, 1.0)); // global z axis
    
    xNameCmd = new G4UIcmdWithAString("/vis/wormPlot/xName",this);
    xNameCmd->SetGuidance("Define the name of the plot xAxis");
    xNameCmd->SetParameterName("xName", true);
    xNameCmd->SetDefaultValue("z");
    
    yAxisCmd = new G4UIcmdWith3Vector("/vis/wormPlot/yAxis",this);
    yAxisCmd->SetGuidance("Define the yAxis direction vector");
    yAxisCmd->SetParameterName("x", "y", "z", true);
    yAxisCmd->SetDefaultValue(G4ThreeVector(0.0, 1.0, 0.0)); // global y axis
    
    yNameCmd = new G4UIcmdWithAString("/vis/wormPlot/yName",this);
    yNameCmd->SetGuidance("Define the name of the plot yAxis");
    yNameCmd->SetParameterName("yName", true);
    yNameCmd->SetDefaultValue("y");
    
    xMinCmd = new G4UIcmdWithADoubleAndUnit("/vis/wormPlot/xMin",this);
    xMinCmd->SetGuidance("Set the minimum co-ordinate for the plot's x axis");
    xMinCmd->SetParameterName("x", true);
    xMinCmd->SetDefaultUnit("m");
    xMinCmd->SetDefaultValue(-1.0); // -1 m
    
    xMaxCmd = new G4UIcmdWithADoubleAndUnit("/vis/wormPlot/xMax",this);
    xMaxCmd->SetGuidance("Set the maximum co-ordinate for the plot's x axis");
    xMaxCmd->SetParameterName("x", true);
    xMaxCmd->SetDefaultUnit("m");
    xMaxCmd->SetDefaultValue(1.0); // 1 m
    
    yMinCmd = new G4UIcmdWithADoubleAndUnit("/vis/wormPlot/yMin",this);
    yMinCmd->SetGuidance("Set the minimum co-ordinate for the plot's y axis");
    yMinCmd->SetParameterName("y", true);
    yMinCmd->SetDefaultUnit("m");
    yMinCmd->SetDefaultValue(-1.0); // -1 m
    
    yMaxCmd = new G4UIcmdWithADoubleAndUnit("/vis/wormPlot/yMax",this);
    yMaxCmd->SetGuidance("Set the maximum co-ordinate for the plot's y axis");
    yMaxCmd->SetParameterName("y", true);
    yMaxCmd->SetDefaultUnit("m");
    yMaxCmd->SetDefaultValue(1.0); // 1 m
    
    z0Cmd   = new G4UIcmdWithADoubleAndUnit("/vis/wormPlot/z0",this);
    z0Cmd->SetGuidance("Set the z (third axis) co-ordinate of the plot's x-y plane position");
    z0Cmd->SetParameterName("z", true);
    z0Cmd->SetDefaultUnit("m");
    z0Cmd->SetDefaultValue(0.0);
    
    fileCmd = new G4UIcmdWithAString("/vis/wormPlot/createPlot",this);
    fileCmd->SetGuidance("Create the worm plot; specify the name of the output file");
    fileCmd->SetParameterName("fileName",true);
    fileCmd->SetDefaultValue("wormPlot.png");
    fileCmd->AvailableForStates(G4State_Idle);
    
}

LBNFWormPlotMessenger::~LBNFWormPlotMessenger()
{
    delete fileCmd;
    delete z0Cmd;
    delete yMaxCmd;
    delete yMinCmd;
    delete xMaxCmd;
    delete xMinCmd;
    delete yNameCmd;
    delete yAxisCmd;
    delete xNameCmd;
    delete xAxisCmd;
    delete wormDir;
}

G4String LBNFWormPlotMessenger::GetCurrentValue(G4UIcommand* command)
{

    G4String currentValue("");
    
    if (command == xAxisCmd) {
	currentValue = xAxisCmd->ConvertToString(theWorm->GetXAxis());
    } else if (command == yAxisCmd) {
	currentValue = yAxisCmd->ConvertToString(theWorm->GetYAxis());
    } else if (command == xMinCmd) {
	currentValue = xMinCmd->ConvertToString(theWorm->GetXMin());
    } else if (command == xMaxCmd) {
	currentValue = xMaxCmd->ConvertToString(theWorm->GetXMax());
    } else if (command == yMinCmd) {
	currentValue = yMinCmd->ConvertToString(theWorm->GetYMin());
    } else if (command == yMaxCmd) {
	currentValue = yMaxCmd->ConvertToString(theWorm->GetYMax());
    }
    
    return currentValue;

}

void LBNFWormPlotMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{

    if (command == xAxisCmd) {
	theWorm->SetXAxis(xAxisCmd->GetNew3VectorValue(newValue));
    } else if (command == xNameCmd) {
	theWorm->SetXName(newValue);
    } else if (command == yAxisCmd) {
	theWorm->SetYAxis(yAxisCmd->GetNew3VectorValue(newValue));
    } else if (command == yNameCmd) {
	theWorm->SetYName(newValue);
    } else if (command == xMinCmd) {
	theWorm->SetXMin(xMinCmd->GetNewDoubleValue(newValue));
    } else if (command == xMaxCmd) {
	theWorm->SetXMax(xMaxCmd->GetNewDoubleValue(newValue));
    } else if (command == yMinCmd) {
	theWorm->SetYMin(yMinCmd->GetNewDoubleValue(newValue));
    } else if (command == yMaxCmd) {
	theWorm->SetYMax(yMaxCmd->GetNewDoubleValue(newValue));
    } else if (command == z0Cmd) {
	theWorm->SetZ0(z0Cmd->GetNewDoubleValue(newValue));
    } else if (command == fileCmd) {
	theWorm->CreatePlot(newValue);
    }
    
}


 



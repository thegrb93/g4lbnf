//#include <fstream>

#include "LBNEDetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"
#include "G4Trap.hh"
#include "G4Cons.hh"
#include "G4Polycone.hh"
#include "G4GenericPolycone.hh"
#include "G4Torus.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4VisAttributes.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVReplica.hh"
#include "G4AssemblyVolume.hh"
#include "LBNEMagneticField.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PVPlacement.hh"
#include "G4RegionStore.hh"
#include "G4SolidStore.hh"
#include "G4GeometryManager.hh"
#include "G4FieldManager.hh"
#include "G4SubtractionSolid.hh"

#include "G4RunManager.hh"
#include "G4ExceptionSeverity.hh"

#include "G4VisExtent.hh"
#include "LBNEPlacementMessenger.hh"
#include "LBNESurveyor.hh"
#include "LBNERunManager.hh"

//---------------------------------------------------------------------------// 
// Handles the tricky volume placements 
//---------------------------------------------------------------------------// 
LBNEVolumePlacements *LBNEVolumePlacements::fInstance = 0;

LBNEVolumePlacements *LBNEVolumePlacements::Instance() {
  if (fInstance == 0) fInstance = new LBNEVolumePlacements;
  return fInstance;
}

LBNEVolumePlacements::LBNEVolumePlacements() {

   fCheckVolumeOverLapWC = true;
   fUse1p2MW = false; // We will make this true at some point.. 
   fUse1p2MW = true; // We do make it true, August/September 2014. 
   fUsePseudoNova = false;
   fConstructPlug = false;
   fConstructPlugInHorn1 = false;
   fRemoveDecayPipeSnout = false; // By default, we have the decay pipe snout, but 
                                  // if we optimize the Horn 1/2, then we may not have room for it. 
				  // We simplify our life by removing temporarely. See G4UI card of the same name. 
   fUseHorn1Polycone = false; // By default, we still use the NuMI Horn1 
                              // It's mother is now a Polycone, but the inner conductor 
			      // is still based on the set of equation froun in drawing 36309x 
			      // However, a simple polycone based Horn1 is available. 
			      // See wiki page for details 
			      // September 2014. 
   fUseHornsPolycone = false; //  
                              // August 2015, Extension to multiple simple horns. 
			      // Similar to Horn1Polycone. 
			      //
   fUseNumberOfHornsPoly = 0; // ditto 			       
   fHorn1RadiusBigEnough = false; // To fully contain (~ 1 m. long) the target.  October 2014: a rather stale command...
                                  // No longer recommended for use. 

   fArbitraryOffsetHystericalOne = 25.3*CLHEP::mm;
   const G4double in = 2.54*CLHEP::cm;
    // declaration of some constant which should not impact the physics at all, 
    // but are needed to build the G4 geometry. 
   fDecayPipeLength=203.7*CLHEP::m; // CDR, Vol 2 LBNE Doc Db 4317-v25
   fDecayPipeLengthCorrection = -1996*CLHEP::mm;  // Thic correction 
   // comes about the fact that the Hadron absorber had to be placed a bit recessed with respect to 
   // end of the decay pipe to avoid volume overlaps. The Hadron absorber is tilte by the beamline angle,
   // as it has be vertical for mechanical reasons. The value above for that correction has been obtained empirically 
   // by running geantino down the beam line. The shift makes sense, based on the height of the Hadron Absorber 
   // enclosure.
   fDecayPipeLength += fDecayPipeLengthCorrection;
   const double aRockLength = 2.0*(fDecayPipeLength + 160.*CLHEP::m ); 
      // Approximate.. 150 m. is for the target hall, Horn1 + horn2, and the Hadron absorber hall + muon alcove. 
   fLengthOfRockDownstr = 2.0*250.0*CLHEP::m; // to absorb muons.. 
   fTotalLength = aRockLength+fLengthOfRockDownstr;
   fDecayPipeRadius = 4.0*CLHEP::m/2.;
   fDecayPipeUpstrWindowThick = 1.3*CLHEP::mm; // After discussion with Alberto M., Sept 3 2013. Material is Berylium 
   fDecayPipeWallThick = 12.5*CLHEP::mm; // CDR, March 2012, Vol-2, p 3.130 
   fDecayPipeLongPosition = 22.2*CLHEP::m; // From the target, CDR-Vol2, March 2012, Decay pipe section, p. 3.130
   // The above will be overwritten in placing the DecayPipeHall, downstream of the decay pipe snout. 
   fDecayPipeGas = G4String("Helium");
    
   fHorn1Length = 138.0*in; // Oversized.. Drawing 8875 - ME - 363093
   fHorn1DownstreamPlateLength = in*(0.00005); // To be defined later... 
		       // With respect to the center of the mother volume UpstreamTargetAssembly
		       // Obsolete, August 2014.. 
//
// Target The first quantity is the target length as far as the beam is concerned 
//  Can be changed vis G4UIMessenger command 
  fTargetSLengthGraphite = 953.8*CLHEP::mm; // Russian drawing 7589-00-00-00.  

  fTargetSLengthDownstrEnd = 0.964*in; // Doc db 6100. Consistent with Russian drawing. 
// 
// The following quantity can be changed via messenger command. 
// 
  fTargetLengthIntoHorn = fTargetSLengthGraphite - 350.*CLHEP::mm + fArbitraryOffsetHystericalOne; //  Integration drawing 8875.000-ME-363028
   //last number emprically set 
     // to have the first fin target at coord 0.3mm, to follow the convention. 
     // Related to the following quantity... 
     
  fTargetLengthOutsideHorn = 450.0*CLHEP::mm; // Default with 1.2 mW (to avoid overlap.. ) 
  fTargetMaterialName = G4String("Graphite");
  fTargetDensity =  1.78*CLHEP::g/CLHEP::cm3; // Assume density of POCO ZXF-5Q  graphite
  fTargetSpecificLambda = 85.8*CLHEP::g/CLHEP::cm2; // Specific nuclear interaction length (g/cm2) for graphite

  fUseRoundedTargetFins = (fUse1p2MW) ? true : false; // If need be, we "back implement" for the 700 kW option. 
  // No fTargetX0, Y0:  These would be surveyed point, part of the alignement 
  // realm, i.e. handled in the Surveyor class.
  fTargetFinHeight = 21.4*CLHEP::mm - 0.005*CLHEP::mm; // It includes half of the cooling tube, which will be inside the fins
    // Subract 200 microns such that the segments in the He containment tube. 
                             // Drawing 7589-01-50-02 
  // The round edges (grinded 
//  fTargetFinWidth = 6.4*CLHEP::mm;
  fTargetFinWidth = 7.4*CLHEP::mm; // 6.4 -> 7.4 Per discussion with Jim and Alberto, July 25 2013.
  fTargetFinWidthRequired =  fTargetFinWidth; // Used only for 1p2MW option..
  fTargetFinContainerWidth = fTargetFinWidth; // old behavior, prior to 1p2 MW upgrade. 
  fTargetFinLength = 20*CLHEP::mm; // 
   // We add the fin that span the Usptream target and downstream target part. 
  
 //  fTargetNumberFins = 47; //  LBNE Doc 6100.. But we have to compute it from the graphite target length 
  // Since fTargetSLengthGraphite can be changed..Use the same algorithm.. 
  fTargetZOffsetStart = 32.2*CLHEP::mm; // From Russian drawing 7589-00-00-00 
  fTargetUpstrUpstrMargin = 1.0*CLHEP::cm; // Perhaps one ought to extend it is Upstream vacuum flange thicker. 
  fTargetUpstrDwnstrMargin = 1.0*CLHEP::mm; 
  fTargetUpstrPlateMaterial = std::string("Aluminum");
  fTargetUpstrPlateHoleRadius = (35.0/2.)*CLHEP::mm;//From Russian drawing 7589-00-00-00
  fTargetUpstrPlateOuterRadius = (214./2.)*CLHEP::mm; //From Russian drawing 7589-00-00-00
  fTargetUpstrPlateThick = 18.4*CLHEP::mm; // From Russian drawing 7589-00-00-00
  fTargetCanMaterial = std::string("Aluminum");
  fTargetCanInnerRadius = (155./2.)*CLHEP::mm; // From Russian drawing 7589-00-00-00
  fTargetCanOuterRadius = fTargetCanInnerRadius + 6.6*CLHEP::mm;
  fTargetCanLength = 9.80*in; // read out from my scree from LBNE Doc 6100, page 1
  fTargetDownstrCanFlangeThick = 0.85*in;
  fTargetDownstrCanFlangeMaterial = std::string("Aluminum"); 
  fTargetDownstrCanFlangeInnerRadius = (155.0/2.)*CLHEP::mm;
  fTargetDownstrCanFlangeOuterRadius = 120.0*CLHEP::mm; // Approximate
//  fTargetDownstrCanFlangeThick = (3.0/8.0)*in; ??? mistake See above 
  fTargetFlangeMaterial = std::string("Steel316"); 
  fTargetFlangeInnerRadius =  15.8*CLHEP::mm; // Approximate... +- 0.5 mm 
  fTargetFlangeOuterRadius= (8.24/2.)*in;;
  fTargetFlangeThick = 25.*CLHEP::mm;
  fTargetCTubeMaterial = std::string("Titanium"); 
  fTargetCTubeOuterRadius = (3. + 0.2)*CLHEP::mm ; // Doc db 6100 
  fTargetCTubeInnerRadius = 3.0*CLHEP::mm;
  fTargetCTubeUpstrLength = 9.621*in; // Length from the upstream bend to the fron the steel drawnstream flange  
  fTargetCTubeUpstrOffset = 2.32*in; // from the beam line tp to the center of the cooling tube. 
  fTargetCTubeUpstrRadCurve = 0.965*in;
  fTargetCTubeUpstrNumSegCurve = 12; // 30 degrees angles Should be good enough. 
  fTargetCTubeReturnHOffset = 0.1*in; // guess !!! barely at 3 sigma of the beam...
  fTargetCTubeReturnDownstrCutAngleStart = 0.35; 
  fTargetCTubeReturnDownstrCutAngleSize = M_PI - 2.0*fTargetCTubeReturnDownstrCutAngleStart;
  fTargetLengthOutsideExtra = 0.; // differ from zero only for the Pseudo Nova option. 
  // Above quantity obsolete, use a simple model for the downstream end of the Helium target return 
  // cooling pipe.   
  // We will model the bends in the cooling tube with a simple tubular section, of titanium, filled with water. 
  const double lEffCTubeReturnDownstr = 2.0*(0.6*in) + 15*CLHEP::mm;  // 1rst term is the bend, horizontal part, top & bottom. 
  const double vCTubeReturnDownstrTitanium = lEffCTubeReturnDownstr*M_PI*
     ( fTargetCTubeOuterRadius*fTargetCTubeOuterRadius - fTargetCTubeInnerRadius*fTargetCTubeInnerRadius);
  const double vCTubeReturnDownstrWater = lEffCTubeReturnDownstr*M_PI*
                                         fTargetCTubeInnerRadius*fTargetCTubeInnerRadius;
  fTargetCTubeReturnDownstrRadInner = 11.0*CLHEP::mm; // Rougly 14.5 - 3 - a bit, to make up the mass 
                                               // without increasing the thickness by too much
  fTargetCTubeReturnDownstrRadOuter = 14.25*CLHEP::mm;
  const double aSurfWedge = fTargetCTubeReturnDownstrRadOuter*fTargetCTubeReturnDownstrRadOuter - 
                          fTargetCTubeReturnDownstrRadInner*fTargetCTubeReturnDownstrRadInner;
  const double aWedgeRelSize = fTargetCTubeReturnDownstrCutAngleSize/(2.0*M_PI);
  fTargetCTubeReturnDownstrThickTitanium = 
      vCTubeReturnDownstrTitanium/(M_PI*aWedgeRelSize*aSurfWedge);
  fTargetCTubeReturnDownstrThickWater =
      vCTubeReturnDownstrWater/(M_PI*aWedgeRelSize*aSurfWedge);
//  std::cerr << " Effective thickness for cTube return, " << fTargetCTubeReturnDownstrThickTitanium <<
//               " Water " << fTargetCTubeReturnDownstrThickWater << std::endl;
 
  // A small correction for the tilt angle 
  fTargetDistFlangeToTargetStart = 32.2*CLHEP::mm;
  fTargetCTubeReturnLengthUpstr = fTargetDistFlangeToTargetStart + 10.25*in; // from the upstream end of target to 
                                 // Upstr bend return 
  fTargetCTubeReturnLengthUpstrEnd = 6.604*in - fTargetDistFlangeToTargetStart;
  fTargetHeContTubeThickness = 0.4*CLHEP::mm;
  fTargetHeContTubeInnerRadius = 30.0*CLHEP::mm/2.0 - fTargetHeContTubeThickness; // Doc DB 6100, page 1 
  fTargetAlignRingMaterial = std::string("Aluminum");
  
  fTargetAlignRingThick = 11.0*CLHEP::mm; // Accurate to +- 1 mm, from Russian drawing. Err on the excess side. 
  fTargetAlignRingInnerRadius = (18.0/2)*CLHEP::mm + 0.25*CLHEP::mm; // final tweak to avoid overlaps. compensate by increaing thickness.
  fTargetAlignRingOuterRadius = fTargetHeContTubeInnerRadius - 0.1*CLHEP::mm;
//  fTargetAlignRingCutAngle = 0.1735; // Assume atan(3.4 mm/fTargetAlignRingInnerRadius), to remove overlaps 
  fTargetAlignRingCutAngle = 0.45; // Too tight... increase it 
  fTargetAlignRingThick *= 1.0 + 2.0*fTargetAlignRingCutAngle/M_PI; // We compensate the deleted region of the rings 
                                                   // by increasing the thickness.                  // 
  fTargetAlignRingSpacing = 243.6*CLHEP::mm; // Such we can have 5 alignment rings over the entire distance. 
  fMaxNumAlignRings = 5; // assiming a target length of ~ 1 m. long 
  fTargetBerylDownstrWindowThick = 0.5*CLHEP::mm; // Docdb 6100 
  fTargetBerylDownstrWindowRadius = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness; 
  fTargetBerylUpstrWindowThick = 1.0*CLHEP::mm; // pure guess 
  fTargetBerylUpstrWindowRadius = 0.8425*in/2.; // ???? Again, guessing... 
//  
// Simple target code: 
//
  fUseSimpleTargetBox = false;  
  fRemoveTargetAlltogether = false;  
  fUseSimpleTargetCylinder = false;  
  fSimpleTargetLength = fTargetSLengthGraphite; // default should be what NUMI used, respecting traditions. 
  fSimpleTargetRadius = 7.4*CLHEP::mm; // not used if box. 
  fSimpleTargetHeight = 19.0*CLHEP::mm; // not used if cylinder 
  fSimpleTargetWidth = 7.4*CLHEP::mm; // not used if cylinder 
// Quynh, August 2014. 
  fUseMultiSphereTarget = false;//Quynh. Set by G4 UI card /LBNE/det/UseMultiSphereTarget
  fMultiSphereTargetLength = fTargetSLengthGraphite;// Quynh.real target.(1) - // Preliminary value, see AdaptForMultiSphereTarget
  fMultiSphereTargetRadius = 8.5*CLHEP::mm ; //controllabe by a UI card
  
  // Section of Horn1 which is the logical volume of the target Upstream Assembly.
  // Drawing NUMI HORN#1, TRANSITION INNER TO OUTER Drawing number 8875. 112-MD-363097
  // August 2014: "target" is now a misnomer, we place these volume in the Horn1PolyM1
  fFillHornsWithArgon = true; // momentarily... ! For CD1-Refresh (pre June 2015), this was Air. 
  fTargetHorn1InnerRadsUpstr.resize(5);
  fTargetHorn1InnerRadsDownstr.resize(5);
  fTargetHorn1TransThick.resize(5);
  fTargetHorn1Lengths.resize(5);
  fTargetHorn1ZPositions.resize(5);
  fTargetHorn1InnerRadsUpstr[0] = 1.88*in;
  fTargetHorn1InnerRadsUpstr[1] = 2.28*in;
  fTargetHorn1InnerRadsUpstr[2] = 2.78*in; // It will a G4tubs, not G4Cons
  fTargetHorn1InnerRadsUpstr[3] = 4.49*in;
  fTargetHorn1InnerRadsUpstr[4] = 5.10*in;
  fTargetHorn1InnerRadsDownstr[0] = 1.573*in;
  fTargetHorn1InnerRadsDownstr[1] = 1.88*in;
  fTargetHorn1InnerRadsDownstr[2] = 4.49*in; // It will a G4tubs, not G4Cons This is the outer radius 
  fTargetHorn1InnerRadsDownstr[3] = 5.10*in;
  fTargetHorn1InnerRadsDownstr[4] = 5.53*in;
  fTargetHorn1TransThick[0] = (0.25*in)/std::cos(M_PI*45./180.);
  fTargetHorn1TransThick[4] = (0.25*in)/std::cos(M_PI*45./180.);
  fTargetHorn1TransThick[1] = (0.25*in)/std::cos(M_PI*60./180.);
  fTargetHorn1TransThick[3] = (0.25*in)/std::cos(M_PI*60./180.);
  fTargetHorn1TransThick[2] = fTargetHorn1InnerRadsDownstr[2] -   fTargetHorn1InnerRadsUpstr[2];
  fTargetHorn1Lengths[0] = 0.457*in;
  fTargetHorn1Lengths[4] = fTargetHorn1Lengths[0];
  fTargetHorn1Lengths[1] = 0.410*in;
  fTargetHorn1Lengths[3] = fTargetHorn1Lengths[1];
  fTargetHorn1Lengths[2] = 0.370*in; // Increase thickness 
  // August 2014 Minor adjustments.. 
  const double aTotalPseudoTargetHorn1Length =  
           fTargetHorn1Lengths[0] +  fTargetHorn1Lengths[1] + fTargetHorn1Lengths[2] + 0.05*CLHEP::mm;
//  fTargetHorn1ZPositions[0] = -fTargetHorn1Lengths[0]/2.  - 2.5*CLHEP::mm ; // With respect to MCZero, or the dowstream end of Upstream Target Assembly
//                                                          // push them bacward a bit to avoid overlaps 
// With respecto the center of the container volume. (August 2014.. )   
  fTargetHorn1ZPositions[0] = aTotalPseudoTargetHorn1Length/2. - fTargetHorn1Lengths[0]/2. - 0.005;
  fTargetHorn1ZPositions[4] = fTargetHorn1ZPositions[0];
  fTargetHorn1ZPositions[1] = -aTotalPseudoTargetHorn1Length/2. + fTargetHorn1Lengths[2] + fTargetHorn1Lengths[1]/2.;
  fTargetHorn1ZPositions[3] = fTargetHorn1ZPositions[1];
  fTargetHorn1ZPositions[2] = -aTotalPseudoTargetHorn1Length/2. + fTargetHorn1Lengths[2]/2 + 0.005*CLHEP::mm; // 
   //
   // Basic algorithm to determine position of the target main daughter volumes 
   //
   // Details about the baffle 
   fBaffleInnerRadius = 6.5*CLHEP::mm; // Per discussion with Jim Hylen, July 22 2013 
   fBaffleOuterRadius = 28.3*CLHEP::mm; // from previous version and le010z185i_NumiDPHelium.input
   fBaffleLength = 1474.8*CLHEP::mm; // Drawing 8875.000-ME-363028
   fBaffleWindowThickness = 0.5*CLHEP::mm; // e-mail from Bob Zwaska to Minerva et al, August 2014. 
   fBaffleZPosition = -1.0e19; // to be determined one the Target Usptream segment is installed 
        // offset with respecto MCZERO of the center of target mother volume,  
  //
  // Horn1 initialization. 
  //
  fHorn1RadialRescaleCnt = 0;
  fHorn1LongRescaleCnt = 0;
  fHorn1RadialRescale = 1.0; // No units... a ratio between the actual dimensions implemented in Geant4 over the nominal 
                             // values found in drawing 8875.112-MD-363xxx
  fHorn1LongRescale = 1.0;
  fWaterLayerThickInHorns = 0.; 
  fWaterLayerThickInHorns = 1.*CLHEP::mm;  // Sept. 2014:New default value, e-mail from Cory Crowfields to Laura, March 26 2014. 
  
//   std::cerr << " LBNEVolumePlacements::LBNEVolumePlacements, O.K. done " << std::endl;
// Now configure the target.  Note: this method can be called from the messenger callback, should the 
// the users decide to run with non-standard target. 
//
   if (fUse1p2MW) this->adaptTargetFor1p2MW();
   this->setEntireTargetDims();  // See below, first coded up August 18 2014. 
   this->DeclareHorn1Dims();  // See in file LBNEDownstrVolPlacements.cc 
   this->setMotherVolumeForHorn1(); // We do not want top do this for the multiHorn Polygone, but it should not hurt... 
   
   fExtendChaseLength = 0.;
   
   this->DeclareHorn2Dims();  // See in file LBNEDownstrVolPlacements.cc 
   
       // Details about the plug 
   fPlugInnerRadius = 0.*CLHEP::mm; // Solid cylinder
   fPlugOuterRadius = 26.*CLHEP::mm; // To fit inside Horn2
   fPlugLength = 250.0*CLHEP::mm;; // Just a reasonable default
   fPlugZPosition = 1250.*CLHEP::mm; // An other reasonable  default.. Please check target length. 
   fPlugMaterial = std::string("GraphiteBaffle"); 
   // Default file name forthe GDML file describing the Hadron generator. 
//
// Ad the decay pipe snout info, this is not very long... 
//
// Old NuMI definition 
//   fDecayPipeLongPosition = 17.3*CLHEP::m; // Tentatively fixzed!!! See "Location Plans and List of Drawing, LBNE CD1 review, CDR-30" 
   // However, for the purpose of Horn placement optimization, this could be overwritten in 
   // ExtendChaseLengthForHorn2 Paul Lebrun, Feb 2015
   // This will be the case for LBNF 
   //
   fDecayPipeWindowZLocation = fHorn2LongPosition +  (fHorn2LongRescale-1.0)*fHorn2Length + 201.484*in; 
//
     // The above has been kindly measured off the actual drawing 2251.000-ME-487107, by Glenn Waver, 
     // Sept 2013. It is the distance between the apex of the window 
     // and the point W.P. H.E. ACRNT2, which the entrance of Horn2.
     // We add to that the extra length of Horn2, should it be rescaled. 
     // However, if so, it is best to leave the decay pipe snout aside, until the 
     // the optimization of the Horn's dimensions and location is settled 
     // The RemoveDecayPipeSnout G4UI card has been introduced, late October 2014.  
     //
   fDecayPipeWindowRadiusBeryl = 7.874*in/2.0;
//   fDecayPipeWindowRadiusAlum = 33.0*in/2.0;
// Correction, April 9 2015, cross-checking again with the drawing 2251.000-ME-487107
   fDecayPipeWindowRadiusAlum = 39.370*in/2.0;
   fDecayPipeWindowThickBeryl = fDecayPipeUpstrWindowThick;  
   fDecayPipeWindowThickAlum = 0.064*in;  // make it a bit thicker, due to curvature. 
   // August 2015, CD1-R decision: 
   fChaseWidthForLBNF = 78.0*in;   
//   fAbsorberGDMLFilename = G4String("./gdml/lbne_absorber_112912.gdml");
//
// Received on  Sept 27 2013  a new version from Diane Reitzner 
//
   fUseMarsTargetHorns = false;

   char* g4lbne_path = getenv("G4LBNE_DIR");
   if(g4lbne_path == NULL) {
     std::cout<<"You have not set $G4LBNE_DIR.... I'm assuming it's your current working directory.  Please set $G4LBNE_DIR to the top level directory of your g4lbne installation if that's not the case."<<std::endl;
       g4lbne_path = static_cast<char *>(malloc(4));
       strncpy(g4lbne_path, "./\0",3);
   }

   fMarsTargetHornsGDMLFilename = G4String(g4lbne_path)+G4String("/gdml/mars_horns.gdml");
   
   fAbsorberGDMLFilename = G4String(g4lbne_path)+G4String("/gdml/abs_geomSept13.gdml");
   
   std::cout << "Using absorber gdml file here: " << fAbsorberGDMLFilename <<std::endl;

   fDoInstallShield = true;
   
//   std::cerr << " Removing the shields until the position of Horn1 is confirmed !!! " << std::endl;
//   fDoInstallShield = false;

// August 2015: restore it, even with Polycone Horns..
   
   fRadiusMilindWire = 0.;
   fCurrentMilindWire = 0.;

   fWriteGDMLFile = false;

//
// Octobre 2015, John LoSecco, P.L.  
//   
   fPolyconeHorn1IsParabolic = false; // default, overwrite with /LBNE/det/SetPolyconeHornParabolic

   // John Back, March 2016
   fUseTargetModule = false; // default, overwrite with /LBNE/det/UseTargetModule
   fTargetRadius = 8.5*CLHEP::mm;
   fTargetLength = 953.8*CLHEP::mm;
   // Specify the target module type: SAT, Cylinder (also TwoCylinder?)
   fTargetModuleType = LBNEVolumePlacements::SAT; // overwrite with /LBNE/det/TargetModuleType
   // The fractional length of the full target outside the horn
   fTargetFracOutHornL = -1.0; // overwrite with /LBNE/det/TargetFracOutHornL
   // The number of interaction lengths for the target object. If =-1, use fTargetLength instead
   fTargetNLambda = -1.0; // overwrite with /LBNE/det/TargetNLambda

}
//
// Compute the number of segments and adjust the overall length, and the spacing such that 
// We still have a fixed number of 2 cm long pieces of graphite. Do the crazy upstrea/downstream split. 
// Obsolete, August/Sept 2014. 
//  

void LBNEVolumePlacements::SegmentTarget() {

  const G4double in = 2.54*CLHEP::cm;
  // simplify the geometry if we have a simple target. No front can, all target related dimensions, including He Comtainment vessel, 
  // have no materail to speak off .. 
  //
  if (fUseSimpleTargetCylinder || fUseSimpleTargetBox) {
    fTargetSLength = fSimpleTargetLength;
    fTargetSLengthGraphite = fSimpleTargetLength+ 0.1*CLHEP::mm;
    fTargetCTubeMaterial = std::string("HeliumTarget");
    double effectiveRadius = fUseSimpleTargetCylinder ? fSimpleTargetRadius :
                        std::max(fSimpleTargetHeight, fSimpleTargetWidth);
    fTargetCTubeOuterRadius = effectiveRadius + 0.020*CLHEP::mm;
    fTargetCTubeInnerRadius = effectiveRadius + 0.010*CLHEP::mm;
    // We do not change the lengths, they won't create conflicts in the Russina-doll type of geometry we have.. 
    fTargetHeContTubeThickness = 0.005*CLHEP::mm;
    fTargetHeContTubeInnerRadius =  effectiveRadius + 0.025*CLHEP::mm;
    return;
  }

  fTargetSLength = fTargetSLengthDownstrEnd + fTargetSLengthGraphite; // Russian drawing 7589-00-00-00 
  if (!fUsePseudoNova) {
    fTargetLengthOutsideHorn = fTargetSLength - fTargetLengthIntoHorn + fTargetFinLength;
  } else {
      fTargetLengthOutsideExtra = fTargetLengthOutsideHorn - fTargetSLength;
      fTargetLengthOutsideHorn = fTargetSLength;
  }
  fTargetFinSpacingLength = 0.3*CLHEP::mm; // Nominal value.. 
  double  targetNumFinsNominal = fTargetSLengthGraphite/(fTargetFinLength + fTargetFinSpacingLength); 
  fTargetNumFins = (int) (targetNumFinsNominal + 0.5);
  double deltaNumFins =  targetNumFinsNominal - (double) fTargetNumFins; 
  if (deltaNumFins > 0.5) fTargetNumFins++;
  if (deltaNumFins < -0.5) fTargetNumFins--;
//  std::cerr << " LBNEVolumePlacements::SegmentTarget, total number of segments " << fTargetNumFins << 
//               " Delta Num fins " << deltaNumFins << std::endl;
//  std::cerr << " fTargetSLength " << fTargetSLength << " fTargetLengthOutsideHorn " << 	fTargetLengthOutsideHorn << std::endl; 
      
  if (fUsePseudoNova) fTargetNumFins++; // Add one to compensate for the fin that will be split.. 
  if ((!fUseSimpleTargetCylinder) && (!fUseSimpleTargetBox)) {  
    const double oldLength = fTargetSLengthGraphite;
    fTargetSLengthGraphite = fTargetNumFins*fTargetFinLength + (fTargetNumFins-1)*fTargetFinSpacingLength; 
    fTargetFinSpacingLength = (fTargetSLengthGraphite  - fTargetNumFins*fTargetFinLength)/(fTargetNumFins - 1); //   
    if (std::abs(oldLength - fTargetSLengthGraphite) > 0.001*fTargetSLengthGraphite)
       std::cout << " LBNEVolumePlacements::segmentTarget: Warning: re-adjust the target length from " <<
        oldLength <<  " to " << fTargetSLengthGraphite << " to get  an integer number of 2 cm long segments " << std::endl;
  }
  if (fUsePseudoNova) {	
    fTargetNumFinsUpstr = fTargetNumFins;
    fTargetNumFinsInHorn = 0; 
    fTargetHeContTubeLengthInHorn = 0.;
    fTargetZ0Upstr = 0.;
    fTargetZ0Downstr = 0. ;
  } else {
    // Compute the number of fins upstream and dowsntream of the break between target and horn1.
   
    double tempNFins = (fTargetLengthOutsideHorn - fTargetFinSpacingLength) /(fTargetFinLength + fTargetFinSpacingLength);
    fTargetNumFinsUpstr = (int) tempNFins;
    fTargetNumFinsInHorn = fTargetNumFins - fTargetNumFinsUpstr; //Note one fin will be split...  	
//  std::cerr << "  Num Fins Ups /Downstr " << fTargetNumFinsUpstr << " / " << fTargetNumFinsInHorn << std::endl;
    fTargetFinLengthSplitUpstr = fTargetLengthOutsideHorn - fTargetFinSpacingLength - 
                                 fTargetNumFinsUpstr*(fTargetFinLength + fTargetFinSpacingLength);
    fTargetFinLengthSplitDwnstr = fTargetFinLength - fTargetFinLengthSplitUpstr;
    if ((	fTargetFinLengthSplitDwnstr < 0.) ||
        (	fTargetFinLengthSplitDwnstr > (fTargetFinSpacingLength + fTargetFinLength)) ||
        (fTargetFinLengthSplitUpstr < 0.) || 
        (fTargetFinLengthSplitUpstr > (fTargetFinSpacingLength + fTargetFinLength))) {
        // split occurs in a spacing. Set this length to 0., we will not install this target segment
          std::ostringstream mStrStr; 
	  mStrStr << " Odd length for the splitted fin ..." << fTargetFinLengthSplitUpstr << " Downstr " << 	
          fTargetFinLengthSplitDwnstr << " tempNFins = " << tempNFins << std::endl;
	  G4String mStr(mStrStr.str());
          G4Exception("LBNEVolumePlacements::SegmentTarget", " ", JustWarning, mStr.c_str()); 
	  if (fTargetFinLengthSplitUpstr > fTargetFinLength) fTargetFinLengthSplitUpstr = fTargetFinLength;
	  if (fTargetFinLengthSplitDwnstr > fTargetFinLength) fTargetFinLengthSplitDwnstr = fTargetFinLength;
	// if lengths are negative, skip the placement
	
    }
  
  // more deduced variables.. 		       
//  fTargetHeContTubeLengthInHorn = fTargetLengthIntoHorn + fTargetSLengthDownstrEnd;
    fTargetHeContTubeLengthInHorn = fTargetLengthIntoHorn + 0.025*CLHEP::mm;
    fTargetZ0Upstr = (-1.0* (fTargetSLength - fTargetLengthIntoHorn))/2. ; // Obsolete... 
    fTargetZ0Downstr = fTargetLengthIntoHorn/2. ;
  }
  fTargetAlignRingSpacing *= fTargetSLengthGraphite/953.8*CLHEP::mm; // must fit 5 rings within the prescribed space. 
//  std::cerr << " Target after segmentation " <<  fTargetSLengthGraphite << std::endl;
  fTargetHeContTubeLengthUpstr = 2.0*in + fTargetCanLength  + fTargetDownstrCanFlangeThick + 
                                 fTargetFlangeThick + fTargetDistFlangeToTargetStart;
				 // Approximate... 
  fTargetAndBaffleLengthApprox = fBaffleLength + fTargetUpstrUpstrMargin  + fTargetUpstrPlateThick + fTargetCanLength 
	                   + fTargetDownstrCanFlangeThick + fTargetFlangeThick + 0.2*CLHEP::mm + 44.18*in + fTargetLengthOutsideExtra +
			   fTargetUpstrDwnstrMargin  +  fTargetDistFlangeToTargetStart + fTargetLengthOutsideHorn + 2.0*CLHEP::mm;
			    // NUMI Drawing 8875.000-ME-363028 with 2 mm margin of error. 
  
}
//
// August 2014, with Horn1 Mother is a Polycone. 
// 
void LBNEVolumePlacements::setEntireTargetDims() {

  const G4double in = 2.54*CLHEP::cm;
  fTargetSLength = fTargetSLengthDownstrEnd + fTargetSLengthGraphite; // Russian drawing 7589-00-00-00 
//  if (!fUsePseudoNova) { // Oboslete, August 2014. 
//    fTargetLengthOutsideHorn = fTargetSLength - fTargetLengthIntoHorn + fTargetFinLength;
//  } else {
//      fTargetLengthOutsideExtra = fTargetLengthOutsideHorn - fTargetSLength;
//      fTargetLengthOutsideHorn = fTargetSLength;
//  }
// Nominal does not defines fTargetLengthOutsideHorn..Must do it here... 
  fTargetLengthOutsideExtra = 95.53*CLHEP::mm; // due to various offsets in the upstream target...

  fTargetFinSpacingLength = 0.3*CLHEP::mm; // Nominal value.. 
  double  targetNumFinsNominal = fTargetSLengthGraphite/(fTargetFinLength + fTargetFinSpacingLength); 
  fTargetNumFins = (int) (targetNumFinsNominal + 0.5);
  double deltaNumFins =  targetNumFinsNominal - (double) fTargetNumFins; 
  if (deltaNumFins > 0.5) fTargetNumFins++;
//  std::cerr << " LBNEVolumePlacements::SetEntireTargetDims, total number of segments " << fTargetNumFins << 
//               " Delta Num fins " << deltaNumFins << std::endl;
//  std::cerr << " fTargetSLength " << fTargetSLength << " fTargetLengthOutsideHorn " << 	fTargetLengthOutsideHorn << std::endl; 
      
  const double oldLength = fTargetSLengthGraphite;
  fTargetSLengthGraphite = fTargetNumFins*fTargetFinLength + (fTargetNumFins-1)*fTargetFinSpacingLength; 
  fTargetFinSpacingLength = (fTargetSLengthGraphite  - fTargetNumFins*fTargetFinLength)/(fTargetNumFins - 1); //   
  if (std::abs(oldLength - fTargetSLengthGraphite) > 0.001*fTargetSLengthGraphite)
     std::cout << " LBNEVolumePlacements::setEntireTargetDims: Warning: re-adjust the target length from " <<
        oldLength <<  " to " << fTargetSLengthGraphite << " to get  an integer number of 2 cm long segments " << std::endl;
  fTargetZ0Upstr = 0.;    // ??? Possibly resurrect for Horn1Mother in one piece,/ Aug/Sept 2014. 
  fTargetZ0Downstr = 0. ;
  // More deduced variables. 
  fTargetHeContTubeLength = fTargetSLengthDownstrEnd + fTargetSLengthGraphite + 0.025*CLHEP::mm; // To bechecked, August/Sept 2014. 
  fTargetAlignRingSpacing *= fTargetSLengthGraphite/953.8*CLHEP::mm; // must fit 5 rings within the prescribed space. 
//  std::cerr << " Target after segmentation " <<  fTargetSLengthGraphite << std::endl;
  fTargetHeContTubeLengthUpstr = 2.0*in + fTargetCanLength  + fTargetDownstrCanFlangeThick + 
                                 fTargetFlangeThick + fTargetDistFlangeToTargetStart;
				 // Most likely obsolete variable..
  
} 

LBNEVolumePlacements::~LBNEVolumePlacements() {
  delete fPlacementMessenger;
}
void LBNEVolumePlacements::InitializeMessenger() {
//   std::cerr << " LBNEVolumePlacements::Initialize, about to instantiate PlacementMessengers " << std::endl;
//   fPlacementMessenger = new LBNEPlacementMessenger();
// Why twice ?? 
}
// Obsolete... Check who calls this... 
void LBNEVolumePlacements::Initialize(const G4LogicalVolume* matriarch) {

   fTopLogicalVolume = matriarch; 
   
}

LBNEVolumePlacementData* 
   LBNEVolumePlacements::Create(const G4String &name) {  

  // Check that it is not already defined. 

  std::map<G4String, LBNEVolumePlacementData>::const_iterator itDupl = fSubVolumes.find(name);
  if (itDupl != fSubVolumes.end()) {
      std::ostringstream mStrStr;
      mStrStr << " Volume named " << name << " Already defined. Fatal ";
      G4String mStr(mStrStr.str());
      G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
    }

   const G4double in = 2.54*CLHEP::cm;
  // look for a pre-existing placement.  If there, fatal error, exit 
  LBNEVolumePlacementData info;
  info.fAlignmentModel = eNominal;
  info.fIsPlaced = false;
  info.fRotation = G4RotationMatrix(); // a bit of a waste of space, as, in most cases, not rotations are applied. 
  info.fRotationIsUnitMatrix= true;
  info.fPosition = G4ThreeVector(0., 0., 0.);
  info.fParams.resize(3, 0.);
  info.fMother = 0;
  info.fTypeName = G4String("Tubs"); // most common type of volume.. 
  std::string volumeName(name); // volumeName += std::string("_solid"); Not necessary 
  if (name == G4String("Tunnel")) {
    info.fParams[0] = std::max((18.5*CLHEP::m), 2.0*(fDecayPipeRadius)); 
       // Note: the total volume is 60 m. wide => plenty enough rocks. The last term is for the Hadron Absorber cavern  
    info.fParams[1] = std::max((50*CLHEP::m), 2.0*(fDecayPipeRadius)); // Too tall... Set by the Hadron absorber requirement 
    info.fParams[2] = fTotalLength - fLengthOfRockDownstr -2.0*CLHEP::cm;
    std::cerr << " Total half length of the tunnel " << info.fParams[2]/2. << std::endl;
    std::cerr << " Total half Height of the tunnel " << info.fParams[1]/2. << std::endl;
    std::cerr << " Total half Width of the tunnel " << info.fParams[0]/2. << std::endl;
    G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. ); 
    info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), name); 
    info.fTypeName = G4String("Box");
  }
  if (name == G4String("TargetHallAndHorn1")) {

    // Initialise the target module (e.g. SAT) geometry parameters if required. 
    // This can affect the placement of the upstream section
    if (fUseTargetModule) {
	std::cout << "We are going to use the Target Module set-up" << std::endl;
	this->InitTargetModule();
    }

    const LBNEVolumePlacementData *plInfo = Find(name, "Tunnel", "Create");
    for (size_t k=0; k != 2; ++k) 
      info.fParams[k] = plInfo->fParams[k] - 2.0*CLHEP::cm; 
    info.fParams[2] = fHorn1DownstreamPlateLength 
                      + fHorn1Length + 1.0*CLHEP::cm + fTargetAndBaffleLengthApprox  ; // 1 cm longitudinally for spare?  
// Release r0 : this implies a relatiion ship between MCZERO and the word coordinate in G4 that 
// depends on the tagert Length. 
// Since the tunnel is centered on Rock and TargetHallAndHorn1 is subdivied in the Upstream Target Assembly and the 
// the Horn1Hall, two volumes that do not have the same length, then MCZero is not Z = 0. in world coordinate. 
//    info.fPosition[2] = 0.; // Definiing MCZERO, Drawing 8875. 112-MD-363097, annotated by jim Hylen
// Jim H. - rightfully so, strongly asked for this to be fixed. We start here, by shifting this volume such that 
// Horn1 starts at Z = 0. 
// Formula found for placing Horn1 in TargetHallAndHorn1 (release r0) 
//    
//    info.fPosition[2] = -1.0*plInfoM->fParams[2]/2. + plInfoC->fParams[2] + info.fParams[2]/2. + 0.020*CLHEP::mm;
// 
    const double totalLengthHorn1 = fHorn1DownstreamPlateLength + fHorn1Length + 0.5*CLHEP::cm;
    const double totalLengthUpstr = fTargetAndBaffleLengthApprox;
    info.fParams[2] = totalLengthHorn1 + totalLengthUpstr; // Obsolete, August 2014.. 
     
    fTotalLengthUpstreamAssembly = fBaffleLength +  fTargetUpstrUpstrMargin  + fTargetUpstrPlateThick + fTargetCanLength 
     			+ fTargetDownstrCanFlangeThick + fTargetFlangeThick + 0.2*CLHEP::mm + 2.0*fBaffleWindowThickness + 
     			fTargetUpstrDwnstrMargin  +  fTargetDistFlangeToTargetStart; 
    info.fParams[2] = fHorn1Length + 1.0*CLHEP::cm + fTargetLengthOutsideHorn + 
    		      fTotalLengthUpstreamAssembly ; //To be adjusted..
    fTotalLengthTargetAndHorn1Hall = info.fParams[2];
    G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
    info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName); 
    info.fPosition[2] = (info.fParams[2]/2 - totalLengthUpstr + 0.5*CLHEP::cm); // Defining MCZERO, Drawing 8875. 112-MD-363097, annotated by jim Hylen
    // Correction for Pseudo Nova: one must shift this volume by the extra length due to the comlete retraction.. 
    info.fPosition[2] -= fTargetLengthOutsideExtra; 
    const double distHorn1UpstrToZ0 = fTotalLengthTargetAndHorn1Hall/2. - (fHorn1Length 
    				       - 2.0*(2.436*in*fHorn1LongRescale) + 30.0*CLHEP::mm);
    info.fPosition[2] = -1.0*distHorn1UpstrToZ0 - 1.84*CLHEP::mm; // Last offset due to various margins.  Adjusted with Geantino's  
    std::cerr << " Placing TargetHallAndHorn1 at " << info.fPosition[2] << "  length " <<  info.fParams[2]
    	      << " as upst off " << distHorn1UpstrToZ0 << std::endl << " ...... H1 Length " << fHorn1Length
    	      << " with fTargetLengthOutsideHorn = " <<  fTargetLengthOutsideHorn << std::endl;
   // Such that we keep the same global coordinate system. 
    info.fTypeName = G4String("Box");
  
  } else if (name == G4String("UpstreamTargetAssembly")) {
    const LBNEVolumePlacementData *plInfo = Find(name, G4String("TargetHallAndHorn1"), G4String("Create"));
    info.fParams[0] = 64*in - 1.0*CLHEP::cm;
    info.fParams[1] = 60*in - 1.0*CLHEP::cm; // approximate, we need room for the rotation (Horn is tilted with respect to 
                                      // true horizontal 
    info.fParams[2] = fTotalLengthUpstreamAssembly - 0.001*CLHEP::mm;
    info.fPosition[2] =-plInfo->fParams[2]/2. + fTotalLengthUpstreamAssembly/2.;
    // August 2014: Push the target into the horn, by pushing the whole assembly. 
    if (fUseSimpleTargetCylinder || fUseSimpleTargetBox) {  // Remove geometrical constraint, such that 
     	info.fParams[0] = 2.0*(fTargetHeContTubeInnerRadius + 0.175*CLHEP::mm);
     	info.fParams[1] = info.fParams[0];
     	std::cerr << " Creating UpstreamTargetAssembly with transv. size " 
	          << info.fParams[0] << " fTotalLengthUpstreamAssembly "  
		  << fTotalLengthUpstreamAssembly << " Position " << info.fPosition[2] << std::endl; 
    } 
    info.fParams[2] = fTotalLengthUpstreamAssembly;
    info.fPosition[2] =-plInfo->fParams[2]/2. + info.fParams[2]/2. + 0.5*CLHEP::mm;

    bool check1 = (!fUseHorn1Polycone) && (!fUseHornsPolycone);
    bool check2 = fUseTargetModule && fUseHorn1Polycone;

    if (check1 || check2) {info.fPosition[2] += fTargetLengthOutsideExtra;}

    std::cerr << " Correcting..  UpstreamTargetAssembly with transv. size " 
    	      << info.fParams[0] << " fTotalLengthUpstreamAssembly "  
    	      << fTotalLengthUpstreamAssembly << " Position " << info.fPosition[2] << std::endl; 
    G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
    info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName); 
    info.fTypeName = G4String("Box");
//    std::cerr << " LBNEVolumePlacements::Create " << name << " half length " << info.fParams[2]/2. << std::endl;
      
  } else if (name == G4String("Baffle")) {
  // more cases here... a long list.. a too long of a  list? 
  // We now go one level down in the hierachy. Start from upstream 
    const LBNEVolumePlacementData *plInfo = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
    info.fParams[0] = fBaffleInnerRadius; 
    info.fParams[1] = fBaffleOuterRadius; 
    info.fParams[2] = fBaffleLength - 0.75*CLHEP::mm; // to put the windows in... 
    G4Tubs* baffleTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
    info.fCurrent = new G4LogicalVolume(baffleTube, G4Material::GetMaterial("GraphiteBaffle"), volumeName); 
    info.fPosition[2] = -plInfo->fParams[2]/2. + fBaffleLength/2.0 + 0.505*CLHEP::mm; 
  } else if (name == G4String("BaffleWindowUpstr")) {
  // more cases here... a long list.. a too long of a  list? 
  // We now go one level down in the hierachy. Start from upstream 
    const LBNEVolumePlacementData *plInfo = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
    info.fParams[0] = 0.; 
    info.fParams[1] = 1.5*fBaffleInnerRadius; // not critical 
    info.fParams[2] = fBaffleWindowThickness;
    G4Tubs* baffleTubeW = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
    info.fCurrent = new G4LogicalVolume(baffleTubeW, G4Material::GetMaterial("Beryllium"), volumeName); 
    info.fPosition[2] = -plInfo->fParams[2]/2. + info.fParams[2]/2 + 0.005*CLHEP::mm; 
  } else if (name == G4String("BaffleWindowDownstr")) { // duplicate..except for position.  
  // more cases here... a long list.. a too long of a  list? 
  // We now go one level down in the hierachy. Start from upstream 
    const LBNEVolumePlacementData *plInfo = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
    info.fParams[0] = 0.; 
    info.fParams[1] = 1.5*fBaffleInnerRadius; // not critical  
    info.fParams[2] = fBaffleWindowThickness;
    G4Tubs* baffleTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
    info.fCurrent = new G4LogicalVolume(baffleTube, G4Material::GetMaterial("Beryllium"), volumeName); 
    info.fPosition[2] = -plInfo->fParams[2]/2. + fBaffleLength + 0.050*CLHEP::mm + info.fParams[2]/2 + fBaffleWindowThickness;
  } 
  //
  // Target stuff.. Lots of it
  // 
  if (name.find("Target") != std::string::npos) {
    if (name.find("TargetUpstr") != std::string::npos) {
      if (name == G4String("TargetUpstrMTop")) { 
         const LBNEVolumePlacementData *plInfo = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
         // Complete container the target Canister and the portion of the target outside the horn 
	 // Subject to misalignment!!!
         info.fParams[1] =  10.*in; // mother volume, must hold the Inner/Outer Horn1 transition. 
	 if (fUseSimpleTargetCylinder || fUseSimpleTargetBox) {
	   info.fParams[1] = plInfo->fParams[0]/2. - 0.025*CLHEP::mm; // radius is 1/2 the width (or height) 
	   std::cerr << " Creating TargetUpstrMTop .... with radius " << info.fParams[1] << std::endl;
         } 
         info.fParams[2] = fTargetUpstrUpstrMargin  + fTargetUpstrPlateThick + fTargetCanLength 
	                   + fTargetDownstrCanFlangeThick + fTargetFlangeThick + 0.05*CLHEP::mm +
			   fTargetUpstrDwnstrMargin  +  fTargetDistFlangeToTargetStart; 
	 std::cerr << " Length of TargetUpstrMTop, ( with Horn1 Mother is Polycone) " << info.fParams[2] << std::endl;
	    
         G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
         info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial("Air"), volumeName); 
         info.fPosition[2] = plInfo->fParams[2]/2. - info.fParams[2]/2. - 0.01*CLHEP::mm; // Nominal: 
	           // Half length of UpstreamTargetAssembly - length of this  
		   
       } 
      if (name == G4String("TargetUpstrM0")) { // Complete container for the target Canister and flange  
         const LBNEVolumePlacementData *plInfo = Find(name, G4String("TargetUpstrMTop"), G4String("Create"));
         info.fParams[1] = fTargetDownstrCanFlangeOuterRadius + 1.0*CLHEP::cm; 
         info.fParams[2] = fTargetUpstrUpstrMargin  + fTargetUpstrPlateThick + fTargetCanLength 
	                   + fTargetDownstrCanFlangeThick + fTargetFlangeThick;
	   // This volume hierarchy could be cleanup some more, evidently. 
	   // August 27 2014.. Leave it as such for now.. 
         info.fParams[2] = plInfo->fParams[2] - 0.010*CLHEP::mm ; // room for the downstream window of the baffle.
	 std::cerr << " Length of TargetUpstrMTop, ( with Horn1 Mother is Polycone) " << info.fParams[2] << std::endl;
         G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
         info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial("HeliumTarget"), volumeName); 
         info.fPosition[2] = -1.0*plInfo->fParams[2]/2. + info.fParams[2]/2. + 0.002*CLHEP::mm; // 
       } 
       if (name.find("TargetUpstrUpstr") != std::string::npos) {
          const LBNEVolumePlacementData *plInfo = Find(name, G4String("TargetUpstrM0"), G4String("Create"));
          if (name == G4String("TargetUpstrUpstrPlate")) { // 
            info.fParams[0] = fTargetUpstrPlateHoleRadius; 
            info.fParams[1] = fTargetUpstrPlateOuterRadius; 
            info.fParams[2] = fTargetUpstrPlateThick ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial("Aluminum"), volumeName); 
            info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin + fTargetUpstrPlateThick/2.;
         } 
           if (name == G4String("TargetUpstrUpstrCan")) { // 
            info.fParams[0] = fTargetCanInnerRadius; 
            info.fParams[1] = fTargetCanOuterRadius; 
            info.fParams[2] = fTargetCanLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetCanMaterial), volumeName); 
            info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                        fTargetUpstrPlateThick + fTargetCanLength/2.0;
         } 
           if (name == G4String("TargetUpstrUpstrCanEndPlate")) { // 
            info.fParams[0] = fTargetCanInnerRadius; 
            info.fParams[1] = fTargetCanOuterRadius + 0.5*in; // ?? Guess  for the 1/2 inch extra. not critical 
            info.fParams[2] = fTargetDownstrCanFlangeThick;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetCanMaterial), volumeName); 
            info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                        fTargetUpstrPlateThick + fTargetCanLength + fTargetDownstrCanFlangeThick/2.;
//      ????? info.fParams[2] = fTargetUpstrUpstrMargin  + fTargetUpstrPlateThick + fTargetCanLength // For M0
//	                   + fTargetDownstrCanFlangeThick + fTargetFlangeThick;
         } 
           if (name == G4String("TargetUpstrUpstrDwstrFlange")) { // 
            info.fParams[0] = fTargetFlangeInnerRadius; 
            info.fParams[1] = fTargetFlangeOuterRadius; 
            info.fParams[2] = fTargetFlangeThick ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetFlangeMaterial), volumeName); 
            info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + fTargetCanLength + 
				 fTargetDownstrCanFlangeThick + fTargetFlangeThick/2.;
         }
           if (name == G4String("TargetUpstrUpstrCoolingTube")) { // 
            info.fParams[0] = 0.; // Solid tube of copper, we'll define water below 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetCTubeUpstrLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetCTubeMaterial), volumeName); 
	    // Leave the position blanks, as there will two copies (top and bottom.) 
	 }
           if (name == G4String("TargetUpstrUpstrCoolingTubeWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetCTubeUpstrLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
	    // Leave the position blanks, as there will two copies (top and bottom.) 
	 }
           if (name == G4String("TargetUpstrUpstrSupportBlockTopLeft")) { // 
            info.fParams[0] = 15.0*CLHEP::mm; // A block of presumed steel to support cooling tubes. +- 2 mm or so. 
            info.fParams[1] = 10.0*CLHEP::mm; 
            info.fParams[2] = 10.0*CLHEP::mm;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Steel316")), volumeName); 
	    info.fPosition[0] = -1.0;// Drawing 7589-00-00-00  Approximate. +- 1 mm 
	    info.fPosition[1] = 22.5;// Drawing 7589-00-00-00
	    info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + 140.*CLHEP::mm;// Drawing 7589-00-00-00
            info.fTypeName = G4String("Box");
	 }
           if (name == G4String("TargetUpstrUpstrSupportBlockBottomLeft")) { // 
            info.fParams[0] = 15.0*CLHEP::mm; // A block of presumed steel to support cooling tubes. +- 2 mm or so.  
            info.fParams[1] = 10.0*CLHEP::mm; 
            info.fParams[2] = 11.0*CLHEP::mm;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Steel316")), volumeName); 
	    info.fPosition[0] = -1.0*CLHEP::mm;// Drawing 7589-00-00-00
	    info.fPosition[1] = -20.0;// Drawing 7589-00-00-00
	    info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + 140*CLHEP::mm;// Drawing 7589-00-00-00
            info.fTypeName = G4String("Box");
	 }
           if (name == G4String("TargetUpstrUpstrSupportBlockRight")) { // 
            info.fParams[0] = 10.0*CLHEP::mm; // A block of presumed steel to support cooling tubes. +- 2 mm or so.  
            info.fParams[1] = 15.0*CLHEP::mm; 
            info.fParams[2] = 11.0*CLHEP::mm;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Steel316")), volumeName); 
	    info.fPosition[0] = 14.0;// Drawing 7589-00-00-00
	    info.fPosition[1] = 7.5;// Drawing 7589-00-00-00
	    info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + 140*CLHEP::mm;// Drawing 7589-00-00-00
            info.fTypeName = G4String("Box");
	 }
            if (name == G4String("TargetUpstrUpstrSupportRod")) { // 
            info.fParams[0] = 0.; // 
            info.fParams[1] = 12.*CLHEP::mm; // A bit oversize, the upstream part is thicker.. 
            info.fParams[2] = 163.0*CLHEP::mm; // Approx to ~ 3 mm. 
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.0*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Steel316")), volumeName); 
            // See special implementation due to multiple copies.  These rods will not be "surveyable"
	    
	 }
            if (name == G4String("TargetUpstrUpstrSupportSleeve")) { // 
            info.fParams[0] = fTargetFlangeInnerRadius - 0.2*CLHEP::mm; // 
            info.fParams[1] = info.fParams[0] + 3.0*CLHEP::mm; //  Approx to 500 microns
            info.fParams[2] = 72.0*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.0*CLHEP::degree );
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Steel316")), volumeName); 
	    info.fPosition[2] =-1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + fTargetCanLength 
				 + fTargetDownstrCanFlangeThick - info.fParams[2]/2. -0.1*CLHEP::mm;
	 }
          if (name == G4String("TargetUpstrUstrHorFin")) { // 
            info.fParams[0] = fTargetFinHeight; // X-Y inverted 
            info.fParams[1] = fTargetFinWidth; 
            info.fParams[2] = fTargetFinLength;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
	    info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + 140*CLHEP::mm;// Drawing 7589-00-00-00
            info.fTypeName = G4String("Box");
	 }
	 // The following are probably too detailed, and too far away from axis. 
	 // Will not be used for now... 
           if (name == G4String("TargetUpstrUpstrCoolingTubeBendSection")) { // 
            info.fParams[0] = 0.; // Solid block of copper, we'll define water below 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetCTubeUpstrRadCurve/fTargetCTubeUpstrNumSegCurve - 0.1*CLHEP::mm; // 100 microns to avoid overlaps
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetCTubeMaterial), volumeName); 
	    // Leave the position blanks, as there will two copies (top and bottom.) 
	 }
           if (name == G4String("TargetUpstrUpstrCoolingTubeBendSectionWater")) { // 
            info.fParams[0] = 0.; // Solid block of copper, we'll define water below 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetCTubeUpstrRadCurve/fTargetCTubeUpstrNumSegCurve - 0.1*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
	    // Leave the position blanks, as there will two copies (top and bottom.) 
	 }
           if (name == G4String("TargetUpstrUpstrCoolingTubeFlangeSection")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetFlangeThick;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(fTargetCTubeMaterial), volumeName); 
	    // Leave the position blanks, as there will be two copies (top and bottom.) 
	 }
           if (name == G4String("TargetUpstrUpstrCoolingTubeFlangeSectionWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetFlangeThick;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
	    // at (0.,0., 0.) 
	 }
	 
	 
                 
       } // UpstreamTargetAssembly as mother volume. 
       

       if (name.find("TargetUpstrDownstr") != std::string::npos) {
          if (name == G4String("TargetUpstrDownstrHeContainer")) { // 
            const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetUpstrM1"), G4String("Create"));
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness; 
            info.fParams[2] = plInfoM->fParams[2]- .005*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Beryllium")), volumeName); 
            info.fPosition[2] = 0.; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetUpstrDownstrHelium")) { // 
            const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetUpstrDownstrHeContainer"), G4String("Create"));
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetHeContTubeInnerRadius; 
            info.fParams[2] = plInfoM->fParams[2]- .005*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
            info.fPosition[2] = 0.; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetUpstrDownstrCoolingTube")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetFinLength+ 0.9*fTargetFinSpacingLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Titanium")), volumeName); 
            info.fPosition[2] = fTargetFinHeight + 0.005*CLHEP::mm; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetUpstrDownstrCoolingTubeWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetFinLength+ 0.9*fTargetFinSpacingLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
         }
          if ((name == G4String("TargetUpstrDownstrSegment")) || 
	      (name == G4String("TargetUpstrDownstrSegmentLeft")) ||
	      (name == G4String("TargetUpstrDownstrSegmentRight")) ) { //
	      
            info.fParams[0] = fTargetFinContainerWidth + 0.025*CLHEP::mm; 
            info.fParams[1] = fTargetFinHeight + 2.0*fTargetCTubeOuterRadius + 0.025*CLHEP::mm; 
	    // To avoid having to place bits of graphit in the corner, extend the segment upwards/downwards 
	    // to completely encompass the cooling tubes. 
	    if (fUse1p2MW) info.fParams[1] += 2.0*fTargetCTubeOuterRadius;
	    
            info.fParams[2] = fTargetFinLength+ 0.95*fTargetFinSpacingLength ;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
	    if (!fUse1p2MW) 
              info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
             else info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
            info.fTypeName = G4String("Box");
         }
          if (name == G4String("TargetUpstrDownstrCoolingTubeLast")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.025*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Titanium")), volumeName); 
            info.fPosition[2] = fTargetFinHeight + 0.005*CLHEP::mm; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetUpstrDownstrCoolingTubeLastWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.03*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
         }
          if ((name == G4String("TargetUpstrDownstrSegmentLast")) || 
	      (name == G4String("TargetUpstrDownstrSegmentLastLeft")) || 
	      (name == G4String("TargetUpstrDownstrSegmentLastRight"))) { // 
            info.fParams[0] = fTargetFinContainerWidth + 0.025*CLHEP::mm; 
            info.fParams[1] = fTargetFinHeight + 2.0*fTargetCTubeOuterRadius + 0.025*CLHEP::mm; 
 	    if (fUse1p2MW) info.fParams[1] += 2.0*fTargetCTubeOuterRadius;
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.01*CLHEP::mm;
	    
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
	    if (!fUse1p2MW) 
              info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
             else info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
             info.fTypeName = G4String("Box");
        }
	 // 
       } // Downstream of the flange of the target canister. 
    } // Upstream Stuff 
    // End of Obsolete splitted target. Kept for bacward compatibility and checks.   
    // Volume definition for the target that will fit into the polycone. August, Sept 2014. 
    // 
     // Cloned from the obsolete section above, with volumes renamed..  
     if (name.find("TargetNoSplit") != std::string::npos) {
       if (name == G4String("TargetNoSplitM1")) { // Complete container volume placed in the Horn1 and target whole
                                                // to contain the downstream part of the target, i.e., the Helium container tube. 
	 std::cerr << " Creating TargetNoSplitM1 ... " << std::endl;					 
         info.fParams[0] = 0.;
         info.fParams[1] = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness + 0.1*CLHEP::mm; // August 2014, tighter 
	 // December 16 2014: save this radius for Perfect Focusing studies. 
	 LBNERunManager *pRunManager=static_cast<LBNERunManager*> (LBNERunManager::GetRunManager());
         const LBNEDetectorConstruction *pDetTmp = 
	    static_cast<const LBNEDetectorConstruction*> (pRunManager->GetUserDetectorConstruction());
	 pDetTmp->SetRCoordOutOfTarget(fTargetHeContTubeInnerRadius); // mutable here, casting away constness... 
         info.fParams[2] = fTargetUpstrDwnstrMargin  +  fTargetDistFlangeToTargetStart + fTargetSLength + 0.01*CLHEP::mm;
         G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
         info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Air")), volumeName); 
         const LBNEVolumePlacementData *plInfo = Find(name, G4String("TargetUpstrMTop"), G4String("Create"));
         info.fPosition[2] = plInfo->fParams[2]/2. - info.fParams[2]/2. - 0.005*CLHEP::mm; // in TargetUpstrMTop To be checked, Aug - Sept 2014.  
	  // August 2014: the mother has to be the TargetHallAndHorn1, such that it can be instered in Horn1 
	 plInfo = Find(name, G4String("TargetHallAndHorn1"), G4String("Create"));
	 const LBNEVolumePlacementData *plInfoUp = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
//
// We attached the real part of the target the upstream segment .
// 
	   info.fPosition[2] = -plInfo->fParams[2]/2. + plInfoUp->fParams[2] + 1.0*CLHEP::cm + info.fParams[2]/2.; 
//	   if ((!fUseHorn1Polycone) && (!fUseHornsPolycone)) info.fPosition[2] += fTargetLengthOutsideExtra;
//
// P.L., L. F.,  Oct 7 2015:  	   
//  The above conditional move should be unconditional ... 
//  To keep consistency on the definition of the target position with v3r3p8, 
// where this move was done for the NuMI-stlye or the Polycone horn, 
// we now just have 

         info.fPosition[2] += fTargetLengthOutsideExtra;
	 std::cerr << " ...............Done Creating TargetNoSplitM1, length of mother " 
	           << plInfo->fParams[2] << " length here " << info.fParams[2] << std::endl
		   << " ...... Length of Graphite " << fTargetSLengthGraphite << " Zoffset  "  
	           << info.fPosition[2] << std::endl;					 
       } else {
          if (name == G4String("TargetNoSplitHeContainer")) { // 
            const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetNoSplitM1"), G4String("Create"));
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness; 
            info.fParams[2] = plInfoM->fParams[2]- .005*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Beryllium")), volumeName); 
            info.fPosition[2] = 0.; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetNoSplitHelium")) { // 
            const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetNoSplitHeContainer"), G4String("Create"));
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetHeContTubeInnerRadius; 
            info.fParams[2] = plInfoM->fParams[2]- .005*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
	    std::cerr << " Length of TargetNoSplitHelium " << info.fParams[2] << std::endl;
            info.fPosition[2] = 0.; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetNoSplitCoolingTube")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetFinLength+ 0.9*fTargetFinSpacingLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Titanium")), volumeName); 
            info.fPosition[2] = fTargetFinHeight + 0.005*CLHEP::mm; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetNoSplitCoolingTubeWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetFinLength+ 0.9*fTargetFinSpacingLength ;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
         }
          if ((name == G4String("TargetNoSplitSegment")) || 
	      (name == G4String("TargetNoSplitSegmentLeft")) ||
	      (name == G4String("TargetNoSplitSegmentRight")) ) { //
	      
            info.fParams[0] = fTargetFinContainerWidth + 0.025*CLHEP::mm; 
            info.fParams[1] = fTargetFinHeight + 2.0*fTargetCTubeOuterRadius + 0.025*CLHEP::mm; 
	    // To avoid having to place bits of graphit in the corner, extend the segment upwards/downwards 
	    // to completely encompass the cooling tubes. 
	    if (fUse1p2MW) info.fParams[1] += 2.0*fTargetCTubeOuterRadius;
	    
            info.fParams[2] = fTargetFinLength+ 0.95*fTargetFinSpacingLength ;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
	    if (!fUse1p2MW) 
              info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
             else info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
            info.fTypeName = G4String("Box");
         }
          if (name == G4String("TargetNoSplitCoolingTubeLast")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeOuterRadius; 
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.025*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Titanium")), volumeName); 
            info.fPosition[2] = fTargetFinHeight + 0.005*CLHEP::mm; // possibly tweak for the margin... 
         }
          if (name == G4String("TargetNoSplitCoolingTubeLastWater")) { // 
            info.fParams[0] = 0.; 
            info.fParams[1] = fTargetCTubeInnerRadius; 
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.03*CLHEP::mm;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
         }
          if ((name == G4String("TargetNoSplitSegmentLast")) || 
	      (name == G4String("TargetNoSplitSegmentLastLeft")) || 
	      (name == G4String("TargetNoSplitSegmentLastRight"))) { // 
            info.fParams[0] = fTargetFinContainerWidth + 0.025*CLHEP::mm; 
            info.fParams[1] = fTargetFinHeight + 2.0*fTargetCTubeOuterRadius + 0.025*CLHEP::mm; 
 	    if (fUse1p2MW) info.fParams[1] += 2.0*fTargetCTubeOuterRadius;
            info.fParams[2] = fTargetFinLengthSplitUpstr - 0.01*CLHEP::mm;
	    
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
	    if (!fUse1p2MW) 
              info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
             else info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
             info.fTypeName = G4String("Box");
        }
	 //
       } 
      } // Downstream of the flange of the target canister. 
    } // Upstream Stuff 
//    
// December 2013 : Reverting back to a much simple target. 
       
       if (name == G4String("TargetSimpleUpstrTube")) { // 
          info.fParams[0] = 0.; 
          info.fParams[1] = fSimpleTargetRadius; 
	  info.fParams[2] = fTargetSLength; // To be recommissioned, August 28 2014. 
          G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
          info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Target")), volumeName); 
          // at (0.,0., 0.) 
       }
       if (name == G4String("TargetSimpleUpstrBox")) { // 
          info.fParams[0] = fSimpleTargetWidth; 
          info.fParams[1] = fSimpleTargetHeight; 
	  info.fParams[2] = fTargetSLength;
          G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
          info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
          // at (0.,0., 0.) 
       }

       if (name == G4String("TargetSimpleDownstrTube")) { //  Obsolete.. 
          info.fParams[0] = 0.; 
          info.fParams[1] = fSimpleTargetRadius; 
          info.fParams[2] = fTargetSLengthGraphite - fTargetLengthOutsideHorn ;
          G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
          info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Target")), volumeName); 
          // at (0.,0., 0.) 
       }
       if (name == G4String("TargetSimpleDownstrBox")) { // 
          info.fParams[0] = fSimpleTargetWidth; 
          info.fParams[1] = fSimpleTargetHeight; 
          info.fParams[2] = fTargetSLengthGraphite - fTargetLengthOutsideHorn ;
          G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2.);
          info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
          // at (0.,0., 0.) 
       }

//Quynh added for multisphere.
       if (name == G4String("TargetMultiSphere")) { 
          info.fParams[0] = 0.; 
          info.fParams[1] = fMultiSphereTargetRadius; 
          info.fParams[2] = 0. ; 
          G4Sphere* aSphere = new G4Sphere(volumeName,
                                         info.fParams[0],//inner radius = 0
                                         info.fParams[1],//outer radius
                                         0.,360.*CLHEP::degree,0.,180.*CLHEP::degree); //Phi and Theta angles
          info.fCurrent = new G4LogicalVolume(aSphere, G4Material::GetMaterial(std::string("Target")), volumeName); 
          
       }
     
    //
    // Bits and pieces for the target itself. 
    //
    if (name == G4String("TargetFinVert")) { // 
            info.fParams[0] = fTargetFinWidth; 
            info.fParams[1] = fTargetFinHeight - 2.0*fTargetCTubeOuterRadius - 0.05*CLHEP::mm; 
            info.fParams[2] = fTargetFinLength ;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
            info.fTypeName = G4String("Box");
    }
    if (name == G4String("TargetFinVertExtra")) { //  Extra pieces to be place left and right of the center
             // of the cooling tube. This extends the target transversely 
	    if (fTargetFinExtraWidth < 0.) {
                  std::ostringstream mStrStr;
                  mStrStr << " Volume named " << name << " can not be handled, negative dimension  ";
                  G4String mStr(mStrStr.str());
                  G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
	    }                   
            info.fParams[0] = fTargetFinExtraWidth; 
            info.fParams[1] = fTargetFinHeight - 0.05*CLHEP::mm; 
            info.fParams[2] = fTargetFinLength ;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
            info.fTypeName = G4String("Box");
    }
    if (name == G4String("TargetFinVertHeliumSide")) { //  The fin container is made of graphite, or whatever target material  
       // if so, we must replace some of back with helium, if the nominal target Fin width is smaller 
       // then the cooling tube radius, which sets the size of the container. 
            const double widthHe = 2.0*fTargetCTubeOuterRadius - fTargetFinWidthRequired - 0.005*CLHEP::mm; 
	    if (widthHe < 0.) {
                  std::ostringstream mStrStr;
                  mStrStr << " Volume named " << name << " can not be handled, negative dimension  ";
                  G4String mStr(mStrStr.str());
                  G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
	    }                   
            info.fParams[0] = widthHe; 
            info.fParams[1] = fTargetFinHeight - 0.07*CLHEP::mm; 
            info.fParams[2] = fTargetFinLength ;
	    // exclude the round corners.. They will already filled with Helium  This is getting out of hand!.. 
	    
	    if (fUseRoundedTargetFins) {
	       info.fParams[2] -= 2.0*fTargetFinWidthRequired - 0.001*CLHEP::mm;
	    }
 	    std::cerr << " Creating TargetFinVertHeliumSide, width .." << widthHe 
	              << " heigth " <<  info.fParams[1] << std::endl;
           G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
            info.fTypeName = G4String("Box");
    }
    if (name.find("TargetFinVertHeliumRounded") != std::string::npos) { //  We carve out the rounded edges
       // We create 4 distinct volumes, because their orientation differ. 
       // all of them carved out the same box 
       info.fParams[0] = fTargetFinContainerWidth - 0.005*CLHEP::mm; 
       info.fParams[1] = fTargetFinWidthRequired;
       info.fParams[2] = fTargetFinHeight - 0.010*CLHEP::mm; // Along z. We'll rotate once the volume subtraction is done.
       std::string aCuboidName(name); aCuboidName += std::string("_Cuboid");
        // Not really a cuboid, only if 12 mm target width       
       G4Box* aCuboid = new G4Box(aCuboidName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
       info.fParams.resize(10);
       info.fParams[3] = 0.; 
       info.fParams[4] = fTargetFinWidthRequired; 
       info.fParams[5] = fTargetFinHeight - 0.0076*CLHEP::mm;
       if (name == G4String("TargetFinVertHeliumRoundedDownstrLeft")) {
         info.fParams[6] = 90.*CLHEP::degree;
	 info.fParams[7] = 180.*CLHEP::degree;
	 info.fParams[8] = info.fParams[0]/2.;
	 info.fParams[9] = -info.fParams[1]/2.;
       } else  if (name == G4String("TargetFinVertHeliumRoundedDownstrRight")) {
         info.fParams[6] = 0.*CLHEP::degree;
	 info.fParams[7] = 90.*CLHEP::degree;
	 info.fParams[8] = -info.fParams[0]/2.;
	 info.fParams[9] = -info.fParams[1]/2.;
       } else if (name == G4String("TargetFinVertHeliumRoundedUpstrRight")) {
         info.fParams[6] = 270.*CLHEP::degree;
	 info.fParams[7] = 360.*CLHEP::degree;
	 info.fParams[8] = -info.fParams[0]/2.;
	 info.fParams[9] = info.fParams[1]/2.;
       } else  if (name == G4String("TargetFinVertHeliumRoundedUpstrLeft")) {
         info.fParams[6] = 180.*CLHEP::degree;
	 info.fParams[7] = 270.*CLHEP::degree;
	 info.fParams[8] = info.fParams[0]/2.;
	 info.fParams[9] = info.fParams[1]/2.;
       }
       std::string aRodName(name); aRodName += std::string("_Rod");       
       G4Tubs* aRod = new G4Tubs(aRodName, info.fParams[3], info.fParams[4], info.fParams[5]/2., info.fParams[6], info.fParams[7]);
       G4ThreeVector xyTrans(info.fParams[8], info.fParams[9], 0.); 
       G4RotationMatrix Ra;
       G4Transform3D transform(Ra, xyTrans);  
       G4SubtractionSolid* subtraction = new G4SubtractionSolid("Box-Cylinder", aCuboid, aRod, transform);
       info.fCurrent = new G4LogicalVolume(subtraction, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
       info.fTypeName = G4String("Subtration");
       std::cerr << " Target Rounded Corners, defined for name " << name 
                 << " Params ";
       for (int k=0; k != 10; k++) std::cerr << " " << info.fParams[k];
       std::cerr << std::endl; 
    }
    
    if (name == G4String("TargetFinVertLastHeliumSide")) { //  The fin container might be made of graphite. 
       // if so, we must replace some of back with helium, if the nominal target Fin width is smaller 
       // then the cooling tube radius, which sets the size of the container. 
            const double widthHe = 2.0*fTargetCTubeOuterRadius - fTargetFinWidth - 0.005*CLHEP::mm; 
	    if (widthHe < 0.) {
                  std::ostringstream mStrStr;
                  mStrStr << " Volume named " << name << " can not be handled, negative dimension  ";
                  G4String mStr(mStrStr.str());
                  G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
	    }                   
            info.fParams[0] = widthHe; 
            info.fParams[1] = fTargetFinHeight - 0.07*CLHEP::mm; 
            info.fParams[2] =  fTargetFinLengthSplitUpstr  - 0.02*CLHEP::mm;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("HeliumTarget")), volumeName); 
            info.fTypeName = G4String("Box");
    }
    if (name == G4String("TargetFinHorizontal")) { // only one of those, fixed size  
            info.fParams[0] = 62.0*CLHEP::mm; 
            info.fParams[1] = fTargetFinWidth; 
            info.fParams[2] = fTargetFinLength ;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
	    info.fPosition[0] = -info.fParams[0]/2. + 10.0*CLHEP::mm; // Russian drawing 7589-00-00 
            const LBNEVolumePlacementData *plInfo = Find(name, G4String("TargetUpstrM0"), G4String("Create"));
	    info.fPosition[0] = -info.fParams[0]/2.0 + 10.*CLHEP::mm;
	    info.fPosition[1] = 0.;
	    info.fPosition[2] = -1.0*plInfo->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + fTargetCanLength + fTargetDownstrCanFlangeThick
				 - 100.0*CLHEP::mm; // Russian drawing 7589-00-00 
            info.fTypeName = G4String("Box");
      } 
    if (name == G4String("TargetFinVertUpstrLast")) { // 
            info.fParams[0] = fTargetFinWidth; 
            info.fParams[1] = fTargetFinHeight - 2.0*fTargetCTubeOuterRadius - 0.1*CLHEP::mm; 
            info.fParams[2] = fTargetFinLengthSplitUpstr  - 0.02*CLHEP::mm; // 20 micron safety. 
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName);
            info.fTypeName = G4String("Box");
 // despite being a single copy, do the placement in a separate method. 	    
    }
    if (name == G4String("TargetFinVertExtraLast")) { //  Extra pieces to be place left and right of the center
             // of the cooling tube. This extends the target transversely 
	    if (fTargetFinExtraWidth < 0.) {
                  std::ostringstream mStrStr;
                  mStrStr << " Volume named " << name << " can not be handled, negative dimension  ";
                  G4String mStr(mStrStr.str());
                  G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
	    }                   
            info.fParams[0] = fTargetFinExtraWidth; 
            info.fParams[1] = fTargetFinHeight - 2.0*fTargetCTubeOuterRadius - 0.05*CLHEP::mm; 
            info.fParams[2] = fTargetFinLengthSplitUpstr  - 0.02*CLHEP::mm; // 20 micron safety.;
            G4Box* aBox = new G4Box(volumeName, info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
            info.fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial(std::string("Target")), volumeName); 
            info.fTypeName = G4String("Box");
     }	    
    // Adding 4 new small volumes to include the bit of graphite locate on each of the 4 corner 
    // Oct. 29 2013: 
    // Adding them as well for the last target segment, of the upstream section which has a different length. 
    // Perhaps an overzealous choice: there will also be 4 more such volumes for the first 
    // target segments of the downstream part of the target. 
     if (name.find("TargetFinVertCorner") != std::string::npos) { // 
       if (fUse1p2MW) {
//              this->declareTargetFinVertCorners(info); // simply to avoid too much code in this method. See LBNEVolumePlacementsAdd.cc
// Obsolete.. 
       } else { 
         info.fParams.resize(7);
         info.fParams[0] = fTargetFinContainerWidth/2. - 0.025*CLHEP::mm; 
         info.fParams[1] = fTargetCTubeOuterRadius - 0.025*CLHEP::mm; 
         info.fParams[2] = fTargetFinLength ;
 	 info.fParams[3] = 0.;
	 info.fParams[4] = fTargetCTubeOuterRadius + 0.250*CLHEP::mm;
	 info.fParams[5] = fTargetFinLength ;
	 if (name.find("TargetFinVertCornerUpstrLast") != std::string::npos) {
	   info.fParams[2] = fTargetFinLengthSplitUpstr  - 0.02*CLHEP::mm;
	   info.fParams[5] = fTargetFinLengthSplitUpstr  - 0.02*CLHEP::mm;
	 }
	 if (name.find("TargetFinVertCornerDownstrFirst") != std::string::npos) {
	   info.fParams[2] = fTargetFinLengthSplitDwnstr  - 0.02*CLHEP::mm;
	   info.fParams[5] = fTargetFinLengthSplitDwnstr  - 0.02*CLHEP::mm;
	 }
         G4Box* aBox = new G4Box(volumeName+G4String("Box"), 
	 			 info.fParams[0]/2, info.fParams[1]/2, info.fParams[2]/2.);
	 
	 G4Tubs* aTubs = new G4Tubs(volumeName+G4String("Tube"), 
	                            info.fParams[3], info.fParams[4], info.fParams[5]/2., 0., 360.0*CLHEP::degree);
	 G4ThreeVector trans(0., 0., 0.);
        if (name.find("UpLeft") != std::string::npos) { //
	    trans[0] = fTargetFinContainerWidth/4.; trans[1] = fTargetCTubeOuterRadius/2.; 
	} else if (name.find("UpRight") != std::string::npos) { 
	    trans[0] = -fTargetFinContainerWidth/4.; trans[1] = fTargetCTubeOuterRadius/2.;
	} else if (name.find("DwnLeft") != std::string::npos) {
	    trans[0] = fTargetFinContainerWidth/4.; trans[1] = -fTargetCTubeOuterRadius/2.;
	} else if (name.find("DwnRight") != std::string::npos) {
	    trans[0] = -fTargetFinContainerWidth/4.; trans[1] = -fTargetCTubeOuterRadius/2.;
	}    
	G4RotationMatrix unitMatrix;			
        G4Transform3D transform(unitMatrix, trans);
	G4SubtractionSolid *aRoundCorner = new G4SubtractionSolid(volumeName, aBox, aTubs, transform);  		
        info.fCurrent = new G4LogicalVolume(aRoundCorner, G4Material::GetMaterial(std::string("Target")), volumeName); 
        info.fTypeName = G4String("G4SubtractionSolid");
      }
    }  
    
    if (name == G4String("TargetAlignmentRingRight")) { // 
	    info.fParams.resize(5);
            info.fParams[0] = fTargetAlignRingInnerRadius;
            info.fParams[1] = fTargetAlignRingOuterRadius; 
            info.fParams[2] = fTargetAlignRingThick;
	    info.fParams[3] = M_PI/2. + fTargetAlignRingCutAngle ;
	    info.fParams[4] = M_PI - 2.0*fTargetAlignRingCutAngle ;
	    G4Tubs* aTubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
				 info.fParams[3],   info.fParams[4]  );
            info.fCurrent =
	      new G4LogicalVolume(aTubs, G4Material::GetMaterial(fTargetAlignRingMaterial), volumeName);

//	    std::cerr << " Created TargetAlignmentRingRight, Params " ;
//	    for (size_t k=0; k!=5; k++) std::cerr << " " <<  info.fParams[k] << " / ";
//	    std::cerr << " dphi " << aTubs->GetDeltaPhiAngle();
//	    std::cerr << std::endl;
 // Multiple copies.. 
 	    
    }
    if (name == G4String("TargetAlignmentRingLeft")) { // 
	    info.fParams.resize(5);
            info.fParams[0] = fTargetAlignRingInnerRadius;
            info.fParams[1] = fTargetAlignRingOuterRadius; 
            info.fParams[2] = fTargetAlignRingThick;
	    info.fParams[3] = -M_PI/2. +  fTargetAlignRingCutAngle + 2.0*M_PI;
	    info.fParams[4] = M_PI - 2.0*fTargetAlignRingCutAngle ;
            G4Tubs* aTubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
	                           info.fParams[3],   info.fParams[4]  );
            info.fCurrent = 
	     new G4LogicalVolume(aTubs, G4Material::GetMaterial(fTargetAlignRingMaterial), volumeName);
 // Multiple copies.. 
//	    std::cerr << " Created TargetAlignmentRingLeft, Params " ;
//	    for (size_t k=0; k!=5; k++) std::cerr << " " <<  info.fParams[k] << " / ";
//	    std::cerr << " dphi " << aTubs->GetDeltaPhiAngle();
//	    std::cerr << std::endl;
 	    
    } 
  // Things that are physically part of Horn1, but placed in at the dowstream end of the target volume 
  // These volumes are surrounding the target.
     if (name == G4String("UpstrHorn1TransInnerOuterCont")) { // Approximate!... 
            info.fParams[0] = fTargetHorn1InnerRadsDownstr[0] - 0.2*CLHEP::mm;
            info.fParams[1] = fTargetHorn1InnerRadsDownstr[4] + fTargetHorn1TransThick[4] + 1.*CLHEP::mm; 
            info.fParams[2] = fTargetHorn1Lengths[0] +  fTargetHorn1Lengths[1] + fTargetHorn1Lengths[2] + 0.05*CLHEP::mm;; // few mm safety... 
            G4Tubs* aTubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
	                           0., 360.0*CLHEP::degree);
            info.fCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(std::string("Air")), volumeName);
	    // Pick volume Upstream TargetAssembly. 
            const LBNEVolumePlacementData *plInfo = Find(name, G4String("Horn1PolyM1"), G4String("Create"));
	   // Correction for Pseudo Nova: Shifted by the extra space we now have between the downstream end of the target. 
	   // The mother volume for this container volume will be Horn1Hall, and it will be placed a bit  upstream 
	   // of the Horn1TopLevelUpstr
	    double lHorn1Real = -10000.0*CLHEP::m;
            for (std::vector<double>::const_iterator il = fMotherHorn1AllLengths.begin(); il != fMotherHorn1AllLengths.end(); il++) 
                if (*il > lHorn1Real  ) lHorn1Real = *il;
	    info.fPosition[2] = -lHorn1Real/2. + info.fParams[2]/2; 
	    std::cerr << " Setting position for UpstrHorn1TransInnerOuterCont, length " << info.fParams[2] 
	              << " length mother " << plInfo->fParams[2] << " zRel " << info.fPosition[2] << std::endl;
       }
       if (name.find("UpstrHorn1TransInnerOuterPart") != std::string::npos) { // Still even more approximate!... 
            size_t nameIndex= name.find("UpstrHorn1TransInnerOuterPart") + name.length() - 1;
	    std::string nameIndexStr(name.substr(nameIndex));
	    std::istringstream nameIndexStrStr(nameIndexStr); 
	    int iPart = -1;
	    nameIndexStrStr >> iPart;
//	    std::cerr << " from LBNEVolumePlacements::Create, for " << name << " Part number " << iPart << std::endl;
	    if (iPart != 2) {
	      info.fParams.resize(5); 
              info.fParams[0] = fTargetHorn1InnerRadsUpstr[iPart];
              info.fParams[2] = fTargetHorn1InnerRadsDownstr[iPart]; 
              info.fParams[1] = fTargetHorn1InnerRadsUpstr[iPart] + fTargetHorn1TransThick[iPart];
              info.fParams[3] = fTargetHorn1InnerRadsDownstr[iPart] + fTargetHorn1TransThick[iPart];
	      info.fParams[4] = fTargetHorn1Lengths[iPart];
              G4Cons* aCons = new G4Cons(volumeName, info.fParams[0], info.fParams[1], info.fParams[2], info.fParams[3],
	                              info.fParams[4]/2., 0., 360.0*CLHEP::degree);
              info.fCurrent = new G4LogicalVolume(aCons, G4Material::GetMaterial(fHorn1AllCondMat), volumeName);
              info.fTypeName = G4String("Cons");
	    } else { 
              info.fParams[0] = fTargetHorn1InnerRadsUpstr[iPart];
              info.fParams[1] = fTargetHorn1InnerRadsUpstr[iPart] + fTargetHorn1TransThick[iPart];
	      info.fParams[2] = fTargetHorn1Lengths[iPart];
              G4Tubs* aTubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
	                           0., 360.0*CLHEP::degree);
				   // Use Aluminum !  P.L. July 2014... 
              info.fCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1AllCondMat), volumeName);
	    }
	    info.fPosition[0] = 0.; info.fPosition[1] = 0.; 
	    info.fPosition[2] = fTargetHorn1ZPositions[iPart];
      }
  //
  // Beginning of Horn1 declarations 
  //
  if (name.find("Horn1") == 0) { 
    if (name == G4String("Horn1Hall")) {
      const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetHallAndHorn1"), G4String("Create"));
       const LBNEVolumePlacementData *plInfoC = Find(name, G4String("UpstreamTargetAssembly"), G4String("Create"));
       info.fParams[0] = 64.0*in - 1.0*CLHEP::cm;
       info.fParams[1] = 50.0*in - 1.0*CLHEP::cm;
       info.fParams[2] = fHorn1Length + fHorn1DownstreamPlateLength;
       G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
       info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName); 
       info.fPosition[2] = -1.0*plInfoM->fParams[2]/2. + plInfoC->fParams[2] + info.fParams[2]/2. + 0.020*CLHEP::mm;
    }  
    // Note: to optimize the geometry, we place the downstream end of the target into the 
    // horn1. Target is not a typo, nor misplaced in the information flow. 
    
    if (name == G4String("Horn1TargetCoolingTubeDwnRetTit")) { // 
	    info.fParams.resize(5);
            info.fParams[0] = fTargetCTubeReturnDownstrRadInner; 
            info.fParams[1] = fTargetCTubeReturnDownstrRadOuter; 
            info.fParams[2] = fTargetCTubeReturnDownstrThickTitanium/2.;
	    info.fParams[3] = M_PI/2. + fTargetCTubeReturnDownstrCutAngleStart;
	    info.fParams[4] = fTargetCTubeReturnDownstrCutAngleSize;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
	       info.fParams[3], info.fParams[4]);
             info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Titanium")), volumeName); 
	   // Two copies will be placed explictely  
//	   std::cerr << " Params for " << name << std::endl;
//	   for (size_t k=0; k != info.fParams.size(); k++) {
//	     std::cerr << " k " << k << " " << info.fParams[k] << std::endl; 
//	   }
    }
    if (name == G4String("Horn1TargetCoolingTubeDwnRetWater")) { //
           // 
	   // August 2014... This has been cloned from the 700 MW. target. we need to review these parameters 
	   // for the 1.2 MW target. 
	   //
            const LBNEVolumePlacementData *plInfoM;
            plInfoM = Find(name, G4String("TargetNoSplitHelium"), G4String("Create"));
	    info.fParams.resize(5);
            info.fParams[0] = fTargetCTubeReturnDownstrRadInner; 
            info.fParams[1] = fTargetCTubeReturnDownstrRadOuter; 
            info.fParams[2] = fTargetCTubeReturnDownstrThickWater;
	    info.fParams[3] = M_PI/2. + fTargetCTubeReturnDownstrCutAngleStart;
	    info.fParams[4] = fTargetCTubeReturnDownstrCutAngleSize;
            G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2.,
	       info.fParams[3], info.fParams[4]);
           info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Water")), volumeName); 
           info.fPosition[0] = 0.; info.fPosition[1] = 0.;
           info.fPosition[2] = 	plInfoM->fParams[2]/2 - 
	       fTargetCTubeReturnDownstrThickWater/2. - fTargetCTubeReturnDownstrThickTitanium/2 - 1.5*CLHEP::mm  ; // leave room for the cap. 
	   if (fUsePseudoNova) info.fPosition[2] += 0.1*CLHEP::mm; // A bit too tight is the target is not split
//	   std::cerr << " Params for " << name << std::endl;
//	   for (size_t k=0; k != info.fParams.size(); k++) {
//	     std::cerr << " k " << k << " " << info.fParams[k] << std::endl; 
//	   }
     }
     if (name == G4String("Horn1TargetDownstrHeContainerCap")) { // 
            const LBNEVolumePlacementData *plInfoM;
            plInfoM = Find(name, G4String("TargetNoSplitHelium"), G4String("Create"));
           info.fParams[0] = 0.; 
           info.fParams[1] = fTargetHeContTubeInnerRadius-0.010*CLHEP::mm; 
           info.fParams[2] = fTargetBerylDownstrWindowThick; // Nominal value 
           G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
           info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Beryllium")), volumeName); 
	   info.fPosition[2] = plInfoM->fParams[2]/2 - info.fParams[2] - 0.5*CLHEP::mm;
     }
    //
    // End of target related stuff. 
    //
    if (name.find("Horn1IOTrans") == 0) { 
      if (name ==  G4String("Horn1IOTransCont")) {  // Transition Inner to Outer Drawing 8875-112-MD-363097
        // This is the part downstream of Z=0., MCZERO (not ACTRN1) 
        const LBNEVolumePlacementData *plInfoM = Find(name, G4String("Horn1PolyM1"), G4String("Create")); 
       info.fParams[0] = fHorn1IOTransInnerRad;
       info.fParams[1] = fHorn1IOTransOuterRad;        
       info.fParams[2] = fHorn1IOTransLength;
       std::cerr << " Params for " << name << " " << info.fParams[0] << " / " << info.fParams[1] 
		 << " / " << info.fParams[2] << "  mother length " << plInfoM->fParams[2] << std::endl;
       G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
       info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Air")), volumeName); 
       info.fPosition[2] = -plInfoM->fParams[2]/2. + info.fParams[2]/2. + 0.025*CLHEP::mm; 
       info.fPosition[2] += (2.436*in - 30*CLHEP::mm)*fHorn1LongRescale; //  Worked on, for Horn1PolyM1 August 22 2014.
       std::cerr << " Checking long offset for " << name << " length of mother " 
                 << plInfoM->fParams[2] << " offset " << info.fPosition[2] << std::endl;
      }
      
    } // End of the Inner Outer Transition for Horn1 
    
    if (name ==  G4String("Horn1TopLevelUpstr")) {  // A container for the section of inner and outer conductors enveloping 
                                                   // the target.  Obsolete!!! Aug-Sept 2014. 
        const LBNEVolumePlacementData *plInfoM = Find(name, G4String("Horn1Hall"), G4String("Create"));
//        info.fParams[0] = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness + 1.5*CLHEP::mm;
// Above is not optimum if misalignment Use the equation set
// Improved a bit on March 25 2014. See below.. 
//  
        const double zMaxDC = fHorn1TopUpstrLength - 3.0*CLHEP::cm;
//	const size_t iEqn = (zMaxDC < (21.0888*in*fHorn1LongRescale)) ? 0 : 1;
// Better model with smooth change in equation number, as in the PlaceFinalHorn1. 
//
        const double rMinEqn1 = fHorn1Equations[0].GetVal(zMaxDC); // Equation 1 or 0
        const double rMinEqn2 = fHorn1Equations[1].GetVal(zMaxDC); // Equation 1 or 0
	const double ratio0vs1 = std::min(1.0, (zMaxDC/(21.0888*in*fHorn1LongRescale)));
        const double rMin = rMinEqn2*ratio0vs1 + (1.0-ratio0vs1)*rMinEqn1;
//	info.fParams[0] = rMin - 0.5*CLHEP::mm;
// Too loose !... Leave only 5 microns We get wrong overlap at 2.9 mm offset. 
	info.fParams[0] = rMin - 0.005*CLHEP::mm;
	if (fUsePseudoNova) info.fParams[0] = 1.0*CLHEP::mm;  // The above equation are not applicable if zMaxDC  ~ fHorn1TopUpstrLength
	const double rMaxOut = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness +0.050*CLHEP::mm;
	
	if ((!fUsePseudoNova) && (info.fParams[0] <  rMaxOut) && (!fHorn1RadiusBigEnough)) {
	  std::ostringstream mStrStr; 
	  mStrStr << " Can't create Horn1TopLevelUpstr, radial clash between Horn1 inner conductor, rMin " <<  
	    info.fParams[0] << " Target Helium tube + min. safety = " << rMaxOut << std::endl;
	  mStrStr << " Length into horn " << fTargetLengthIntoHorn  << std::endl;
	  G4String mStr(mStrStr.str());
          G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
	} else {
	  std::cerr << " Successfull (preliminary check) of targetinto with max Z = " << zMaxDC 
	   << " (Drawing CS), rMin = " << rMin << std::endl;
	}
        info.fParams[1] = fHorn1TopUpstrOuterRad + 3.0*in;  // room for the flanges.        
        info.fParams[2] = fHorn1TopUpstrLength - 0.010*CLHEP::mm;
//        std::cerr << " Params for " << name << " " << info.fParams[0] << " / " << info.fParams[1] 
//		 << " / " << info.fParams[2] << std::endl;
//        std::cerr << "  .... into Horn1Hall " << plInfoM->fParams[0] << " / " << plInfoM->fParams[1] 
//		 << " / " << plInfoM->fParams[2] << std::endl;
        G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
        info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Air")), volumeName); 
        info.fPosition[2] = -plInfoM->fParams[2]/2. + info.fParams[2]/2. + 0.005*CLHEP::mm;
	// Correction for Pseudo Nova: Shift by the extra space we now have between the downstream end of the target. 
	// and  Horn1.
	info.fPosition[2] += fTargetLengthOutsideExtra;
       // This is a surveyed volume, tweaked in PlaceFinal  
     }
    if (name ==  G4String("Horn1TopLevelDownstr")) {  // A container for the section of inner and outer conductors 
                                                      // downstream of the target 
        const LBNEVolumePlacementData *plInfoM = Find(name, G4String("Horn1Hall"), G4String("Create"));
        const LBNEVolumePlacementData *plInfoC =  Find(name, G4String("Horn1PolyM1"), G4String("Create")); 
       info.fParams[0] = 0.;
       info.fParams[1] = fHorn1TopDownstrOuterRad + 15.0*in;  // big stuff at the downstream end..       
       info.fParams[2] = fHorn1TopDownstrLength +  0.010*CLHEP::mm;
       G4Tubs* aTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.*CLHEP::degree);
       info.fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial(std::string("Air")), volumeName); 
       info.fPosition[2] = -plInfoM->fParams[2]/2. + plInfoC->fParams[2] + info.fParams[2]/2. + 0.005*CLHEP::mm;
       // This is a surveyed volume, tweaked in PlaceFinal  
	// Correction for Pseudo Nova: Shift by the extra space we now have between the downstream end of the target. 
	// and  Horn1.
	// Correction for the NoSplit configuration: Offsets to be checked, August 22 2014 
	info.fPosition[2] += fTargetLengthOutsideExtra;
	double rMin = 1.0e10*CLHEP::mm;
	for (std::vector<double>::const_iterator il=fMotherHorn1AllRads.begin(); il != fMotherHorn1AllRads.end(); il++) 
	   if (*il < rMin) rMin = *il;
	info.fParams[0] = rMin - 0.010*CLHEP::mm ;
	const double z30p150 = fHorn1LongRescale*30.150*in; //Equation change.  We will put the split Upst/Downstr here. 
        const double zOffsetDrawingUpstrEdge = 5.752*in*fHorn1LongRescale; // Drawing 363097 
	info.fPosition[2] = - plInfoC->fParams[2]/2. + zOffsetDrawingUpstrEdge + z30p150 + fHorn1TopDownstrLength/2. + 0.005*CLHEP::mm; 
	// see PlaceFinalNoSplitHorn1.. 
	std::cerr << " Checking long offset for " << name 
		  << " length of mother " << plInfoC->fParams[2] << " length " << info.fParams[2] << " ZPos " 
		  << info.fPosition[2] << std::endl;
     }

     if (name ==  G4String("Horn1PolyM1")) {  // A container for the section of inner and outer conductors 
                                                      // downstream of the target 
        const LBNEVolumePlacementData *plInfoM = Find(name, G4String("TargetHallAndHorn1"), G4String("Create"));
	std::cerr << " Horn1Poly Placement, mother info , long. info, length  " 
                  << plInfoM->fParams[2] << " at Z = " << plInfoM->fPosition[2] << std::endl;
        info.fParams[0] = 0.; info.fParams[1] = 0.; 
	info.fParams[2] = -100000.*CLHEP::m; 
	for (std::vector<double>::const_iterator il = fMotherHorn1AllLengths.begin(); il != fMotherHorn1AllLengths.end(); il++) 
	    if (*il > info.fParams[2]) info.fParams[2] = *il;
	//
	// We shift the Z position by half the length, such the relative definition is the same as for the other volumes. 
	std::vector<double> zz(fMotherHorn1AllLengths);
	for (std::vector<double>::iterator il = zz.begin(); il != zz.end(); il++) *il -= info.fParams[2]/2.;
//	std::cerr << " Dump of Horn1 Polycon, mother,  parameters.. Ih Z r  " << std::endl; 
//	for (size_t k=0; k!= zz.size(); k++) 
//	  std::cerr << "        " << k << "    " << zz[k] << "     " << fMotherHorn1AllRads[k] << std::endl;
        G4GenericPolycone* aPCon = new G4GenericPolycone(volumeName, 0.*CLHEP::degree, 360.0*CLHEP::degree, static_cast<int>(fMotherHorn1AllLengths.size()), 
	                                    &fMotherHorn1AllRads[0], &zz[0]);
	if (fFillHornsWithArgon) {  				    
           info.fCurrent = new G4LogicalVolume(aPCon, G4Material::GetMaterial(std::string("Argon")), volumeName); 
	} else {
           info.fCurrent = new G4LogicalVolume(aPCon, G4Material::GetMaterial(std::string("Air")), volumeName); 
	}
	const double lastCorrOnHorn1 = 9.99*CLHEP::mm*fHorn1LongRescale;
        info.fPosition[2] = plInfoM->fParams[2]/2. - info.fParams[2]/2. - lastCorrOnHorn1; // Probably needs corrections.
	// Indeed... No need for this last correction.. if Custom Horn1. November 7 2014. 
	if (fUseHorn1Polycone || fUseHornsPolycone)  info.fPosition[2] = plInfoM->fParams[2]/2. - info.fParams[2]/2. - 0.005*CLHEP::mm;
	// if Horn1 is tilted, leave some room downstream.. Testing here, I should find out what that misalignment is.. 
	//	
        LBNESurveyor* theSurvey = LBNESurveyor::Instance();
	const std::string myHorn1SName("Horn1");
	// Use a fixed shift for now.  To be refine when (if ever) we do the systemac analysis on misalignment. 
        if (theSurvey->IsVolumeMisaligned(myHorn1SName)) info.fPosition[2] -= 5.0*CLHEP::mm;
	std::cerr << " Horn1Poly Placed, long. info, length  " 
                  << info.fParams[2] << " at Z = " << info.fPosition[2] << std::endl;
      }
  
  } // End of Horn1
  if (name.find("Horn2") == 0) {
     if (name == G4String("Horn2Hall")) { // not align-able. 
        const LBNEVolumePlacementData *plInfoTunnel = Find(name, G4String("Tunnel"), G4String("Create"));
        for (size_t k=0; k != 2; ++k) 
        info.fParams[k] = plInfoTunnel->fParams[k] - 0.5*CLHEP::cm; 
        info.fParams[2] = fHorn2Length + 4.0*fHorn2LengthMargin; // Add extra margin, 2 on each side, as there will 
	                                                         // be the container volume  
        G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
        info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName);
// The geometric center of tunnel is at z=0 (MCZERO=0), so, we simply have: 
        info.fPosition[2] = info.fParams[2]/2. + fHorn2LongPosition - fHorn2OffsetIOTr1;
    } 
     if (name == G4String("Horn2TopLevel")) { //  align-able. Use survey data in PlaceFinal
       info.fParams[0] = 0.; 
       info.fParams[1] = fHorn2OuterTubeOuterRadMax + 2.0*in;
       info.fParams[2] = fHorn2Length + 2.0*fHorn2LengthMargin; // Add extra margin, 2 on each side, as there will 
	                                                         // be the container volume  
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], 
	                           info.fParams[2]/2., 0. , 360.0*CLHEP::degree );
	if (fFillHornsWithArgon) {  				    
           info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial(std::string("Argon")), volumeName); 
	} else {
           info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial(std::string("Air")), volumeName); 
	}
        info.fPosition[2] = 0.;
    }
    // Other subvolume defined the usual way in PlaceFinalHorn2
  } // End of Horn2 
  //
  // LBNF design, Polycone Horns, generic design.  
  if (name.find("LBNFChaseDwnstrHorn1") == 0) {
        const LBNEVolumePlacementData *plInfoTunnel = Find(name, G4String("Tunnel"), G4String("Create"));
        for (size_t k=0; k != 2; ++k) 
        info.fParams[k] = plInfoTunnel->fParams[k] - 0.5*CLHEP::cm;
	const double zBeg = fHornsPolyZStartPos[1] - 1.0*CLHEP::mm;
	size_t lastIndex = fMotherHornsAllLengths[fHornsPolyZStartPos.size()-1].size() - 1;
	const double zEnd = fHornsPolyZStartPos[fHornsPolyZStartPos.size()-1] + 
	                    fMotherHornsAllLengths[fHornsPolyZStartPos.size()-1][lastIndex] + 1.0*CLHEP::mm;
			     
        info.fParams[2] = (zEnd - zBeg); //Simply.. They are ordered in Z.  
        G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
        info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName);
// The geometric center of tunnel is at z=0 (MCZERO=0), so, we simply have: 
        info.fPosition[2] = fHornsPolyZStartPos[1] + info.fParams[2]/2. - 0.5*CLHEP::mm;
  }
  
  if (name.find("LBNFSimpleHorn") == 0) {
        size_t ii = name.find("Horn"); ii+=4;
	char hornNumChr = name[ii];
//	const char *hornNumChrPtr = &hornNumChr; // What was this ? P.L. May 18 2016 
	size_t iH = hornNumChr - '0' - 1;
        const LBNEVolumePlacementData *plInfoM = Find(name, G4String("Tunnel"), G4String("Create"));
        for (size_t k=0; k != 2; ++k)  info.fParams[k] = plInfoM->fParams[k] - 0.2*CLHEP::mm;
        info.fParams[2] = fMotherHornsAllLengths[iH][fMotherHornsAllLengths[iH].size() -1] + 2.*CLHEP::mm; //Simply.. They are ordered in Z.  
//
// Make it a tube, such that we can stick a plug if need be. Pick the smalles radii 
//
        info.fParams[0] = 1.0e8;
	for (size_t k=0; k != fMotherHornsAllRads[iH].size(); k++) 
	  info.fParams[0] = std::min(info.fParams[0], fMotherHornsAllRads[iH][k]);
	info.fParams[0] -= 0.1*CLHEP::mm;
	info.fParams[1] = fHornsPolyOuterRadius[iH] + 5.0*CLHEP::mm; 
        G4Tubs* hallTube = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], info.fParams[2]/2., 0., 360.0*CLHEP::degree );
        info.fCurrent = new G4LogicalVolume(hallTube, G4Material::GetMaterial("Air"), volumeName);
	// Assume the tunnel center is at Z = 0., G4 world coordinate.. 
	info.fPosition[2] =  fHornsPolyZStartPos[iH] + info.fParams[2]/2.;
	std::cerr << " .... Placing Mother volume " << volumeName << " r min " <<  info.fParams[0] 
	          << " rMax " << info.fParams[1] << " Length " << info.fParams[2] << " At Z = " << info.fPosition[2] << std::endl;
  }
  if (name.find("DecayPipe") == 0) {
    if (name == G4String("DecayPipeHall")) { // Surveyable, for ease of convenience, let us do it at this level. 
       const LBNEVolumePlacementData *plInfoTunnel = Find(name, G4String("Tunnel"), G4String("Create"));
       for (size_t k=0; k != 2; ++k) 
          info.fParams[k] = plInfoTunnel->fParams[k] - 50.*CLHEP::cm; // 50 cm misalignement unlikely. 
       info.fParams[2] = fDecayPipeLength + 4.0*CLHEP::cm; // Add extra margin, 2 on each side, as there will 
	                                                         // be the container volume  
       G4Box* hallBox = new G4Box(volumeName, info.fParams[0]/2., info.fParams[1]/2., info.fParams[2]/2. );
       info.fCurrent = new G4LogicalVolume(hallBox, G4Material::GetMaterial("Air"), volumeName);
	// we decide to place the decay pipe after the snout, and save 
       if (!fRemoveDecayPipeSnout) {              
            const LBNEVolumePlacementData *plInfoS = Find(name, G4String("DecayPipeSnout"), G4String("Create"));
	    fDecayPipeLongPosition = plInfoS->fPosition[2] + plInfoS->fParams[2]/2.;
       } else {
	   std::cerr << " The decay pipe will start " << fDecayPipeLongPosition 
	             << " meters from the start of Horn1, without Decay Pipe snout..  " << std::endl; 
       }
	// longitudinal position of the entrance of the decay pipe hall. 
       info.fPosition[2] = info.fParams[2]/2. +  fDecayPipeLongPosition + 2.0*CLHEP::cm;
       std::cerr << " VolumePlacement, decay pipe, position " <<  info.fPosition[2] << std::endl;
    }
    if (name == G4String("DecayPipeConcrete")) {
       const LBNEVolumePlacementData *plInfo = Find(name, G4String("DecayPipeHall"), G4String("Create"));       
        info.fParams[0] = fDecayPipeRadius + 2.0*fDecayPipeWallThick + 20.0*CLHEP::cm + 2.0*CLHEP::cm;
        info.fParams[1] = std::min(plInfo->fParams[0], plInfo->fParams[1])/2.0 - 1.0*CLHEP::m; 
	// fill with concrete.  Rock is a bit different 
	// from concrete, but O.K. at such last distance from beam axis. Leave one meter for misalignment..
        info.fParams[2] = fDecayPipeLength + 2.0*CLHEP::cm; // Add extra margin, 2 on each side, as there will 
	                                                         // be the container volume  
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1],
	                            info.fParams[2]/2., 0., 360.0*CLHEP::degree );
        info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial("Concrete"), volumeName);
    }
    if (name == G4String("DecayPipeWall")) {
        info.fParams[0] = fDecayPipeRadius; // such that the decay pipe wall & volume 
	  // can be surveyable. 
        info.fParams[1] = fDecayPipeRadius + fDecayPipeWallThick;
        info.fParams[2] = fDecayPipeLength + 1.0*CLHEP::cm; // Add extra margin 
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], 
	                         info.fParams[2]/2., 0., 360.*CLHEP::degree);
        info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial("Steel316"), volumeName);
    }
    if (name == G4String("DecayPipeOuterWall")) {
        info.fParams[0] = fDecayPipeRadius + 20.0*CLHEP::cm + fDecayPipeWallThick; // such that the decay pipe wall & volume 
	  // can be surveyable. 
        info.fParams[1] = fDecayPipeRadius + 2.0*fDecayPipeWallThick + 20.0*CLHEP::cm ;
        info.fParams[2] = fDecayPipeLength + 1.0*CLHEP::cm; // Add extra margin 
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1], 
	                         info.fParams[2]/2., 0., 360.*CLHEP::degree);
        info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial("Steel316"), volumeName);
    }
     if (name == G4String("DecayPipeVolume")) {
        info.fParams[0] = 0.; // such that the decay pipe wall & volume 
	  // can be surveyable. 
        info.fParams[1] = fDecayPipeRadius - 0.010*CLHEP::mm;
        info.fParams[2] = fDecayPipeLength ; 
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1],
	                         info.fParams[2]/2., 0., 360.0*CLHEP::degree);
        info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial("DecayPipeGas"), volumeName);
    }
     if (name == G4String("DecayPipeSnout")) {
	LBNERunManager *pRunManager=static_cast<LBNERunManager*> (LBNERunManager::GetRunManager());
        const LBNEDetectorConstruction *pDet = 
	    static_cast<const LBNEDetectorConstruction*> (pRunManager->GetUserDetectorConstruction());
        info.fParams[0] = 0.; // Centered  
	  // can be surveyable. 
        info.fParams[1] = ((50.0*in*std::sqrt(2.))/std::cos(pDet->GetBeamlineAngle()) + 1.0*CLHEP::cm)/2.;
        info.fParams[2] = 25.5*in +
	                  25.0*in*std::abs(std::sin(pDet->GetBeamlineAngle())) + 15.0*in + 201.*in;
			  // 15 in is the space upstream of the window box 
			  // and the upstream part is the window holder, which will have to be rotated. 
			  // the distance between the downstream end of the window unit and the beginning of 
			  // the decay pipe. 
			  // Ref Drawing 2251.000-ME-487107 + drawing from Rich Stefaneck, who obtained it
			  // from Diane Reiztner
			  // see also Doc db 5339-v5, page 30 
	std::cerr << " radius of the snout " << info.fParams[1] << " Length " << info.fParams[2] << std::endl;		  
        G4Tubs* tubs = new G4Tubs(volumeName, info.fParams[0], info.fParams[1],
	                            info.fParams[2]/2., 0., 360.*CLHEP::degree);
        info.fCurrent = new G4LogicalVolume(tubs, G4Material::GetMaterial("DecayPipeGas"), volumeName);
	info.fPosition[2] = fDecayPipeWindowZLocation + info.fParams[2]/2. - 19.84*in; 
	 // This last number is the distance 
	 // between the upstream side of the container volume and the window itself, 
	 // and the length of the buffer volume, snout to snout container.  
	std::cerr << " Z Position  of the snout window " << info.fPosition[2] << std::endl;
	// the last number deduced from drawing 2251.000-ME-487107, right side (Scale 1/4 ) Correct within an inch 
	// or so 
    }
   
  } // End if decay pipe. 
  fSubVolumes.insert(std::pair<G4String, LBNEVolumePlacementData>(name, info));
  return &(fSubVolumes.find(name)->second);
}
// Place a volume, single copy. 
G4PVPlacement* LBNEVolumePlacements::PlaceFinal(const G4String &name, G4VPhysicalVolume *mother) {
	
    const bool debugIsOn = false;			       
    G4LogicalVolume *lMother=mother->GetLogicalVolume();			       
    std::map<G4String, LBNEVolumePlacementData>::iterator it = fSubVolumes.find(name);
    if (it == fSubVolumes.end()) {
      std::ostringstream mStrStr;
      mStrStr << " Internal error for " << name << " Mother Volume not found " << std::endl;
      G4String mStr(mStrStr.str());
      G4Exception("LBNEVolumePlacements::PlaceFinal", " ", FatalErrorInArgument, mStr.c_str()); 
    }  
    std::string surveyedPtName("blank");
    if (debugIsOn) std::cerr << " LBNEVolumePlacements::PlaceFinal, name is " << name << std::endl;
    // List of suported cases, nomenclature confusion.. Otherwise, name Surveyed point would get too long. 
    if (name == G4String("UpstreamTargetAssembly")) surveyedPtName = std::string("Canister");
    if (name == G4String("TargetUpstrDownstrHeContainer")) surveyedPtName = std::string("HeTube");
    if (name == G4String("Horn1TargetDownstrHeContainer")) surveyedPtName = std::string("HeTube");
    if (name == G4String("Horn1TopLevelUpstr")) surveyedPtName = std::string("Horn1");
    if (name == G4String("Horn1TopLevelDownstr")) surveyedPtName = std::string("Horn1");
    // Simplified geometry, Horn1 Mother volume is polycone and contains the entire Horn1 
    if (name == G4String("TargetNoSplitHeContainer")) surveyedPtName = std::string("HeTube");
    if (name == G4String("Horn1PolyM1")) surveyedPtName = std::string("Horn1");
    if (name == G4String("LBNFSimpleHorn1Container")) surveyedPtName = std::string("Horn1");
    if (name == G4String("LBNFSimpleHorn2Container")) surveyedPtName = std::string("Horn2");
    if (name == G4String("LBNFSimpleHorn3Container")) surveyedPtName = std::string("Horn3");
    if (name == G4String("Horn2Hall")) surveyedPtName = std::string("Horn2");
    if (name == G4String("DecayPipeHall")) surveyedPtName = std::string("DecayPipe");
   
    LBNEVolumePlacementData &info=it->second;
    info.fMother  =  mother; 
    LBNESurveyor* theSurvey = LBNESurveyor::Instance();
    std::vector<double> posA(3,0.); 
    for (size_t k=0; k!=3; ++k) posA[k] = info.fPosition[k]; 
    if (debugIsOn) std::cerr << " LBNEVolumePlacements::PlaceFinal, Prior to alignment " << name << " half size " 
		<< info.fParams[2]/2. << " position X " << info.fPosition[0] 
	       << " Y " << info.fPosition[1] << " Z  " << info.fPosition[2] << std::endl;
    if (theSurvey->size() != 0) {
      G4ThreeVector deltaUpstrLeft(0., 0., 0.); 
      G4ThreeVector deltaUpstrRight(0., 0., 0.); 
      G4ThreeVector deltaDownstrLeft(0., 0., 0.); 
      G4ThreeVector deltaDownstrRight(0., 0., 0.); 
      std::vector<double> deltaUpstr(3, 0.); 
      std::vector<double> deltaDownstr(3, 0.); 
      std::vector<double> deltaSlopes(2, 0.); 
      // This code could be optimize a bit, by removing extra vector copies.. 
      for(std::vector<LBNESurveyedPt>::const_iterator itSurv = theSurvey->begin(); 
            itSurv != theSurvey->end(); itSurv++) {
	    if (itSurv->GetName().find(surveyedPtName) == std::string::npos) continue;
	      if (itSurv->GetName().find("Upstr") !=  std::string::npos) {
	        if (itSurv->GetName().find("Left") !=  std::string::npos) deltaUpstrLeft = itSurv->GetPosition(); 
		else if (itSurv->GetName().find("Right") !=  std::string::npos) deltaUpstrRight = itSurv->GetPosition(); 
	      } else if (itSurv->GetName().find("Downstr") !=  std::string::npos) {
	        if (itSurv->GetName().find("Left") !=  std::string::npos) deltaDownstrLeft = itSurv->GetPosition(); 
		else if (itSurv->GetName().find("Right") !=  std::string::npos) deltaDownstrRight = itSurv->GetPosition(); 
	      } 
       }
      for (size_t k=0; k != 3; ++k)  {
         if ((std::abs(deltaUpstrLeft[k]) > 2.0e-3*CLHEP::mm) && (std::abs(deltaUpstrLeft[k]) > 2.0e-3*CLHEP::mm)) 
	   deltaUpstr[k] = 0.5*(deltaUpstrLeft[k] + deltaUpstrRight[k]);
	 else if (std::abs(deltaUpstrLeft[k]) > 2.0e-3*CLHEP::mm)  deltaUpstr[k] = deltaUpstrLeft[k];
	 else if (std::abs(deltaUpstrRight[k]) > 2.0e-3*CLHEP::mm)  deltaUpstr[k] = deltaUpstrRight[k];
         if ((std::abs(deltaDownstrLeft[k]) > 2.0e-3*CLHEP::mm) && (std::abs(deltaDownstrLeft[k]) > 2.0e-3*CLHEP::mm)) 
	   deltaDownstr[k] = 0.5*(deltaDownstrLeft[k] + deltaDownstrRight[k]);
	 else if (std::abs(deltaDownstrLeft[k]) > 2.0e-3*CLHEP::mm)  deltaDownstr[k] = deltaDownstrLeft[k];
	 else if (std::abs(deltaDownstrRight[k]) > 2.0e-3*CLHEP::mm)  deltaDownstr[k] = deltaDownstrRight[k];
	 // Special case for the Helium tube that contains the target segments: 
	 // Only the dowstream measurements make sense 
	 if ((name == "TargetUpstrDownstrHeContainer") || (name == "Horn1TargetDownstrHeContainer"))  { 
//	   if (std::abs(deltaUpstr[k]) >  2.0e-3*CLHEP::mm) {
//	     std::cerr << " LBNEVolumePlacements::PlaceFinal The upstream section of the He Container is misaligned." << std::endl; 
//	     std::cerr << "   This makes little sense from a mechanical point of view. " << std::endl; 
//	     std::cerr << "   Suggested action: misaling the target canister instead. " << std::endl;
//	     std::cerr << "   Meanwhile, setting the deltaUpstream to 0. " << std::endl;
//	     deltaUpstr[k] = 0.;
//	   }
	 }
	 if (k != 2) {// Case by case for composite volumes..
           if (surveyedPtName == std::string("Horn1")) {
	      const double aTotalLength = fHorn1TopUpstrLength + fHorn1TopDownstrLength;
	
	      deltaSlopes[k] = (deltaDownstr[k] - deltaUpstr[k])/aTotalLength;
              if(name == G4String("Horn1TopLevelUpstr")) {
	        info.fPosition[k] += deltaUpstr[k] + deltaSlopes[k]*(fHorn1TopUpstrLength/2.) ;
	      } else if(name == G4String("Horn1TopLevelDownstr")) {
	        info.fPosition[k] += deltaUpstr[k] + deltaSlopes[k]*
	        	    (fHorn1TopUpstrLength + fHorn1TopDownstrLength/2.) ;
	      } 
           } else { //generic surveyed volume .
	      info.fPosition[k] += 0.5*(deltaUpstr[k] + deltaDownstr[k]);
              deltaSlopes[k] = (deltaDownstr[k] - deltaUpstr[k])/info.fParams[2]; 
	   }
	 }
      }
      if ((std::abs(deltaSlopes[0]) > 2.0e-9) || (std::abs(deltaSlopes[1]) > 2.0e-9)) { 
        info.fRotationIsUnitMatrix = false;
        info.fRotation.rotateY(-1.0*deltaSlopes[0]); // change sign?????? 	
        info.fRotation.rotateX(deltaSlopes[1]); // rotation in the YZ plane, axis is then X Commutative for small angles.. 
	// Shift the volume longitudinally if a signficant slope exists. 
	const double maxSlope = std::max(std::abs(deltaSlopes[0]), std::abs(deltaSlopes[1]));
	// assume they are tubes..for now, all surveyed points are assigned to tubes..Shift everything downstream 
	// (an convention arbitrary convention!...) 
	info.fParams[2] += std::sin(maxSlope)*info.fParams[1];
	// Note: position might have to revised as well.. On a case by case basis? 	
      }
    } // End of alignment. 
    if (debugIsOn)   std::cerr << " LBNEVolumePlacements::PlaceFinal, " << name << " half size " 
		<< info.fParams[2]/2. << " position X " << info.fPosition[0] 
	       << " Y " << info.fPosition[1] << " Z  " << info.fPosition[2] << std::endl;
    std::map<G4String, LBNEVolumePlacementData>::iterator itMother = 
       fSubVolumes.find(lMother->GetName());
    if (itMother != fSubVolumes.end()) {
//       LBNEVolumePlacementData &infoMother=itMother->second;
//      std::cerr << " .... Params for mother ";
//      for (size_t k=0; k!= infoMother.fParams.size(); k++) std::cerr << " " << infoMother.fParams[k] << ",";
//      std::cerr << " . " << std::endl;

    }
    G4PVPlacement *placement=0;
    G4String vpName(name); vpName += G4String("_P");
    if (name == "Horn1IOTransCont") {
      std::cerr << " PlaceFinal, Z-offset for " << name << " is " << info.fPosition << std::endl;
    }
//    std::cerr << " PlaceFinal , " << info.fCurrent->GetName() << " Last Position " 
//              << info.fPosition << " Length " << info.fParams[2] << std::endl;
    if (info.fRotationIsUnitMatrix) 
        placement=new G4PVPlacement((G4RotationMatrix *) 0, 
	                             info.fPosition, info.fCurrent, vpName, lMother, false, 0, fCheckVolumeOverLapWC);
    else {    
        std::cerr << " Rotated element, slope  X= " << info.fRotation.xz() << " Y " 
	          << info.fRotation.yz() << std::endl;
        placement=new G4PVPlacement(&info.fRotation, info.fPosition, info.fCurrent, 
	                             vpName, lMother, false, 0, fCheckVolumeOverLapWC);
    }
//    std::cerr << " PlaceFinal, done for volume " << name << std::endl;			     
    info.fIsPlaced = true;
    // Optionally, we could test for overlap after installing each new volume here. 				     
    return placement;			       
}
//
void LBNEVolumePlacements::PlaceFinalUpstrTarget(G4PVPlacement *mother) {
//
// We no longer support the 700 kW option, nor the implementation of the split target.. 			       
     this->PlaceFinalUpstrTarget1p2MW(mother);
}


void LBNEVolumePlacements::PrintAll() const {

 std::cout << "======================================================================================" << std::endl;
 std::cout << " Dump of all the Placed Volumes " << std::endl;
 for (std::map<G4String, LBNEVolumePlacementData>::const_iterator it= fSubVolumes.begin(); it!=fSubVolumes.end();
                              it++) {
			      
    const LBNEVolumePlacementData &info = it->second;
    std::cout << " SubVolume " << it->first << " of type " << info.fTypeName;
    if (info.fMother == 0) std::cout << " has no assigned mother volume" << std::endl;
    else std::cout << " is in logical volume " << info.fMother->GetLogicalVolume()->GetName() << std::endl;
    std::cout << " .... Params Values ";
    for (std::vector<double>::const_iterator ip=info.fParams.begin(); ip!=info.fParams.end(); ip++) 
      std::cout << " " << (*ip) << ", ";
    std::cout << std::endl;  
    std::cout << " .... Position " << info.fPosition;
    std::cout << std::endl;  
    if (!info.fRotationIsUnitMatrix) {
     std::cout << " .......... Has a rotation matrix ";
     info.fRotation.print(std::cout);
    }  
    std::cout << "-----------------------------------------------------------------------------------" << std::endl;
    
  }
  std::cout << "======================================================================================" << std::endl;
}
			       

const LBNEVolumePlacementData* LBNEVolumePlacements::Find(const G4String &name, 
                                                          const char *motherName, const char *descr) const {
  std::map<G4String, LBNEVolumePlacementData>::const_iterator itM = fSubVolumes.find(G4String(motherName));
  if (itM == fSubVolumes.end()) {
      std::ostringstream mStrStr;
      mStrStr << " Internal error for " << name << " to be placed in " 
              << motherName << "; mother not found " << "  descriptor " << G4String(descr) <<  std::endl;
      G4String mStr(mStrStr.str());
      G4String origin("LBNEVolumePlacements::Find");    
      G4Exception(origin, " ", FatalErrorInArgument, mStr.c_str());
      return 0; 
  }
  return &(itM->second); 
}  
//
// Currently a clone of G4PVPlacement::CheckOverlaps.  But we plan to upgrade it 
// to handle volume that interesct each other at tiny surfaces. 
// 
bool LBNEVolumePlacements::CheckOverlaps(const G4PVPlacement *plVol, G4int res, G4double tol, G4bool verbose) const {

  if (res<=0) { return false; }

  G4VSolid* solid = plVol->GetLogicalVolume()->GetSolid();
  G4LogicalVolume* motherLog = plVol->GetMotherLogical();
  if (!motherLog) { return false; }

  G4VSolid* motherSolid = motherLog->GetSolid();

  if (verbose)
  {
    G4cout << "Checking overlaps for volume " << plVol->GetName() << " ... ";
  }

  // Create the transformation from daughter to mother
  //
  G4AffineTransform Tm( plVol->GetRotation(), plVol->GetTranslation() );

  for (G4int n=0; n<res; n++)
  {
    // Generate a random point on the solid's surface
    //
    G4ThreeVector point = solid->GetPointOnSurface();

    // Transform the generated point to the mother's coordinate system
    //
    G4ThreeVector mp = Tm.TransformPoint(point);

    // Checking overlaps with the mother volume
    //
    if (motherSolid->Inside(mp)==kOutside)
    {
      G4double distin = motherSolid->DistanceToIn(mp);
      if (distin > tol)
      {
        if (verbose) G4cout << G4endl;
        if (verbose) G4cout << "WARNING - G4PVPlacement::CheckOverlaps()" << G4endl
               << "          Overlap is detected for volume "
               << plVol->GetName() << G4endl
               << "          with its mother volume "
               << motherLog->GetName() << G4endl
               << "          at mother local point " << mp << ", "
               << "overlapping by at least: " << G4BestUnit(distin, "Length")
               << G4endl;
        if (verbose) G4Exception("G4PVPlacement::CheckOverlaps()", "InvalidSetup",
                    JustWarning, "Overlap with mother volume !");
        return true;
      }
    }

    // Checking overlaps with each 'sister' volume
    //
    for (G4int i=0; i<motherLog->GetNoDaughters(); i++)
    {
      G4VPhysicalVolume* daughter = motherLog->GetDaughter(i);

      if (daughter == plVol) { continue; }

      // Create the transformation for daughter volume and transform point
      //
      G4AffineTransform Td( daughter->GetRotation(),
                            daughter->GetTranslation() );
      G4ThreeVector md = Td.Inverse().TransformPoint(mp);

      G4VSolid* daughterSolid = daughter->GetLogicalVolume()->GetSolid();
      if (daughterSolid->Inside(md)==kInside)
      {
        G4double distout = daughterSolid->DistanceToOut(md);
        if (distout > tol)
        {
         if (verbose)  G4cout << G4endl;
         if (verbose)  G4cout << "WARNING - G4PVPlacement::CheckOverlaps()" << G4endl
                 << "          Overlap is detected for volume "
                 << plVol->GetName() << G4endl
                 << "          with " << daughter->GetName() << " volume's"
                 << G4endl
                 << "          local point " << md << ", "
                 << "overlapping by at least: " << G4BestUnit(distout,"Length")
                 << G4endl;
         if (verbose)  G4Exception("G4PVPlacement::CheckOverlaps()", "InvalidSetup",
                      JustWarning, "Overlap with volume already placed !");
          return true;
        }
      }

      // Now checking that 'sister' volume is not totally included and
      // overlapping. Do it only once, for the first point generated
      //
      if (n==0)
      {
        // Generate a single point on the surface of the 'sister' volume
        // and verify that the point is NOT inside the current volume

        G4ThreeVector dPoint = daughterSolid->GetPointOnSurface();

        // Transform the generated point to the mother's coordinate system
        // and finally to current volume's coordinate system
        //
        G4ThreeVector mp2 = Td.TransformPoint(dPoint);
        G4ThreeVector msMp2 = Tm.Inverse().TransformPoint(mp2);

        if (solid->Inside(msMp2)==kInside)
        {
          if (verbose)  G4cout << G4endl;
          if (verbose)  G4cout << "WARNING - G4PVPlacement::CheckOverlaps()" << G4endl
                  << "          Overlap is detected for volume "
                  << plVol->GetName() << G4endl
                  << "          apparently fully encapsulating volume "
                  << daughter->GetName() << G4endl
                  << "          at the same level !" << G4endl;
          if (verbose)  G4Exception("G4PVPlacement::CheckOverlaps()", "InvalidSetup",
                       JustWarning, "Overlap with volume already placed !");
          return true;
        }
      }
    }
  }

  if (verbose)
  {
    G4cout << "OK! " << G4endl;
  }

  return false;
}
double LBNEVolumePlacements::GetMaxRadiusMotherHorn1() {
  double maxR=-1.0;
  for (size_t k=0; k!= fMotherHorn1AllRads.size(); k++) maxR = std::max(fMotherHorn1AllRads[k], maxR);
  return maxR; 
}


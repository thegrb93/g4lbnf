//#include <fstream>

#include "LBNEDetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"
#include "LBNEPlacementMessenger.hh"

LBNEPlacementMessenger* LBNEPlacementMessenger::fInstance = 0;
LBNEPlacementMessenger* LBNEPlacementMessenger::Instance() {
  if (fInstance == 0) fInstance = new LBNEPlacementMessenger();
  return fInstance;
}

LBNEPlacementMessenger::~LBNEPlacementMessenger() {

}

LBNEPlacementMessenger::LBNEPlacementMessenger()
 {

//   std::cerr << " LBNEPlacementMessenger::LBNEPlacementMessenger, contructor, starts " << std::endl;
   LBNEVolumePlacements* volP=LBNEVolumePlacements::Instance();
   {
   fDecayPipeLength  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/decayPipeLength",this);
   fDecayPipeLength->SetGuidance("Length of the decay Pipe");
   fDecayPipeLength->SetParameterName("decayPipeLength",true);
   double value = volP->GetDecayPipeLength();
    fDecayPipeLength->SetDefaultValue (value);
   fDecayPipeLength->SetDefaultUnit ("m");
   fDecayPipeLength->SetUnitCandidates ("cm m");
   fDecayPipeLength->AvailableForStates(G4State_PreInit);
   }
   {
   fDecayPipeLongPosition  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/decayPipeLongPosition",this);
   fDecayPipeLongPosition->SetGuidance("Longitudinal Position of the entrance window of the decay pipe with respect to target");
   fDecayPipeLongPosition->SetParameterName("decayPipeLongPosition",true);
   double value = volP->GetDecayPipeLongPosition();
    fDecayPipeLongPosition->SetDefaultValue (value);
   fDecayPipeLongPosition->SetDefaultUnit ("m");
   fDecayPipeLongPosition->SetUnitCandidates ("m");
   fDecayPipeLongPosition->AvailableForStates(G4State_PreInit);
   }
   {
   fDecayPipeRadius  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/decayPipeRadius",this);
   fDecayPipeRadius->SetGuidance("Radius of the decay Pipe");
   fDecayPipeRadius->SetParameterName("decayPipeRadius",true);
   double value = volP->GetDecayPipeRadius();
    fDecayPipeRadius->SetDefaultValue (value);
   fDecayPipeRadius->SetDefaultUnit ("m");
   fDecayPipeRadius->SetUnitCandidates ("cm m");
   fDecayPipeRadius->AvailableForStates(G4State_PreInit);
   }
   {
   fDecayPipeUpstreamWindowThickness  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/decayPipeUsptreamWindowThickness",this);
   fDecayPipeUpstreamWindowThickness->SetGuidance("Thickness of the upstream window of the decay pipe.");
   fDecayPipeUpstreamWindowThickness->SetParameterName("decayPipeUpstreamWindowThickness",true);
   double value = volP->GetDecayPipeUpstrWindowThick();
    fDecayPipeUpstreamWindowThickness->SetDefaultValue (value);
   fDecayPipeUpstreamWindowThickness->SetDefaultUnit ("mm");
   fDecayPipeUpstreamWindowThickness->SetUnitCandidates ("cm");
   fDecayPipeUpstreamWindowThickness->AvailableForStates(G4State_PreInit);
   }
   {
   fDecayPipeGas  = new G4UIcmdWithAString("/LBNE/det/decayPipeGas",this);
   fDecayPipeGas->SetGuidance("Gas inside of the decay Pipe. Only two options so far, Air or Helium ");
   fDecayPipeGas->SetParameterName("decayPipeGas",true);
   fDecayPipeGas->SetDefaultValue (G4String("Helium"));
   fDecayPipeGas->AvailableForStates(G4State_PreInit);
   }

   fWaterLayerThickInHorn  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/waterThickInHorn",this);
   fWaterLayerThickInHorn->SetGuidance("Water Thicknes on the inner conductor of the horn");
   fWaterLayerThickInHorn->SetParameterName("waterThickInHorn",true);
   fWaterLayerThickInHorn->SetDefaultValue (0.);
   fWaterLayerThickInHorn->SetDefaultUnit ("mm");
   fWaterLayerThickInHorn->SetUnitCandidates ("mm cm m");
   fWaterLayerThickInHorn->AvailableForStates(G4State_PreInit);
   { 
     fHorn1Length  = new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn1Length",this);
     G4String guidance("Length of Horn1.\n  ");
     guidance += std::string(" The length of the outer conductor, excludes the upstream transition inner/outer, \n");
     guidance += std::string("  and flanges with bolts. If extended by user, it is assumed that conductor are extended.. " ); 
     guidance += std::string("  NUMI Horn1, FNAL Drawing number 8875. 112-ME-363092 " ); 
     guidance += std::string(" Obsolete data card, use /LBNE/det/Horn1LongRescale instead ");
     fHorn1Length->SetGuidance(guidance);
     fHorn1Length->SetParameterName("Horn1Length",true);
     double value = volP->GetHorn1Length(); //  
     
     SetMyUnitsAndConditions(fHorn1Length, value);
   }
   {
     fHorn1InnerCondMat = new G4UIcmdWithAString("/LBNE/det/Horn1InnerConductorMaterial", this);
     G4String guidance("Material for the inner conductor  \n  ");
     guidance += std::string(" Aluminum is the base line and default. This option should only be used \n");
     guidance += std::string(" for studying the relevance of the material budget.  \n  " );
     guidance += std::string(" That is, run with a fictious Horn with air or vacuum as material carrying 200kA   \n  " );      
     fHorn1InnerCondMat->SetGuidance(guidance);
     fHorn1InnerCondMat->SetParameterName("Horn1InnerCondMaterial",true);
     G4String value = volP->GetHorn1InnerConductorMaterial(); //  
     fHorn1InnerCondMat->SetDefaultValue(value);
   }
   {
     fHorn1AllCondMat = new G4UIcmdWithAString("/LBNE/det/Horn1AllConductorMaterial", this);
     G4String guidance("Material for the All conductors in Horn1 (inner and outer)   \n  ");
     guidance += std::string(" Aluminum is the base line and default. This option should only be used \n");
     guidance += std::string(" for studying the relevance of the material budget.  \n  " );
     guidance += std::string(" That is, run with a fictious Horn with air or vacuum as material carrying 200kA   \n  " );      
     fHorn1AllCondMat->SetGuidance(guidance);
     fHorn1AllCondMat->SetParameterName("Horn1AllCondMaterial",true);
     G4String value = volP->GetHorn1AllConductorMaterial(); //  
     fHorn1AllCondMat->SetDefaultValue(value);
   }

   {
     fTargetSLengthGraphite = new G4UIcmdWithADoubleAndUnit("/LBNE/det/GraphiteTargetLength", this);
     G4String guidance("Length of the Graphite Target.\n  ");
     guidance += std::string(" Note: the length of the indiviudal fins are fixed to 2 cm. So, we will re-adjusted  \n");
     guidance += std::string(" abit to get an integer number of fins. For details, see LBNE-Doc-6100  " ); 
     fTargetSLengthGraphite->SetGuidance(guidance);
     fTargetSLengthGraphite->SetParameterName("GraphiteTargetLength",true);
     double value = volP->GetTargetSLengthGraphite(); //  
     SetMyUnitsAndConditions(fTargetSLengthGraphite, value);
   }
   {
     fTargetFinWidth = new G4UIcmdWithADoubleAndUnit("/LBNE/det/GraphiteTargetFinWidth", this);
     G4String guidance("Width of the Graphite Target Fins.\n  ");
     guidance += std::string(" For details, see LBNE-Doc-6100  " ); 
     fTargetFinWidth->SetGuidance(guidance);
     fTargetFinWidth->SetParameterName("GraphiteTargetFinWidth",true);
     double value = volP->GetTargetFinWidth(); //  
     SetMyUnitsAndConditions(fTargetFinWidth, value);
   }
   {
     fTargetMaterial = new G4UIcmdWithAString("/LBNE/det/TargetMaterial", this);
     G4String guidance("Material for the target \n  ");
     guidance += std::string(" Note: Only Beryllium, Graphite and Aluminum accepted for now..   \n");
     guidance += std::string(" The physical length of the target is left unchanged upon use of this data card. \n  " );
     guidance += std::string(" Default value: graphite \n  " );      
     fTargetMaterial->SetGuidance(guidance);
     fTargetMaterial->SetParameterName("TargetMaterial",true);
     G4String value = volP->GetTargetMaterialName(); //  
     fTargetMaterial->SetDefaultValue(value);
   }
   
   {
     fPlugMaterial = new G4UIcmdWithAString("/LBNE/det/PlugMaterial", this);
     G4String guidance("Material for the 2n target or plug \n  ");
     guidance += std::string(
     " Note: If not usual materials (Beryllium, graphite.., please check that the material is defined in LBNEDetectorContruction   \n");
     guidance += std::string(" The physical length of the this target is left unchanged upon use of this data card. \n  " );
     guidance += std::string(" Default value: graphite \n  " );      
     fPlugMaterial->SetGuidance(guidance);
     fPlugMaterial->SetParameterName("PlugMaterial",true);
     G4String value = volP->GetPlugMaterial(); //  
     fPlugMaterial->SetDefaultValue(value);
   }
   {
     fTargetDensity = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetDensity", this);
     G4String guidance("Density of the target. Only relevant for the graphite target. \n  ");
     fTargetDensity->SetGuidance(guidance);
     fTargetDensity->SetParameterName("TargetDensity",true);
     double value = volP->GetTargetDensity(); //  
     fTargetDensity->SetDefaultValue (value);
     fTargetDensity->SetDefaultUnit ("g/cm3");
     fTargetDensity->SetUnitCandidates ("g/cm3");
     fTargetDensity->AvailableForStates(G4State_PreInit);
   }
   {
     fTargetLengthIntoHorn = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetLengthIntoHorn", this);
     G4String guidance("Length of the Graphite Target into the horn.\n  ");
     guidance += std::string(" More specifically, the length of graphite from MCZERO to the dowstream tip of graphite. \n");
     guidance += std::string(" FNAL Drawing number 8875. 112-ME-363092  " ); 
     guidance += std::string(" !!! Judged too confusing, discontinued command !!!!  " ); 
     fTargetLengthIntoHorn->SetGuidance(guidance);
     fTargetLengthIntoHorn->SetParameterName("TargetLengthIntoHorn",true);
     double value = volP->GetTargetLengthIntoHorn(); //  
     SetMyUnitsAndConditions(fTargetLengthIntoHorn, value);
   }
   {
     fTargetLengthOutsideHorn = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetLengthOutsideHorn", this);
     G4String guidance("Length of the Graphite Target outside the horn.\n  ");
     guidance += std::string(" More specifically, the length of graphite from the most upstream point on \n ");
     guidance += std::string(" the 1rst vertical fin  to MCZERO. \n");
     guidance += std::string(" FNAL Drawing number 8875. 112-ME-363092  " ); 
     fTargetLengthOutsideHorn->SetGuidance(guidance);
     fTargetLengthOutsideHorn->SetParameterName("TargetLengthOutsideHorn",true);
     double value = volP->GetTargetLengthIntoHorn(); //  
     double valueL = volP->GetTargetSLengthGraphite(); //  
     SetMyUnitsAndConditions(fTargetLengthOutsideHorn, valueL - value);
   }
   {
     fSimpleTargetRadius = new G4UIcmdWithADoubleAndUnit("/LBNE/det/SimpleTargetRadius", this);
     G4String guidance("Radius of the simple target.\n  ");
     guidance += std::string(" Should the option UseSimpleCylindicalTarget be selected,  \n ");
     guidance += std::string(" this command sets the radius of the target. \n");
     guidance += std::string(" No FNAL Drawing number referenced  " ); 
     fSimpleTargetRadius->SetGuidance(guidance);
     fSimpleTargetRadius->SetParameterName("SimpleTargetRadius",true);
     double value = volP->GetSimpleTargetRadius(); //  
     SetMyUnitsAndConditions(fSimpleTargetRadius, value);
   }
   {
     fSimpleTargetLength = new G4UIcmdWithADoubleAndUnit("/LBNE/det/SimpleTargetLength", this);
     G4String guidance("Length of the simple target.\n  ");
     guidance += std::string(" Should the option UseSimpleCylindicalTarget (or UseSimpleBoxTarget  be selected,  \n ");
     guidance += std::string(" this command sets the length of the target. \n");
     guidance += std::string(" No FNAL Drawing number referenced  " ); 
     fSimpleTargetLength->SetGuidance(guidance);
     fSimpleTargetLength->SetParameterName("SimpleTargetLength",true);
     double value = volP->GetSimpleTargetLength(); //  
     SetMyUnitsAndConditions(fSimpleTargetLength, value);
   }
// August 2014, Quynh added new cards for multi sphere:
   {
     fMultiSphereTargetRadius = new G4UIcmdWithADoubleAndUnit("/LBNE/det/MultiSphereTargetRadius", this);
     G4String guidance("Radius of the multi sphere target.\n  ");
     guidance += std::string(" Should the option MultiSphereTargetRadius be selected,  \n ");
     guidance += std::string(" this command sets the radius of the target. \n");
     guidance += std::string(" No FNAL Drawing number referenced  " ); 
     fMultiSphereTargetRadius->SetGuidance(guidance);
     fMultiSphereTargetRadius->SetParameterName("MultiSphereTargetRadius",true);
     double value = volP->GetMultiSphereTargetRadius(); //  
     SetMyUnitsAndConditions(fMultiSphereTargetRadius, value);
   }
  { 
     fUseMultiSphereTarget = new G4UIcmdWithABool("/LBNE/det/UseMultiSphereTarget", this);
     G4String guidance("If true, the NUMI-style target is replaced by a simple spherical.  \n  ");
     guidance += std::string(" See MultiSphereTargetLength(Height) for setting it's dimensions,  \n ");
     fUseMultiSphereTarget->SetGuidance(guidance);
     fUseMultiSphereTarget->SetParameterName("UseMultiSphereTarget",true);
     bool value = volP->GetUseMultiSphereTarget(); //defined in LBNEVolumePlacements.hh     
     fUseMultiSphereTarget->SetDefaultValue(value);
   }
   {
     fSimpleTargetHeight = new G4UIcmdWithADoubleAndUnit("/LBNE/det/SimpleTargetHeight", this);
     G4String guidance("Height of the simple box-like target.\n  ");
     guidance += std::string(" Should the option UseSimpleBoxTarget be selected,  \n ");
     guidance += std::string(" this command sets the height of the target. \n");
     guidance += std::string(" No FNAL Drawing number referenced  " ); 
     fSimpleTargetHeight->SetGuidance(guidance);
     fSimpleTargetHeight->SetParameterName("SimpleTargetHeight",true);
     double value = volP->GetSimpleTargetHeight(); //  
     SetMyUnitsAndConditions(fSimpleTargetHeight, value);
   }
   {
     fSimpleTargetWidth = new G4UIcmdWithADoubleAndUnit("/LBNE/det/SimpleTargetWidth", this);
     G4String guidance("Width of the simple box-like target.\n  ");
     guidance += std::string(" Should the option UseSimpleBoxTarget be selected,  \n ");
     guidance += std::string(" this command sets the height of the target. \n");
     guidance += std::string(" No FNAL Drawing number referenced  " ); 
     fSimpleTargetWidth->SetGuidance(guidance);
     fSimpleTargetWidth->SetParameterName("SimpleTargetWidth",true);
     double value = volP->GetSimpleTargetWidth(); //  
     SetMyUnitsAndConditions(fSimpleTargetWidth, value);
   }
    { 
     fUseSimpleTargetBox = new G4UIcmdWithABool("/LBNE/det/UseSimpleBoxTarget", this);
     G4String guidance("If true, the NUMI-style target is replaced by a simple box.  \n  ");
     guidance += std::string(" See SimpleTargetLengthWidth (Height) for setting it's dimensions,  \n ");
     fUseSimpleTargetBox->SetGuidance(guidance);
     fUseSimpleTargetBox->SetParameterName("UseSimpleBoxTarget",true);
     bool value = volP->GetUseSimpleTargetBox(); //  
     fUseSimpleTargetBox->SetDefaultValue(value);
   }
    { 
     fRemoveTargetAlltogether = new G4UIcmdWithABool("/LBNE/det/RemoveTarget", this);
     G4String guidance("If true, the NUMI-style target is simply gone.  \n  ");
     guidance += std::string(" You got 3 pulses of beam to do your experiment, \n");
     guidance += std::string(" after which you'll have to rebuild the Hadron dump.  \n ");
     fRemoveTargetAlltogether->SetGuidance(guidance);
     fRemoveTargetAlltogether->SetParameterName("RemoveTarget",true);
     fRemoveTargetAlltogether->SetDefaultValue(false);
   }
    { 
     fConstructPlug = new G4UIcmdWithABool("/LBNE/det/ConstructPlug", this);
     G4String guidance("If true, a plug (or second target) will be placed either in horn 1, or in horn2, (Optimized design) ");
     fConstructPlug->SetGuidance(guidance);
     fConstructPlug->SetParameterName("ConstructPlug",true);
     bool value = volP->GetConstructPlug(); //  
     fConstructPlug->SetDefaultValue(value);
   }
    { 
     fConstructPlugInHorn1 = new G4UIcmdWithABool("/LBNE/det/ConstructPlugInHorn1", this);
     G4String guidance("If true, a graphite plug will be placed in the beamline downstream of the neck of Horn1, in Horn1.");
     fConstructPlugInHorn1->SetGuidance(guidance);
     fConstructPlugInHorn1->SetParameterName("ConstructPlugInHorn1",true);
     bool value = volP->GetConstructPlugInHorn1(); //  
     fConstructPlugInHorn1->SetDefaultValue(value);
   }
   {
     fPlugLength = new G4UIcmdWithADoubleAndUnit("/LBNE/det/PlugLength", this);

     G4String guidance("Length of the beam plug.\n  ");
     guidance += std::string(" Should the option ConstructPlug be selected,  \n ");
     fPlugLength->SetGuidance(guidance);
     fPlugLength->SetParameterName("PlugLength",true);
     double value = volP->GetPlugLength(); //  
     SetMyUnitsAndConditions(fPlugLength, value);
   fPlugLength->SetDefaultUnit ("cm");
   fPlugLength->SetUnitCandidates ("cm m");
   fPlugLength->AvailableForStates(G4State_PreInit);
   }
   {
     fPlugOuterRadius = new G4UIcmdWithADoubleAndUnit("/LBNE/det/PlugOuterRadius", this);
     G4String guidance("Outer radius of the beam plug.\n  ");
     guidance += std::string(" Should the option ConstructPlug be selected,  \n");
     guidance += std::string(" this command sets the outer radius of the plug.\n");
     guidance += std::string(" It needs to be smaller than the hole in horn2, or Horn1 ");
     fPlugOuterRadius->SetGuidance(guidance);
     fPlugOuterRadius->SetParameterName("PlugOuterRadius",true);
     double value = volP->GetPlugOuterRadius(); //  
     SetMyUnitsAndConditions(fPlugOuterRadius, value);
   fPlugOuterRadius->SetDefaultUnit ("cm");
   fPlugOuterRadius->SetUnitCandidates ("cm m");
   fPlugOuterRadius->AvailableForStates(G4State_PreInit);
   }
   {
     fPlugInnerRadius = new G4UIcmdWithADoubleAndUnit("/LBNE/det/PlugInnerRadius", this);
     G4String guidance("Inner radius of the beam plug.\n  ");
     guidance += std::string(" Should the option ConstructPlug be selected,  \n");
     guidance += std::string(" this command sets the inner radius of the plug. \n");
     guidance += std::string(" In general a solid plus should have this set to zero, the default  " ); 
     fPlugInnerRadius->SetGuidance(guidance);
     fPlugInnerRadius->SetParameterName("PlugInnerRadius",true);
     double value = volP->GetPlugInnerRadius(); //  
     SetMyUnitsAndConditions(fPlugInnerRadius, value);
   fPlugInnerRadius->SetDefaultUnit ("cm");
   fPlugInnerRadius->SetUnitCandidates ("cm m");
   fPlugInnerRadius->AvailableForStates(G4State_PreInit);
   }
   {
     fPlugZPosition = new G4UIcmdWithADoubleAndUnit("/LBNE/det/PlugZPosition", this);
     G4String guidance("The Z position of the beam plug.\n  ");
     guidance += std::string(" Should the option ConstructPlug be selected,  \n");
     guidance += std::string(" this is the shift upstream relative to the downstream end of horn2, or Horn1 if In Horn1 \n");
     fPlugZPosition->SetGuidance(guidance);
     fPlugZPosition->SetParameterName("PlugZPosition",true);
     double value = volP->GetPlugZPosition(); //  
     SetMyUnitsAndConditions(fPlugZPosition, value);
   fPlugZPosition->SetDefaultUnit ("cm");
   fPlugZPosition->SetUnitCandidates ("cm m");
   fPlugZPosition->AvailableForStates(G4State_PreInit);
   }

    { 
     fUseSimpleTargetCylinder = new G4UIcmdWithABool("/LBNE/det/UseSimpleCylindricalTarget", this);
     G4String guidance("If true, the NUMI-style target is replaced by a simple cylinder.  \n  ");
     guidance += std::string(" See SimpleTargetLengthWidth (Height) for setting it's dimensions,  \n ");
     fUseSimpleTargetCylinder->SetGuidance(guidance);
     fUseSimpleTargetCylinder->SetParameterName("UseSimpleCylindricalTarget",true);
     bool value = volP->GetUseSimpleTargetBox(); //  
     fUseSimpleTargetCylinder->SetDefaultValue(value);
   }
   
    {
     fTargetBerylCapThickness = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetBerylliumCapThickness", this);
     G4String guidance("Thickness of the Beryllium cap, downstream end of the target Helium containment vessel .\n  ");
     guidance += std::string(" Use to study the relevance of adding/removing material in the target \n");
     fTargetBerylCapThickness->SetGuidance(guidance);
     fTargetBerylCapThickness->SetParameterName("TargetBerylliumCapThickness",true);
     double value = volP->GetTargetBerylDownstrWindowThick(); //  
     SetMyUnitsAndConditions(fTargetBerylCapThickness, value);
   }
  
   
    {
     fHorn1RadialRescale = new G4UIcmdWithADouble("/LBNE/det/Horn1RadialRescale", this);
     G4String guidance("A ratio between the actual radii for this run over the nominal values for Horn1 \n  ");
     guidance += std::string(" More specifically, all (excluding target elements in Horn1) will be rescale by  \n");
     guidance += std::string(" that factor. Suggested value for modification: no more than 105 for a start!..   \n");
     guidance += std::string(" FNAL Drawing number 8875. 112-ME-363096, 363097,... " ); 
     fHorn1RadialRescale->SetGuidance(guidance);
     fHorn1RadialRescale->SetParameterName("Horn1RadialRescale",true);
     fHorn1RadialRescale->SetDefaultValue(1.0);
    }
    { 
     fHorn1LongRescale = new G4UIcmdWithADouble("/LBNE/det/Horn1LongRescale", this);
     G4String guidance("A ratio between the actual lengths for this run over the nominal values for Horn1 \n  ");
     guidance += std::string(" FNAL Drawing number 8875. 112-ME-363096, 363097,...   " ); 
     guidance += std::string(" More specifically, all (excluding target elements in Horn1) will be rescale by  \n");
     guidance += std::string(" that factor. Suggested value for modification: no more than a few % change for start!..   \n");
     fHorn1LongRescale->SetGuidance(guidance);
     fHorn1LongRescale->SetParameterName("Horn1LongRescale",true);
     fHorn1LongRescale->SetDefaultValue(1.0);
   }
    { 
     fHorn1RadialSafetyMargin = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetAndHorn1RadialSafety", this);
     G4String guidance(" A back door command to re-adjust the tolerance on the minimum distance  \n  ");
     guidance += std::string(" between the tip of the Target Helium tube and the Horn 1 inner conductor.   " ); 
     guidance += std::string(" To be used exclusively for target misalignment studies.   \n");
     fHorn1RadialSafetyMargin->SetGuidance(guidance);
     fHorn1RadialSafetyMargin->SetParameterName("TargetAndHorn1RadialSafety",true);
     const double valDef = volP->GetHorn1RadialSafetyMargin();
     SetMyUnitsAndConditions(fHorn1RadialSafetyMargin, valDef);
   }
    { 
     fHorn1RadiusBigEnough = new G4UIcmdWithABool("/LBNE/det/TargetAndHorn1RadiusBigEnough", this);
     G4String guidance(" A back door command to re-adjust the inner conductor radius   \n  ");
     guidance += std::string(" uspstream of the neck such that the 1 m. long target can be completly inserted .   " ); 
     guidance += std::string(" in Horn1, to maximize the neutrino flux.  \n");
     guidance += std::string(" The neck is left unchanged, so there is abrupt and unphysical change of ");
     guidance += std::string(" of the radius entering the neck.  ");
     fHorn1RadiusBigEnough->SetGuidance(guidance);
     fHorn1RadiusBigEnough->SetParameterName("TargetAndHorn1RadiusBigEnough",true);
     const bool valDef = volP->IsHorn1RadiusBigEnough();
     fHorn1RadiusBigEnough->SetDefaultValue(valDef);
   }
    {
     fHorn2RadialRescale = new G4UIcmdWithADouble("/LBNE/det/Horn2RadialRescale", this);
     G4String guidance("A ratio between the actual radii for this run over the nominal values for Horn2 \n  ");
     guidance += std::string(" More specifically, all (excluding target elements in Horn2) will be rescale by  \n");
     guidance += std::string(" that factor. Suggested value for modification: no more than 105 for a start!..   \n");
     guidance += std::string(" FNAL Drawing number 8875.112 MD-363383 " ); 
     fHorn2RadialRescale->SetGuidance(guidance);
     fHorn2RadialRescale->SetParameterName("Horn2RadialRescale",true);
     fHorn2RadialRescale->SetDefaultValue(1.0);
    }
    {
     fHorn2RadialRescaleCst = new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn2RadialRescaleCst", this);
     G4String guidance("The new radial coordinates will now be = radialRescale*r_old + RadialRescaleCst \n  ");
     guidance += std::string(" More specifically, all (excluding target elements in Horn2) will be rescale by  \n");
     guidance += std::string(" that linear function.   \n");
     guidance += std::string(" Note: For this parameter to be effective, you must invoke Horn2RadialRescale   \n");
     guidance += std::string(" FNAL Drawing number 8875.112 MD-363383 " ); 
     fHorn2RadialRescaleCst->SetGuidance(guidance);
     fHorn2RadialRescaleCst->SetParameterName("Horn2RadialRescaleCst",true);
     fHorn2RadialRescaleCst->SetDefaultUnit ("mm");
     fHorn2RadialRescaleCst->SetUnitCandidates ("cm mm m");
     fHorn2RadialRescaleCst->SetDefaultValue(0.0); // Identity relation, by default. 
    }
    { 
     fHorn2LongRescale = new G4UIcmdWithADouble("/LBNE/det/Horn2LongRescale", this);
     G4String guidance("A ratio between the actual lengths for this run over the nominal values for Horn2 \n  ");
     guidance += std::string(" More specifically, all (excluding target elements in Horn2) will be rescale by  \n");
     guidance += std::string(" that factor. Suggested value for modification: no more than 105 for a start!..   \n");
     guidance += std::string(" FNAL Drawing number 8875.112 MD-363383 " ); 
     fHorn2LongRescale->SetGuidance(guidance);
     fHorn2LongRescale->SetParameterName("Horn2LongRescale",true);
     fHorn2LongRescale->SetDefaultValue(1.0);
   }
    { 
     fHorn2LongPosition = new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn2LongPosition", this);
     G4String guidance("The Z position of Horn2 with respect to the entrance of Horn1 (a.k.a MCZero) \n  ");
     fHorn2LongPosition->SetGuidance(guidance);
     fHorn2LongPosition->SetParameterName("Horn2LongPosition",true);
     double value = volP->GetHorn2LongPosition(); //  
     
     SetMyUnitsAndConditions(fHorn2LongPosition, value);
   }
   {
     fHorn2InnerCondMat = new G4UIcmdWithAString("/LBNE/det/Horn2InnerConductorMaterial", this);
     G4String guidance("Material for the inner conductor  \n  ");
     guidance += std::string(" Aluminum is the base line and default. This option should only be used \n");
     guidance += std::string(" for studying the relevance of the material budget.  \n  " );
     guidance += std::string(" That is, run with a fictious Horn with air or vacuum as material carrying 200kA   \n  " );      
     fHorn2InnerCondMat->SetGuidance(guidance);
     fHorn2InnerCondMat->SetParameterName("Horn2InnerCondMaterial",true);
     G4String value = volP->GetHorn2InnerConductorMaterial(); //  
     fHorn2InnerCondMat->SetDefaultValue(value);
   }
    {
     fHorn2AllCondMat = new G4UIcmdWithAString("/LBNE/det/Horn2AllConductorMaterial", this);
     G4String guidance("Material for all conductors, inner, outer, I/O transition, etc..   \n  ");
     guidance += std::string(" Aluminum is the base line and default. This option should only be used \n");
     guidance += std::string(" for studying the relevance of the material budget.  \n  " );
     guidance += std::string(" That is, run with a fictious Horn with air or vacuum as material carrying 200kA   \n  " );      
     fHorn2AllCondMat->SetGuidance(guidance);
     fHorn2AllCondMat->SetParameterName("Horn2AllCondMaterial",true);
     G4String value = volP->GetHorn2AllConductorMaterial(); //  
     fHorn2AllCondMat->SetDefaultValue(value);
   }
 
    { 
     fUseMarsTargetHorns = new G4UIcmdWithABool("/LBNE/det/UseMarsTargetHorns", this);
     G4String guidance("If true (default is false) use GDML geometry exported from MARS rather than standard G4LBNE target/horns  \n  ");
     fUseMarsTargetHorns->SetGuidance(guidance);
     fUseMarsTargetHorns->SetParameterName("UseMarsTargetHorns",true);
     bool value = volP->GetUseMarsTargetHorns(); //  
     fUseMarsTargetHorns->SetDefaultValue(value);
   }

   {     
     fMarsTargetHornsGDMLFilename = new G4UIcmdWithAString("/LBNE/det/GDMLMarsTargetHornsFilename", this);
     G4String guidance("File name generated by MARS/ROOT describing the target/horns. \n  ");
     guidance += std::string(" Path name can be relative, if the executable runs from the correct directory  \n");
     guidance += std::string(" Typically .../g4lbne \n  " );
     guidance += std::string(" Or, absolute i.e, /scratch/.../g4blne/gdml/lbne_absorber_112912.gdml \n  " );      
     guidance += std::string(" The translation of an (eventual) Unix environment variable is currently not implemented \n  " );      
     fMarsTargetHornsGDMLFilename->SetGuidance(guidance);
     fMarsTargetHornsGDMLFilename->SetParameterName("GDMLMarsTargetHornsFilename",true);
     G4String value = volP->GetMarsTargetHornsGDMLFilename(); //  
     fMarsTargetHornsGDMLFilename->SetDefaultValue(value);
   }  

   {
     fAbsorberGDMLFilename = new G4UIcmdWithAString("/LBNE/det/GDMLAbsorberFilename", this);
     G4String guidance("File name generated by MARS/ROOT describing the Hadron absorber at the end of the beam line. \n  ");
     guidance += std::string(" Path name can be relative, if the executable runs from the correct directory  \n");
     guidance += std::string(" Typically .../g4lbne \n  " );
     guidance += std::string(" Or, absolute i.e, /scratch/.../g4blne/gdml/lbne_absorber_112912.gdml \n  " );      
     guidance += std::string(" The translation of an (eventual) Unix environment variable is currently not implement \n  " );      
     fAbsorberGDMLFilename->SetGuidance(guidance);
     fAbsorberGDMLFilename->SetParameterName("GDMLAbsorberFilename",true);
     G4String value = volP->GetAbsorberGDMLFilename(); //  
     fAbsorberGDMLFilename->SetDefaultValue(value);
   }  
    { 
     fInstallShield = new G4UIcmdWithABool("/LBNE/det/InstallShield", this);
     G4String guidance("If true (default) install beamline rotated rectangular chunck of steel surrounding the horns \n  ");
     fInstallShield->SetGuidance(guidance);
     fInstallShield->SetParameterName("InstallShieldAroundHorns",true);
     bool value = volP->GetDoInstallShield(); //  
     fInstallShield->SetDefaultValue(value);
   }
    { 
     fLengthOfRockDownstr = new G4UIcmdWithADoubleAndUnit("/LBNE/det/LengthOfRockDownstr", this);
     G4String guidance(" The length of rock  downstream of the muon alcove. \n  ");
     fLengthOfRockDownstr->SetGuidance(guidance);
     fLengthOfRockDownstr->SetParameterName("LengthOfRockDownstr",true);
     const double valDef = volP->GetLengthOfRockDownstreamAlcove();
     SetMyUnitsAndConditions(fLengthOfRockDownstr, valDef);
   }
   //
   // 1.2 MW option We keep the above data card as define above, but will have a "modal" flag when we work with the 1.2 MW option. 
   //
   { 
     fUse1p2MW = new G4UIcmdWithABool("/LBNE/det/Use1p2MW", this);
     G4String guidance("If true, the NUMI-style target will be adapted for 1.2 MW operation, prelimnary design.   \n  ");
     guidance += std::string(" See DocDB note 8639-v1 by J. Hylen  \n ");
     fUse1p2MW->SetGuidance(guidance);
     fUse1p2MW->SetParameterName("Use1p2MW",true);
     bool value = volP->GetUse1p2MW(); //  
     fUse1p2MW->SetDefaultValue(value);
   }
   { 
     fUseRoundedTargetFins = new G4UIcmdWithABool("/LBNE/det/UseRoundedTargetFins", this);
     G4String guidance("If true, the vertical fins will rounded. Only implemented for 1.2 MW option. Default is true.    \n  ");
     guidance += std::string(" See IHEP target documentation, serial number 3, received on March 26 from J. Hylen  \n ");
     fUseRoundedTargetFins->SetGuidance(guidance);
     fUseRoundedTargetFins->SetParameterName("UseRoundedTargetFins",true);
     bool value = volP->GetUseRoundedTargetFins(); //  
     fUseRoundedTargetFins->SetDefaultValue(value);
   }
   //
   // September 2014: adding the arbitrary shaped Horn1 (G4Polycone type )
   //
   {
     fUseHorn1Polycone = new G4UIcmdWithABool("/LBNE/det/UseHorn1Polycone", this);
     G4String guidance("If true, Horn1 Inner conductor is a simple Polycone. See documentation on the g4lbne wiki for usage.    \n  ");
     fUseHorn1Polycone->SetGuidance(guidance);
     fUseHorn1Polycone->SetParameterName("UseHorn1Polycone",true);
     bool value = volP->GetUseHorn1Polycone(); // 
     fUseHorn1Polycone->SetDefaultValue(value);
   }
   {
     fUseHorn1PolyNumInnerPts = new G4UIcmdWithAnInteger("/LBNE/det/NumInnerPtsHorn1Polycone", this);
     G4String guidance("Number of inner points defining the Inner conductor. See documentation on the g4lbne wiki for usage.    \n  ");
     fUseHorn1PolyNumInnerPts->SetGuidance(guidance);
     fUseHorn1PolyNumInnerPts->SetParameterName("NumInnerPtsHorn1Polycone",true);
     fUseHorn1PolyNumInnerPts->SetDefaultValue(10);
   }
   {
     const size_t numCmdsHorn1PolyPts = 20;
     fHorn1PolyListRinThickZVects.resize(numCmdsHorn1PolyPts);
     G4String guidance("Inner Conductor pts, rInner, thicknes,, Zlocation See documentation on the g4lbne wiki for usage.      \n  ");
     for (size_t k=0; k !=numCmdsHorn1PolyPts; k++) {
       std::ostringstream tmpStrStr; 
       tmpStrStr << "/LBNE/det/Horn1PolyPt" << k << "RinThickZ";
       G4String tmpStr(tmpStrStr.str());
       fHorn1PolyListRinThickZVects[k] = new G4UIcmdWith3VectorAndUnit(tmpStr.c_str(), this);
       fHorn1PolyListRinThickZVects[k]->SetGuidance(guidance);
       fHorn1PolyListRinThickZVects[k]->SetParameterName("Horn1PolyPtxxxRin", "Horn1PolyPtxxxThick", "Horn1PolyPtxxxZCoord", true);
       fHorn1PolyListRinThickZVects[k]->SetDefaultUnit ("mm");
       fHorn1PolyListRinThickZVects[k]->SetUnitCandidates ("mm cm m");
     }
   }
   {
     fHorn1PolyOuterRadius =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn1PolyOuterRadius", this);
     G4String guidance("Horn1 Polycone Outer Radius conductor. See documentation on the g4lbne wiki for usage.    \n  ");
     fHorn1PolyOuterRadius->SetGuidance(guidance);
     fHorn1PolyOuterRadius->SetParameterName("Horn1PolyOuterRadius", true);
     bool value = volP->GetHorn1PolyOuterRadius(); // 
     fHorn1PolyOuterRadius->SetDefaultValue(value);
     fHorn1PolyOuterRadius->SetDefaultUnit ("mm");
     fHorn1PolyOuterRadius->SetUnitCandidates ("mm cm m");
   }
   //
   // August 2015: We add an arbitrary number of G4PolyCones, up to 5 of them..
   //
   {
      fUseNumberOfHornsPoly = new G4UIcmdWithAnInteger("/LBNE/det/NumberOfHornsPolycone", this);
      G4String guidance("Number of simplified Horns, type Polycones. See documentation on the g4lbne wiki for usage.    \n  ");
      fUseNumberOfHornsPoly->SetGuidance(guidance);
      fUseNumberOfHornsPoly->SetParameterName("NumberOfHornsPolycone",true);
      fUseNumberOfHornsPoly->SetDefaultValue(0);
   }
   //
   // October/November 2015, John LoSecco, P. L., adding the possibility of parabolic
   //  horns. 
   //
   {
      fSetPolyconeHornParabolic = new G4UIcmdWithAnInteger("/LBNE/det/SetPolyconeHornParabolic", this);
      G4String guidance("Set the Polycone horn's hape to parabolic.  Technically, still a G4Polycone shape \n");
      guidance += std::string(" but, linear segment radial increment of one mm, following a parabola, linear within 1 mm.    \n  ");
      guidance += std::string(" Valid Argument, integer, between 1 and 5 \n  ");
      fSetPolyconeHornParabolic->SetGuidance(guidance);
      fSetPolyconeHornParabolic->SetParameterName("SetPolyconeHornParabolic",true);
      fSetPolyconeHornParabolic->SetDefaultValue(0);
      
   }
   // Brute force.. Embarrasingly lengthy, horns by horns, 2 to 5, inclusive. 
   // Horn2 
   {
     fUseHorn2PolyNumInnerPts = new G4UIcmdWithAnInteger("/LBNE/det/NumInnerPtsHorn2Polycone", this);
     G4String guidance("Number of inner points defining the Inner conductor, for Horn2.  \n  ");
     fUseHorn2PolyNumInnerPts->SetGuidance(guidance);
     fUseHorn2PolyNumInnerPts->SetParameterName("NumInnerPtsHorn2Polycone",true);
     fUseHorn2PolyNumInnerPts->SetDefaultValue(10);
   }
   {
     const size_t numCmdsHorn2PolyPts = 20;
     fHorn2PolyListRinThickZVects.resize(numCmdsHorn2PolyPts);
     G4String guidance("Inner Conductor pts, rInner, thicknes,, for Horn2     \n  ");
     for (size_t k=0; k !=numCmdsHorn2PolyPts; k++) {
       std::ostringstream tmpStrStr; 
       tmpStrStr << "/LBNE/det/Horn2PolyPt" << k << "RinThickZ";
       G4String tmpStr(tmpStrStr.str());
       fHorn2PolyListRinThickZVects[k] = new G4UIcmdWith3VectorAndUnit(tmpStr.c_str(), this);
       fHorn2PolyListRinThickZVects[k]->SetGuidance(guidance);
       fHorn2PolyListRinThickZVects[k]->SetParameterName("Horn2PolyPtxxxRin", "Horn2PolyPtxxxThick", "Horn2PolyPtxxxZCoord", true);
       fHorn2PolyListRinThickZVects[k]->SetDefaultUnit ("mm");
       fHorn2PolyListRinThickZVects[k]->SetUnitCandidates ("mm cm m");
     }
   }
   {
     fHorn2PolyOuterRadius =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn2PolyOuterRadius", this);
     G4String guidance("Horn2 Polycone Outer Radius conductor, for Horn2    \n  ");
     fHorn2PolyOuterRadius->SetGuidance(guidance);
     fHorn2PolyOuterRadius->SetParameterName("Horn2PolyOuterRadius", true);
     bool value = 0.; // 
     fHorn2PolyOuterRadius->SetDefaultValue(value);
     fHorn2PolyOuterRadius->SetDefaultUnit ("mm");
     fHorn2PolyOuterRadius->SetUnitCandidates ("mm cm m");
   }
   {
     fHorn2PolyZStartPos =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn2PolyZStartPos", this);
     G4String guidance("Horn2 Polycone Starting Position (Z of the first inner coord, in G4 world syst., for Horn2    \n  ");
     fHorn2PolyZStartPos->SetGuidance(guidance);
     fHorn2PolyZStartPos->SetParameterName("Horn2PolyZStartPos", true);
     bool value = -5.0e3*CLHEP::m; // 
     fHorn2PolyZStartPos->SetDefaultValue(value);
     fHorn2PolyZStartPos->SetDefaultUnit ("mm");
     fHorn2PolyZStartPos->SetUnitCandidates ("mm cm m");
   }
   // 
   // Horn3 
   //
    {
     fUseHorn3PolyNumInnerPts = new G4UIcmdWithAnInteger("/LBNE/det/NumInnerPtsHorn3Polycone", this);
     G4String guidance("Number of inner points defining the Inner conductor, for Horn3.  \n  ");
     fUseHorn3PolyNumInnerPts->SetGuidance(guidance);
     fUseHorn3PolyNumInnerPts->SetParameterName("NumInnerPtsHorn3Polycone",true);
     fUseHorn3PolyNumInnerPts->SetDefaultValue(10);
   }
   {
     const size_t numCmdsHorn3PolyPts = 20;
     fHorn3PolyListRinThickZVects.resize(numCmdsHorn3PolyPts);
     G4String guidance("Inner Conductor pts, rInner, thicknes,, for Horn3     \n  ");
     for (size_t k=0; k !=numCmdsHorn3PolyPts; k++) {
       std::ostringstream tmpStrStr; 
       tmpStrStr << "/LBNE/det/Horn3PolyPt" << k << "RinThickZ";
       G4String tmpStr(tmpStrStr.str());
       fHorn3PolyListRinThickZVects[k] = new G4UIcmdWith3VectorAndUnit(tmpStr.c_str(), this);
       fHorn3PolyListRinThickZVects[k]->SetGuidance(guidance);
       fHorn3PolyListRinThickZVects[k]->SetParameterName("Horn3PolyPtxxxRin", "Horn3PolyPtxxxThick", "Horn3PolyPtxxxZCoord", true);
       fHorn3PolyListRinThickZVects[k]->SetDefaultUnit ("mm");
       fHorn3PolyListRinThickZVects[k]->SetUnitCandidates ("mm cm m");
     }
   }
   {
     fHorn3PolyOuterRadius =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn3PolyOuterRadius", this);
     G4String guidance("Horn3 Polycone Outer Radius conductor, for Horn3    \n  ");
     fHorn3PolyOuterRadius->SetGuidance(guidance);
     fHorn3PolyOuterRadius->SetParameterName("Horn3PolyOuterRadius", true);
     fHorn3PolyOuterRadius->SetDefaultValue(75.0*CLHEP::cm);
     fHorn3PolyOuterRadius->SetDefaultUnit ("mm");
     fHorn3PolyOuterRadius->SetUnitCandidates ("mm cm m");
   }
   {
     fHorn3PolyZStartPos =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn3PolyZStartPos", this);
     G4String guidance("Horn3 Polycone Starting Position (Z of the first inner coord, in G4 world syst., for Horn3    \n  ");
     fHorn3PolyZStartPos->SetGuidance(guidance);
     fHorn3PolyZStartPos->SetParameterName("Horn3PolyZStartPos", true);
     bool value = -5.0e3*CLHEP::m; // 
     fHorn3PolyZStartPos->SetDefaultValue(value);
     fHorn3PolyZStartPos->SetDefaultUnit ("mm");
     fHorn3PolyZStartPos->SetUnitCandidates ("mm cm m");
   }
   //
   // Horn4 
   //
    {
     fUseHorn4PolyNumInnerPts = new G4UIcmdWithAnInteger("/LBNE/det/NumInnerPtsHorn4Polycone", this);
     G4String guidance("Number of inner points defining the Inner conductor, for Horn4.  \n  ");
     fUseHorn4PolyNumInnerPts->SetGuidance(guidance);
     fUseHorn4PolyNumInnerPts->SetParameterName("NumInnerPtsHorn4Polycone",true);
     fUseHorn4PolyNumInnerPts->SetDefaultValue(10);
   }
   {
     const size_t numCmdsHorn4PolyPts = 20;
     fHorn4PolyListRinThickZVects.resize(numCmdsHorn4PolyPts);
     G4String guidance("Inner Conductor pts, rInner, thicknes,, for Horn4     \n  ");
     for (size_t k=0; k !=numCmdsHorn4PolyPts; k++) {
       std::ostringstream tmpStrStr; 
       tmpStrStr << "/LBNE/det/Horn4PolyPt" << k << "RinThickZ";
       G4String tmpStr(tmpStrStr.str());
       fHorn4PolyListRinThickZVects[k] = new G4UIcmdWith3VectorAndUnit(tmpStr.c_str(), this);
       fHorn4PolyListRinThickZVects[k]->SetGuidance(guidance);
       fHorn4PolyListRinThickZVects[k]->SetParameterName("Horn4PolyPtxxxRin", "Horn4PolyPtxxxThick", "Horn4PolyPtxxxZCoord", true);
       fHorn4PolyListRinThickZVects[k]->SetDefaultUnit ("mm");
       fHorn4PolyListRinThickZVects[k]->SetUnitCandidates ("mm cm m");
     }
   }
   {
     fHorn4PolyOuterRadius =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn4PolyOuterRadius", this);
     G4String guidance("Horn4 Polycone Outer Radius conductor, for Horn4    \n  ");
     fHorn4PolyOuterRadius->SetGuidance(guidance);
     fHorn4PolyOuterRadius->SetParameterName("Horn4PolyOuterRadius", true);
     fHorn4PolyOuterRadius->SetDefaultValue(75.0*CLHEP::cm);
     fHorn4PolyOuterRadius->SetDefaultUnit ("mm");
     fHorn4PolyOuterRadius->SetUnitCandidates ("mm cm m");
   }
   {
     fHorn4PolyZStartPos =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn4PolyZStartPos", this);
     G4String guidance("Horn4 Polycone Starting Position (Z of the first inner coord, in G4 world syst., for Horn4    \n  ");
     fHorn4PolyZStartPos->SetGuidance(guidance);
     fHorn4PolyZStartPos->SetParameterName("Horn4PolyZStartPos", true);
     bool value = -5.0e3*CLHEP::m; // 
     fHorn4PolyZStartPos->SetDefaultValue(value);
     fHorn4PolyZStartPos->SetDefaultUnit ("mm");
     fHorn4PolyZStartPos->SetUnitCandidates ("mm cm m");
   }
   //
   // Horn5 
   //
    {
     fUseHorn5PolyNumInnerPts = new G4UIcmdWithAnInteger("/LBNE/det/NumInnerPtsHorn5Polycone", this);
     G4String guidance("Number of inner points defining the Inner conductor, for Horn5.  \n  ");
     fUseHorn5PolyNumInnerPts->SetGuidance(guidance);
     fUseHorn5PolyNumInnerPts->SetParameterName("NumInnerPtsHorn5Polycone",true);
     fUseHorn5PolyNumInnerPts->SetDefaultValue(10);
   }
   {
     const size_t numCmdsHorn5PolyPts = 20;
     fHorn5PolyListRinThickZVects.resize(numCmdsHorn5PolyPts);
     G4String guidance("Inner Conductor pts, rInner, thicknes,, for Horn5     \n  ");
     for (size_t k=0; k !=numCmdsHorn5PolyPts; k++) {
       std::ostringstream tmpStrStr; 
       tmpStrStr << "/LBNE/det/Horn5PolyPt" << k << "RinThickZ";
       G4String tmpStr(tmpStrStr.str());
       fHorn5PolyListRinThickZVects[k] = new G4UIcmdWith3VectorAndUnit(tmpStr.c_str(), this);
       fHorn5PolyListRinThickZVects[k]->SetGuidance(guidance);
       fHorn5PolyListRinThickZVects[k]->SetParameterName("Horn5PolyPtxxxRin", "Horn5PolyPtxxxThick", "Horn5PolyPtxxxZCoord", true);
       fHorn5PolyListRinThickZVects[k]->SetDefaultUnit ("mm");
       fHorn5PolyListRinThickZVects[k]->SetUnitCandidates ("mm cm m");
     }
   }
   {
     fHorn5PolyOuterRadius =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn5PolyOuterRadius", this);
     G4String guidance("Horn5 Polycone Outer Radius conductor, for Horn5    \n  ");
     fHorn5PolyOuterRadius->SetGuidance(guidance);
     fHorn5PolyOuterRadius->SetParameterName("Horn5PolyOuterRadius", true);
     fHorn5PolyOuterRadius->SetDefaultValue(75.0*CLHEP::cm);
     fHorn5PolyOuterRadius->SetDefaultUnit ("mm");
     fHorn5PolyOuterRadius->SetUnitCandidates ("mm cm m");
   }
   {
     fHorn5PolyZStartPos =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/Horn5PolyZStartPos", this);
     G4String guidance("Horn5 Polycone Starting Position (Z of the first inner coord, in G4 world syst., for Horn5    \n  ");
     fHorn5PolyZStartPos->SetGuidance(guidance);
     fHorn5PolyZStartPos->SetParameterName("Horn5PolyZStartPos", true);
     bool value = -5.0e3*CLHEP::m; // 
     fHorn5PolyZStartPos->SetDefaultValue(value);
     fHorn5PolyZStartPos->SetDefaultUnit ("mm");
     fHorn5PolyZStartPos->SetUnitCandidates ("mm cm m");
   }
  
      
   //
   // October 2014: Adding the option of removing the decay pipe snout to leave more room for extend horns
   //
   {
     fRemoveDecayPipeSnout = new G4UIcmdWithABool("/LBNE/det/RemoveDecayPipeSnout", this);
     G4String guidance("If true, We will not place the decay pipe snout in fron of the entrance of decay pipe    \n  ");
     fRemoveDecayPipeSnout->SetGuidance(guidance);
     fRemoveDecayPipeSnout->SetParameterName("RemoveDecayPipeSnout",true);
     bool value = volP->GetRemoveDecayPipeSnout(); // 
     fRemoveDecayPipeSnout->SetDefaultValue(value);
   }
   //
   // March 2015: Adding a wire all the way down the beam pipe. 
   //
   {
     fRadiusMilindWire =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/MillindWireRadius", this);
     G4String guidance("Set the radius of the write that runs down the beam pipe.     \n  ");
     fRadiusMilindWire->SetGuidance(guidance);
     fRadiusMilindWire->SetParameterName("MillindWireRadius", true);
     bool value = volP->GetRadiusMilindWire(); // 
     fRadiusMilindWire->SetDefaultValue(value);
     fRadiusMilindWire->SetDefaultUnit ("mm");
     fRadiusMilindWire->SetUnitCandidates ("mm cm");
   }
   {
     fCurrentMilindWire =  new G4UIcmdWithADoubleAndUnit("/LBNE/det/MillindWireCurrent", this);
     G4String guidance("Set the Current of the write that runs down the beam pipe.     \n  ");
     fCurrentMilindWire->SetGuidance(guidance);
     fCurrentMilindWire->SetParameterName("MillindWireCurrent", true);
     bool value = volP->GetCurrentMilindWire(); // 
     fCurrentMilindWire->SetDefaultValue(value);
     fCurrentMilindWire->SetDefaultUnit ("A");
     fCurrentMilindWire->SetUnitCandidates ("A");
   }
   // January 2016: Make writing out a gdml file easier:
   {
     fWriteGDMLFile = new G4UIcmdWithABool("/LBNE/det/WriteGDMLFile", this);
     G4String guidance("Write a GDML file containing the entire tunnel geometry    \n  ");
     fWriteGDMLFile->SetGuidance(guidance);
     fWriteGDMLFile->SetParameterName("WriteGDMLFile",true);
     fWriteGDMLFile->SetDefaultValue(false);
   }


   // John Back, March 2016: target module (e.g. SAT) options
   {
       fUseTargetModule = new G4UIcmdWithABool("/LBNE/det/UseTargetModule", this);
       G4String guidance("If true, the NuMI target is replaced by the Target Module\n");
       fUseTargetModule->SetGuidance(guidance);
       fUseTargetModule->SetParameterName("UseTargetModule", true);
       bool value = volP->GetUseTargetModule(); //defined in LBNEVolumePlacements.hh
       fUseTargetModule->SetDefaultValue(value);
   }
   {
       fTargetModuleType = new G4UIcmdWithAnInteger("/LBNE/det/TargetModuleType", this);
       G4String guidance("Set the type for the target module: SAT = 0, Cylinder = 1, TwoCylinder = 2\n");
       fTargetModuleType->SetGuidance(guidance);
       fTargetModuleType->SetParameterName("TargetModuleType", true);
       fTargetModuleType->SetDefaultValue(0);
   }
   {
       fTargetRadius = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetRadius", this);
       G4String guidance("Target object radius\n  ");
       guidance += std::string(" Should the option TargetRadius be selected (UseTargetModule=true), \n ");
       guidance += std::string(" this command sets the radius of each target object. \n");
       guidance += std::string(" No FNAL Drawing number referenced  " ); 
       fTargetRadius->SetGuidance(guidance);
       fTargetRadius->SetParameterName("TargetRadius",true);
       double value = volP->GetTargetRadius(); //  
       SetMyUnitsAndConditions(fTargetRadius, value);
   }
   {
       fTargetLength = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetLength", this);
       G4String guidance("The length of the target object\n  ");
       guidance += std::string(" Should the option TargetLength be selected (UseTargetModule=true), \n ");
       guidance += std::string(" this command sets the length of the target object.\n");
       guidance += std::string(" No FNAL Drawing number referenced  " ); 
       fTargetLength->SetGuidance(guidance);
       fTargetLength->SetParameterName("TargetLength",true);
       double value = volP->GetTargetLength(); //  
       SetMyUnitsAndConditions(fTargetLength, value);
   }
   {
       fTargetNLambda = new G4UIcmdWithADouble("/LBNE/det/TargetNLambda", this);
       G4String guidance("Specify the target object length using nuclear interaction length lambda\n  ");
       guidance += std::string(" Should the option TargetNLambda be selected (UseTargetModule=true), \n ");
       guidance += std::string(" this command sets the length of the target object.\n");
       guidance += std::string(" No FNAL Drawing number referenced  " ); 
       fTargetNLambda->SetGuidance(guidance);
       fTargetNLambda->SetParameterName("TargetNLambda",true);
       double value = volP->GetTargetNLambda();
       fTargetNLambda->SetDefaultValue(value);
   }
   {
       fTargetFracOutHornL = new G4UIcmdWithADouble("/LBNE/det/TargetFracOutHornL", this);
       G4String guidance("The fractional length of the target outside Horn1 \n  ");
       guidance += std::string(" Should the option TargetFracOutHornL be selected (UseTargetModule=true) , \n ");
       guidance += std::string(" this command will update the length of the target outside Horn1.\n");
       guidance += std::string(" No FNAL Drawing number referenced  " ); 
       fTargetFracOutHornL->SetGuidance(guidance);
       fTargetFracOutHornL->SetParameterName("TargetFracOutHornL",true);
       double value = volP->GetTargetFracOutHornL();
       fTargetFracOutHornL->SetDefaultValue(value);
   }
   {
       fTargetLengthOutsideExtra = new G4UIcmdWithADoubleAndUnit("/LBNE/det/TargetLengthOutsideExtra", this);
       G4String guidance("Set TargetLengthOutsideExtra to align the target if required\n  ");
       fTargetLengthOutsideExtra->SetGuidance(guidance);
       fTargetLengthOutsideExtra->SetParameterName("TargetLengthOutsideExtra",true);
       double value = volP->GetTargetLengthOutsideExtra(); //  
       SetMyUnitsAndConditions(fTargetLengthOutsideExtra, value);
   }

  
}
// Just to avoid code bloat.. 

void LBNEPlacementMessenger::SetMyUnitsAndConditions(G4UIcmdWithADoubleAndUnit *cmd, double value) {

    cmd->SetDefaultValue (value);
    cmd->SetDefaultUnit ("m");
    cmd->SetUnitCandidates ("cm m");
    cmd->AvailableForStates(G4State_PreInit);
}
void LBNEPlacementMessenger::SetNewValue(G4UIcommand* command,  G4String newValue) {
   LBNEVolumePlacements* volP=LBNEVolumePlacements::Instance();
   if (command == fWaterLayerThickInHorn) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetWaterLayerThickInHorns(cmdWD->GetNewDoubleValue(newValue));
//     std::cerr << " Changed WaterLayerThickInHorns , o.k. " << std::endl;  // exit(2);
   }
   if (command == fDecayPipeLength) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetDecayPipeLength(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fDecayPipeRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetDecayPipeRadius(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fDecayPipeLongPosition) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetDecayPipeLongPosition(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fDecayPipeUpstreamWindowThickness) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetDecayPipeUpstrWindowThick(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fDecayPipeGas) {
     volP->SetDecayPipeGas(newValue);
   }
   if (command == fHorn1Length) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     G4Exception("LBNEPlacementMessenger::SetNewValue ", " ", FatalErrorInArgument,
           " Obsolete data card, use Horn1LongRescale instead ");
     volP->SetHorn1Length(cmdWD->GetNewDoubleValue(newValue));
   }
    
   if (command == fTargetSLengthGraphite) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetTargetSLengthGraphite(cmdWD->GetNewDoubleValue(newValue));
     volP->SetSimpleTargetLength(cmdWD->GetNewDoubleValue(newValue)); // Used only if Simple target used.
     volP->SetMultiSphereTargetLength(cmdWD->GetNewDoubleValue(newValue));//Quynh. real target.(4). should i delete the above line?
     volP->SegmentTarget();
   }
   if (command == fTargetFinWidth) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetTargetFinWidth(cmdWD->GetNewDoubleValue(newValue));
     volP->SegmentTarget();
   }
   if (command == fTargetDensity) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetTargetDensity(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fTargetBerylCapThickness) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     std::cout << " !!! You have chosen a unusual thickness for the target He tube cap " 
              << newValue << " \n  .... Please check that the volume don't overlap in the listing..  " << std::endl;
     volP->SetTargetBerylDownstrWindowThick(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fTargetMaterial) {
     std::cout << " !!! You have chosen to use " << newValue << " as target material " << std::endl;
     volP->SetTargetMaterialName(newValue);
   }
   if (command == fPlugMaterial) {
     std::cout << " !!! You have chosen to use " << newValue << " as 2n target (or plug) material " << std::endl;
     volP->SetPlugMaterial(newValue);
   }
   if (command == fTargetLengthIntoHorn) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     std::cout << " !!! You have chosen the seemingly confusing way to define the length of the target which \n "
               << " the horn.  This command should be deprecated. Please use TargetLengthOutsideHorn " << std::endl;     
     volP->SetTargetLengthIntoHorn(cmdWD->GetNewDoubleValue(newValue));
     volP->SegmentTarget();
   }
   if (command == fTargetLengthOutsideHorn) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetTargetLengthOutsideHorn(cmdWD->GetNewDoubleValue(newValue) );
     /*
     ** Obsolete, August - Sept 2014. Assume from now on 
     double valueL = volP->GetTargetSLengthGraphite(); //
     const double lengthReturnFlow = 25.3*CLHEP::mm; //  empirically set.. Needs improvement. Must use variable in VolumePlacements. 
           volP->SetTargetLengthOutsideHorn(cmdWD->GetNewDoubleValue(newValue) );
     double valIH =  valueL - cmdWD->GetNewDoubleValue(newValue) + lengthReturnFlow;
     // Not sure what to do for the multiSphere target. Need more info about the extra space for return 
     // flow of Helium.  Also, study precedence of data cards!!!! P.L., August 2014. 
     if (valIH <  -2*CLHEP::mm) {
       if (!volP->GetUse1p2MW()) {
          std::ostringstream mStrStr;
          mStrStr << " The target is completely retracted from Horn1. We assume a \"Nova-like\" configuration. \n " 
	           << " However, the 700 kW target is selected. Unsupported for now.... Fatal ";
          G4String mStr(mStrStr.str());
          G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str()); 
          
       } else {
         if (volP->GetHorn1MotherIsPolycone()) { // Hopefully obsolete. 
           std::cout << " The target is completely retracted from Horn1. We assume a \"Nova-like\" configuration. \n " 
	           << "  ........ valIH " << valIH << " target Length " << valueL << std::endl;
           volP->SetTargetLengthOutsideHorn(cmdWD->GetNewDoubleValue(newValue) );
	   volP->SetUsePseudoNova(true);
	 } else { 
	   volP->SetUsePseudoNova(false); // We no longer use this option, it stays false 
	 } 	   
       } 
     }
     else if ((!volP->GetHorn1MotherIsPolycone()) && (valIH <  (lengthReturnFlow + 5.0*CLHEP::mm)) && ( valIH > -2.0*CLHEP::mm)) {
          std::ostringstream mStrStr;
          mStrStr << " The target is almost completely retracted from Horn1. " <<
	          " The Upstream/Downstream arbitrary split cuts into the target end.  " 
	           << " Unsupported for now.... Fatal ";
          G4String mStr(mStrStr.str());
          G4Exception("LBNEVolumePlacements::Create", " ", FatalErrorInArgument, mStr.c_str());
     }          
//     volP->SetTargetLengthIntoHorn(valIH);
     // to have the first fin target at coord 0.3mm, to follow the convention. 
//     volP->SegmentTarget();
*/
   }
//Quynh, August 2014. 
   if (command == fUseMultiSphereTarget) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetUseMultiSphereTarget(cmdB->GetNewBoolValue(newValue.c_str()));
     G4String aNameM("Beryllium");
     volP->SetTargetMaterialName(aNameM);
     volP->AdaptForMultiSphereTarget();
   }
   if (command == fUseSimpleTargetBox) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetUseSimpleTargetBox(cmdB->GetNewBoolValue(newValue.c_str()));
     volP->SegmentTarget(); // To update the much simplified target... 
   }
    if (command == fRemoveTargetAlltogether) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetRemoveTargetAlltogether(cmdB->GetNewBoolValue(newValue.c_str()));
   }
  
   
   if (command == fUseSimpleTargetCylinder) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetUseSimpleTargetCylinder(cmdB->GetNewBoolValue(newValue.c_str()));
     volP->SegmentTarget(); // To update the much simplified target... 
   }
   if (command == fSimpleTargetRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     if (!volP->GetUseSimpleTargetCylinder()) {
       std::cout << " !!! You are channging the radius of the simple cylindrical target, \n, but the " <<  
        " NUMI-style target is still installed. Consider using first the GUI command UseSimpleCylindricalTarget \n";
     } else {
       std::cerr << " Setting new target radius to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
     } 
     volP->SetSimpleTargetRadius(cmdWD->GetNewDoubleValue(newValue)); 
     volP->SegmentTarget(); // To update the much simplified target... 
   }
   if (command == fSimpleTargetLength) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     if (!volP->GetUseSimpleTargetCylinder()) {
       std::cout << " !!! You are channging the length of the simple cylindrical target, \n, but the " <<  
        " NUMI-style target is still installed. Consider using first the GUI command UseSimpleCylindricalTarget \n";
     } else {
       std::cerr << " Setting new targetlength to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
     } 
     volP->SetSimpleTargetLength(cmdWD->GetNewDoubleValue(newValue)); 
     volP->SegmentTarget(); // To update the much simplified target... 
   }
   if (command == fMultiSphereTargetRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     if (!volP->GetUseMultiSphereTarget()) {
       std::cout << " !!! You are channging the radius of the multi sphere target, \n, but the " <<  
        " NUMI-style target is still installed. Consider using first the GUI command UseMultiSphereTarget \n";
     } else {
       std::cerr << " Setting new target sphere radii to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
     } 
     volP->SetMultiSphereTargetRadius(cmdWD->GetNewDoubleValue(newValue)); 
     volP->AdaptForMultiSphereTarget();
   }
   if (command == fSimpleTargetWidth) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     if (!volP->GetUseSimpleTargetBox()) {
       std::cout << " !!! You are channging the width of the simple box-like target, \n, but the " <<  
        " NUMI-style target is still installed. Consider using first the GUI command UseSimpleBoxTarget \n";
     } 
     volP->SetSimpleTargetWidth(cmdWD->GetNewDoubleValue(newValue)); 
     volP->SegmentTarget(); // To update the much simplified target... 
   }
   if (command == fSimpleTargetHeight) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     if (!volP->GetUseSimpleTargetBox()) {
       std::cout << " !!! You are channging the height of the simple box-like target, \n, but the " <<  
        " NUMI-style target is still installed. Consider using first the GUI command UseSimpleBoxTarget \n";
     } 
     volP->SetSimpleTargetHeight(cmdWD->GetNewDoubleValue(newValue)); 
     volP->SegmentTarget(); // To update the much simplified target... 
   }
   
   if (command == fHorn1InnerCondMat) {
    std::cout << " You are now using non-standard material " << newValue 
              << " for the Horn1 Inner Conductor material " << std::endl;
    volP->SetHorn1InnerConductorMaterial(newValue);
   }
   if (command == fHorn1AllCondMat) {
    std::cout << " You are now using non-standard material " << newValue 
              << " for all of the Horn1 Conductors (inner, outer, I/O transition.. " << std::endl;
    volP->SetHorn1InnerConductorMaterial(newValue);
    volP->SetHorn1AllConductorMaterial(newValue);
   }
   if (command == fHorn1RadialSafetyMargin ) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     const double val =   cmdWD->GetNewDoubleValue(newValue);    
     std::cout << " You are changing the safety margin in the minimum distance between \n";
     std::cout << " the downstream tip of the target and Horn1 inner conductor, from \n"; 
     std::cout << " the default of 2.5 mm down to " << val << std::endl;
     if (val < 0.) {
      G4Exception("LBNEPlacementMessenger::SetNewValue ", " ", FatalErrorInArgument,
           " The value the Horn radial safet margin can not be nergative ");
     }      
     volP->SetHorn1RadialSafetyMargin(val);
   }
   if (command == fHorn1RadiusBigEnough ) {
     G4UIcmdWithABool* cmdWD = dynamic_cast<G4UIcmdWithABool*> (command);
     const bool val =   cmdWD->GetNewBoolValue(newValue);    
     std::cout << " The inner conductor radius will be set large enough to accomodate \n";
     std::cout << " the Helium filled Target container vessel, up the ! 1 m. into the horn1.  \n"; 
     std::cout << " The default is false  " << val << std::endl;
     volP->SetHorn1RadiusBigEnough(val);
   }
   
   if (command == fHorn1RadialRescale) {
     G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
     volP->SetHorn1RadialRescale(cmdWD->GetNewDoubleValue(newValue));
     volP->RescaleHorn1Radially();
   }
   if (command == fHorn1LongRescale) {
     G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
     volP->SetHorn1LongRescale(cmdWD->GetNewDoubleValue(newValue));
     volP->RescaleHorn1Lengthwise();
   }
   if (command == fHorn2RadialRescale) {
     G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
     volP->SetHorn2RadialRescale(cmdWD->GetNewDoubleValue(newValue));
     volP->RescaleHorn2Radially();
   }
   if (command == fHorn2RadialRescaleCst) {
     G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
     volP->SetHorn2RadialRescaleCst(cmdWD->GetNewDoubleValue(newValue));
     volP->ShiftHorn2Radially();
   }
   if (command == fHorn2LongRescale) {
     G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
     volP->SetHorn2LongRescale(cmdWD->GetNewDoubleValue(newValue));
     volP->RescaleHorn2Lengthwise();
   }
   if (command == fHorn2InnerCondMat) {
    std::cout << " You are now using non-standard material " << newValue 
              << " for the Horn2 Inner Conductor material " << std::endl;
    volP->SetHorn2InnerConductorMaterial(newValue);
   }
   if (command == fHorn2AllCondMat) {
    std::cout << " You are now using non-standard material " << newValue 
              << " for all of the Horn2 Conductor material " << std::endl;
    volP->SetHorn2InnerConductorMaterial(newValue);
    volP->SetHorn2AllConductorMaterial(newValue);
   }
   if (command == fHorn2LongPosition) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHorn2LongPosition(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fUseMarsTargetHorns) {
     volP->SetUseMarsTargetHorns(newValue);
   }

   if (command == fMarsTargetHornsGDMLFilename) {
          volP->SetMarsTargetHornsGDMLFilename(newValue);
   }
   if (command == fAbsorberGDMLFilename) {
          volP->SetAbsorberGDMLFilename(newValue);
   }
   if (command == fLengthOfRockDownstr) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetLengthOfRockDownstreamAlcove(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fInstallShield) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetDoInstallShield(cmdB->GetNewBoolValue(newValue.c_str()));
   }
   if (command == fConstructPlug) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetConstructPlug(cmdB->GetNewBoolValue(newValue.c_str()));
   }
   if (command == fConstructPlugInHorn1) {
     G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetConstructPlugInHorn1(cmdB->GetNewBoolValue(newValue.c_str()));
   }
   if (command == fPlugLength) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetPlugLength(cmdWD->GetNewDoubleValue(newValue));
   }
   // 1.2 MW option. 
   if (command == fUse1p2MW) {
      G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
      volP->SetUse1p2MW(cmdB->GetNewBoolValue(newValue.c_str()));
      std::cout << " You are now using the 1.2 MW design, first version, Feb. 2014. " << std::endl;
      std::cout << " See LBNE-doc-8639-v1 " << std::endl;
      if (volP->GetUse1p2MW()) {
          volP->adaptTargetFor1p2MW();
	  volP->SegmentTarget();
      }
   }
   if (command == fUseRoundedTargetFins) {
      G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
      volP->SetUseRoundedTargetFins(cmdB->GetNewBoolValue(newValue.c_str()));
      if (cmdB->GetNewBoolValue(newValue.c_str())) 
          std::cout << " You are now using the Rounded fins target, at it should be  " << std::endl;
      else std::cout << " You are now using thestraight edge fins for the target, " << std::endl << 
                         "  .... you should probably reduce the fins width a few % (1p2MW default thickness) " << std::endl;  
   }
   if (command == fPlugOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetPlugOuterRadius(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fPlugInnerRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetPlugInnerRadius(cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fPlugZPosition) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetPlugZPosition(cmdWD->GetNewDoubleValue(newValue));
   }
   //
   // September 2014: Install the custom Horn1 Polycone. 
   //
   if (command == fUseHorn1Polycone) {
     G4UIcmdWithABool* cmdWD = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetUseHorn1Polycone(cmdWD->GetNewBoolValue(newValue));
   }
   if (command == fUseHorn1PolyNumInnerPts) {
     G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
     volP->SetUseHorn1PolyNumInnerPts(cmdWD->GetNewIntValue(newValue));
     // August 2015:  Also update the new set of paramters... 
     volP->SetUseHornsPolyNumInnerPts(1, cmdWD->GetNewIntValue(newValue));
   }
   if (command == fHorn1PolyOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHorn1PolyOuterRadius(cmdWD->GetNewDoubleValue(newValue));
     volP->SetHornsPolyOuterRadius(1, cmdWD->GetNewDoubleValue(newValue));
   }
   for (size_t kCmd=0; kCmd != fHorn1PolyListRinThickZVects.size(); kCmd++) { 
     if (command == fHorn1PolyListRinThickZVects[kCmd]) {
       G4UIcmdWith3VectorAndUnit* cmdWD = dynamic_cast<G4UIcmdWith3VectorAndUnit*> (command);
       G4ThreeVector v = cmdWD->GetNew3VectorValue(newValue);
       volP->SetHorn1PolyInnerThreeVect(kCmd, v);
       volP->SetHornsPolyInnerThreeVect(1, kCmd, v);
       break;
     }
  }
  //
  // August 2015.. 
  //
  if (command == fUseNumberOfHornsPoly) {
    G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
    const int nn = cmdWD->GetNewIntValue(newValue);
    volP->SetNumberOfHornsPolycone(nn);
  }
  //
  // October 2015.. John LoSecco, P.L. 
  //
  if (command == fSetPolyconeHornParabolic) {
    G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
    const int nn = cmdWD->GetNewIntValue(newValue);
    if ((nn < 1) || (nn > 5)) {
      G4Exception("LBNEPlacementMessenger::SetNewValue ", " ", FatalErrorInArgument,
           " SetPolyconeHornParabolic, valid argument between 1 and 5  ");
    }      
    volP->SetPolyconeHornParabolic(static_cast<size_t>(nn), true);
    if (nn == 1) volP->SetPolyconeHorn1Parabolic(true);
  }
  if (command == fUseHorn2PolyNumInnerPts) {
     G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
     volP->SetUseHornsPolyNumInnerPts(2, cmdWD->GetNewIntValue(newValue));
   }
   if (command == fHorn2PolyOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyOuterRadius(2, cmdWD->GetNewDoubleValue(newValue));
   }
   for (size_t kCmd=0; kCmd != fHorn2PolyListRinThickZVects.size(); kCmd++) { 
     if (command == fHorn2PolyListRinThickZVects[kCmd]) {
       G4UIcmdWith3VectorAndUnit* cmdWD = dynamic_cast<G4UIcmdWith3VectorAndUnit*> (command);
       G4ThreeVector v = cmdWD->GetNew3VectorValue(newValue);
       volP->SetHornsPolyInnerThreeVect(2, kCmd, v);
       break;
     }
  }
   if (command == fHorn2PolyZStartPos) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyZStartPos(2, cmdWD->GetNewDoubleValue(newValue));
   }
  // 
  // Repeat for Horns 3, 4, 5. 
  //
  if (command == fUseHorn3PolyNumInnerPts) {
     G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
     volP->SetUseHornsPolyNumInnerPts(3, cmdWD->GetNewIntValue(newValue));
   }
   if (command == fHorn3PolyOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyOuterRadius(3, cmdWD->GetNewDoubleValue(newValue));
   }
   for (size_t kCmd=0; kCmd != fHorn3PolyListRinThickZVects.size(); kCmd++) { 
     if (command == fHorn3PolyListRinThickZVects[kCmd]) {
       G4UIcmdWith3VectorAndUnit* cmdWD = dynamic_cast<G4UIcmdWith3VectorAndUnit*> (command);
       G4ThreeVector v = cmdWD->GetNew3VectorValue(newValue);
       volP->SetHornsPolyInnerThreeVect(3, kCmd, v);
       break;
     }
  }
   if (command == fHorn3PolyZStartPos) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyZStartPos(3, cmdWD->GetNewDoubleValue(newValue));
   }
   if (command == fUseHorn4PolyNumInnerPts) {
     G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
     volP->SetUseHornsPolyNumInnerPts(4, cmdWD->GetNewIntValue(newValue));
   }
   if (command == fHorn4PolyOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyOuterRadius(4, cmdWD->GetNewDoubleValue(newValue));
   }
   for (size_t kCmd=0; kCmd != fHorn4PolyListRinThickZVects.size(); kCmd++) { 
     if (command == fHorn4PolyListRinThickZVects[kCmd]) {
       G4UIcmdWith3VectorAndUnit* cmdWD = dynamic_cast<G4UIcmdWith3VectorAndUnit*> (command);
       G4ThreeVector v = cmdWD->GetNew3VectorValue(newValue);
       volP->SetHornsPolyInnerThreeVect(4, kCmd, v);
       break;
     }
  }
   if (command == fHorn4PolyZStartPos) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyZStartPos(4, cmdWD->GetNewDoubleValue(newValue));
   }
  if (command == fUseHorn5PolyNumInnerPts) {
     G4UIcmdWithAnInteger* cmdWD = dynamic_cast<G4UIcmdWithAnInteger*> (command);
     volP->SetUseHornsPolyNumInnerPts(5, cmdWD->GetNewIntValue(newValue));
   }
   if (command == fHorn5PolyOuterRadius) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyOuterRadius(5, cmdWD->GetNewDoubleValue(newValue));
   }
   for (size_t kCmd=0; kCmd != fHorn5PolyListRinThickZVects.size(); kCmd++) { 
     if (command == fHorn5PolyListRinThickZVects[kCmd]) {
       G4UIcmdWith3VectorAndUnit* cmdWD = dynamic_cast<G4UIcmdWith3VectorAndUnit*> (command);
       G4ThreeVector v = cmdWD->GetNew3VectorValue(newValue);
       volP->SetHornsPolyInnerThreeVect(5, kCmd, v);
       break;
     }
  }
   if (command == fHorn5PolyZStartPos) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetHornsPolyZStartPos(5, cmdWD->GetNewDoubleValue(newValue));
   }
  //
  // With a caveat: we may not have the room for decay pipe snout, so we remove it..  
  //
   if (command == fRemoveDecayPipeSnout) {
     G4UIcmdWithABool* cmdWD = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetRemoveDecayPipeSnout(cmdWD->GetNewBoolValue(newValue));
   }
   //
   // Milind Wire in the decay pipe.. 
    if (command == fRadiusMilindWire) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetRadiusMilindWire(cmdWD->GetNewDoubleValue(newValue));
   }
    if (command == fCurrentMilindWire) {
     G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
     volP->SetCurrentMilindWire(cmdWD->GetNewDoubleValue(newValue));
   }
 
   if (command == fWriteGDMLFile) {
     G4UIcmdWithABool* cmdWD = dynamic_cast<G4UIcmdWithABool*> (command);
     volP->SetWriteGDMLFile(cmdWD->GetNewBoolValue(newValue));
   }

   // John Back, March 2016: target module (e.g. SAT) options
   if (command == fUseTargetModule) {
       G4UIcmdWithABool* cmdB = dynamic_cast<G4UIcmdWithABool*> (command);
       volP->SetUseTargetModule(cmdB->GetNewBoolValue(newValue.c_str()));
       // Also initialise the fTargetLengthOutsideExtra to zero for now to ensure
       // no overlapping volumes for any Horn arrangement
       volP->SetTargetLengthOutsideExtra(0.0);
   }

   if (command == fTargetModuleType) {
       G4UIcmdWithAnInteger* cmdB = dynamic_cast<G4UIcmdWithAnInteger*> (command);
       volP->SetTargetModuleType(cmdB->GetNewIntValue(newValue.c_str()));
   }

   if (command == fTargetRadius) {
       G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
       if (!volP->GetUseTargetModule()) {
	   std::cerr << " !!! You are changing target object radius for the target module, \n, but the " <<  
	       " NUMI-style target is still installed. Consider using first the GUI command UseTargetModule. \n";
       } else {
	   std::cout << " Setting new target module object radius to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
       } 
       volP->SetTargetRadius(cmdWD->GetNewDoubleValue(newValue)); 

   }

   if (command == fTargetLength) {
       G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
       if (!volP->GetUseTargetModule()) {
	   std::cerr << " !!! You are changing the total length of the target module (object), \n, but the " <<  
	       " NUMI-style target is still installed. Consider using first the GUI command UseTargetModule. \n";
       } else {
	   std::cout << " Setting new target module object length to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
       } 
       volP->SetTargetLength(cmdWD->GetNewDoubleValue(newValue));

   }

   if (command == fTargetNLambda) {
       G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
       if (!volP->GetUseTargetModule()) {
	   std::cerr << " !!! You are specifying the number of interaction lengths for the target module (object), \n, but the " <<  
	       " NUMI-style target is still installed. Consider using first the GUI command UseTargetModule. \n";
       } else {
	   std::cout << " Setting number of interaction lengths to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
       } 
       volP->SetTargetNLambda(cmdWD->GetNewDoubleValue(newValue));

   }

   if (command == fTargetFracOutHornL) {
       G4UIcmdWithADouble* cmdWD = dynamic_cast<G4UIcmdWithADouble*> (command);
       if (!volP->GetUseTargetModule()) {
	   std::cerr << " !!! You are specifying the length fraction of the target module outside Horn1, \n, but the " <<  
	       " NUMI-style target is still installed. Consider using first the GUI command UseTargetModule. \n";
       } else {
	   std::cout << " Setting new length fraction of target module outside Horn1 to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
       } 
       volP->SetTargetFracOutHornL(cmdWD->GetNewDoubleValue(newValue));

   }

   if (command == fTargetLengthOutsideExtra) {
       G4UIcmdWithADoubleAndUnit* cmdWD = dynamic_cast<G4UIcmdWithADoubleAndUnit*> (command);
       std::cout << " Setting new TargetLengthOutsideExtra shift to " << cmdWD->GetNewDoubleValue(newValue) << std::endl;
       volP->SetTargetLengthOutsideExtra(cmdWD->GetNewDoubleValue(newValue));

   }



}

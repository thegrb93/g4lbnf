#include "LBNEDetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"

#include "G4Polycone.hh"
#include "G4GenericPolycone.hh"
#include "G4Trap.hh"
#include "G4Cons.hh"
#include "G4Torus.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4VisAttributes.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVReplica.hh"
#include "G4AssemblyVolume.hh"
#include "LBNEMagneticField.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PVPlacement.hh"
#include "G4RegionStore.hh"
#include "G4SolidStore.hh"
#include "G4GeometryManager.hh"
#include "G4FieldManager.hh"
#include "G4SubtractionSolid.hh"

#include "G4RunManager.hh"
#include "G4ExceptionSeverity.hh"

#include "G4VisExtent.hh"
#include "LBNEPlacementMessenger.hh"
#include "LBNESurveyor.hh"
#include "LBNERunManager.hh"

//---------------------------------------------------------------------------// 
// Additional code for placing the volume and supporting multiple options
// We start here with the 1.2 MW plan, as of February 2014. 
// Paul L. G. Lebrun
//---------------------------------------------------------------------------// 
void LBNEVolumePlacements::adaptTargetFor1p2MW() {
    fTargetHeContTubeInnerRadius = 35.2*CLHEP::mm/2.; // LBNE Docdb 8639-v1
    fTargetCTubeOuterRadius = 3.0*CLHEP::mm;
    fTargetCTubeInnerRadius = 5.2*CLHEP::mm/2.;
    fTargetFinHeight = 2.0*(13.3675 - 3.0 + 0.005)*CLHEP::mm; // This excludes the cooling tubes.
    fTargetFinWidth = 10*CLHEP::mm; // // ajustable via G4 UI data card. 
    fTargetFinWidth /= 2; // we divide by 2:We will place two of them, side by side, center offset of fTargetFinWidth/2. 
    fTargetFinWidthRequired =  fTargetFinWidth;
    fTargetFinContainerWidth = 2.0*fTargetCTubeOuterRadius + 0.010*CLHEP::mm;
    fTargetFinExtraWidth = -1.0*CLHEP::mm; // No additional piece on the side, by defaul 
    fBaffleInnerRadius = 17.0*CLHEP::mm/2.;
    // 
    // The longitudinal segmentation of the target will be unchanged. 
    // April 11 2014 : set the default the target length outside Horn1 to 450 mm.  
    // I put it to 500 mm originally because of the presumed need for more tolerance  
    // near the neck, but 450 mm. is good enough after revision of the Horn1 geometry.. 
    // 
//    fTargetLengthIntoHorn = fTargetSLengthGraphite - 500.*CLHEP::mm + 25.3; //  Integration drawing 8875.000-ME-363028
    fTargetLengthIntoHorn = fTargetSLengthGraphite - 450.*CLHEP::mm + fArbitraryOffsetHystericalOne; //  Integration drawing 8875.000-ME-363028
    fHorn1RadialSafetyMargin = 0.25*CLHEP::mm;
    fRotVertical.set(CLHEP::HepRotationX(-90.0*CLHEP::deg));
    fUseRoundedTargetFins = (fUse1p2MW) ? true : false; // If need be, we "back implement" for the 700 kW option. 
}

//
// Clone of v3r0p12 PlaceFinalUpstrTarget.
// Cloning is always a bad idea with respect to maintenance, but it is somewhat unlikely 
// that we will have to maintain the previous iteration. 
//
// August 2014: Avoid now to split the target, as the mother volume for Horn1 is a G4Polycone. 
// Thus, the "Upstr" substring in the name of this method is superfluous, but kept for backwards readability. 
// 
void LBNEVolumePlacements::PlaceFinalUpstrTarget1p2MW(G4PVPlacement *mother) {
			       
    // Define in LBNEDetectorConstruction..    
    //Start by defining the top level of the target container volume, which includes the canister (TargetUpstrM0) 
   // and the target section which is outside Horn1 (TargetUpstrM1)
   
   // 
   // We leave all the volume that are upstream of the first graphite target segment identical..
   // We have no mechanicl drawing for this part yet.
   // 
    Create("TargetUpstrMTop");
    G4PVPlacement *vMTop = PlaceFinal(std::string("TargetUpstrMTop"), mother); // Obsolete.. But keep for reference 
    if (fRemoveTargetAlltogether) {
       G4Exception("LBNEVolumePlacements::PlaceFinalUpstrTarget1p2MW", "No target! ", JustWarning, "Target has been removed");
       return;
    }

    if (fUseSimpleTargetCylinder || fUseSimpleTargetBox) {
      Create("TargetNoSplitM1");
      G4PVPlacement *vMt1 = PlaceFinal(std::string("TargetNoSplitM1"),  fTargetHorn1HallPhysPtr);
      Create("TargetNoSplitHeContainer");
      G4PVPlacement *vMt2 = PlaceFinal(std::string("TargetNoSplitHeContainer"), vMt1);
      if (fUseSimpleTargetCylinder) {
        PlaceFinalUpstrTargetSimpleCylinder(vMt2);
      } else {
        PlaceFinalUpstrTargetSimpleBox(vMt2);
      }
      return;
   }
    
//    std::cerr << " TargetUpstrMTop  created and placed ... " << std::endl;
    LBNEVolumePlacementData *plM0 = Create("TargetUpstrM0");
//    std::cerr << " TargetUpstrM0  created ... " << std::endl;
//    std::cerr << " TargetUpstrM1  created ... " << std::endl;
    G4PVPlacement *vM0 = PlaceFinal(std::string("TargetUpstrM0"), vMTop);
    std::cerr << " TargetUpstrM0  placed ... " << std::endl;

    // Place the target module, e.g. the spherical array target (SAT) setup
    std::cout<<"fUseTargetModule = "<<(int)fUseTargetModule<<std::endl;
    if (fUseTargetModule) {
	PlaceTargetModule();
	return;
    }

    G4String targetTopStr; LBNEVolumePlacementData *plM1; G4PVPlacement *vM1=0;
    targetTopStr = std::string("TargetNoSplitM1");
    plM1 = Create(targetTopStr);
    vM1 = PlaceFinal(targetTopStr, fTargetHorn1HallPhysPtr);
    std::cerr << " .. " <<  targetTopStr 
    	      << " is now placed in PlaceFinalUpstrTarget1p2MW...in fTargetHorn1HallPhysPtr " << std::endl;
    std::string tUpUp("TargetUpstrUpstr");
    Create(tUpUp + std::string("Plate"));
    Create(tUpUp + std::string("Can"));
    Create(tUpUp + std::string("CanEndPlate"));
    Create(tUpUp + std::string("DwstrFlange"));
    Create(tUpUp + std::string("CoolingTube"));
    Create(tUpUp + std::string("CoolingTubeWater"));
    Create(tUpUp + std::string("SupportBlockTopLeft"));
    Create(tUpUp + std::string("SupportBlockBottomLeft"));
    Create(tUpUp + std::string("SupportBlockRight"));
    Create(tUpUp + std::string("SupportSleeve"));
    // None of these are surveyable, position is fixed. 
    PlaceFinal(tUpUp + std::string("Plate"), vM0);
    PlaceFinal(tUpUp + std::string("Can"), vM0);
    PlaceFinal(tUpUp + std::string("CanEndPlate"), vM0);
    PlaceFinal(tUpUp + std::string("DwstrFlange"), vM0);
    // Cooling tubes: two copies. This breaks the Placement data model. Never mind, assume here that misalignment 
    // of these cooling tube has negligible effect on the physics. 
    
    G4String nameTmp5(tUpUp + std::string("CoolingTube"));
    std::map<G4String, LBNEVolumePlacementData>::iterator itTmp = fSubVolumes.find(nameTmp5);
    LBNEVolumePlacementData &infoTmp = itTmp->second;
    G4ThreeVector posTmp;
    posTmp[0] = 0.;
    posTmp[1] = 0.5*fTargetFinHeight;
    posTmp[2] = -1.0*plM0->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + fTargetCanLength + 
				 fTargetDownstrCanFlangeThick + fTargetFlangeThick - infoTmp.fParams[2]/2 - 1.0*CLHEP::mm;
    G4String vpNameTmp5(nameTmp5);				 			 
//    std::cerr << " Position for " << vpNameTmp5+G4String("_PTop") << " X = " 
//                                  << posTmp[0] << " Y " << posTmp[1] << " Z " << posTmp[2] << std::endl;  
    G4PVPlacement *vCoolingTubeTop = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTmp.fCurrent, vpNameTmp5+G4String("_PTop"),
				     vM0->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC );
    posTmp[1] = -0.5*fTargetFinHeight;
    G4PVPlacement *vCoolingTubeBottom = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTmp.fCurrent, vpNameTmp5+std::string("_PBottom"),
				     vM0->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
    
    PlaceFinal(tUpUp + std::string("CoolingTubeWater"), vCoolingTubeTop);
    PlaceFinal(tUpUp + std::string("CoolingTubeWater"), vCoolingTubeBottom);
    PlaceFinal(tUpUp + std::string("SupportBlockTopLeft"), vM0);
    PlaceFinal(tUpUp + std::string("SupportBlockBottomLeft"), vM0);
    PlaceFinal(tUpUp + std::string("SupportBlockRight"), vM0);
    PlaceFinal(tUpUp + std::string("SupportSleeve"), vM0);
    // We need three support/alignment rods. Identical volume, but at different locations. 
    std::string nameTmp10(tUpUp + std::string("SupportRod"));
    Create(nameTmp10);
    itTmp = fSubVolumes.find(nameTmp10);
    infoTmp = itTmp->second;
    // Top Left alignment/support rod. 
    posTmp[0] = -20.5*CLHEP::mm; // Again, accurate to +- 1 mm or so. 
    posTmp[1] = 24.0*CLHEP::mm;
    posTmp[2] = -1.0*plM0->fParams[2]/2.0 + fTargetUpstrUpstrMargin +
	                         fTargetUpstrPlateThick + fTargetCanLength + 
				 fTargetDownstrCanFlangeThick - infoTmp.fParams[2]/2 - 1.0*CLHEP::mm;
    G4String vpNameTmp10(nameTmp10);				 			 
    new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTmp.fCurrent, vpNameTmp10+std::string("_PTopLeft"), 
				            vM0->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
    posTmp[0] = -15.5*CLHEP::mm; // Again, accurate to +- 1 mm or so. 
    posTmp[1] = -36.0*CLHEP::mm;
    new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTmp.fCurrent, vpNameTmp10+std::string("_PBottomLeft"),
				     vM0->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
    posTmp[0] = 33.0*CLHEP::mm; // Again, accurate to +- 1 mm or so. 
    posTmp[1] = 5.0*CLHEP::mm;
    new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTmp.fCurrent, vpNameTmp10+std::string("_PRight"), 
				    vM0->GetLogicalVolume(), false, 2, fCheckVolumeOverLapWC);
    
//  Horizontal fin for beam alignment. Most important thing in the canister, in terms of material budget 

    if ((!fUseSimpleTargetCylinder) &&  (!fUseSimpleTargetBox)) { 
      std::string nameTmp11("TargetFinHorizontal");
      Create(nameTmp11);
       PlaceFinal(nameTmp11, vM0);
    }
    // Done with the target canister..Now assemble the upstream part of the target it self. (Upstream of the horn1)  
    std::string tUpDown("TargetUpstrDownstr"); 
    tUpDown = std::string("TargetNoSplit");
    Create(tUpDown + std::string("HeContainer")); 
    G4PVPlacement *vHeTube = PlaceFinal(tUpDown + std::string("HeContainer"), vM1); // Surveyable 
    LBNEVolumePlacementData *plHelium = Create(tUpDown + std::string("Helium")); 
    G4PVPlacement *vHelium = PlaceFinal(tUpDown + std::string("Helium"), vHeTube); // Fixed 
    if (fUseMultiSphereTarget) {
      PlaceFinalMultiSphereTarget(plHelium, vHelium);
      return;
    }
    // First alignment ring, locate flush with the end plate (within 1 mm ) , left and right 
    // As suggested during the Feb 20 th meeting, remove the alignment rings for now.. 
    //      
//    LBNEVolumePlacementData *infoTmpRLeft = Create(G4String("TargetAlignmentRingLeft"));
//    LBNEVolumePlacementData *infoTmpRRight = Create(G4String("TargetAlignmentRingRight"));
//
//    posTmp[0] = 0.; // The alignment rings are always centered.. 
//    posTmp[1] = 0.;
//    posTmp[2] = -1.0*plHelium->fParams[2]/2.0 + infoTmpRLeft->fParams[2]/2. + 1.0*CLHEP::mm; // 1 mm spacing Left and right have the same thickness. 
//    int copyNumber = 0;
//    while (true) {
//      std::ostringstream cNumStrStr; cNumStrStr << "_P" << copyNumber;
//      new G4PVPlacement((G4RotationMatrix *) 0, 
//				  posTmp, infoTmpRLeft->fCurrent, G4String("TargetAlignmentRingLeft")+cNumStrStr.str(), 
//					vHelium->GetLogicalVolume(), false, copyNumber, fCheckVolumeOverLapWC);
//      new G4PVPlacement((G4RotationMatrix *) 0, 
//				posTmp, infoTmpRRight->fCurrent, G4String("TargetAlignmentRingRight")+ cNumStrStr.str(), 
//				      vHelium->GetLogicalVolume(), false, copyNumber, fCheckVolumeOverLapWC);
//      posTmp[2] += fTargetAlignRingSpacing;      
//      copyNumber++;
//      if ( posTmp[2] > (plM1->fParams[2]/2.0 - 1.0*CLHEP::mm)) break;
//    }				  
    //
    G4String nameTgtUpDownSegLeft("");
    G4String nameTgtUpDownSegRight("");
    LBNEVolumePlacementData *plTargetCoolingTube = 0; 
    LBNEVolumePlacementData  *plTargetCoolingTubeWater = 0;
    LBNEVolumePlacementData *plTargetGenSegmentLeft = 0;
    plTargetCoolingTube = Create(G4String("TargetNoSplitCoolingTube"));    
    plTargetCoolingTubeWater = Create(G4String("TargetNoSplitCoolingTubeWater"));
    nameTgtUpDownSegLeft = G4String("TargetNoSplitSegmentLeft");
    plTargetGenSegmentLeft = Create(nameTgtUpDownSegLeft);
    nameTgtUpDownSegRight = G4String("TargetNoSplitSegmentRight");    
    std::vector< std::string > namesSegment(2,"");
    namesSegment[0] = nameTgtUpDownSegLeft;
    namesSegment[1] = nameTgtUpDownSegRight;
    LBNEVolumePlacementData *plTargetUpstrDownstrSegmentRight = Create(nameTgtUpDownSegRight);
    std::cerr << "LBNEVolumePlacements::PlaceFinalUpstrTarget1p2MW,  fTargetFinExtraWidth " 
              << fTargetFinExtraWidth << " central width " << fTargetFinWidth << std::endl;
    LBNEVolumePlacementData *plTargetFinExtra = (fTargetFinExtraWidth > 0.) ? 
                                                 Create(G4String("TargetFinVertExtra")) :  0;						 
    LBNEVolumePlacementData *plTargetFinHeSide = (fTargetFinExtraWidth < 0.) ? 
                                                 Create(G4String("TargetFinVertHeliumSide")) :  0;
    LBNEVolumePlacementData *plTargetRoundCornerUpstrLeft = (fUseRoundedTargetFins) ?
                                                           Create(G4String("TargetFinVertHeliumRoundedUpstrLeft")) :  0;	 					 
    LBNEVolumePlacementData *plTargetRoundCornerUpstrRight = (fUseRoundedTargetFins) ?
                                                           Create(G4String("TargetFinVertHeliumRoundedUpstrRight")) :  0;
    LBNEVolumePlacementData *plTargetRoundCornerDownstrLeft = (fUseRoundedTargetFins) ?
                                                           Create(G4String("TargetFinVertHeliumRoundedDownstrLeft")) :  0;	 					 
    LBNEVolumePlacementData *plTargetRoundCornerDownstrRight = (fUseRoundedTargetFins) ?
                                                           Create(G4String("TargetFinVertHeliumRoundedDownstrRight")) :  0;
    G4PVPlacement *vSeg[2];
    double zCoordTmp = -1.0*plM1->fParams[2]/2.0;
    zCoordTmp += 0.050*CLHEP::mm;
    int copyNumberT = 0;
    int numSegHere = fTargetNumFins;
    for (int iSeg=0; iSeg != numSegHere; iSeg++) { // Place with no misalignment,  The last segment is placed below  
      posTmp[0] = -0.5*fTargetFinContainerWidth - 0.023*CLHEP::mm; posTmp[1] = 0.;      
      posTmp[2] =  zCoordTmp
                  + plTargetGenSegmentLeft->fParams[2]/2. + iSeg*(fTargetFinLength + fTargetFinSpacingLength);
//      std::cerr << " Placing target segments...  At Z = " << posTmp[2] << std::endl;	  
      std::ostringstream cNumStrStrL; cNumStrStrL << "_PLeft" << iSeg;
      vSeg[0] = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetGenSegmentLeft->fCurrent, 
				      nameTgtUpDownSegLeft+cNumStrStrL.str(), 
				          vHelium->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC);
      copyNumberT ++;
      posTmp[0] *= -1.0;
      std::ostringstream cNumStrStrR; cNumStrStrR << "_PRight" << iSeg;
      vSeg[1] = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetUpstrDownstrSegmentRight->fCurrent, 
				      nameTgtUpDownSegRight+cNumStrStrR.str(), 
				          vHelium->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC);

       if (plTargetFinExtra != 0) {
         posTmp[0] = -1.0*fTargetFinContainerWidth  - 0.5*fTargetFinExtraWidth -0.040*CLHEP::mm;
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtra->fCurrent, 
				      namesSegment[0] + std::string("Extra")+cNumStrStrL.str(), 
				          vHelium->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC); 
         posTmp[0] *= -1.0;
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtra->fCurrent, 
				    namesSegment[1] + std::string("Extra")+cNumStrStrR.str(), 
				          vHelium->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC); 
       
       }      				  
//      std::cerr << " Placing target at Zs " << 	posTmp[2] << std::endl;			  
    }
//    std::cerr << " Placing Cooling tube at height " << posTmp[1] << std::endl;
    	
    for (size_t kLeftRight=0; kLeftRight !=2; kLeftRight++) {
      posTmp[0] = 0.; posTmp[1] = fTargetFinHeight/2. + fTargetCTubeOuterRadius - 0.005*CLHEP::mm; posTmp[2] =	0.;
      std::cerr <<  " Height of the cooling tube Up... .. " << posTmp[1] <<std::endl;
      G4PVPlacement *vTubeUp =  new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetCoolingTube->fCurrent, 
				      namesSegment[kLeftRight]+G4String("CoolingTubeUp_PTop"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
					  
      posTmp[1] *= -1.0;			  
      std::cerr <<  " Height of the cooling tube Down... .. "<< posTmp[1] << std::endl;
      G4PVPlacement *vTubeDwn = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetCoolingTube->fCurrent,
				     namesSegment[kLeftRight]+ G4String("CoolingTubeUp_PBottom"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] = 0.;			  
      new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetCoolingTubeWater->fCurrent,
				    namesSegment[kLeftRight]+ G4String("CoolingTubeWater_PUp"), 
				          vTubeUp->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);

      new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetCoolingTubeWater->fCurrent,
				    namesSegment[kLeftRight]+ G4String("CoolingTubeWater_PDwn"), 
				          vTubeDwn->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      
//
// This is no longer really needed, as we the container is graphite.. but keep it, for 
// debugging the geometry.. 
//      posTmp[0] = (kLeftRight == 0) ?  -1.0*( fTargetFinContainerWidth - fTargetFinWidth)/2. -0.010:
//                                      ( fTargetFinContainerWidth - fTargetFinWidth)/2. + 0.010;
//      std::cerr << " TargetFin Horizontal shift " <<  posTmp[0] 
//                << " Container width " << fTargetFinContainerWidth << " TargetFin Width " << fTargetFinWidth << std::endl;
//      posTmp[1] = 0.;				      
//      new G4PVPlacement((G4RotationMatrix *) 0, 
//	                            posTmp, plTargetFin->fCurrent, 
//				          namesSegment[kLeftRight]+G4String("TargetFinVert_P"), 
//				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
//
//   Put back some helium into the container.. 
//
      if (plTargetFinHeSide != 0) {
        const double heThick = plTargetFinHeSide->fParams[0];
	const double heThickShift =  -0.5*fTargetFinContainerWidth + 0.5*heThick + 0.0175*CLHEP::mm;
	std::cerr << " Helium back fill.. width " << heThick 
	          << " Container width " << fTargetFinContainerWidth << " shift " << heThickShift << std::endl;
        posTmp[0] = (kLeftRight == 0) ? heThickShift : -1.0*heThickShift;
        posTmp[1] = 0.;				      
        new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinHeSide->fCurrent, 
				          namesSegment[kLeftRight]+G4String("TargetFinVertHeSide_P"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      }
      if (fUseRoundedTargetFins) {
	 posTmp[0] = 0.;
	 posTmp[1] = 0.;
	 if (kLeftRight == 0) {
	   posTmp[2] = plTargetGenSegmentLeft->fParams[2]/2. - plTargetRoundCornerDownstrLeft->fParams[1]/2. - 0.001*CLHEP::mm;
	   std::cerr << " Z shift for round cornres .. DownstrLeft " << posTmp[2] << " half length Target " << 
	              plTargetGenSegmentLeft->fParams[2]/2. << " cuboid width " << 
		      plTargetRoundCornerDownstrLeft->fParams[0]/2. << std::endl;
           new G4PVPlacement(&fRotVertical, posTmp, 
	                       plTargetRoundCornerDownstrLeft->fCurrent, 
				          namesSegment[kLeftRight]+G4String("TargetFinVertRoundCornerDL_P"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
	   posTmp[2] *= -1.0;		       
           new G4PVPlacement(&fRotVertical, posTmp, 
	                       plTargetRoundCornerUpstrLeft->fCurrent, 
				          namesSegment[kLeftRight]+G4String("TargetFinVertRoundCornerUL_P"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
	  } else {
	   posTmp[2] = plTargetUpstrDownstrSegmentRight->fParams[2]/2. - plTargetRoundCornerDownstrRight->fParams[1]/2. - 0.001*CLHEP::mm;
           new G4PVPlacement(&fRotVertical, posTmp, 
	                       plTargetRoundCornerDownstrRight->fCurrent, 
				          namesSegment[kLeftRight]+G4String("TargetFinVertRoundCornerDR_P"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
	   posTmp[2] *= -1.0;		       
           new G4PVPlacement(&fRotVertical, posTmp, 
	                       plTargetRoundCornerUpstrRight->fCurrent, 
				          namesSegment[kLeftRight]+G4String("TargetFinVertRoundCornerUR_P"), 
				          vSeg[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
	 }
					  
      }					  
//      				  
    }					  
    // Repeat the last few mantra for the last segment ... But we skip the round corner.. 
   
   if (fUsePseudoNova) { // Include now the target downstream parts that used to be inside 
     
      // Now the end 
      LBNEVolumePlacementData *plCoolingTubeReturn = Create("Horn1TargetCoolingTubeDwnRetTit");
      posTmp[0] = 0.; posTmp[1] = 0.; 
      posTmp[2] = plHelium->fParams[2]/2 - 
	       fTargetCTubeReturnDownstrThickWater - 1.5*fTargetCTubeReturnDownstrThickTitanium/2. - 1.5*CLHEP::mm;
	       //  Hopefully some roo to spare..Hummm not really. 
      std::cerr << " Placing Horn1TargetCoolingTubeDwnRetTit, front, length of helium " <<  plHelium->fParams[2]/2  
	          <<  " at Z =  "  <<  posTmp[2] << std::endl;   			  
      new G4PVPlacement(&plCoolingTubeReturn->fRotation, 
	                            posTmp, plCoolingTubeReturn->fCurrent, 
				    G4String("Horn1TargetCoolingTubeDwnRetTit_PFront"), 
				          vHelium->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      posTmp[2] = plHelium->fParams[2]/2 -  0.5*fTargetCTubeReturnDownstrThickTitanium/2. - 1.3*CLHEP::mm;			  
      std::cerr << " Placing Horn1TargetCoolingTubeDwnRetTit, back, length of helium " << posTmp[2] << std::endl;   			  
      new G4PVPlacement(&plCoolingTubeReturn->fRotation, 
				  posTmp, plCoolingTubeReturn->fCurrent, 
				  G4String("Horn1TargetCoolingTubeDwnRetTit_PBack"), 
					vHelium->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
    
      Create("Horn1TargetCoolingTubeDwnRetWater");
      PlaceFinal("Horn1TargetCoolingTubeDwnRetWater", vHelium); 
      
      Create("Horn1TargetDownstrHeContainerCap");
      PlaceFinal("Horn1TargetDownstrHeContainerCap", vHelium);					  				  
   }					  				  
    // End for the segment of the target which is outside Horn1
}

// We need eight little corner volumes (2 x 4, asymmetric volume subtraction. )
// No, we don't !! The container volume are now made of graphite, so we have a bit of extra material 
// on top of the cooling tube.  

// void LBNEVolumePlacements::declareTargetFinVertCorners(LBNEVolumePlacementData &info) {
  // In this method we need to define of the 8 little corner piece of graphite 
  // on top/bottom of the graphite fin, left and right of the cooling tube. 
  // Since the fin is split into two logical part, this is twice as many as in the 700 kW version.. 
  //
  // Too complicated, and running out of time.. 
  // Note the in the new version, the segment is made of graphite, si these little corner are included. 
  //


//}

void LBNEVolumePlacements::PlaceFinalDownstrTarget1p2MW(G4PVPlacement *mother) {
    
    // Obsolete code... 
    Create("Horn1TargetDownstrHeContainer");
    G4PVPlacement *vMTop = PlaceFinal(std::string("Horn1TargetDownstrHeContainer"), mother);
    LBNEVolumePlacementData *plDowstrHe = Create("Horn1TargetDownstrHelium");
     G4PVPlacement *vMHe = PlaceFinal(std::string("Horn1TargetDownstrHelium"), vMTop);
    // Skip the alignment ring for now.. 
    // Deal with alignment ring located in the dowstream part of the target. We assume here that 
    // they have been already defined while dealing with the Upstream target portion 
    // First alignment ring, locate flush with the end plate (within 1 mm ) , left and right      
//    std::map<G4String, LBNEVolumePlacementData>::iterator itTmpRLeft = fSubVolumes.find(G4String("TargetAlignmentRingLeft"));
//    LBNEVolumePlacementData &infoTmpRLeft = itTmpRLeft->second;
//    std::map<G4String, LBNEVolumePlacementData>::iterator itTmpRRight = fSubVolumes.find(G4String("TargetAlignmentRingRight"));
//    LBNEVolumePlacementData &infoTmpRRight = itTmpRRight->second;
    std::map<G4String, LBNEVolumePlacementData>::iterator itMother = fSubVolumes.find(G4String("Horn1TargetDownstrHelium"));
//    LBNEVolumePlacementData *plMother = &itMother ->second;
//    if (fUseSimpleTargetCylinder) { 
//        PlaceFinalDownstrTargetSimpleCylinder(plMother, vMHe);
//	return;
//    }
    LBNEVolumePlacementData *plTargetFinExtra = 0;
    if (fTargetFinExtraWidth > 0.) {
        std::map<G4String, LBNEVolumePlacementData>::iterator itM5 = fSubVolumes.find(G4String("TargetFinVertExtra"));
	plTargetFinExtra = &itM5->second;
    }
                             
    G4ThreeVector posTmp(3, 0.);
    LBNEVolumePlacementData *infoTargSegFirstLeft = 0;
    G4String nameTgtUpDownSegFirst("Horn1TargetDownstrSegmentFirst");
    if (fTargetFinLengthSplitDwnstr > 0.3*CLHEP::mm) { 
      LBNEVolumePlacementData *plTargetFinExtr2 = (fTargetFinExtraWidth > 0.) ?
                                                 Create(G4String("Horn1TargetFinVertExtraFirst")) :  0;
      infoTargSegFirstLeft = Create("Horn1TargetSegmentFirstLeft"); 
      LBNEVolumePlacementData *infoTargSegFirstRight = Create("Horn1TargetSegmentFirstRight"); 
      G4PVPlacement *vTargSegFirst[2];
      posTmp[0] = -0.5*fTargetFinContainerWidth - 0.023*CLHEP::mm;  posTmp[1] = 0.;
    
      posTmp[2] = infoTargSegFirstLeft->fPosition[2];
      vTargSegFirst[0] = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTargSegFirstLeft->fCurrent, 
				      G4String("TargetUpstrDowstrSegmentFirstLeft"),
				          vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      posTmp[0] *= -1.0;				  
      vTargSegFirst[1] = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, infoTargSegFirstRight->fCurrent, 
				      G4String("TargetUpstrDowstrSegmentFirstRight"),
				          vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
       if (plTargetFinExtr2 != 0) {
         posTmp[0] = -1.0*fTargetFinContainerWidth  - 0.5*fTargetFinExtraWidth - 0.040*CLHEP::mm;
	 posTmp[1] = 0.; posTmp[2] = infoTargSegFirstLeft->fPosition[2]; 
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtr2->fCurrent, 
				      nameTgtUpDownSegFirst + std::string("ExtraLeft"), 
				          vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC); 
         posTmp[0] *= -1.0;
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtr2->fCurrent, 
				      nameTgtUpDownSegFirst + std::string("ExtraRight"), 
				          vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC); 
       }      				  

//       LBNEVolumePlacementData *infoFinVert  = Create("Horn1TargetFinVertFirst");
       LBNEVolumePlacementData *plCoolingTubeFirst = Create("Horn1TargetCoolingTubeFirst");
       LBNEVolumePlacementData *plCoolingTubeWaterFirst = Create("Horn1TargetCoolingTubeFirstWater");
       LBNEVolumePlacementData *plTargetFinHeSide = (2.0*fTargetCTubeOuterRadius > fTargetFinWidth) ? 
                                                 Create(G4String("Horn1TargetFinVertFirstHeliumSide")) :  0;
       for (size_t kLeftRight=0; kLeftRight !=2; kLeftRight++) { 				  
     
          posTmp[0] = 0.; posTmp[1] = fTargetFinHeight/2. + fTargetCTubeOuterRadius - 0.005*CLHEP::mm; posTmp[2] = 0.;			  
          G4PVPlacement *vTubeUp = new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plCoolingTubeFirst->fCurrent, 
				    G4String("Horn1TargetSegmentCoolingTubeFirst_PTop"), 
				          vTargSegFirst[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
          posTmp[1] *= -1.0;			  
          G4PVPlacement *vTubeDown =  new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plCoolingTubeFirst->fCurrent, 
				    G4String("Horn1TargetSegmentCoolingTubeFirst_PBottom"), 
				          vTargSegFirst[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
          posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] = 0.;			  
          new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plCoolingTubeWaterFirst->fCurrent, 
				    nameTgtUpDownSegFirst+G4String("CoolingTubeWater_PTop"), 
				          vTubeUp->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
          new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plCoolingTubeWaterFirst->fCurrent, 
				    nameTgtUpDownSegFirst+G4String("CoolingTubeWater_PBottom"), 
				          vTubeDown->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
      
//          posTmp[0] = (kLeftRight == 0) ? ( fTargetFinContainerWidth - fTargetFinWidth) :
//                                      -1.0*( fTargetFinContainerWidth - fTargetFinWidth);
//          posTmp[1]=0.; posTmp[2]=0.;
//          new G4PVPlacement((G4RotationMatrix *) 0, 
//	                            posTmp, infoFinVert->fCurrent, 
//				    G4String("Horn1TargetFinVertFirst_PLeft"), 
//				          vTargSegFirst[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
//      					  
//
//   Put back some helium into the target segment, if target is thinner that the cooling tube diameter. 
//
        if (plTargetFinHeSide != 0) {
           const double heThick = plTargetFinHeSide->fParams[0];
	   const double heThickShift =  -0.5*fTargetFinContainerWidth + 0.5*heThick + 0.0175*CLHEP::mm;
           posTmp[0] = (kLeftRight == 0) ? heThickShift : -1.0*heThickShift;
           posTmp[1] = 0.;
//	   std::cerr << " Positioning helium side at X " << heThickShift << " width " << heThick << std::endl;				      
           new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinHeSide->fCurrent, 
				           nameTgtUpDownSegFirst+G4String("TargetFinVertHeSide_P"), 
				          vTargSegFirst[kLeftRight]->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
        }					  
       } // left/right part of the target. 
     }
     
    // Now place the previously defined standard target segment. Note: they already contain their cooling and 
    // real target. 
//    LBNEVolumePlacementData  *plHorn1TargetSegment = Create("Horn1TargetSegment");
    std::map<G4String, LBNEVolumePlacementData>::iterator itTargSegLeft = 
              fSubVolumes.find(G4String("TargetUpstrDownstrSegmentLeft"));
    std::map<G4String, LBNEVolumePlacementData>::iterator itTargSegRight = 
              fSubVolumes.find(G4String("TargetUpstrDownstrSegmentRight"));
    LBNEVolumePlacementData  *plTargetUpstrDownstrSegmentLeft= &itTargSegLeft->second;    
    LBNEVolumePlacementData  *plTargetUpstrDownstrSegmentRight= &itTargSegRight->second;    
    
    double zCoordTmp = (infoTargSegFirstLeft == 0) ? 
                       (-1.0*plDowstrHe->fParams[2]/2. + plTargetUpstrDownstrSegmentLeft->fParams[2]/2. + 0.005*CLHEP::mm) :
                        ( infoTargSegFirstLeft->fPosition[2] + infoTargSegFirstLeft->fParams[2]/2. + 
                       plTargetUpstrDownstrSegmentLeft->fParams[2]/2. + 0.002*CLHEP::mm);	       
    int copyNumberT = 100;
//    G4PVPlacement *vSeg[2];
    std::string nameTgtUpDownSegLeft("Horn1TargetDownstrSegLeft");
    std::string nameTgtUpDownSegRight("Horn1TargetDownstrSegRight");
    if (fTargetFinExtraWidth > 0.) { 
      std::map<G4String, LBNEVolumePlacementData>::iterator itExtraThin = 
              fSubVolumes.find(G4String("TargetFinVertExtra"));
      plTargetFinExtra = &itExtraThin->second;
    }
    for (int iSeg=0; iSeg != fTargetNumFinsInHorn; iSeg++) { // Place with no misalignment
      posTmp[0] = -0.5*fTargetFinContainerWidth - 0.023*CLHEP::mm;    
      posTmp[1] = 0.;
      posTmp[2] =  zCoordTmp;
      std::ostringstream cNumStrStr; cNumStrStr << "_PDwnstr" << iSeg;
      new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetUpstrDownstrSegmentLeft->fCurrent, 
				      nameTgtUpDownSegLeft+cNumStrStr.str(), 
				          vMHe->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC);
      copyNumberT ++;
      posTmp[0] *= -1.0;
      new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetUpstrDownstrSegmentRight->fCurrent, 
				      nameTgtUpDownSegRight+cNumStrStr.str(), 
				          vMHe->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC);

       if (plTargetFinExtra != 0) {
         posTmp[0] = -1.0*fTargetFinContainerWidth  - 0.5*fTargetFinExtraWidth - 0.040*CLHEP::mm; 
	 // to avoid false overlap errors. 
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtra->fCurrent, 
				      nameTgtUpDownSegLeft + std::string("Extra")+cNumStrStr.str(), 
				          vMHe->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC); 
         posTmp[0] *= -1.0;
	 new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, plTargetFinExtra->fCurrent, 
				      nameTgtUpDownSegRight + std::string("Extra")+cNumStrStr.str(), 
				          vMHe->GetLogicalVolume(), false, copyNumberT, fCheckVolumeOverLapWC); 
       
       }      				  
      zCoordTmp += (fTargetFinLength + fTargetFinSpacingLength);
    }
    
    // Now the end 
    LBNEVolumePlacementData *plCoolingTubeReturn = Create("Horn1TargetCoolingTubeDwnRetTit");
    posTmp[0] = 0.; posTmp[1] = 0.; 
    posTmp[2] = plDowstrHe->fParams[2]/2 - 
	       fTargetCTubeReturnDownstrThickWater - 1.5*fTargetCTubeReturnDownstrThickTitanium/2. - 2.0*CLHEP::mm;
	       //  Hopefully some roo to spare.. 
    new G4PVPlacement(&plCoolingTubeReturn->fRotation, 
	                            posTmp, plCoolingTubeReturn->fCurrent, 
				    G4String("Horn1TargetCoolingTubeDwnRetTit_PFront"), 
				          vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
    posTmp[2] = plDowstrHe->fParams[2]/2 -  0.5*fTargetCTubeReturnDownstrThickTitanium/2. - 1.3*CLHEP::mm;			  
    new G4PVPlacement(&plCoolingTubeReturn->fRotation, 
				  posTmp, plCoolingTubeReturn->fCurrent, 
				  G4String("Horn1TargetCoolingTubeDwnRetTit_PBack"), 
					vMHe->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
    
    Create("Horn1TargetCoolingTubeDwnRetWater");
    PlaceFinal("Horn1TargetCoolingTubeDwnRetWater", vMHe);

    Create("Horn1TargetDownstrHeContainerCap");
    PlaceFinal("Horn1TargetDownstrHeContainerCap", vMHe);					  				  
}
void LBNEVolumePlacements::PlaceFinalUpstrTargetSimpleCylinder(G4PVPlacement *mother) {
//     std::cerr << " Entering LBNEVolumePlacements::PlaceFinalUpstrTargetSimpleCylinder ...radius  " 
//               << fSimpleTargetRadius <<  std::endl;
     // 
     // Straight G4 coding.  No alignment needed. Simple is Simple 
     //
     std::vector<double> fParams(3,0.);
     fParams[1] = fSimpleTargetRadius ; 
     fParams[2] = fSimpleTargetLength;
     std::string volumeName("TargetUsptreamSimpleCylinder");
     G4Tubs* aTube = new G4Tubs(volumeName, fParams[0], fParams[1], fParams[2]/2., 0., 360.*CLHEP::deg);
     G4LogicalVolume* fCurrent = new G4LogicalVolume(aTube, G4Material::GetMaterial("Target"), volumeName); 
     G4ThreeVector posTmp; posTmp[1] = 0.;  posTmp[1] = 0.;  posTmp[2] = 0.; 
     new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, fCurrent, 
				    G4String("TargetUsptreamSimpleCylinder_P"), 
				   mother->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
}
void LBNEVolumePlacements::PlaceFinalUpstrTargetSimpleBox(G4PVPlacement *mother) {
//     std::cerr << " Entering LBNEVolumePlacements::PlaceFinalUpstrTargetSimpleCylinder ...radius  " 
//               << fSimpleTargetRadius <<  std::endl;
     // 
     // Straight G4 coding.  No alignment needed. Simple is Simple 
     //
     std::vector<double> fParams(3,0.);
     fParams[0] = fSimpleTargetWidth; 
     fParams[1] = fSimpleTargetHeight; 
     fParams[2] = fSimpleTargetLength;
     std::string volumeName("TargetUsptreamSimpleBox");
     G4Box* aBox = new G4Box(volumeName, fParams[0]/2, fParams[1]/2, fParams[2]/2.);
     G4LogicalVolume* fCurrent = new G4LogicalVolume(aBox, G4Material::GetMaterial("Target"), volumeName); 
     G4ThreeVector posTmp; posTmp[1] = 0.;  posTmp[1] = 0.;  posTmp[2] = 0.; 
     new G4PVPlacement((G4RotationMatrix *) 0, 
	                            posTmp, fCurrent, 
				    G4String("TargetUsptreamSimpleCylinder_P"), 
				   mother->GetLogicalVolume(), false, 0, fCheckVolumeOverLapWC);
}

// Quynh, August 2014. 
void LBNEVolumePlacements::AdaptForMultiSphereTarget() { 

    fTargetSLengthGraphite = 969.285*CLHEP::mm ;//953.8*CLHEP::mm; // Quynh calculation . real target.(7). 57 spheres x (17mm diameter + 0.005 tolerance) 

    fTargetSLengthDownstrEnd = 0; // Quynh. remove this. real target. (8). not sure. based on T2K target, add 9mm for Helium return when calculate target position.
// 
// The following quantity can be changed via messenger command. 
// 
//    fTargetLengthIntoHorn = fTargetSLengthGraphite- 400*CLHEP::mm; //Quynh -> this can be overwritten by a data card
//	fTargetLengthIntoHorn = fTargetSLengthGraphite - 350.*CLHEP::mm + 25.3; //  Integration drawing 8875.000-ME-363028
// Does not belong here!. P.L. 
   //last number emprically set 
     // to have the first fin target at coord 0.3mm, to follow the convention. 
    double  targetNumSphereNominal = fTargetSLengthGraphite/(fMultiSphereTargetRadius*2+0.005*CLHEP::mm); 
    std::cout << "---Sphere Diameter: " << fMultiSphereTargetRadius*2;
    std::cout << "---Nominal number of Sphere: "<< targetNumSphereNominal <<std::endl;

    fTargetNumSphere = (int) (targetNumSphereNominal + 0.5); //rounding-fTargetNumSphere has been defined as int in .hh
 	 std::cout << "fTargetNumSphere is rounded to: "<< fTargetNumSphere <<std::endl;

    double deltaNumSphere =  targetNumSphereNominal - (double) fTargetNumSphere; 
 	 
    std::cout << "LBNEVolumePlacements::SegmentTarget, total number of sphere:  " << fTargetNumSphere << 
"---Delta Num of Sphere: " << deltaNumSphere << std::endl; 
    std::cout << "fTargetSLength: " << fTargetSLength << "---fTargetLengthOutsideHorn before adjustment: " 
 						 <<	 fTargetLengthOutsideHorn << std::endl; 

     const double oldLength = fTargetSLengthGraphite;
     std::cout << "Input fTargetsLengthGraphite: "<< oldLength <<std::endl;
     fTargetSLengthGraphite = fTargetNumSphere*(fMultiSphereTargetRadius*2 + 0.005*CLHEP::mm); //tolerance 5 micron
 	 std::cout	 <<"---fTargetSLengthGraphite adjusted to " <<fTargetSLengthGraphite<<"="<<	 fTargetNumSphere <<"x ("
 						 <<	 fMultiSphereTargetRadius<<"x 2 + 0.005mm)"<<std::endl;
 			 
 	 
     if (std::abs(oldLength - fTargetSLengthGraphite) > 0.001*CLHEP::mm)
    	  std::cout << "LBNEVolumePlacements::segmentTarget: Warning: re-adjust the target length from " <<
      	oldLength <<  " to " << fTargetSLengthGraphite << " to get  an integer number of spheres " << std::endl;
    // 
    // Change the diameter of the Helium Tube 
    // 	
    fTargetHeContTubeInnerRadius = 35.2*CLHEP::mm/2.; // T2K drawing, August 2014. To be reviewed, Alberto in contact with Rutherford. 
}

void LBNEVolumePlacements::PlaceFinalMultiSphereTarget(LBNEVolumePlacementData *plHelium, G4PVPlacement *mother) {
//Quynh. real target . (8)
     LBNEVolumePlacementData* plInfo = this->Create(G4String("TargetMultiSphere"));
     G4ThreeVector posTmp;
//     const double zCoordTmp = -1.0*fParams[2]/2.+(fParams[2]-fTargetLengthOutsideHorn)+(fMultiSphereTargetRadius+0.005*CLHEP::mm);//Quynh.. the container is longer than target and need to be fixed. not sure how
     const double zCoordTmp = -plHelium->fParams[2]/2. +  fMultiSphereTargetRadius + 0.005*CLHEP::mm ; // Simplify... P.L. 
     posTmp[0] = 0.; posTmp[1] = 0.;
     for (int iSphere=0; iSphere !=fTargetNumSphere; iSphere++) {
	posTmp[2] = zCoordTmp + iSphere*(fMultiSphereTargetRadius*2+0.005*CLHEP::mm);
	std::ostringstream cNumStrStr; cNumStrStr << iSphere;
	new G4PVPlacement((G4RotationMatrix *) 0, posTmp, plInfo->fCurrent, 
		          (G4String("TargetUsptreamMultiSphere_P") + cNumStrStr.str()),
		          mother->GetLogicalVolume(), false, iSphere, fCheckVolumeOverLapWC);	//copy Number = iSphere 
//	std::cout<<"sphere: "<< iSphere<< std::endl;
    }
		
}
void LBNEVolumePlacements::PlaceFinalNoSplitHorn1(G4PVPlacement* mother, G4PVPlacement* motherTop) {

   std::cerr << " Entering LBNEVolumePlacements::PlaceFinalNoSplitHorn1, mother name  " << 
             mother->GetLogicalVolume()->GetName() << std::endl; 
//
   if (fHorn1RadiusBigEnough) {
     std::cerr << " Obsolete data card /LBNE/det/TargetAndHorn1RadiusBigEnough used, quit here for good ! " << std::endl;
     G4Exception("LBNEVolumePlacements::PlaceFinalNoSplitHorn1", "InvalidSetup", FatalErrorInArgument, "Wrong data card");
   }
   const bool doCheckOverlapNoG4ConsBug = false; // in v4.9.6., this check is worthless.
//   const bool doCheckOverlapNoG4ConsBug = true; 
    // in v4.9.6., bug corrected by Tatiana, Sept. 3 2014, if G4Cons source file added to this directory..
   
   //
   // New Plug Code. Based on what John LoSecco did. Adapted for the new geometry, August 31 2014. 
   // 
  if(fConstructPlugInHorn1){
    G4ThreeVector posTmp; posTmp[0] =0.; posTmp[1] =0.; 
    G4Tubs* plugTube = new G4Tubs("Plug", fPlugInnerRadius, fPlugOuterRadius, fPlugLength*.5, 0., 360.*CLHEP::deg);
    G4LogicalVolume *plugl = new G4LogicalVolume(plugTube, G4Material::GetMaterial(fPlugMaterial.c_str()), "PlugInHorn1");
    const LBNEVolumePlacementData *plMotherTop = Find(G4String("pluggingHorn1"), "TargetHallAndHorn1", G4String("Create"));
    const double posHorn1 = plMotherTop->fPosition[2]; // With respect to Z = 0 (MCZERO, drawing 36309x)
    posTmp[2] = -1.0*posHorn1 + fPlugZPosition;
    new G4PVPlacement((G4RotationMatrix *) 0, posTmp, plugl, "PlugInHorn1_P",
		      motherTop->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
  }
  if (fUseHorn1Polycone || fUseHornsPolycone) {
     std::cerr << "LBNEVolumePlacements::PlaceFinalNoSplitHorn1, calling for Simple Horn PolyNumber 1 " << std::endl;
     PlaceFinalSimpleHornPolyNumber(0, mother);
     return;
  }
// 
// Start with upstream Inner to Out transition.
//
{
      G4String nameHorn1IO("UpstrHorn1TransInnerOuter");
      G4String nameHorn1IOCont(nameHorn1IO + G4String("Cont"));
      Create(nameHorn1IOCont);
      G4PVPlacement *vHorn1IOCont = PlaceFinal(nameHorn1IOCont, mother);
      for (size_t iPartIO = 0; iPartIO != fTargetHorn1InnerRadsUpstr.size(); iPartIO++) {
        std::ostringstream nameStrStr; nameStrStr << nameHorn1IO << "Part" << iPartIO;
        G4String nameStr(nameStrStr.str());
        Create(nameStr);
        PlaceFinal(nameStr, vHorn1IOCont);
      }
   }
   const double in = 2.54*CLHEP::cm;
//   const double innerConductThicknessNeck = 4.0*CLHEP::mm; // only relevant if fHorn1RadiusBigEnough is true. 
//   const double fHorn1RMinAllBG = fTargetHeContTubeInnerRadius + fTargetHeContTubeThickness + 1.25*CLHEP::mm; // Only 250 micron tolerance.  
//
//   std::cerr << " Start testing PlaceFinalHorn1, mother Logical volume name " << mother->GetLogicalVolume()->GetName() << std::endl;
   if ((std::abs(fHorn1LongRescale - 1.0) < 1.0e-5) && (std::abs(fHorn1RadialRescale - 1.0) < 1.0e-5)) 
                  fHorn1Equations[0].test1(); // this supposed to work.  But not if we rescale the Horn1, 
		  // since we compare with hardtyped data extracted from a blue print 
//   std::cerr << " Test 1 passed " << std::endl;
//   fHorn1Equations[3].test1(); // This supposed to fail... 
   LBNEVolumePlacementData *plTrUpst = this->Create("Horn1IOTransCont");
   G4PVPlacement *vTrUpst = this->PlaceFinal("Horn1IOTransCont", mother); //August 21, check longitudinal offsets here... 
   //
   // These sub-volumes will be never misaligned with respect to each other, or the container volume 
   // above.  So skip the VolumePlacement utilities. Code bloat here instead in the Create method.. Oh well.. 
   //
   for (size_t k=0; k!=fHorn1UpstrLengths.size(); k++) {
     std::ostringstream nameStrStr; nameStrStr << "Horn1IOTransInnerPart" << k;
     G4String nameStr(nameStrStr.str());
     G4Cons *aCons = new G4Cons(nameStr, fHorn1UpstrInnerRadsUpstr[k],fHorn1UpstrInnerRadsOuterUpstr[k],
                                         fHorn1UpstrInnerRadsDownstr[k],fHorn1UpstrInnerRadsOuterDownstr[k],
	                              fHorn1UpstrLengths[k]/2., 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrent = new G4LogicalVolume(aCons, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;  
     posTmp[2] = -1.0*(plTrUpst->fParams[2])/2. + fHorn1UpstrZPositions[k];			      
     new G4PVPlacement((G4RotationMatrix *) 0,	posTmp, pCurrent, nameStr + std::string("_P"), 
                        vTrUpst->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));
//     const G4PVPlacement *plHorn1IO = new G4PVPlacement((G4RotationMatrix *) 0,	posTmp, pCurrent, nameStr + std::string("_P"), 
//                        vTrUpst->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));
// Debugging misshaped volume, Oct 28 2013. 
//     if (k == 3) {
//        this->CheckOverlaps(plHorn1IO, 10000, 1.0e-5, true); 	
//        std::cerr << " Placing IOTrans Sub elem " << k << " at z = " << fHorn1UpstrZPositions[k] << " (abs), rel " << 
//            posTmp[2] << " length " << fHorn1UpstrLengths[k] << std::endl;
//        std::cerr << " RIUps " << fHorn1UpstrInnerRadsUpstr[k] << " ROUps " << fHorn1UpstrInnerRadsOuterUpstr[k];  			    
//        std::cerr << " RIDwn " << fHorn1UpstrInnerRadsDownstr[k] << " RODwn " << fHorn1UpstrInnerRadsOuterDownstr[k] << std::endl; 
//     }			    
   }
   
   for (size_t k=0; k!= fHorn1UpstrOuterIOTransInnerRads.size(); k++) {
     std::ostringstream nameStrStr; nameStrStr << "Horn1IOTransOuterPart" << k;
     G4String nameStr(nameStrStr.str());
     G4Tubs *aTubs = new G4Tubs(nameStr, fHorn1UpstrOuterIOTransInnerRads[k],
                                          fHorn1UpstrOuterIOTransInnerRads[k] + fHorn1UpstrOuterIOTransThicks[k],
	                              fHorn1UpstrOuterIOTransLengths[k]/2., 0., 360.0*CLHEP::deg);
     G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;  
     posTmp[2] = -1.0*(plTrUpst->fParams[2])/2. + fHorn1UpstrOuterIOTransPositions[k];			      
     G4LogicalVolume *pCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1AllCondMat), nameStr);
     new G4PVPlacement(	(G4RotationMatrix *) 0,	posTmp, pCurrent, nameStr + std::string("_P"), 
                        vTrUpst->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	      
   }
   //
   // We place more volumes in the mother volume
   //
   G4PVPlacement *vUpst = mother;
   const LBNEVolumePlacementData *plHUpst = Find(G4String("InnerConductors"), "Horn1PolyM1", G4String("Create"));
  
// Start by checking possible radial overlap at the downstream end of the target. 
// The size and position of Horn1TopLevelUpstr and Horn1TargetDownstrHeContainer

  fZHorn1ACRNT1Shift = 3.316*in*fHorn1LongRescale; //  Drawing 8875.112-MD-363097
  // Correct here for the new dimension of the "No split" mother volume. 

  this->CheckHorn1InnerConductAndTargetRadii();

  const double z21p088 = fHorn1LongRescale*21.088*in; //Equation change.  
  const double zOffsetDrawingUpstrEdge = 5.752*in*fHorn1LongRescale; // On the inner conductor, excluding I/O trans. Drawing 363097 
     
  double lengthInnerConductUpstr = z21p088 - (3.316*in*fHorn1LongRescale) - 0.010*CLHEP::mm; // Up to Z = 21.088, Drawing coordinate system.  
  
  int numSubSect = GetNumberOfInnerHornSubSections(0, 0., lengthInnerConductUpstr, 10);
   // Fill with it one or more inner conductor conical section
   // We require a precision of 5 microns in the radius. 
   double deltaZ = lengthInnerConductUpstr/numSubSect;
   for (int iSub=0; iSub != numSubSect; iSub++) { 
     //
      const double zzBegin = fZHorn1ACRNT1Shift + (iSub*deltaZ); // from the 
      const double zzEnd = zzBegin + deltaZ;
     std::ostringstream nameStrStr; nameStrStr << "Horn1UpstrSubSect" << iSub;
     G4String nameStr(nameStrStr.str());
// Smooth transition between equation 1 and 2 
     const double rMin1Eqn1 = fHorn1Equations[0].GetVal(zzBegin); // Equation 1 or 0
     const double rMin2Eqn1 = fHorn1Equations[0].GetVal(zzEnd);
     const double rMin1Eqn2 = fHorn1Equations[1].GetVal(zzBegin); // Equation 1 or 0
     const double rMin2Eqn2 = fHorn1Equations[1].GetVal(zzEnd);
     const double ratio10vs1 = std::min(1.0, (zzBegin/(21.0888*in*fHorn1LongRescale)));
     double rMin1 = rMin1Eqn2*ratio10vs1 + (1.0-ratio10vs1)*rMin1Eqn1;
     const double ratio20vs1 = std::min(1.0, (zzEnd/(21.0888*in*fHorn1LongRescale)));
     double rMin2 = rMin2Eqn2*ratio20vs1 + (1.0-ratio20vs1)*rMin2Eqn1;
     double rMax1 = fHorn1Equations[5].GetVal(zzBegin) + fWaterLayerThickInHorns + 0.0025*CLHEP::mm; 
       // Equation 6 (Drawing 8875.112-MD 363104)
     double rMax2 = fHorn1Equations[5].GetVal(zzEnd) + fWaterLayerThickInHorns + 0.0025*CLHEP::mm;     
     std::cerr << " Inner radius for section " << nameStr << " At zzBegin " << zzBegin << " to " << zzEnd 
               << " rMin1 " << rMin1 << " rMin2 " << rMin2 
	       << " rMax1 " << rMax1 << " rMax2 " << rMax2 << std::endl;
     G4Cons *aCons = new G4Cons(nameStr, rMin1, rMax1,rMin2, rMax2,
	                              (deltaZ - 0.005*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrent = new G4LogicalVolume(aCons, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;
     // plHUpst constain part of the Inner Outer Transition. Shift downtream by it's length 
     posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zOffsetDrawingUpstrEdge + deltaZ/2. + iSub*deltaZ + 0.055*CLHEP::mm;
     std::cerr << " Placing section " << nameStr << " at z = " << posTmp[2] << std::endl;			      
     std::cerr << " Beginning of the volume with respect to upstre edge "
               << posTmp[2] + plHUpst->fParams[2]/2 - deltaZ/2. << " downstrEdge " << 
	       posTmp[2] + plHUpst->fParams[2]/2 + deltaZ/2. << std::endl;
// Shift in Z until no clash?????
//     posTmp[2]  += 30*CLHEP::mm;
// Unresolved clash here!!! 
//	       
     G4PVPlacement *vSub = new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrent, nameStr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
			
    if (fWaterLayerThickInHorns	> 0.002*CLHEP::mm) {
     nameStrStr.str("");  nameStrStr.clear(); nameStrStr << "Horn1UpstrSubSect" << iSub << "Water";
     nameStr = nameStrStr.str();
     G4Cons *aConsW = new G4Cons(nameStr, rMax1 - fWaterLayerThickInHorns, rMax1-0.001*CLHEP::mm,
                                         rMax2 - fWaterLayerThickInHorns, rMax2-0.001*CLHEP::mm,
	                              (deltaZ - 0.0075*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrentW = new G4LogicalVolume(aConsW, G4Material::GetMaterial(std::string("Water")), nameStr);
     posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] =0.;			      
     new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentW, nameStr + std::string("_P"), 
                        vSub->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
    }
   }
    // on the number of subsections for the inner conductor, for the upstream part of Horn1 
   // Now add the welding joint between the most upstream part of the inner conductor and the Inner Outer transition section
   // Drawing Drawing 8875.112-MD 363104
   {
     G4String nameStr("Horn1UpstrSubSect0WeldUpstr");
     const double length = 12.0*CLHEP::mm; // Make it a bit shorter, it is rounded... 
     double rTmp1 = fHorn1Equations[5].GetVal(3.2645*in) 
                              + 0.02*CLHEP::mm + fWaterLayerThickInHorns;
			      
        // place it a little more detached..Also, the weld is on top of the layer of water.. Oh well.. 
      const double rTmp2 = rTmp1 + 1.8*CLHEP::mm; // 
     G4Tubs *aTubs = new G4Tubs(nameStr, rTmp1, rTmp2, 
	                           length/2.   , 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;
     posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zOffsetDrawingUpstrEdge + length/2. + 1.0*CLHEP::mm; // extra space..
     
     std::cerr << " Placing section " << nameStr << " at z = " << posTmp[2] << " radii " 
               << rTmp1 << " , " <<  rTmp2  << " length " << length << std::endl;			      
     new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrent, nameStr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
   
   }
   // Now place the first support ring, and the spider hang
   const double lengthHangerRing = fHorn1LongRescale*0.750*in;
   std::cerr << " First Spider Hanger is in Horn1Upstr section " << std::endl;
   
   G4String nameStrFirstHanger("Horn1UpstrSpiderHanger");
   const double zPosCenterMotherVolume = -1.0*(plHUpst->fParams[2])/2.  + 2.436*in + 0.5*CLHEP::mm + 
                                         19.347*in*fHorn1LongRescale + lengthHangerRing/2. ;  // Drawing 363093 and  363097			   
   const double zPosUpstrDrawingCoord = 19.347*in*fHorn1LongRescale; 			   
   
   this->Horn1InstallSpiderHanger(nameStrFirstHanger, zPosUpstrDrawingCoord, 
	                                    zPosCenterMotherVolume, vUpst );			       
   // Outer tube 
   const double zShiftDrawingDownstr = 2.436*in*fHorn1LongRescale; 
      // from now, a better orgin is the upstram point on mother Horn1PolyM1
   
   G4String nameStr("Horn1OutrTube");
   const double lengthOutT = (127.550 - 1.510)*in*fHorn1LongRescale - 1.0*CLHEP::mm;
   // displace 100 microns to avoid clash with spider hanger.  
   G4Tubs *aTubs = new G4Tubs(nameStr, fHorn1OuterTubeInnerRad + 0.1*CLHEP::mm, 
                                        fHorn1OuterTubeOuterRad + 0.1*CLHEP::mm, lengthOutT/2.   , 0., 360.0*CLHEP::deg);
   G4LogicalVolume *pCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1AllCondMat), nameStr);
   G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;
   const double zOffOut = 3.316*in*fHorn1LongRescale;
   posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zShiftDrawingDownstr + zOffOut + lengthOutT/2. + 0.25*CLHEP::mm;			      
   std::cerr << " Installing Outer tube sptream at " << posTmp[2] << " lengthOutT " << lengthOutT << std::endl;
   new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrent, nameStr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
				
   // Outer Tube Upstream flange. See drawing  363094
   G4String nameStrOutFlUpstr("Horn1UpstrOutrTubeFlange");
   const double lengthOutFlUpstr = 1.0*in*fHorn1LongRescale; //Not cleanly shown on drawing 363094 
   const double rTmpOutFlUpstrInner = fHorn1OuterTubeOuterRad + 0.1*CLHEP::mm;
   const double rTmpOutFlUpstrOuter = rTmpOutFlUpstrInner + 2.5*in*fHorn1RadialRescale; // Still a guess.. Probably a bit oversized.  
   aTubs = new G4Tubs(nameStrOutFlUpstr,  rTmpOutFlUpstrInner, rTmpOutFlUpstrOuter, lengthOutFlUpstr/2.0, 0., 360.0*CLHEP::deg);
   pCurrent = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1AllCondMat), nameStrOutFlUpstr);
   posTmp[0] = 0.; posTmp[1] = 0.;
   posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zShiftDrawingDownstr + zOffOut + lengthOutFlUpstr + 0.055*CLHEP::mm;
   new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrent, nameStrOutFlUpstr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
			//
   
   double lengthToTheNeck = (30.3150 - 21.088)*in*fHorn1LongRescale; // Drawing 363105 
   
   // Downstream of the neck, we install everything in Horn1PolyM1.. Simpler.. 
   
   // Let us call this the mid-section.. Historical notation.. It will install in the upstream section.. 
  //
  // Install the length of inner conductor, from downstream end of the target to Zdrawing 21.0888 inches 
  //
//  std::cerr << " ... Length to the neck " << lengthToTheNeck << std::endl;
  {
     numSubSect = 4; 
     deltaZ = lengthToTheNeck/numSubSect;
     const double zOffStart = 21.088*in*fHorn1LongRescale + 0.025*CLHEP::mm;
//     std::cerr << " ...... delta z to 21.0888 " << deltaZ << std::endl;
     for (int iSub = 0; iSub != numSubSect; iSub++) {					      
       const double zzBegin = z21p088 + iSub*deltaZ;
       const double zzEnd = zzBegin + deltaZ;
       std::ostringstream nameStrStr; nameStrStr << "Horn1ToNeckPartM0SubSect" << iSub;
       nameStr = nameStrStr.str();
       const double rMin1Eqn1 = fHorn1Equations[0].GetVal(zzBegin); // Equation 1 or 0
       const double rMin2Eqn1 = fHorn1Equations[0].GetVal(zzEnd);
       const double rMin1Eqn2 = fHorn1Equations[1].GetVal(zzBegin); // Equation 1 or 0
       const double rMin2Eqn2 = fHorn1Equations[1].GetVal(zzEnd);
       const double rMin1 = ((numSubSect - iSub -1)*rMin1Eqn1 + ((iSub+1)*rMin1Eqn2))/numSubSect;
       const double rMin2 = ((numSubSect - iSub -1)*rMin2Eqn1 + ((iSub+1)*rMin2Eqn2))/numSubSect;
       const double rMax1 = fHorn1Equations[5].GetVal(zzBegin) + fWaterLayerThickInHorns + 0.0025; 
       // Equation 6 (Drawing 8875.112-MD 363104)
       const double rMax2 = fHorn1Equations[5].GetVal(zzEnd) + fWaterLayerThickInHorns + 0.0025;
       const double lengthTmp = deltaZ - 0.050*CLHEP::mm;    
       G4Cons *aCons = new G4Cons(nameStr, rMin1, rMax1,rMin2, rMax2, lengthTmp/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentSu = new G4LogicalVolume(aCons, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zShiftDrawingDownstr + zOffStart + deltaZ/2. + iSub*deltaZ;
       std::cerr << " Installing mid section Horn1, length " << lengthTmp << " at Z = " << posTmp[2] 
                 << " Rads " << rMin1 << " , " << rMin2 << " max " << rMax1 << " " << rMax2 << std::endl;
       G4PVPlacement *vSub = new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentSu, nameStr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
			
      if (fWaterLayerThickInHorns	> 0.002*CLHEP::mm) {
       nameStrStr.str(""); nameStrStr.clear(); nameStrStr << "Horn1ToNeckPartM0SubSect" << iSub << "Water";
       nameStr=nameStrStr.str();
       G4Cons *aConsW = new G4Cons(nameStr, rMax1 - fWaterLayerThickInHorns, rMax1-0.001*CLHEP::mm,
                                         rMax2 - fWaterLayerThickInHorns, rMax2-0.001*CLHEP::mm,
	                              (lengthTmp - 0.0075*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentW = new G4LogicalVolume(aConsW, G4Material::GetMaterial(std::string("Water")), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] =0.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentW, nameStr + std::string("_P"), 
                        vSub->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
      }
    } // of the number of subsections in the upstream part of the neck region (zDrawing = 21.0888 inches) 
  }
    // The first weld for this section. 
   {
     nameStr = std::string("Horn1UpstrSubSect1Weld0");
     posTmp[0] = 0.; posTmp[1] = 0.;
     double length = 24.0*CLHEP::mm; //Cover two real sections... 
     posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + z21p088 + length/2 + zShiftDrawingDownstr; // with respecto the upstr edge of Horn1TopLevelDownstr
     double rTmp1 = fHorn1Equations[5].GetVal(z21p088 - length - 1.0*CLHEP::mm) 
                         + 0.015*CLHEP::mm + fWaterLayerThickInHorns;
        // place it a little more detached..The radius is estimated on the upstream side, biggest radius.
     double rTmp2 = rTmp1 + 1.8*CLHEP::mm; //
     G4Tubs *aTubsW = new G4Tubs(nameStr, rTmp1, rTmp2, length/2.   , 0., 360.0*CLHEP::deg);
     std::cerr << " Installing the weld   " << nameStr << " at Z " << posTmp[2] << std::endl;
     G4LogicalVolume *pCurrentW = new G4LogicalVolume(aTubsW, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentW, nameStr + std::string("_P"), 
                        vUpst->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
   }
   // 
   // Now the dowstream section. We no longer create the mother volume for it. We will install it directly in 
   // Horn1PolyM1.  				
   //
   //
   //
   // Now, the neck. Just a tube 
   //
   {
     nameStr = G4String("Horn1Neck");
     const double zNeckDrawing = fHorn1LongRescale*(30.3150)*in; //start of the neck.. 
     double rTmp1 = fHorn1RadialRescale*(0.709*in/2.); // Drawing 8875.112-MD 363105
     double rTmp2 = fHorn1RadialRescale*(1.063*in/2.) + fWaterLayerThickInHorns + 0.025*CLHEP::mm; 
      // Drawing 8875.112-MD 363105
     fHorn1NeckInnerRadius = rTmp1; // For use in computing the magnetic field 
//     fHorn1NeckOuterRadius = rTmp2; // For use in computing the magnetic field 
// Bug fix, September 2014 : there are no skin depth effect in water!... 
     fHorn1NeckOuterRadius = rTmp2 - fWaterLayerThickInHorns - 0.025*CLHEP::mm; // For use in computing the magnetic field 
     const double length = fHorn1LongRescale*1.5680*in - 0.050*CLHEP::mm; // last term to absord 
        // small shifts in the upstream part.. 
     G4Tubs *aTubsNe1 = new G4Tubs(nameStr, rTmp1, rTmp2, 
	                           length/2.   , 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrentNe1 = new G4LogicalVolume(aTubsNe1, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     posTmp[0] = 0.; posTmp[1] = 0.;
     posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zNeckDrawing  + zShiftDrawingDownstr + length/2. + 0.025*CLHEP::mm;
     			      
     G4PVPlacement* vSub = new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentNe1, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
     if (fWaterLayerThickInHorns > 0.002*CLHEP::mm) {
       G4String nameStrW(nameStr); nameStrW += G4String("Water");
       G4Tubs *aTubsW = new G4Tubs(nameStrW, rTmp2-fWaterLayerThickInHorns-0.012*CLHEP::mm, rTmp2-0.012*CLHEP::mm, 
	                           length/2.   , 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentW = new G4LogicalVolume(aTubsW, 
                             G4Material::GetMaterial(std::string("Water")), nameStrW);
       posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] =0.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentW, nameStrW + std::string("_P"), 
                        vSub->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
      }
   }				
    // The downstream part of the real section that has the neck.  
   {
     const double zStartDrawing =  fHorn1LongRescale*31.8827*in;
     const double zEndDrawing = fHorn1LongRescale*(41.0776)*in;
     fHorn1ZDEndNeckRegion = zEndDrawing;
     numSubSect = GetNumberOfInnerHornSubSections(3, zStartDrawing, 
                                                      zEndDrawing, 10); // These Z position are from the start of the inner conductor.   
     deltaZ = (zEndDrawing - zStartDrawing)/numSubSect;
     for (int iSub = 0; iSub != numSubSect; iSub++) {					      
       const double zzBegin = zStartDrawing + iSub*deltaZ;
       const double zzEnd = zzBegin + deltaZ;
       std::ostringstream nameStrStr; nameStrStr << "Horn1DownstrPart1SubSect" << iSub;
       nameStr = nameStrStr.str();
       double rMin1 = fHorn1Equations[3].GetVal(zzBegin); 
       double rMin2 = fHorn1Equations[3].GetVal(zzEnd);
       double rMax1 = fHorn1Equations[7].GetVal(zzBegin) + fWaterLayerThickInHorns + 0.0025; 
                 // Equation 6 (Drawing 8875.112-MD 363104)
       double rMax2 = fHorn1Equations[7].GetVal(zzEnd) + fWaterLayerThickInHorns + 0.0025;     
       G4Cons *aConsSuU = new G4Cons(nameStr, rMin1, rMax1,rMin2, rMax2,
	                              (deltaZ - 0.005*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentSuU = new G4LogicalVolume(aConsSuU, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zzBegin   + zShiftDrawingDownstr + deltaZ/2.;			      
       G4PVPlacement *vSub = new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentSuU, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
			
      if (fWaterLayerThickInHorns > 0.002*CLHEP::mm) {
       nameStrStr.str(""); nameStrStr.clear();  nameStrStr << "Horn1DownstrPart1SubSect" << iSub << "Water";
       nameStr = nameStrStr.str();
       G4Cons *aConsWa = new G4Cons(nameStr, rMax1 - fWaterLayerThickInHorns, rMax1-0.001*CLHEP::mm,
                                         rMax2 - fWaterLayerThickInHorns, rMax2-0.001*CLHEP::mm,
	                              (deltaZ - 0.0075*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentWa = new G4LogicalVolume(aConsWa, G4Material::GetMaterial(std::string("Water")), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] =0.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentWa, nameStr + std::string("_P"), 
                        vSub->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
      }
    } // of the number of subsection to the neck
    // The weld at the end  
   {
     nameStr = std::string("Horn1DownstrPart1Weld1");
     const double zWW = fHorn1LongRescale*(41.0776)*in;; 
      const double length = 24.0*CLHEP::mm; //Cover two real sections... 
     const double rTmp1 = fHorn1Equations[7].GetVal(zWW + length) + 0.150*CLHEP::mm + fWaterLayerThickInHorns;
        // place it a little more detached..Also, the weld is on top of the layer of water.. Oh well.. 
      const double rTmp2 = rTmp1 + 1.8*CLHEP::mm; // 
     G4Tubs *aTubsWW = new G4Tubs(nameStr, rTmp1, rTmp2, 
	                           length/2.   , 0., 360.0*CLHEP::deg);
     G4LogicalVolume *pCurrentWW = new G4LogicalVolume(aTubsWW, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
     posTmp[0] = 0.; posTmp[1] = 0.;
     posTmp[2] =  -1.0*(plHUpst->fParams[2])/2. + zWW  + zShiftDrawingDownstr + length/2.;			      
     new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentWW, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
   }
  } // The downstream part horn1, starting downstream of the real section that has the neck
  // More Inner conductors, covering the drawings 8875.112-MD 363105 through 363109 included.
  // Radial equation 5 and 8 (indices 4 and 7 in our arrays) 
  // From ZDrawing 41.0576 to 117.126
  {
     const double zStartDrawing =  fHorn1LongRescale*41.108*in;
     const double zEndDrawing = fHorn1LongRescale*117.126*in;
     fHorn1ZEndIC = zEndDrawing; // For use in the Magnetic field class. 
     numSubSect = GetNumberOfInnerHornSubSections(4, zStartDrawing, 
                                                      zEndDrawing, 10); // These Z position are from the start of the inner conductor.   
     deltaZ = (zEndDrawing - zStartDrawing)/numSubSect;
//     std::cerr << " Number of subsection for the downstream half of Horn1 " << numSubSect 
//                 << " deltaz " << deltaZ << std::endl;
     for (int iSub = 0; iSub != numSubSect; iSub++) {					      
       const double zzBegin = zStartDrawing + iSub*deltaZ;
       const double zzEnd = zzBegin + deltaZ;
       std::ostringstream nameStrStr; nameStrStr << "Horn1DownstrPart1SubSect" << iSub;
       nameStr = nameStrStr.str();
       const double rMin1 = fHorn1Equations[4].GetVal(zzBegin); // Equation 1
       const double rMin2 = fHorn1Equations[4].GetVal(zzEnd);
       const double rMax1 = fHorn1Equations[7].GetVal(zzBegin) + fWaterLayerThickInHorns + 0.0025; 
          // Equation 6 (Drawing 8875.112-MD 363104)
       const double rMax2 = fHorn1Equations[7].GetVal(zzEnd) + fWaterLayerThickInHorns + 0.0025;     
       G4Cons *aConsSu = new G4Cons(nameStr, rMin1, rMax1,rMin2, rMax2,
	                              (deltaZ - 0.005*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentSu = new G4LogicalVolume(aConsSu, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zzBegin  + zShiftDrawingDownstr + deltaZ/2.;			      
       G4PVPlacement *vSub = new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentSu, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
			
      if (fWaterLayerThickInHorns > 0.002*CLHEP::mm) {
       nameStrStr.str(""); nameStrStr.clear(); nameStrStr << "Horn1DownstrPart1SubSect" << iSub << "Water";
       nameStr = nameStrStr.str();
       G4Cons *aConsWa = new G4Cons(nameStr, rMax1 - fWaterLayerThickInHorns, rMax1-0.001*CLHEP::mm,
                                         rMax2 - fWaterLayerThickInHorns, rMax2-0.001*CLHEP::mm,
	                              (deltaZ - 0.0075*CLHEP::mm)/2., 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentWa = new G4LogicalVolume(aConsWa, G4Material::GetMaterial(std::string("Water")), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.; posTmp[2] =0.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentWa, nameStr + std::string("_P"), 
                        vSub->GetLogicalVolume(), false, 1, (fCheckVolumeOverLapWC && doCheckOverlapNoG4ConsBug));	
      }
    } // of the number of subsection to the neck 
   {
     // Now the Hangers. (two of them.. ) 
     {
       G4String nameStr2ndHanger("Horn1DownstrSecondSpiderHanger");
       double zLocDrawing = zStartDrawing + fHorn1LongRescale*1.416*in;
       double zLocPosM =  -1.0*(plHUpst->fParams[2])/2. + zLocDrawing + zShiftDrawingDownstr + 0.375*in*fHorn1LongRescale; // with respect to the center of 
       // of the mother volume. 
       this->Horn1InstallSpiderHanger( nameStr2ndHanger, zLocDrawing, zLocPosM, mother); 
       
       G4String nameStrSecondHanger("Horn1DownstrThirdSpiderHanger");
       zLocDrawing = fHorn1LongRescale*(80.9951 + 1.791)*in;
//       zLocPosM =   -1.0*(plHUpst->fParams[2])/2. + -1.0*(plHUpst->fParams[2])/2. + zLocDrawing + zShiftDrawingDownstr + 0.375*in*fHorn1LongRescale;
// Typo, unveiled looking at the HEPRep representation, by Laura F., Aug 4-5 2015. 
       zLocPosM =   -1.0*(plHUpst->fParams[2])/2. + zLocDrawing + zShiftDrawingDownstr + 0.375*in*fHorn1LongRescale;
       this->Horn1InstallSpiderHanger( nameStrSecondHanger, zLocDrawing, zLocPosM, mother); 
     }
     // now a few welds.. 
     std::vector<double> zLocWelds(4,0.); // Drawing coordinate system
     zLocWelds[0] = fHorn1LongRescale*61.0464*in;
     zLocWelds[1] = fHorn1LongRescale*81.0151*in;
     zLocWelds[2] = fHorn1LongRescale*100.9839*in;
     zLocWelds[3] = fHorn1LongRescale*116.5*in; // Cheat a bit, place it upstream to make sure it does not overlap with the end
     // April 2 .. Zeongtae notice this cheat, it manifest itself as a visible gap of a bout 1/2 inch in Z 
     // Let us fix this by re-adjusting the position of this weld, which, after checking the end and 
     // beginning of the sub section number 5 and flange below, we now have: 
     zLocWelds[3] = fHorn1LongRescale*117.1126*in - 12.0*CLHEP::mm; 
     // final adjustment to avoid collision with the flange.. Wehereby substract 1/2 of the length., + 
     for (size_t iW=0; iW !=zLocWelds.size(); iW++) { 
       std::ostringstream nameStrStr; nameStrStr << "Horn1DownstrPart1Weld" << iW+2;
       nameStr = nameStrStr.str();
       const double length = 24.0*CLHEP::mm; //Cover two real sections... 
       const double zW = zLocWelds[iW];   
       double rTmp1 = fHorn1Equations[7].GetVal(zW + length) + 0.015*CLHEP::mm + fWaterLayerThickInHorns;
       // To make it a bit nice on fancy plots. 
       if (iW == 3) rTmp1 += 0.150*CLHEP::mm; // To be safe at the downstream end. 
       const double rTmp2 = rTmp1 + 1.8*CLHEP::mm; // 
       G4Tubs *aTubsWn = new G4Tubs(nameStr, rTmp1, rTmp2, 
	                           length/2.   , 0., 360.0*CLHEP::deg);
       std::cerr << " Horn1, Weld " << iW  << " volume name " << nameStr << " rTmp1 " 
                 << rTmp1 << " rTmp2 " << rTmp2 << std::endl;			   
       G4LogicalVolume *pCurrentWn = new G4LogicalVolume(aTubsWn, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       posTmp[2] =  -1.0*(plHUpst->fParams[2])/2. + zW + zShiftDrawingDownstr + length/2.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentWn, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
     }
   }
    {
   //Flange for the Inner Downstream, drawing 8875-112 363096 .. Two tubes
       // Upstream part 
       nameStr = std::string("Horn1InnerDownstrFlangePart0");
       const double rTmp1 = fHorn1RadialRescale*(7.750*in/2.0);
       const double rTmp2 = fHorn1RadialRescale*(8.50*in/2.0);; // 
       const double length = fHorn1LongRescale*(12.244 - 1.10)*in - 12.5*CLHEP::mm; 
       // Subtract the 1/2 the length weld to avoid collision with the Horn1DownstrPart1Weld
       G4Tubs *aTubsFl0 = new G4Tubs(nameStr, rTmp1, rTmp2, length/2.   , 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentFl0 = new G4LogicalVolume(aTubsFl0, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       const double zDrawing = fHorn1LongRescale*(117.1126*in) + 12.5*CLHEP::mm; // small shift to handle collisions 
       posTmp[2] = -1.0*(plHUpst->fParams[2])/2. + zDrawing + zShiftDrawingDownstr + length/2.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentFl0, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
      }
     {
   //Flange per-se drawing 8875-112 363096 ..
       nameStr = std::string("Horn1InnerDownstrFlangePart1");
       const double rTmp1 = fHorn1RadialRescale*7.750*in/2.0 + 1.0*CLHEP::mm;
       const double rTmp2 = fHorn1RadialRescale*11.271*in/2.0 + 1.0*CLHEP::mm; // 
       const double length = fHorn1LongRescale*(1.25)*in; // Add a bit for the connectors.
       G4Tubs *aTubsFl1 = new G4Tubs(nameStr, rTmp1, rTmp2, length/2.   , 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentFl1 = new G4LogicalVolume(aTubsFl1, G4Material::GetMaterial(fHorn1InnerCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.;
       const double zDrawing = fHorn1LongRescale*117.1126*in + fHorn1LongRescale*((12.244 - 1.10)*in + 0.5*CLHEP::mm); 
       posTmp[2] =  -1.0*(plHUpst->fParams[2])/2. + zDrawing + zShiftDrawingDownstr + length/2.;			      
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentFl1, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
      }
   
   
  }  // The downstream part of the real section that has the neck. 
  
  // The outer flange (downstream connector and bulk heads) Just a relatively thick tube. 
   {
       nameStr = G4String("Horn1OuterTubeDowsntreamFlanges");
       const double rTmp1 = fHorn1OuterTubeOuterRad + 2.0*CLHEP::mm;
       const double rTmp2 = 23.5*in/2.; // 
       const double length = 3.0*in; 
       const double zDrawing =  (117.1126 + 6.0)*in*fHorn1LongRescale; // 6" is still aproximate
       G4Tubs *aTubsFl = new G4Tubs(nameStr, rTmp1, rTmp2, length/2.   , 0., 360.0*CLHEP::deg);
       G4LogicalVolume *pCurrentFl = new G4LogicalVolume(aTubsFl, G4Material::GetMaterial(fHorn1AllCondMat), nameStr);
       posTmp[0] = 0.; posTmp[1] = 0.; 
       posTmp[2] =-1.0*(plHUpst->fParams[2])/2. + zDrawing + zShiftDrawingDownstr + length/2.;	;
       new G4PVPlacement((G4RotationMatrix *) 0, posTmp, pCurrentFl, nameStr + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	
  }
 
  
}
//
// Simple Horn1, based on a Polycone, for 1rst stage optimization of Horn1 September 2014.
// Revised  Oct. 2015, for more clarity and fix a small volume overlap at the edge of the outer conductor.
// Also fix a sign mistake in addition of epsil..  
//
void LBNEVolumePlacements::UpdateParamsForHorn1MotherPoly() {
 //
 // Convert the array of 3-vector to the fMotherHorn1AllLengths, AllRads
 //
 
 fMotherHorn1AllLengths.clear();
 fMotherHorn1AllRads.clear();
 if (fHorn1PolyListRinThickZVects.size() != static_cast<size_t>(fUseHorn1PolyNumInnerPts)) {
   G4Exception("LBNEVolumePlacements::UpdateParamsForHorn1MotherPoly", 
              " ", FatalErrorInArgument, "Inconsistency in sizing Hor1 Polycone "); 
 }
 const double epsil = 0.050*CLHEP::mm;
 const double thickInnerAtZStart = fHorn1PolyListRinThickZVects[0][1];
 //
 // This "fMotherHornsAll" Lengths/Radiis is used to defined  
 // The first point in these arrays is located on the external surface of the outer conductor, most upstream point
 // We set the point to be epsil away from the physical conductor. We add the thickness of the innerconductor 
 // to avoid volume overlap when we will define the IC, and the water layer.  This is clearly unphysical, but avoids 
 // the complexity of having to define more volumes. 
 // Note the water layer thickness is assumed to be uniform across the length of the device. Clearly wrong...
 // This is a more serious approximation...
 //
 const double rOut = fHorn1PolyOuterRadius +  2.5*CLHEP::cm + epsil + thickInnerAtZStart + fWaterLayerThickInHorns;
 fHorn1OuterTubeOuterRad = rOut;
 fMotherHorn1AllRads.push_back(rOut);
 fMotherHorn1AllLengths.push_back(0.);
 for(size_t k=0; k != fHorn1PolyListRinThickZVects.size(); k++) {
    const double r =  fHorn1PolyListRinThickZVects[k][0] - epsil ;
    // Should be needed for k != 0, but easier this way.  The location of the IC will be off by epsil, 
    // no physics inplications..
    const double z =  fHorn1PolyListRinThickZVects[k][2] + epsil;
    fMotherHorn1AllLengths.push_back(z);
    fMotherHorn1AllRads.push_back(r);
  }
  if (std::abs(fHorn1PolyListRinThickZVects[0][2]) > 1.0e-10) {
   G4Exception("LBNEVolumePlacements::UpdateParamsForHorn1MotherPoly", 
              " ", FatalErrorInArgument, " The first point must be at Z =0., for consistency...  "); 
  }
  //
  // Now the outer...extended a bit (kind of arbitrary at this point in time.. ) 
  // 
  const double zLast = fHorn1PolyListRinThickZVects[fHorn1PolyListRinThickZVects.size()-1][2] + 2.0*epsil;
  fMotherHorn1AllLengths.push_back(zLast);
  fMotherHorn1AllRads.push_back(rOut);
  fHorn1Length = zLast + 1.0*CLHEP::mm;
  std::cerr << " LBNEVolumePlacements::UpdateParamsForHorn1MotherPoly, total length of Horn1 " << 
            zLast << " Outer Radius " << rOut << std::endl; 
}
//
// Generalization for multiple simple horns, ranging from 0 to 4. 
//
void LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum(size_t iH) {
 //
 // Convert the array of 3-vector to the fMotherHorn1AllLengths, AllRads
 //
 const bool debugIsOn = false;
 if (debugIsOn) std::cerr << " LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum, index " << iH << std::endl;
 if (fMotherHornsAllLengths.size() != static_cast<size_t>(fUseNumberOfHornsPoly)) {
   G4Exception("LBNEVolumePlacements::UpdateParamsForHornsMotherPoly", 
              " ", FatalErrorInArgument, "Inconsistency in the number of simple Polycone Horns "); 
 }
 if ((iH >= fMotherHornsAllLengths.size()) || (iH >= fMotherHornsAllRads.size())) {
   G4Exception("LBNEVolumePlacements::UpdateParamsForHornsMotherPoly", 
              " ", FatalErrorInArgument, "Inconsistency in indexing simple Polycone Horns number"); 
 }
 fMotherHornsAllLengths[iH].clear();
 fMotherHornsAllRads[iH].clear();
 fMotherHornsAllThick[iH].clear();
 
 if (fHornsPolyListRinThickZVects[iH].size() != static_cast<size_t>(fUseHornsPolyNumInnerPts[iH])) {
   std::ostringstream message; message << " Inconsistency in sizing Horn Polycone " << iH;
   std::string messStr(message.str());
   G4Exception("LBNEVolumePlacements::UpdateParamsForHornsMotherPoly", 
              " ", FatalErrorInArgument, messStr.c_str()); 
 }
 //
 // This "fMotherHornsAll" Lengths/Radiis is used to defined  
 // The first point in these arrays is located on the external surface of the outer conductor, most upstream point
 // We set the point to be epsil away from the physical conductor. We add the thickness of the innerconductor 
 // to avoid volume overlap when we will define the IC, and the water layer.  This is clearly unphysical, but avoids 
 // the complexity of having to define more volumes. 
 // Note the water layer thickness is assumed to be uniform across the length of the device. Clearly wrong...
 // This is a more serious approximation...
 //
 const double epsil = 0.050*CLHEP::mm;
 const double thickInnerAtZStart = fHornsPolyListRinThickZVects[iH][0][1];
 const double rOut = fHorn1PolyOuterRadius +  2.5*CLHEP::cm + epsil + thickInnerAtZStart + fWaterLayerThickInHorns;
 fHorn1OuterTubeOuterRad = rOut;
 fMotherHornsAllRads[iH].push_back(rOut);
 fMotherHornsAllLengths[iH].push_back(0.);
 fMotherHornsAllThick[iH].push_back(0.25*25.4*CLHEP::mm);
 if (debugIsOn) std::cerr<< " LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum... for IH " 
                         << iH <<std::endl; /* for debugging */
 for(size_t k=0; k != fHornsPolyListRinThickZVects[iH].size(); k++) {
    const double r =  fHornsPolyListRinThickZVects[iH][k][0] - epsil;
    const double z =  fHornsPolyListRinThickZVects[iH][k][2] + epsil;
    const double thick = fHornsPolyListRinThickZVects[iH][k][1];
    if (debugIsOn) std::cerr<< "  ..... at k  "<< k << " z  " << fHornsPolyListRinThickZVects[iH][k][2] 
                << " r "<< fHornsPolyListRinThickZVects[iH][k][0] 
                << " thick " << thick << std::endl; /* for debugging */
    // Not the last point
    if (fPolyconeHornsAreParabolic[iH]) { 
      if(k+1!=fHornsPolyListRinThickZVects[iH].size()){
      // If the radius changes it could, should be a parabola
       if (debugIsOn) std::cerr<< "  ..... Delta R at k  "<< k << " to  " << k+1 << " = " <<
           std::abs(fHornsPolyListRinThickZVects[iH][k][0] - fHornsPolyListRinThickZVects[iH][k+1][0]) << std::endl;
       if(std::abs(fHornsPolyListRinThickZVects[iH][k][0] - fHornsPolyListRinThickZVects[iH][k+1][0]) > 1.5*mm){
          double a=(fHornsPolyListRinThickZVects[iH][k][2]-fHornsPolyListRinThickZVects[iH][k+1][2])/
	           (fHornsPolyListRinThickZVects[iH][k][0]*fHornsPolyListRinThickZVects[iH][k][0]-
		     fHornsPolyListRinThickZVects[iH][k+1][0]*fHornsPolyListRinThickZVects[iH][k+1][0]);
         // z=a*r^2+c  --> a=(z1-z2)/(r1^2-r2^2)
// Check for a flange ... a flange is a very rapid change in r over small change in z
//      if(std::abs(a)<1.e-4)continue;
         if (debugIsOn) std::cerr<< " ....radial jump...   at k  "<< k << " a " << a << std::endl; /* for debugging */
         if(std::abs(a)<1.e-4){
          fMotherHornsAllLengths[iH].push_back(z);
          fMotherHornsAllRads[iH].push_back(r);
          fMotherHornsAllThick[iH].push_back(fHornsPolyListRinThickZVects[iH][k][1]);
         } else {
          double c= fHornsPolyListRinThickZVects[iH][k][2]
	            -a*fHornsPolyListRinThickZVects[iH][k][0]*fHornsPolyListRinThickZVects[iH][k][0];
      // 1 mm radial spacing?  Number of points
          int np=((int)std::abs(fHornsPolyListRinThickZVects[iH][k][0]-fHornsPolyListRinThickZVects[iH][k+1][0]))+1;
          double rstep=(fHornsPolyListRinThickZVects[iH][k][0]-fHornsPolyListRinThickZVects[iH][k+1][0])/(double)np;
          if (debugIsOn) std::cerr<< "  ..... ..... Parabolic section intiated at k... "<< k << " a " << a << " c " << c << 
	        " np " << np << " z "<< z << std::endl; /* for debugging */
          for(int numr=0; numr<np; numr++){
	    double radTmp1=fHornsPolyListRinThickZVects[iH][k][0]-numr*rstep;
	    double zl=a*(radTmp1*radTmp1)+c-epsil;
	    double rl = (radTmp1 > epsil) ? (radTmp1 - epsil) : radTmp1;
	    fMotherHornsAllLengths[iH].push_back(zl);
	    fMotherHornsAllRads[iH].push_back(rl);
	    fMotherHornsAllThick[iH].push_back(fHornsPolyListRinThickZVects[iH][k][1]);
	    //	std::cout<<"HornsPoly "<<zl<<" "<<rl<<std::endl; /* for debugging */
          }
        }
      } else { // If no change in radius just a pipe.  Change z keep r fixed
          fMotherHornsAllLengths[iH].push_back(z);
          fMotherHornsAllRads[iH].push_back(r);
          fMotherHornsAllThick[iH].push_back(fHornsPolyListRinThickZVects[iH][k][1]);
	  if (fPolyconeHornsAreParabolic[iH]) std::cerr << " ..... Keep linear profile at k " << k << std::endl;
        }
      }  else { // the last point
        fMotherHornsAllLengths[iH].push_back(z);
        fMotherHornsAllRads[iH].push_back(r);
        fMotherHornsAllThick[iH].push_back(fHornsPolyListRinThickZVects[iH][k][1]);
      }
    } else { // linear horns.. 
      fMotherHornsAllLengths[iH].push_back(z);
      fMotherHornsAllRads[iH].push_back(r);
      fMotherHornsAllThick[iH].push_back(thick);
    }
  } // on number of segments. 
  
   const G4ThreeVector dataRZT = fHornsPolyListRinThickZVects[iH][0];
   if (dataRZT[2] > 1.0e-10) {
     std::ostringstream message; message << " The first point must be at Z =0., for consistency.. Horn " << (iH +1);
     std::string messStr(message.str());
     G4Exception("LBNEVolumePlacements::UpdateParamsForHornMotherPoly", 
              " ", FatalErrorInArgument, messStr.c_str()); 
  }
  const double zLast = fHornsPolyListRinThickZVects[iH][fHornsPolyListRinThickZVects[iH].size()-1][2] + 2.0*epsil;
  fMotherHornsAllLengths[iH].push_back(zLast);
  fMotherHornsAllRads[iH].push_back(rOut);
  fHornsLength[iH] = zLast + 2.0*epsil;
  fMotherHornsAllThick[iH].push_back(0.25*25.4*CLHEP::mm);
  if (debugIsOn) std::cerr << " LBNEVolumePlacements::UpdateParamsForHornMotherPoly, total length of Horn " << (iH + 1) << 
            " = " <<  zLast <<  " Length of thickneses " << fMotherHornsAllThick[iH].size() << std::endl;
  //
  // Check the consistency 
  //
  for (size_t iHH = 0; iHH != static_cast<size_t>(fUseNumberOfHornsPoly);  iHH++) {
//    std::cerr << " LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum, checks, at Horn " << (iHH+1) 
//              << " ZStart " <<  fHornsPolyZStartPos[iHH] << std::endl;
    if (fHornsPolyZStartPos[iHH] < -5000.*CLHEP::m) continue; // Not declared yet. 
    if ((iHH != 0) && (fHornsPolyZStartPos[iHH-1] > fHornsPolyZStartPos[iHH-1])) {
        std::ostringstream mStrStr; mStrStr 
	<< " The Polycones Horns must be declared in sequential order along the beam axis, \n "
	<< " Upstream to downstream. Problem for Horn  " 
                                           << (iHH) << " to " << (iHH+1);
       std::string mStr(mStrStr.str()); 
           G4Exception("LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum",
	               " ", FatalErrorInArgument, mStr.c_str());
   
    }
    double zPosPtLast = fMotherHornsAllLengths[iHH][fMotherHornsAllLengths[iHH].size()-1];
    double zCurr = fHornsPolyZStartPos[iHH] + zPosPtLast + 1.0*CLHEP::mm;
    bool badLength= false;
    std::ostringstream mblStrStr;     
    if (iHH == static_cast<size_t>(fUseNumberOfHornsPoly-1)) {
      badLength = (zCurr > fDecayPipeLongPosition);
      if (badLength) 
       mblStrStr << " Likely longitudinal overlap for Horn  " 
                                           << (iHH+1) << " Z End of this horn " << zCurr/CLHEP::m << " meters" << std::endl  
					   << " ZStart of this horn   "  << fHornsPolyZStartPos[iHH] << " length" 
					   << zPosPtLast <<  " Start of decay Pipe  " << fDecayPipeLongPosition << std::endl;
    } else { 
      badLength = ((zCurr + 1.0*CLHEP::mm) > fHornsPolyZStartPos[(iHH+1)]); 
      if (badLength) 
       mblStrStr << " Likely longitudinal overlap for Horn  " 
                                           << (iHH+1) << " Z End of this horn " << zCurr/CLHEP::m << " meters" << std::endl  
					   << " ZStart of this horn   "  << fHornsPolyZStartPos[iHH] << " length  " 
					   << zPosPtLast <<  " Start ofNext horn   " << fHornsPolyZStartPos[iHH+1] << std::endl;
    }
    if (badLength) {
       std::string mblStr(mblStrStr.str()); 
           G4Exception("LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum",
	               " ", FatalErrorInArgument, mblStr.c_str());
    }
    const bool badOutRadius = (fHornsPolyOuterRadius[iH] > (fChaseWidthForLBNF - 152.4*CLHEP::mm));
    if (badOutRadius) {
    
       std::ostringstream mStrStr;
        mStrStr << "  Horn  " << (iHH+1) 
                << " will not fit easily into the Chase, outer radius too big, " << fHornsPolyOuterRadius[iH] << " meters";
       std::string mStr(mStrStr.str()); 
           G4Exception("LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum",
	               " ", FatalErrorInArgument, mStr.c_str());
    
    }
  }
  if (iH == 0) { // copy the parameters to the old (Sept. 2014 ) arrays for Horn1 
    fMotherHorn1AllLengths = fMotherHornsAllLengths[iH];
    fMotherHorn1AllRads = fMotherHornsAllRads[iH];
    fMotherHorn1AllThick = fMotherHornsAllThick[iH];
    fHorn1Length = fMotherHornsAllLengths[iH][fMotherHornsAllLengths[iH].size()-1] + 1.0*CLHEP::mm;
    if (debugIsOn) std::cerr << " LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum, setting Horn1 length to " << 
       fHorn1Length << std::endl;
  }
  // make sure they are all defined... 
  if (iH == fMotherHornsAllLengths.size()-1) this->DumpAllHornsPolyconeParameters();
}

void LBNEVolumePlacements::DumpAllHornsPolyconeParameters() {
  const double epsil = 0.050*CLHEP::mm;
  for (size_t iH=0; iH != fMotherHornsAllLengths.size(); iH++) {
    std::ostringstream aNStrStr; aNStrStr << "./AllHornsPolyconeParametersHorn_" << (iH+1) << ".txt"; 
    std::string aNStr(aNStrStr .str());
    std::ofstream fOut(aNStr.c_str());
    fOut << " ip Z Rin ROut " << std::endl;
    for (size_t k=0; k != fMotherHornsAllLengths[iH].size(); k++) {
      fOut << " " << k << " " << fMotherHornsAllLengths[iH][k] - epsil 
           << " " <<  fMotherHornsAllRads[iH][k] + epsil << " " 
	   << fMotherHornsAllRads[iH][k] + epsil + fMotherHornsAllThick[iH][k] << std::endl; // Not counting the water layer. 
    } 
    fOut.close();
  }
}
void LBNEVolumePlacements::PlaceFinalSimpleHornPolyNumber(size_t iH, G4PVPlacement *mother) {
//
// Place the inner conductor. A G4Polycone, fitting inside the mother G4Polycone, or the tube. 
// Note that we create a few temporary arrrays, first for the outer raddi of the inner conducter itself 
// second, for the water layer (inside the inner conductor, to make the volume tree logical hierachy more vertical) 
// The number of points on the inner C polygon (and the water layer ) is 2*(fMotherHornsAllRads.size() -2), 
// since we skip the outer conductor, but we install the nearly vertical flange.
// Correction: we do not put a water layer on the vertical flange..   
//
  const bool debugIsOn = false;
  if (debugIsOn) std::cerr << " LBNEVolumePlacements::PlaceFinalSimpleHornPolyNumber " << iH  << " num segments " 
                           << fMotherHornsAllRads[iH].size() << std::endl;
  std::vector<double> innerRads(fMotherHornsAllRads[iH].size()-2);
  std::vector<double> innerZs(fMotherHornsAllLengths[iH].size()-2);
  // Skip the outer conductor. 
  if (debugIsOn) std::cerr << " ................. innerRads size " << innerRads.size()  << std::endl;
  for (size_t k=1; k!= fMotherHornsAllRads[iH].size()-1; k++) {
   innerRads[k-1] = fMotherHornsAllRads[iH][k];
   innerZs[k-1] = fMotherHornsAllLengths[iH][k];
  }
  std::vector<double> allRads(innerRads); // not yet full size... Only 1/2 of it. 
  std::vector<double> allZs(innerZs);
  std::vector<double> outerRads(innerRads.size(), 0.); // covering volume (rin + thick + water + epsil) 
  const double epsil = 0.050*CLHEP::mm;
  double zMax = -1.0e23;
  double zMin = 1.0e23;
  for (size_t k=0; k != allZs.size(); k++) {
    allRads[k] += epsil;
    allZs[k] -= epsil;
    innerRads[k] += epsil;
    innerZs[k] -= epsil;
    zMax = std::max(zMax, innerZs[k]);
    zMin = std::min(zMin, innerZs[k]);
    outerRads[k] = innerRads[k] + fMotherHornsAllThick[iH][k];
  }
  // Futher at the edges. 
  innerZs[0] += epsil;
  innerZs[innerZs.size()-1] -= epsil;
  allZs[0] += epsil;
  allZs[allZs.size()-1] -= epsil;
  //
  // Now we implement the return loop, downstream to upstream. We made two arrays, such that, if the "all" vector move in memory,
  // we are safe... 
  //
  size_t nPtsHalf = innerZs.size();
  const double thickUsptream = fMotherHornsAllThick[iH][1]; // Index 1 : the first thicknes is the outer conductor... 
  const double thickDownstream = (fMotherHornsAllThick[iH].size() < 2) ? 
        thickUsptream  : fMotherHornsAllThick[iH][fMotherHornsAllThick[iH].size()-2];
  if (debugIsOn) std::cerr << " Vertical Flange thicknesses, upstream " 
                 << thickUsptream << " downstream " << 	thickDownstream << std::endl;
  for (size_t k=0; k != nPtsHalf; k++) {
    size_t kk = nPtsHalf -1 - k;
    double zzLocal = innerZs[kk];
    if (k == 0)  zzLocal -= thickDownstream; 
    if (kk == 0)  zzLocal += thickUsptream; // to avoid R-Z crossing for the vertical flange
    // only valid if the flange is vertical.. Hopefully, this is the case for all the Horns Laura F. is considering. 
    if (k == 1)  zzLocal -= thickDownstream;
    if (kk == 1)  zzLocal += thickUsptream; // to avoid R-Z crossing for the vertical flange
    double radius = outerRads[kk];
    if (k == 0) radius =  outerRads[kk] - thickDownstream; // Again, valid only if flange is nearly vertical. 
    if (kk == 0) radius = outerRads[kk] - thickUsptream; // Again, valid only if flange is nearly vertical. 
    allRads.push_back(radius);
    allZs.push_back(zzLocal);
    zMax = std::max(zMax, zzLocal);
    zMin = std::min(zMin, zzLocal);
  }
  //
  // We shift the Z position by half the length, such the relative definition is the same as for the other volumes. 
  std::vector<double> zz(allZs);
  const double zMiddle = (zMax + zMin)/2.;
  if (debugIsOn) std::cerr << " .................. Zmin " << zMin << " max " << " zMax " 
                           << zMax  << " zMiddle " << zMiddle << std::endl 
                           << " .................. InnerRads size " << innerRads.size() << std::endl;
  for (std::vector<double>::iterator il = zz.begin(); il != zz.end(); il++) *il -= zMiddle;
  if (debugIsOn) {
    std::ostringstream aNStrStr; aNStrStr << "./AllHornsPolyconeParametersHornInner_" << (iH+1) << ".txt"; 
    std::string aNStr(aNStrStr .str());
    std::ofstream fOut(aNStr.c_str());
    fOut << " ip Z R " << std::endl;
    for (size_t k=0; k != allZs.size(); k++) {
      fOut << " " << k << " " << zz[k] << " " << allRads[k]  << " " <<  std::endl;
    } 
    fOut.close();
  }

  std::ostringstream vNameInnerStr; vNameInnerStr << "Horn" << (iH+1) << "PolyInnerC";
  G4String vNameInner(vNameInnerStr.str());
  G4GenericPolycone* aPCon = new G4GenericPolycone(vNameInner, 0., 360.0*CLHEP::deg, static_cast<int>(zz.size()), 
	                                    &allRads[0], &zz[0]);
  G4ThreeVector posTmp; posTmp[0] = 0.; posTmp[1] = 0.;   posTmp[2] = 0.;  
  G4LogicalVolume *pCurrent = new G4LogicalVolume(aPCon, 
                                                  G4Material::GetMaterial(std::string(fHorn1InnerCondMat)), vNameInner);
  new G4PVPlacement((G4RotationMatrix *) 0, 
                          posTmp, pCurrent, vNameInner + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);
				      
  if (debugIsOn) std::cerr << " LBNEVolumePlacements::PlaceFinalSimpleHornPolyNumber ... Inner conductor placed  "  << std::endl;
  if (std::abs(fWaterLayerThickInHorns) > 0.00002*CLHEP::mm) {
    // We assume that there are no water on vertical flange... 
    if (innerRads.size() < 2) {
       std::ostringstream mStrStr;
        mStrStr << "  Horn  " << (iH+1) 
                << " has to few segments ( " << innerRads.size()  <<" ) to support a water layer.. Review the geometry. "; 
       std::string mStr(mStrStr.str()); 
       G4Exception("LBNEVolumePlacements::PlaceFinalSimpleHornPolyNumber",
	               " ", JustWarning, mStr.c_str());
    } else {
    // 
    // We skip the vertical flange.. 
    //
      std::vector<double> innerWaterRads((outerRads.size()-2), 0.); // Aluminum only. (rin + thick + water + epsil) 
      std::vector<double> innerWaterZs(innerWaterRads.size(), 0.);
      for (size_t k=1; k != innerRads.size()-1; k++) {
        innerWaterRads[k-1] = outerRads[k] + 2.0*epsil; // to take possible edge effect away, for sharped angle sections (unphysical, anyways..) 
        innerWaterZs[k-1] = zz[k]; // we take the shifted array, to ease debugging, and avoid volume overlap. 
        if (k  == 1) innerWaterZs[k-1] += 2.0*thickUsptream; // Chop the most upstream section, it clashes with vertical flange.  
        else if (k  ==  (innerRads.size()-2)) innerWaterZs[k-1] -= 2.0*thickDownstream; // same, assume symmetrical thickness. 
	// assume it will fit, otherwise...  
      }
      //
     // Now we implement again the same  the return loop, downstream to upstream. 
     size_t nPtsWHalf = innerWaterZs.size();
     std::vector<double> allWaterRads(innerWaterRads);
     std::vector<double> allWaterZs(innerWaterZs);
//     double zWMin = 1.0e23; double zWMax = -1.0e23;
     for (size_t k=0; k != nPtsWHalf; k++) {
       size_t kk = nPtsWHalf -1 - k;
       allWaterRads.push_back(allWaterRads[kk] + fWaterLayerThickInHorns);
       allWaterZs.push_back(innerWaterZs[kk]);
//       zWMin = std::min(innerWaterZs[kk], zWMin);
//       zWMax = std::max(innerWaterZs[kk], zWMax);
     }
     // 
    // We do not shift, we are already in the correct frame with respect to the mother volume. 
    // important if the thicknesses of the flange are asymmetric. 
    //
     std::vector<double> zzWater(allWaterZs);
//     const double zWMiddle = (zWMax + zWMin)/2.;
//     if (debugIsOn) std::cerr << " .................. ZWmin " << zWMin << " max " << " zWMax " 
//                              << zWMax  << " zWMiddle " << zWMiddle << std::endl;
//     for (std::vector<double>::iterator il = zzWater.begin(); il != zzWater.end(); il++) *il -= zWMiddle;
     if (debugIsOn) {
          std::cerr << ".......... ... Water layer placed, nPtsWHalf "  << nPtsWHalf << std::endl;
         std::ostringstream aNStrStr; aNStrStr << "./AllHornsPolyconeParametersHornInnerWater_" << (iH+1) << ".txt"; 
         std::string aNStr(aNStrStr .str());
         std::ofstream fOut(aNStr.c_str());
         fOut << " ip Z R " << std::endl;
         for (size_t k=0; k != zzWater.size(); k++) {
            fOut << " " << k << " " << zzWater[k] << " " << allWaterRads[k]  << " " <<  std::endl;
         } 
         fOut.close();
     }
     std::ostringstream vNameInnerWStr; vNameInnerWStr << "Horn" << (iH+1) << "PolyInnerCWaterL";
     G4String vNameInnerW(vNameInnerWStr.str());
     G4GenericPolycone* aPConW5 = new G4GenericPolycone(vNameInnerW, 0.*CLHEP::deg, 360.0*CLHEP::deg, static_cast<int>(allWaterRads.size()), 
	                                    &allWaterRads[0], &zzWater[0]);
     posTmp[0] = 0.; posTmp[1] = 0.;   posTmp[2] = 0.;  
     G4LogicalVolume *pCurrentW5 = new G4LogicalVolume(aPConW5, 
                                                  G4Material::GetMaterial(std::string("Water")), vNameInnerW);
     new G4PVPlacement((G4RotationMatrix *) 0, 
                          posTmp, pCurrentW5, vNameInnerW + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	      
    }
  }
  //
  // Now the outer Tube
  //
  const double rOuterInner = fHornsPolyOuterRadius[iH] - 0.250*25.4*CLHEP::mm - 0.1*CLHEP::mm;
  const double rOuterOuter = fHornsPolyOuterRadius[iH] - 0.1*CLHEP::mm;
  const double lengthOT = fHornsPolyListRinThickZVects[iH][fUseHornsPolyNumInnerPts[iH]-1][2] - 5.0*CLHEP::mm; // to fit inside the edges of IC
  std::ostringstream nameOTStr; nameOTStr << "Horn" << (iH+1) << "OuterC";
  G4String nameOT(nameOTStr.str());
  G4Tubs *aTubs = new G4Tubs(nameOT, rOuterInner, rOuterOuter, lengthOT/2., 0., 360.0*CLHEP::deg);
  G4LogicalVolume *pCurrentOT = new G4LogicalVolume(aTubs, G4Material::GetMaterial(fHorn1InnerCondMat), nameOT);
  new G4PVPlacement((G4RotationMatrix *) 0, 
                          posTmp, pCurrentOT, nameOT + std::string("_P"), 
                        mother->GetLogicalVolume(), false, 1, fCheckVolumeOverLapWC);	      
  
}

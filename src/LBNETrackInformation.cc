//
// LBNETrackInformation.cc
//
// Refurbished,Using now the more formal and exact G4VUserTrackInformation class
// P.L. November 2014. 
//
#include "G4VProcess.hh"
#include "LBNETrackInformation.hh"
#include "G4ios.hh"

G4Allocator<LBNETrackInformation> aTrackInformationAllocator;

LBNETrackInformation::LBNETrackInformation()
  :decay_code(0),
   tgen(0),
   Nimpwt(1.0),
   fPerfectFocusingFlag(0),
   fParentMomentumAtThisProduction(0.0,0.0,0.0)
{
// std::cerr << " Creating LBNETrackInformation.. " << (void*) this << std::endl;
}


LBNETrackInformation::LBNETrackInformation(const LBNETrackInformation* aTrackInfo)
{
  decay_code = aTrackInfo->decay_code; // Why does this work.. decay_code is private.. 
  tgen = aTrackInfo->tgen;
  Nimpwt = aTrackInfo->Nimpwt;
  fPerfectFocusingFlag = aTrackInfo->GetPFFlag();
  fParentMomentumAtThisProduction = aTrackInfo->fParentMomentumAtThisProduction; 
  std::cerr << " LBNETrackInformation copy from const pointer done " << std::endl;
}
LBNETrackInformation::LBNETrackInformation(const LBNETrackInformation& aTrackInfo):
G4VUserTrackInformation()
{
  decay_code = aTrackInfo.GetDecayCode();  
  tgen = aTrackInfo.GetTgen();
  Nimpwt = aTrackInfo.GetNImpWt();
  fPerfectFocusingFlag = aTrackInfo.GetPFFlag();
  fParentMomentumAtThisProduction = aTrackInfo.fParentMomentumAtThisProduction;  
  std::cerr << " LBNETrackInformation copy constr const by ref done " << std::endl;
}
LBNETrackInformation::LBNETrackInformation(LBNETrackInformation* aTrackInfo)
{
  decay_code = aTrackInfo->decay_code; // Why does this work.. decay_code is private.. 
  tgen = aTrackInfo->tgen;
  Nimpwt = aTrackInfo->Nimpwt;
  fPerfectFocusingFlag = aTrackInfo->GetPFFlag();
  fParentMomentumAtThisProduction = aTrackInfo->fParentMomentumAtThisProduction;  
  std::cerr << " LBNETrackInformation copy from pointer done " << std::endl;
}
LBNETrackInformation::LBNETrackInformation(LBNETrackInformation& aTrackInfo):
G4VUserTrackInformation()
{
  decay_code = aTrackInfo.GetDecayCode();  
  tgen = aTrackInfo.GetTgen();
  Nimpwt = aTrackInfo.GetNImpWt();
  fPerfectFocusingFlag = aTrackInfo.GetPFFlag();
  fParentMomentumAtThisProduction = aTrackInfo.fParentMomentumAtThisProduction;  
  std::cerr << " LBNETrackInformation copy constr by ref done " << std::endl;
}
LBNETrackInformation& LBNETrackInformation::operator=(const LBNETrackInformation& right)
{
  decay_code = right.GetDecayCode();
  tgen = right.GetTgen();
  Nimpwt = right.GetNImpWt();
  fPerfectFocusingFlag = right.GetPFFlag();
  fParentMomentumAtThisProduction = right.fParentMomentumAtThisProduction;
  std::cerr << " LBNETrackInformation assignment from const done " << std::endl;
  return *this;
}
LBNETrackInformation& LBNETrackInformation::operator=(LBNETrackInformation& right)
{
  decay_code = right.GetDecayCode();
  tgen = right.GetTgen();
  Nimpwt = right.GetNImpWt();
  fPerfectFocusingFlag = right.GetPFFlag();
  fParentMomentumAtThisProduction = right.fParentMomentumAtThisProduction;  
  std::cerr << " LBNETrackInformation assignment done " << std::endl;
  return *this;
}

LBNETrackInformation::~LBNETrackInformation(){}

void LBNETrackInformation::Print() const
{
    G4cout 
     << "Decay code = " << decay_code << G4endl;
    G4cout 
     << "tgen = " << tgen << G4endl;
    G4cout 
     << "nimpwt = " << Nimpwt << " Perfect Focusing flag " << fPerfectFocusingFlag << G4endl;
}

void LBNETrackInformation::Print(const G4Track *aTrack) const
{ 
  const G4String spaces = "   ";

  G4cout << spaces << "Track ID       = " << aTrack->GetTrackID()            << G4endl
	 << spaces << "Particle Name  = " << aTrack->GetDefinition()->GetParticleName()         << G4endl
	 << spaces << "Parent ID      = " << aTrack->GetParentID()           << G4endl;
  if(aTrack->GetVolume()) G4cout << spaces << "Current volume = " << aTrack->GetVolume()-> GetName() << G4endl;
  else                    G4cout << spaces << "Current volume = NOT CURRENTLY AVAILABLE" << G4endl;
  if(aTrack->GetCreatorProcess())  G4cout << spaces << "Creator Process = "<< aTrack->GetCreatorProcess()->GetProcessName() << G4endl;
  else                             G4cout << spaces << "Creator Process = NOT CURRENTLY AVAILABLE" << G4endl;
    
}

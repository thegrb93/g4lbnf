#include <vector>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include "LBNERunManager.hh"
#include "LBNEQuickPiToNu.hh"

LBNEQuickPiToNu::LBNEQuickPiToNu():
   evtNum(0),
   trNum(-9999),
   sign(0),
   piEnergy(0.),
   nuEnergy(0.),
   piMomentumTarget(3, 0.),
   piPosAtHangerRing(3, 0.),
   piMomentumAtHangerRing(3, 0.),
   nuMomentum(3, 0.)
{;}
LBNEQuickPiToNuVect* LBNEQuickPiToNuVect::m_pInstance=0;

LBNEQuickPiToNuVect* LBNEQuickPiToNuVect::Instance() {
 
   if (m_pInstance== NULL) m_pInstance=new LBNEQuickPiToNuVect;

  return m_pInstance;
}


LBNEQuickPiToNuVect::LBNEQuickPiToNuVect() {
 doItForReal = false;
}


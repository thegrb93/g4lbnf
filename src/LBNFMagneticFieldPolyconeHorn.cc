//----------------------------------------------------------------------
//LBNFMagneticFieldPolyconeHorn 
// $Id
//----------------------------------------------------------------------

#include "LBNFMagneticFieldPolyconeHorn.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ios.hh"
#include "G4TransportationManager.hh"
#include "LBNEVolumePlacements.hh"
#include "LBNESurveyor.hh"
#include "Randomize.hh"
#include "LBNERunManager.hh"

//magnetic field between conductors ====================================================

LBNFMagneticFieldPolyconeHorn::LBNFMagneticFieldPolyconeHorn(size_t iH) : 
hornNumber(iH), // indexed 0 to 4..
fHornCurrent(0.),
fEffectiveLength(0.),
fOuterRadius(1.0e9),
fOuterRadiusEff(1.0e9),
fSkinDepth(4.1*CLHEP::mm),
fSkinDepthInnerRad(1.0e10*CLHEP::mm), // Unrealistic high, because that was the default sometime ago. 
fSkinDepthIIRInv(std::sqrt(2)/fSkinDepthInnerRad),
fDeltaEccentricityIO(0.),
fDeltaEllipticityI(0.),
fShift(2, 0.),
fShiftSlope(2,0.)
{
// 
// Magnetic field for the simple Polycone horn.  This field is attached to container volume 
// LBNFSimpleHornxContainer, which is a box. The field is defined the Polycone, we keep our own geometry 
// such we can implement field error in the future, should it be necessary..
//
  const LBNEVolumePlacements *aPlacementHandler = LBNEVolumePlacements::Instance();
  fZShiftDrawingCoordinate = 0.; // Check with Geantino here... 
  fZDCBegin.clear();
  fRadsInnerC.clear();
  fRadsOuterC.clear();
  double zzMinTmp = 1.0e20;
  double zzMaxTmp = -1.0e20;
  fZShiftDrawingCoordinate = aPlacementHandler->GetHornsPolyZStartPos(hornNumber + 1);
//       std::cerr << " Check coordinates of inner radius of Horn1 Polycone inner conductor... fOuterRadius " << fOuterRadius << std::endl;
  if (aPlacementHandler->IsHornPnParabolic(hornNumber + 1)) {
    size_t nPts = aPlacementHandler->GetHornParabolicNumInnerPts(iH);
    const double epsil = 0.050*CLHEP::mm; // from LBNEVolumePlacements::UpdateParamsForHornMotherPolyNum
    for (size_t k=0; k !=  nPts; k++) {
      const double zzz = aPlacementHandler->GetHornParabolicRZCoord(iH, k) + fZShiftDrawingCoordinate + epsil;
      zzMinTmp = std::min(zzMinTmp, zzz);      
      zzMaxTmp = std::max(zzMaxTmp, zzz);
      fZDCBegin.push_back(zzz);
      const double rIn = aPlacementHandler->GetHornParabolicRIn(iH, k) - epsil;
      fRadsInnerC.push_back(rIn);
      const double rOut = rIn + aPlacementHandler->GetHornParabolicThick(iH, k);
      fRadsOuterC.push_back(rOut);
    } 
  } else {
    for (size_t k=0; k!= static_cast<size_t>(aPlacementHandler->GetUseHornsPolyNumInnerPts(hornNumber + 1)); k++) {
      const double zzz = aPlacementHandler->GetHornsPolyInnerConductorZCoord(hornNumber + 1, k) + fZShiftDrawingCoordinate;
      zzMinTmp = std::min(zzMinTmp, zzz);      
      zzMaxTmp = std::max(zzMaxTmp, zzz);
      fZDCBegin.push_back(zzz);
      const double rIn =  aPlacementHandler->GetHornsPolyInnerConductorRadius(hornNumber + 1, k);	 
      fRadsInnerC.push_back(rIn);
      const double rOut = rIn + aPlacementHandler->GetHornsPolyInnerConductorThickness(hornNumber + 1, k);
      fRadsOuterC.push_back(rOut);
//	 std::cerr << " k " << k << " z " << fZDCBegin[fZDCBegin.size()-1] << " InnerC " << rIn << " out " << rOut << std::endl;
   }
  }
  fOuterRadius = aPlacementHandler->GetHornsPolyOuterRadius(hornNumber + 1);
  fEffectiveLength = zzMaxTmp - zzMinTmp;
  
//       std::cerr << " And quit for now.. I say so.. " << std::endl; exit(2);
//
  
// Use now the survey data (simulated for now.. ) to establish the coordinate transform..  
//
  LBNESurveyor *theSurvey= LBNESurveyor::Instance();
  std::ostringstream hornIndexStrStr; hornIndexStrStr << hornNumber+1;
  std::string hornIndexStr(hornIndexStrStr.str());
  std::string upstrStr("Upstream");std::string downstrStr("Downstream");
  std::string leftStr("Left"); std::string rightStr("Right"); 
  std::string ballStr("BallHorn");
  std::vector<LBNESurveyedPt>::const_iterator itUpLeft=
    theSurvey->GetPoint(G4String(upstrStr + leftStr + ballStr + hornIndexStr));
  std::vector<LBNESurveyedPt>::const_iterator itUpRight=
    theSurvey->GetPoint(G4String(upstrStr + rightStr + ballStr + hornIndexStr));
  std::vector<LBNESurveyedPt>::const_iterator itDwLeft=
    theSurvey->GetPoint(G4String(downstrStr + leftStr + ballStr + hornIndexStr));
  std::vector<LBNESurveyedPt>::const_iterator itDwRight=
    theSurvey->GetPoint(G4String(downstrStr + rightStr + ballStr + hornIndexStr));
  const G4ThreeVector pUpLeft = itUpLeft->GetPosition();
  const G4ThreeVector pUpRight = itUpRight->GetPosition();
  const G4ThreeVector pDwLeft = itDwLeft->GetPosition();
  const G4ThreeVector pDwRight = itDwRight->GetPosition();
  std::cerr << " For Horn " << hornNumber+1 << " pUpLeft " 
            << pUpLeft << " right " <<  pUpRight << " pDwLeft " 
	    << pDwLeft << " effLength " << fEffectiveLength << std::endl;
  fHornIsMisaligned  = false;
  for (size_t k=0; k!=2; k++) {
    fShift[k] = -0.50*(pUpLeft[k] + pUpRight[k]); // minus sign comes from the global to local. 
                                                  // Transverse shoft at the upstream  entrance of Horn1 
    fShiftSlope[k] = 0.5*(pUpLeft[k] + pUpRight[k] -  pDwLeft[k] - pDwRight[k])/fEffectiveLength; 
    if (std::abs(fShiftSlope[k]) > 1.0e-8) fHornIsMisaligned = true;
  }
  if (fHornIsMisaligned) {
    std::ostringstream aNameStrStr; aNameStrStr << "LBNFSimpleHorn" << (hornNumber+1) << "Container";
    std::string namePlacedVol(aNameStrStr.str()); 
    if (hornNumber == 0) namePlacedVol = std::string("Horn1PolyM1");
    const LBNEVolumePlacementData* plDat= 
       aPlacementHandler->Find("BFieldInitialization", namePlacedVol.c_str(), "BFieldInitialization");
    fRotMatrixHornContainer = plDat->fRotation;
    std::cerr << " Horn " << hornNumber+1 << " is misaligned " 
              <<  " xz term " << fRotMatrixHornContainer[0][2] << " yz " << fRotMatrixHornContainer[1][2] << std::endl;
//    fHornIsMisaligned = false;
//    std::cerr << " !!!! Back door setting of the HornisMialigned, we will not rotate the field " << std::endl; 
  }
  
//
// Open a file to study the value of the field for displaced trajectory... 
//  
// if (!fOutTraj.is_open()) {
//   G4String fNameStr("./FieldTrajectories_Horn");
//   if (amHorn1) fNameStr += G4String("1_1.txt");
//   else  fNameStr += G4String("2_1.txt");
//   fOutTraj.open(fNameStr.c_str()); // we will place a GUI interface at a later stage... 
//   fOutTraj << " id x y z bx by " << std::endl;
// }
}

void LBNFMagneticFieldPolyconeHorn::GetFieldValue(const double Point[3],double *Bfield) const
{

  
   G4double current = fHornCurrent/CLHEP::ampere/1000.;
//   std::cerr << " LBNFMagneticFieldPolyconeHorn::GetFieldValue Z gobal  " << Point[2] 
//            << " fZDCBegin, first " << fZDCBegin[0] << " last " <<  fZDCBegin[fZDCBegin.size() -1]
//             << " for horn " << (hornNumber+1) << " current " << current << std::endl;
   for (size_t k=0; k!=3; ++k) Bfield[k] = 0.;
   // Intialization of the coordinate transform. 
//   const LBNEVolumePlacements *aPlacementHandler = LBNEVolumePlacements::Instance(); // no longer needed here. See above.. 
   std::vector<double> ptTrans(2,0.);
   const double zGlobal = Point[2]; // In G4 World Volume coordinate system. 
   for (size_t k=0; k != 2; k++) 
     ptTrans[k] = Point[k] + fShift[k] + (zGlobal-fZDCBegin[0])*fShiftSlope[k]; 
   const double r = std::sqrt(ptTrans[0]*ptTrans[0] + ptTrans[1]*ptTrans[1]); 
   if (r > fOuterRadius) return;
   if ( r < 1.0e-3) return; // The field should go smoothly to zero at the center of the horm 

   double magBField = current / (5.*r/CLHEP::cm)/10*CLHEP::tesla; //B(kG)=i(kA)/[5*r(cm)], 1T=10kG
   // Implement the azimuthal anisotropy due to imperfection of the current equalizer section. 
   if ((std::abs(fCurrentEqualizerQuadAmpl) > 1.0e-9) || 
       (std::abs(fCurrentEqualizerOctAmpl) > 1.0e-9)) { 
      double zFactAzi = 1.0e-6;
      switch(hornNumber) {
        case 0:
        case 2:
	  zFactAzi = ((zGlobal - fZDCBegin[0]) > 0.) ? (zGlobal - fZDCBegin[0])/fCurrentEqualizerLongAbsLength : 1.0e-6;
	  break;
	case 1:
	  zFactAzi = ((fZDCBegin[fZDCBegin.size() -1] - zGlobal) > 0.) ? 
	                 (fZDCBegin[fZDCBegin.size() -1] - zGlobal)/fCurrentEqualizerLongAbsLength : 1.0e-6; 
	  break;
	default:
	 zFactAzi = 1.0e-6; // undefined, only 3 horns   
      }
      const double phi = std::atan2(Point[1], Point[0]);
      const double factCE = std::exp(-1.0*std::abs(zFactAzi)) * (fCurrentEqualizerQuadAmpl*std::sin(2.0*phi) 
                                                   + fCurrentEqualizerOctAmpl*std::sin(4.0*phi));
      magBField *= (1.0 + factCE);					   
//      std::cerr << " ... Current Equalizer factor " << factCE << " Z " << zFactAzi << std::endl;					    
   }
   

   double radIC=0.;
   double radOC=0.;   
   this->fillHornPolygonRadii(zGlobal, &radIC, &radOC);
   if (std::abs(radIC) > 1.0e6) return;
   fEffectiveInnerRad = radIC;
   fEffectiveOuterRad = radOC;
   fEffectiveSkinDepthFact = 1.0;
     // Study of systematic effects. Define an effective radIC wich is large than the physics radIC.
     // We assume here that the finite skin depth matter, and the current density is not 
     // uniform. Simply increase the radIC
   if (r < radIC && (fDeltaEccentricityIO < 1.0e-20)) return; // zero field region (Amps law). 
       // Unless the Inner conductor is displaced, off center.. 
       // However, this will only take effect if the volume to whic hthis field is attached 
       // contains the cylinder.  For LBNF, Polycone this is *not* the case!. 
       // This is ineffective code... P.L., April 13 
       // 
   if (r < radIC && ((std::abs(fDeltaEccentricityIO) > 1.0e-20) || (std::abs(fDeltaEllipticityI) > 1.0e-20))) {
       const double rDelta = sqrt((ptTrans[0] - fDeltaEccentricityIO)*(ptTrans[0] - fDeltaEccentricityIO) +
                              ptTrans[1]*ptTrans[1]);
       const double phi = std::atan2(ptTrans[1], ptTrans[0]);
       const double r12Fact = fEffectiveInnerRad*fEffectiveInnerRad/
           (fEffectiveOuterRad*fEffectiveOuterRad - fEffectiveInnerRad*fEffectiveInnerRad);
       double deltaMagFieldPhiEc = magBField * r12Fact * (fDeltaEccentricityIO / rDelta) * std::cos(phi);
       double magFieldREc = magBField * r12Fact * (fDeltaEccentricityIO / rDelta) * std::sin(phi);
       const double deltaMagFieldPhiEl = magBField * r12Fact * fEffectiveInnerRad * 
                                          (fDeltaEllipticityI /(r*r)) * std::cos(2.0*phi);
       const double magFieldREl = magBField * r12Fact * fEffectiveInnerRad * (fDeltaEllipticityI /(r * r)) * std::sin(2.0*phi);
       const double magFieldPhi = magBField - deltaMagFieldPhiEc - deltaMagFieldPhiEl;
       const double magFieldR = magFieldREl + magFieldREc;
       Bfield[0] = -magFieldPhi*ptTrans[1]/r +  magFieldR*ptTrans[0]/r; 
       Bfield[1] = magFieldPhi*ptTrans[0]/r +  magFieldR*ptTrans[1]/r;
       return; 
     }
//     const double deltaRIC = radOC - radIC;
// Version from Dec 18-19 2013.      
//     radIC += deltaRIC*std::min(0.99, fSkinDepthCorrInnerRad); 
     //
//     if ((r > radIC) && (r < radOC)) {
//      const double surfCyl = radOC*radOC - radIC*radIC;
//      const double deltaRSq = (r*r - radIC*radIC);
//      magBField *= deltaRSq/surfCyl; // Assume uniform current density and apply Amps law. 
//     }
// Version from Zarko's thesis. ( (MINOS Document 5694-v1) ) 
// 
     if ((r > radIC) && (r < radOC)) {
        const double surfCyl = radOC*radOC - radIC*radIC;
        const double deltaRSq = (r*r - radIC*radIC);
	const double factR = deltaRSq/surfCyl;     
	fEffectiveSkinDepthFact = factR;
       if (fSkinDepthInnerRad > 10.*CLHEP::mm) { // default assume very large skin depth, assume constant current 
                                      // current density .
         magBField *= factR; // Assume uniform current density and apply Amps law. 
       } else { // Realistic skin depth. 
         magBField *= factR*getNormCurrDensityInnerC((r-radIC), (radOC-radIC))
	                    /getNormCurrDensityInnerC((radOC-radIC), (radOC-radIC));
       }
     }
// Sin and cos factor bphi -> bx, by 
     if (std::abs(fDeltaEllipticityI) < 1.0e-20) { 
       Bfield[0] = -magBField*ptTrans[1]/r;
       Bfield[1] = magBField*ptTrans[0]/r;
     } else {
       // We assume here the conductor which is elliptical is inner one.. 
       const double rDelta = sqrt((ptTrans[0] - fDeltaEccentricityIO)*(ptTrans[0] - fDeltaEccentricityIO) +
                              ptTrans[1]*ptTrans[1]);
       const double phi = std::atan2(ptTrans[1], ptTrans[0]);
       const double r12Fact = fEffectiveInnerRad*fEffectiveInnerRad/
           (fEffectiveOuterRad*fEffectiveOuterRad - fEffectiveInnerRad*fEffectiveInnerRad);
       double deltaMagFieldPhiEc = magBField * r12Fact * (fDeltaEccentricityIO / rDelta) * std::cos(phi);
       double magFieldREc = magBField * r12Fact * (fDeltaEccentricityIO / rDelta) * std::sin(phi);
       double deltaMagFieldPhiEl = magBField * r12Fact * fEffectiveInnerRad * (fDeltaEllipticityI /(r*r)) * std::cos(2.0*phi);
       double magFieldREl = magBField * r12Fact * fEffectiveInnerRad * (fDeltaEllipticityI /(r * r)) * std::sin(2.0*phi);
       double magFieldPhi = magBField - deltaMagFieldPhiEl - deltaMagFieldPhiEc;
       const double magFieldR = magFieldREl + magFieldREc;
       Bfield[0] = -magFieldPhi*ptTrans[1]/r +  magFieldR*ptTrans[0]/r; 
       Bfield[1] = magFieldPhi*ptTrans[0]/r +  magFieldR*ptTrans[1]/r;
     }
//
// Important correction.. May 27 2017... ( a bug from a long time ago !?)
// One needs to rotate the field as well.
    if (fHornIsMisaligned) { 
       std::vector<double> bFieldLocal(3); 
       for(size_t k=0; k != 3; k++) { bFieldLocal[k] = Bfield[k]; Bfield[k] = 0.; } 
       for (size_t k1=0; k1 !=3; k1++) {
         for (size_t k2=0; k2 !=3; k2++) Bfield[k1] += fRotMatrixHornContainer[k1][k2]*bFieldLocal[k2];
       }
//       std::cerr << " Misaligned Horn" << hornNumber+1 << ", rotation of the field ... " << std::endl;
//       for(size_t k=0; k != 3; k++) std::cerr << " Component " << k 
//                                              << " local " << bFieldLocal[k] << " global " << Bfield[k] << std::endl;
    }
}
void LBNFMagneticFieldPolyconeHorn::dumpField() const {

  std::ostringstream fNameStr;  fNameStr << "./FieldMapHorn" << (hornNumber +1) << ".txt";
  std::string fName(fNameStr.str());
  std::ofstream fOut(fName.c_str());
  const double zStart = fZShiftDrawingCoordinate;
  const double zEnd = zStart + fEffectiveLength;
  std::cerr << "LBNFMagneticFieldPolyconeHorn::dumpField .. fZShiftDrawingCoordinate " 
            << fZShiftDrawingCoordinate << " length " << fEffectiveLength << std::endl;
  if ((std::abs(fDeltaEllipticityI) < 1.0e-20) && (std::abs(fDeltaEccentricityIO) < 1.0e-20) ) {
    fOut << " z zd r bphi br  " << std::endl;
    double rMax = fOuterRadius + 5.0*CLHEP::mm;
//  if (amHorn1) std::cerr << " Horn1, Dumpfield, r Max = " << rMax << std::endl;
//  else std::cerr << " Horn2, Dumpfield, r Max = " << rMax << std::endl;
    const int numZStep = 100;
    const int numRStep= 200;
    const double zStep = (zEnd-zStart)/numZStep;
    const double rStep = rMax/numRStep;
    double point[3];
    for (int iZ=0; iZ !=numZStep; iZ++) { 
      double z = zStart +  iZ*zStep;
      point[2] = z;
      const double zLocD = point[2];
      double radIC = 0.; double radOC = 0.;
      this->fillHornPolygonRadii(zLocD, &radIC, &radOC);
      double r = radIC - 0.25*CLHEP::mm;
    // inside the conductor... 
      while (r < rMax)  {
        const double phi = 2.0*M_PI*G4RandFlat::shoot();
        point[0] = r*std::cos(phi);
        point[1] = r*std::sin(phi);
        double bf[3];
        this->GetFieldValue(point, &bf[0]);
        const double bphi = bf[0]*std::sin(phi) - bf[1]*std::cos(phi);
        const double br = bf[0]*std::cos(phi) + bf[1]*std::sin(phi);
        fOut << " " << z << "  " << zLocD << " " << r << " " 
           << bphi/CLHEP::tesla << " " << br/CLHEP::tesla << std::endl;
        if (r < (radIC + 5.0*CLHEP::mm)) r += 0.1*CLHEP::mm;
        else  r += rStep;
     }
    }
  } else { 
    fOut << " z zd x y r bphi br bx by " << std::endl;
//  if (amHorn1) std::cerr << " Horn1, Dumpfield, r Max = " << rMax << std::endl;
//  else std::cerr << " Horn2, Dumpfield, r Max = " << rMax << std::endl;
    const int numZStep = 100;
    const int numRStep= 20;
    const int numPhiStep= 20;
    const double zStep = (zEnd-zStart)/numZStep;
    double point[3];
    for (int iZ=0; iZ !=numZStep; iZ++) { 
      double z = zStart +  iZ*zStep;
      point[2] = z;
      const double zLocD = point[2];
      double radIC = 0.; double radOC = 0.;
      this->fillHornPolygonRadii(zLocD, &radIC, &radOC);
      double r = 0.001;
      const double rMax = radIC + 10.0*CLHEP::mm;
      const double rStep = rMax/numRStep;
    // inside the conductor... 
      while (r < rMax)  {
        for (size_t kPhi=0; kPhi!= (numPhiStep+1); kPhi++) { 
          const double phi = (2.0*M_PI*kPhi)/numPhiStep;
          point[0] = r*std::cos(phi);
          point[1] = r*std::sin(phi);
          double bf[3];
          this->GetFieldValue(point, &bf[0]);
          const double bphi = bf[0]*std::sin(phi) - bf[1]*std::cos(phi);
          const double br = bf[0]*std::cos(phi) +  bf[1]*std::sin(phi);
          fOut << " " << z << "  " << zLocD << " " << point[0] << " " << point[1] << " " << r << " " 
           << bphi/CLHEP::tesla << " " << br/CLHEP::tesla 
	   << " " << bf[0]/CLHEP::tesla << " " << bf[1]/CLHEP::tesla << std::endl;
	 }
         r += rStep;
      }
    }
  }  
   
  fOut.close();
}  

void LBNFMagneticFieldPolyconeHorn::fillTrajectories(const double Point[3], double bx, double by) const {
 if (!fOutTraj.is_open()) return; 
 LBNERunManager* pRunManager = dynamic_cast<LBNERunManager*>(G4RunManager::GetRunManager());
 fOutTraj << " " << pRunManager->GetCurrentEvent()->GetEventID();
 for (size_t k=0; k!= 3; k++) fOutTraj << " " << Point[k];
 fOutTraj << " " << bx/CLHEP::tesla << " " << " " << by/CLHEP::tesla << std::endl;
 // we flush, the destructor never called.. 
 fOutTraj.flush();
}
LBNFMagneticFieldPolyconeHorn::~LBNFMagneticFieldPolyconeHorn() {
   std::cerr << " Closing Output Trajectory file in LBNFMagneticFieldPolyconeHorn " << std::endl;
   if (fOutTraj.is_open()) fOutTraj.close();
}
